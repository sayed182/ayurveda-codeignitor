if (modules == 'email_template') {
    var email_template_table;
    email_template_table = $('#emailtemplate_table').DataTable({
        'ordering': false,
        "processing": true, //Feature control the processing indicator.
        'bnDestroy': true,
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": base_url + 'admin/email_template/email_template_list',
            "type": "POST",
            "data": function (data) {
            },
            error: function () {

            }
        },

        //Set column definition initialisation properties.
        "columnDefs": [
            {
                "targets": [0], //first column / numbering column
                "orderable": false, //set not orderable
            },
        ],

    });


}

$(document).ready(function () {

    var tz = jstz.determine();
    var timezone = tz.name();
    $.post(base_url + 'ajax/set_timezone', {timezone: timezone}, function (res) {

    })

    $.post(base_url + 'ajax/currency_rate', function (res) {
        //console.log(res);
    })
});

if (modules == 'appointments' || modules == 'dashboard') {


    var appoinment_table;

    appoinment_table = $('#appoinment_table').DataTable({
        'ordering': false,
        "processing": true, //Feature control the processing indicator.
        'bnDestroy': true,
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": base_url + 'admin/appointments/appoinments_list',
            "type": "POST",
            "data": function (data) {
            },
            error: function () {

            }
        },

        //Set column definition initialisation properties.
        "columnDefs": [
            {
                "targets": [0], //first column / numbering column
                "orderable": false, //set not orderable
            },
        ],

    });


    function appoinments_table()
    {

        appoinment_table.ajax.reload(null, false);
    }

    var upappoinment_table;
    upappoinment_table = $('#upappoinment_table').DataTable({
        'ordering': false,
        "processing": true, //Feature control the processing indicator.
        'bnDestroy': true,
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": base_url + 'admin/appointments/upappoinments_list',
            "type": "POST",
            "data": function (data) {
            },
            error: function () {

            }
        },

        //Set column definition initialisation properties.
        "columnDefs": [
            {
                "targets": [0], //first column / numbering column
                "orderable": false, //set not orderable
            },
        ],

    });

    function upappoinments_table()
    {
        upappoinment_table.ajax.reload(null, false);
    }


    var missedappoinment_table;
    missedappoinment_table = $('#missedappoinment_table').DataTable({
        'ordering': false,
        "processing": true, //Feature control the processing indicator.
        'bnDestroy': true,
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": base_url + 'admin/appointments/missedappoinments_list',
            "type": "POST",
            "data": function (data) {
            },
            error: function () {

            }
        },

        //Set column definition initialisation properties.
        "columnDefs": [
            {
                "targets": [0], //first column / numbering column
                "orderable": false, //set not orderable
            },
        ],

    });

    function missedappoinments_table()
    {
        missedappoinment_table.ajax.reload(null, false);
    }


}

if (modules == 'payment_requests') {
    var payment_request_table;
    payment_request_table = $('#payment_request_table').DataTable({
        'ordering': false,
        "processing": true, //Feature control the processing indicator.
        'bnDestroy': true,
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": base_url + 'admin/payment_requests/payment_requests_list',
            "type": "POST",
            "data": function (data) {
            },
            error: function () {

            }
        },

        //Set column definition initialisation properties.
        "columnDefs": [
            {
                "targets": [0], //first column / numbering column
                "orderable": false, //set not orderable
            },
        ],

    });

    function payment_requests_table()
    {
        payment_request_table.ajax.reload(null, false);
    }

    function view_bankdetails(bank_name, branch_name, account_no, account_name)
    {
        $('.bankname').html(bank_name);
        $('.branchname').html(branch_name);
        $('.accountno').html(account_no);
        $('.accountname').html(account_name);
        $('#bank_details_modal').modal('show');
    }

    function payment_status(id, status)
    {

        $.post(base_url + "admin/payment_requests/payment_status", {id: id, status: status}, function (data) {
            payment_requests_table();
        });

    }
}

if (modules == 'reviews') {
    var reviews_table;
    reviews_table = $('#reviews_table').DataTable({
        'ordering': false,
        "processing": true, //Feature control the processing indicator.
        'bnDestroy': true,
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": base_url + 'admin/reviews/reviews_list',
            "type": "POST",
            "data": function (data) {
            },
            error: function () {

            }
        },

        //Set column definition initialisation properties.
        "columnDefs": [
            {
                "targets": [0], //first column / numbering column
                "orderable": false, //set not orderable
            },
        ],

    });

    function reviews_tables()
    {
        reviews_table.ajax.reload(null, false);
    }

    function delete_reviews(id)
    {
        $('#reviews_id').val(id);
        $('#delete_reviews').modal('show');
    }
    function reviews_delete()
    {
        var reviews_id = $('#reviews_id').val();
        $('#change_btn').attr('disabled', true);
        $('#change_btn').html('<div class="spinner-border text-light" role="status"></div>');
        $.post(base_url + 'admin/reviews/reviews_delete', {reviews_id: reviews_id}, function (res) {

            reviews_tables();

            $('#change_btn').attr('disabled', false);
            $('#change_btn').html('Yes');
            $('#delete_reviews').modal('hide');


        });
    }


}

if (modules == 'users') {
    if (pages == 'doctors') {
        var doctor_table;
        doctor_table = $('#doctors_table').DataTable({
            'ordering': false,
            "processing": true, //Feature control the processing indicator.
            'bnDestroy': true,
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.

            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": base_url + 'admin/users/doctors_list',
                "type": "POST",
                "data": function (data) {
                },
                error: function () {

                }
            },

            //Set column definition initialisation properties.
            "columnDefs": [
                {
                    "targets": [0], //first column / numbering column
                    "orderable": false, //set not orderable
                },
            ],

        });

        function doctors_table()
        {
            doctor_table.ajax.reload(null, false);
        }
    }

    if (pages == 'patients') {
        var patient_table;
        patient_table = $('#patients_table').DataTable({
            'ordering': false,
            "processing": true, //Feature control the processing indicator.
            'bnDestroy': true,
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.

            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": base_url + 'admin/users/patients_list',
                "type": "POST",
                "data": function (data) {
                },
                error: function () {

                }
            },

            //Set column definition initialisation properties.
            "columnDefs": [
                {
                    "targets": [0], //first column / numbering column
                    "orderable": false, //set not orderable
                },
            ],

        });

        function patients_table()
        {
            patient_table.ajax.reload(null, false);
        }
    }

    function change_usersStatus(id)
    {
        var stat = $('#status_' + id).prop('checked');

        if (stat == true)
        {
            var status = 1;
        } else
        {
            var status = 2;
        }
        $.post(base_url + "admin/users/change_usersStatus", {id: id, status: status}, function (data) {});

    }

    $(document).ready(function () {

        $.ajax({
            type: "GET",
            url: base_url + "ajax/get_country_code",
            data: {id: $(this).val()},
            beforeSend: function () {
                $('#country_code').find("option:eq(0)").html("Please wait..");
            },
            success: function (data) {
                /*get response as json */
                $('#country_code').find("option:eq(0)").html("Select Country Code");
                var obj = jQuery.parseJSON(data);
                $(obj).each(function ()
                {
                    var option = $('<option />');
                    option.attr('value', this.value).text(this.label);
                    $('#country_code').append(option);
                });


                /*ends */

            }
        });

        $("#register_form").validate({
            rules: {
                first_name: "required",
                last_name: "required",
                mobileno: {
                    required: true,
                    minlength: 7,
                    maxlength: 12,
                    digits: true,
                    remote: {
                        url: base_url + "admin/users/check_mobileno",
                        type: "post",
                        data: {
                            mobileno: function () {
                                return $("#mobileno").val();
                            }
                        }
                    }
                },
                email: {
                    required: true,
                    email: true,
                    remote: {
                        url: base_url + "admin/users/check_email",
                        type: "post",
                        data: {
                            email: function () {
                                return $("#email").val();
                            }
                        }
                    }
                },
                password: {
                    required: true,
                    minlength: 6
                },
                confirm_password: {
                    required: true,
                    equalTo: "#password"
                }

            },
            messages: {
                first_name: "Please enter your first name",
                last_name: "Please enter your last name",
                mobileno: {
                    required: "Please enter mobileno",
                    maxlength: "Please enter valid mobileno",
                    minlength: "Please enter valid mobileno",
                    digits: "Please enter valid mobileno",
                    remote: "Your mobile no already exits"
                },
                email: {
                    required: "Please enter email",
                    email: "Please enter valid email address",
                    remote: "Your email address already exits"
                },
                password: {
                    required: "Please enter password",
                    minlength: "Your password must be 6 characters"
                },
                confirm_password: {
                    required: "Please enter confirm password",
                    equalTo: "Your password does not match"
                }

            },
            submitHandler: function (form) {

                $.ajax({
                    url: base_url + 'admin/users/signup',
                    data: $("#register_form").serialize(),
                    type: "POST",
                    beforeSend: function () {
                        $('#register_btn').attr('disabled', true);
                        $('#register_btn').html('<div class="spinner-border text-light" role="status"></div>');
                    },
                    success: function (res) {
                        $('#register_btn').attr('disabled', false);
                        $('#register_btn').html('Submit');
                        var obj = JSON.parse(res);

                        if (obj.status === 200)
                        {
                            $('#register_form')[0].reset();
                            if ($('#role').val() == '1')
                            {
                                doctors_table();
                            }
                            if ($('#role').val() == '2')
                            {
                                patients_table();
                            }
                            $('#user_modal').modal('hide');

                        } else
                        {
                            toastr.error(obj.msg);
                        }
                    }
                });
                return false;
            }
        });

    });
}

if (modules == 'specialization')
{
    var specialization_table;
    $(document).ready(function () {

        //datatables
        specialization_table = $('#specializationtable').DataTable({
            'ordering': false,
            "processing": true, //Feature control the processing indicator.
            'bnDestroy': true,
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.

            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": base_url + "admin/specialization/specialization_list",
                "type": "POST",
                "data": function (data) {

                },
                error: function () {
                    //window.location.href=base_url+'admin/dashboard';
                }
            },

            //Set column definition initialisation properties.
            "columnDefs": [
                {
                    "targets": [0], //first column / numbering column
                    "orderable": false, //set not orderable
                },
            ],

        });


    });

    function add_specialization()
    {
        $('[name="method"]').val('insert');
        $('#specialization_form')[0].reset(); // reset form on modals
        $('#modal_form').modal('show'); // show bootstrap modal
        $('.modal-title').text('Add Specialization'); // Set Title to Bootstrap modal title
        $('#specialization_images').html('');
        $('#specialization_img').val('');

    }

    function edit_specialization(id)
    {
        $('[name="method"]').val('update');
        $('#specialization_form')[0].reset(); // reset form on modals

        //Ajax Load data from ajax
        $.ajax({
            url: base_url + "admin/specialization/specialization_edit/" + id,
            type: "GET",
            dataType: "JSON",
            success: function (data)
            {

                $('[name="id"]').val(data.id);
                $('[name="specialization"]').val(data.specialization);
                $('#specialization_img').val(data.specialization_img);
                $('#specialization_images').html('<img src="' + base_url + data.specialization_img + '" style="width:80px;height:30px">');

                $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
                $('.modal-title').text('Edit Specialization'); // Set title to Bootstrap modal title

            },
            error: function () {
                window.location.href = base_url + 'admin/dashboard';
            }
        });
    }

    function specialization_reload_table()
    {
        specialization_table.ajax.reload(null, false); //reload datatable ajax
    }



    function delete_specialization(id)
    {
        if (confirm('Are you sure delete this specialization?'))
        {
            // ajax delete data to database
            $.ajax({
                url: base_url + "admin/specialization/specialization_delete/" + id,
                type: "POST",
                dataType: "JSON",
                success: function (data)
                {
                    //if success reload ajax table
                    $('#specialization_form').modal('hide');
                    specialization_reload_table();
                    toastr.success('Specialization deleted successfully');
                },
                error: function () {
                    //window.location.href=base_url+'admin/dashboard';
                }
            });

        }
    }


    $(document).ready(function (e) {
        $("#specialization_form").on('submit', (function (e) {
            e.preventDefault();

            var specialization = $('#specialization').val();
            var specialization_img = $('#specialization_img').val();

            if (specialization == '') {
                toastr.error('Please enter specialization');
                return false;
            }

            if (specialization_img == '') {
                if (!document.getElementById("specialization_image").files[0]) {
                    toastr.error('Please upload specialization image');
                    return false;
                }
            }

            var file = document.getElementById("specialization_image").files[0];
            if (file)
            {
                const  fileType = file['type'];
                const validImageTypes = ['image/gif', 'image/jpeg', 'image/png'];
                if (!validImageTypes.includes(fileType)) {
                    toastr.error('Please upload image files only');
                    return false;
                }
            }

            $.ajax({
                url: base_url + 'admin/specialization/create_specialization',
                type: "POST",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function () {

                    $('#btnspecializationsave').html('<div class="spinner-border text-light" role="status"></div>');
                    $('#btnspecializationsave').attr('disabled', true);

                },
                success: function (data) {

                    $('#btnspecializationsave').html('Submit');
                    $('#btnspecializationsave').attr('disabled', false);

                    var obj = jQuery.parseJSON(data);
                    if (obj.result == 'true')
                    {

                        toastr.success(obj.status);

                        $('#modal_form').modal('hide');
                        $('#specialization_form')[0].reset();

                        specialization_table.ajax.reload(null, false);
                    } else if (obj.result == 'false')
                    {
                        toastr.error(obj.status);

                    } else if (obj.result == 'exe')
                    {
                        toastr.error(obj.status);
                    } else
                    {
                        window.location.reload();
                    }
                },
                error: function () {
                    window.location.href = base_url + 'admin/dashboard';
                }

            });
        }));
    });
}
if (modules == 'settings') {
    var specialKeys = new Array();
    specialKeys.push(8); //Backspace
    function IsNumeric(e) {
        var keyCode = e.which ? e.which : e.keyCode
        var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
        return ret;
    }
}

if (modules == 'country') {

    if (pages == 'country') {
        $('#country_table').DataTable();



        $(document).ready(function () {

            $("#show_countryform").click(function () {
                $("#password_tab").show();
            });
            $("#country_edit").on('submit', (function (e) {


                e.preventDefault();

                var sortname = $('#esortname').val();
                var country = $('#ecountry').val();
                var phonecode = $('#ephonecode').val();

                if (sortname == '') {
                    toastr.error('Please enter sortname');
                    return false;
                }

                if (country == '') {
                    toastr.error('Please enter country');
                    return false;
                }

                if (phonecode == '') {
                    toastr.error('Please enter phonecode');
                    return false;
                }


                $.ajax({
                    url: base_url + 'admin/country/country_update',
                    type: "POST",
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend: function () {

                        $('#btncountryupdate').html('<div class="spinner-border text-light" role="status"></div>');
                        $('#btncountryupdate').attr('disabled', true);

                    },
                    success: function (data) {

                        $('#btncountryupdate').html('Submit');
                        $('#btncountryupdate').attr('disabled', false);

                        var obj = jQuery.parseJSON(data);
                        if (obj.result == 'true')
                        {

                            toastr.success(obj.status);

                            $('#modal_form').modal('hide');
                            $('#country_edit')[0].reset();

                            window.location.reload();
                        } else if (obj.result == 'false')
                        {
                            toastr.error(obj.status);

                        } else if (obj.result == 'exe')
                        {
                            toastr.error(obj.status);
                        } else
                        {
                            window.location.reload();
                        }
                    },
                    error: function () {
                        window.location.href = base_url + 'admin/dashboard';
                    }

                });
            }));



        });



        function edit_country(id)
        {
            $('[name="method"]').val('update');


            //Ajax Load data from ajax
            $.ajax({
                url: base_url + "admin/country/country_edit",
                type: "POST",
                dataType: "JSON",
                data: {id: id},
                success: function (data)
                {



                    $('[name="id"]').val(data.countryid);
                    $('[name="esortname"]').val(data.sortname);
                    $('[name="ecountry"]').val(data.country);
                    $('[name="ephonecode"]').val(data.phonecode);


                    $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
                    $('.modal-title').text('Edit Country'); // Set title to Bootstrap modal title

                },
                error: function () {
                    //window.location.href=base_url+'admin/dashboard';
                }
            });
        }

        function delete_country(id)
        {
            if (confirm('Are you sure delete this country?'))
            {
                // ajax delete data to database
                $.ajax({
                    url: base_url + "admin/country/country_delete/" + id,
                    type: "POST",
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (data)
                    {
                        console.log(data);
                        var obj = jQuery.parseJSON(data);

                        if (obj.result == 'true')
                        {

                            toastr.success(obj.status);



                            window.location.reload();
                        } else if (obj.result == 'false')
                        {
                            toastr.error(obj.status);

                        } else if (obj.result == 'exe')
                        {
                            toastr.error(obj.status);
                        } else
                        {
                            window.location.reload();
                        }
                    },
                    error: function () {
                        window.location.href = base_url + 'admin/dashboard';
                    }
                });

            }
        }




        $("#country_add").validate({

            rules: {
                sortname: {
                    required: true,
                    remote: {
                        url: base_url + "admin/country/check_sortname",
                        type: "post",
                        data: {
                            sortname: function () {
                                return $("#sortname").val();
                            }
                        }
                    }
                },

                country: {
                    required: true,
                    remote: {
                        url: base_url + "admin/country/check_country",
                        type: "post",
                        data: {
                            country: function () {
                                return $("#country").val();
                            }
                        }
                    }
                },
                phone_code: {
                    required: true,
                    remote: {
                        url: base_url + "admin/country/check_phonecode",
                        type: "post",
                        data: {
                            phone_code: function () {
                                return $("#phone_code").val();
                            }
                        }
                    }
                },

            },
            messages: {
                sortname: {
                    required: "Please enter sortname",
                    remote: "Your sortname is already exits"
                },
                country: {
                    required: "Please enter country value",
                    remote: "Your country value is already exit"
                },
                phone_code: {
                    required: "Please enter country value",
                    remote: "Your country value is already exit"
                },

            }

        });

    }

    if (pages == 'city') {


        function edit_city(id)
        {
            $('[name="method"]').val('update');


            //Ajax Load data from ajax
            $.ajax({
                url: base_url + "admin/country/city_edit",
                type: "POST",
                dataType: "JSON",
                data: {id: id},
                success: function (data)
                {



                    $('[name="id"]').val(data.id);
                    $('[name="ecity"]').val(data.city);



                    $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
                    $('.modal-title').text('Edit City'); // Set title to Bootstrap modal title

                },
                error: function () {
                    //window.location.href=base_url+'admin/dashboard';
                }
            });
        }

        function delete_city(id)
        {
            if (confirm('Are you sure delete this city?'))
            {
                // ajax delete data to database
                $.ajax({
                    url: base_url + "admin/country/city_delete/" + id,
                    type: "POST",
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (data)
                    {
                        console.log(data);
                        var obj = jQuery.parseJSON(data);

                        if (obj.result == 'true')
                        {

                            toastr.success(obj.status);



                            window.location.reload();
                        } else if (obj.result == 'false')
                        {
                            toastr.error(obj.status);

                        } else if (obj.result == 'exe')
                        {
                            toastr.error(obj.status);
                        } else
                        {
                            window.location.reload();
                        }
                    },
                    error: function () {
                        //window.location.href=base_url+'admin/dashboard';
                    }
                });

            }
        }

        function city_list(country_id, state_id) {




            var city_table;
            city_table = $('#city_list').DataTable({
                'ordering': false,
                "processing": true, //Feature control the processing indicator.
                'bnDestroy': true,
                "serverSide": true, //Feature control DataTables' server-side processing mode.
                "order": [], //Initial no order.

                // Load data for the table's content from an Ajax source
                "ajax": {
                    "url": base_url + 'admin/country/city_list',
                    "type": "POST",
                    "data": function (data) {
                        data.country_id = country_id;
                        data.state_id = state_id;

                    },
                    error: function () {

                    }
                },

                //Set column definition initialisation properties.
                "columnDefs": [
                    {
                        "targets": [0], //first column / numbering column
                        "orderable": false, //set not orderable
                    },
                ],

            });


        }





        $(document).ready(function () {


            $('#state1').bind("keyup change", function () {



                var country_id = $("#country1").val();
                var state_id = $("#state1").val();
                $("#city_list").dataTable().fnDestroy()

                city_list(country_id, state_id);

            });




            $("#show_cityform").click(function () {
                $("#city_tab").show();
            });

            $("#city_edit").on('submit', (function (e) {


                e.preventDefault();

                var city = $('#ecity').val();


                if (city == '') {
                    toastr.error('Please enter city');
                    return false;
                }




                $.ajax({
                    url: base_url + 'admin/country/city_update',
                    type: "POST",
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend: function () {

                        $('#btncityupdate').html('<div class="spinner-border text-light" role="status"></div>');
                        $('#btncityupdate').attr('disabled', true);

                    },
                    success: function (data) {

                        $('#btncityupdate').html('Submit');
                        $('#btncityupdate').attr('disabled', false);

                        var obj = jQuery.parseJSON(data);
                        if (obj.result == 'true')
                        {

                            toastr.success(obj.status);

                            $('#modal_form').modal('hide');
                            $('#city_edit')[0].reset();

                            window.location.reload();
                        } else if (obj.result == 'false')
                        {
                            toastr.error(obj.status);

                        } else if (obj.result == 'exe')
                        {
                            toastr.error(obj.status);
                        } else
                        {
                            window.location.reload();
                        }
                    },
                    error: function () {
                        //window.location.href=base_url+'admin/dashboard';
                    }

                });
            }));


            $.ajax({
                type: "GET",
                url: base_url + "ajax/get_country",
                data: {id: $(this).val()},
                beforeSend: function () {
                    $('#country').find("option:eq(0)").html("please wait");
                },
                success: function (data) {
                    /*get response as json */
                    $('#country').find("option:eq(0)").html("select country");
                    var obj = jQuery.parseJSON(data);
                    $(obj).each(function ()
                    {
                        var option = $('<option />');
                        option.attr('value', this.value).text(this.label);
                        $('#country').append(option);

                    });


                    $('#country').val(country);


                    /*ends */

                }
            });

            $.ajax({
                type: "GET",
                url: base_url + "ajax/get_country",
                data: {id: $(this).val()},
                beforeSend: function () {
                    $('#country1').find("option:eq(0)").html("please wait");
                },
                success: function (data) {
                    /*get response as json */
                    $('#country1').find("option:eq(0)").html("select country");
                    var obj = jQuery.parseJSON(data);
                    $(obj).each(function ()
                    {
                        var option = $('<option />');
                        option.attr('value', this.value).text(this.label);
                        $('#country1').append(option);

                    });


                    $('#country1').val(country);
                    state_list


                    /*ends */

                }
            });

            $('body').on('change', '.country', function () {


                $.ajax({
                    type: "POST",
                    url: base_url + "ajax/get_state",
                    data: {id: $(this).val()},
                    beforeSend: function () {
                        $("#statestate_list option:gt(0)").remove();

                        $('#state').find("option:eq(0)").html("Please wait");

                    },
                    success: function (data) {
                        /*get response as json */
                        $('#state').find("option:eq(0)").html("Select state");
                        var obj = jQuery.parseJSON(data);
                        $(obj).each(function ()
                        {
                            var option = $('<option />');
                            option.attr('value', this.value).text(this.label);
                            $('#state').append(option);
                        });

                        /*ends */

                    }
                });
            });


            $('body').on('change', '#country1', function () {


                $.ajax({
                    type: "POST",
                    url: base_url + "ajax/get_state",
                    data: {id: $(this).val()},
                    beforeSend: function () {
                        $("#state1 option:gt(0)").remove();

                        $('#state1').find("option:eq(0)").html("Please wait");

                    },
                    success: function (data) {
                        /*get response as json */
                        $('#state1').find("option:eq(0)").html("Select state");
                        var obj = jQuery.parseJSON(data);
                        $(obj).each(function ()
                        {
                            var option = $('<option />');
                            option.attr('value', this.value).text(this.label);
                            $('#state1').append(option);
                        });

                        /*ends */

                    }
                });
            });


            $("#city_add").click(function () {
                var country = $("#country").val();
                var state = $("#state").val();
                var city = $("#city").val();


                if (country == '' || country == null) {
                    toastr.error('Please select country');
                    return false;
                }

                if (state == '') {
                    toastr.error('Please select state');
                    return false;
                }

                if (city == '') {
                    toastr.error('Please enter city');
                    return false;
                }


                var dataString = 'country=' + country + '&state=' + state + '&city=' + city;

                $.ajax({
                    url: base_url + 'admin/country/city_insert',
                    type: "POST",
                    data: dataString,

                    beforeSend: function () {

                        $('#city_add').html('<div class="spinner-border text-light" role="status"></div>');
                        $('#city_add').attr('disabled', true);

                    },
                    success: function (data) {

                        $('#city_add').html('Submit');
                        $('#city_add').attr('disabled', false);

                        var obj = jQuery.parseJSON(data);
                        if (obj.result == 'true')
                        {

                            toastr.success(obj.status);



                            window.location.reload();
                        } else if (obj.result == 'false')
                        {
                            toastr.error(obj.status);

                        } else if (obj.result == 'exe')
                        {
                            toastr.error(obj.status);
                        } else
                        {
                            window.location.reload();
                        }
                    },
                    error: function () {
                        window.location.href = base_url + 'admin/dashboard';
                    }

                });


            });





        });

    }

    if (pages == 'state')
    {

        // state_list(1);

        function state_list(country_id) {



            //Stock table value
            var state_table;
            state_table = $('#state_table').DataTable({
                'ordering': false,
                "processing": true, //Feature control the processing indicator.
                'bnDestroy': true,
                "serverSide": true, //Feature control DataTables' server-side processing mode.
                "order": [], //Initial no order.

                // Load data for the table's content from an Ajax source
                "ajax": {
                    "url": base_url + 'admin/country/state_list',
                    "type": "POST",
                    "data": function (data) {
                        data.country_id = country_id;
                    },
                    error: function () {

                    }
                },

                //Set column definition initialisation properties.
                "columnDefs": [
                    {
                        "targets": [0], //first column / numbering column
                        "orderable": false, //set not orderable
                    },
                ],

            });

        }

        function edit_state(id)
        {
            $('[name="method"]').val('update');


            //Ajax Load data from ajax
            $.ajax({
                url: base_url + "admin/country/state_edit",
                type: "POST",
                dataType: "JSON",
                data: {id: id},
                success: function (data)
                {



                    $('[name="id"]').val(data.id);
                    $('[name="estate"]').val(data.statename);



                    $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
                    $('.modal-title').text('Edit State'); // Set title to Bootstrap modal title

                },
                error: function () {
                    //window.location.href=base_url+'admin/dashboard';
                }
            });
        }

        function delete_state(id)
        {
            if (confirm('Are you sure delete this state?'))
            {
                // ajax delete data to database
                $.ajax({
                    url: base_url + "admin/country/state_delete/" + id,
                    type: "POST",
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (data)
                    {
                        console.log(data);
                        var obj = jQuery.parseJSON(data);

                        if (obj.result == 'true')
                        {

                            toastr.success(obj.status);



                            window.location.reload();
                        } else if (obj.result == 'false')
                        {
                            toastr.error(obj.status);

                        } else if (obj.result == 'exe')
                        {
                            toastr.error(obj.status);
                        } else
                        {
                            window.location.reload();
                        }
                    },
                    error: function () {
                        //window.location.href=base_url+'admin/dashboard';
                    }
                });

            }
        }

        $(document).ready(function () {

            $('#country1').bind("keyup change", function () {



                var country_id = $("#country1").val();
                $("#state_table").dataTable().fnDestroy()

                state_list(country_id);

            });

            $("#show_stateform").click(function () {
                $("#state_tab").show();
            });

            $("#show_cityform").click(function () {
                $("#city_tab").show();
            });




            $("#state_add").click(function () {
                var country = $("#country").val();
                var state = $("#state").val();



                if (country == '' || country == null) {
                    toastr.error('Please select country');
                    return false;
                }

                if (state == '') {
                    toastr.error('Please enter state');
                    return false;
                }


                var dataString = 'country=' + country + '&state=' + state;

                $.ajax({
                    url: base_url + 'admin/country/state_insert',
                    type: "POST",
                    data: dataString,

                    beforeSend: function () {

                        $('#state_add').html('<div class="spinner-border text-light" role="status"></div>');
                        $('#state_add').attr('disabled', true);

                    },
                    success: function (data) {

                        $('#state_add').html('Submit');
                        $('#state_add').attr('disabled', false);

                        var obj = jQuery.parseJSON(data);
                        if (obj.result == 'true')
                        {

                            toastr.success(obj.status);



                            window.location.reload();
                        } else if (obj.result == 'false')
                        {
                            toastr.error(obj.status);

                        } else if (obj.result == 'exe')
                        {
                            toastr.error(obj.status);
                        } else
                        {
                            window.location.reload();
                        }
                    },
                    error: function () {
                        window.location.href = base_url + 'admin/dashboard';
                    }

                });


            });

            $("#state_edit").on('submit', (function (e) {


                e.preventDefault();

                var statename = $('#estate').val();


                if (statename == '') {
                    toastr.error('Please enter statename');
                    return false;
                }




                $.ajax({
                    url: base_url + 'admin/country/state_update',
                    type: "POST",
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend: function () {

                        $('#btnstateupdate').html('<div class="spinner-border text-light" role="status"></div>');
                        $('#btnstateupdate').attr('disabled', true);

                    },
                    success: function (data) {

                        $('#btnstateupdate').html('Submit');
                        $('#btnstateupdate').attr('disabled', false);

                        var obj = jQuery.parseJSON(data);
                        if (obj.result == 'true')
                        {

                            toastr.success(obj.status);

                            $('#modal_form').modal('hide');
                            $('#state_edit')[0].reset();

                            window.location.reload();
                        } else if (obj.result == 'false')
                        {
                            toastr.error(obj.status);

                        } else if (obj.result == 'exe')
                        {
                            toastr.error(obj.status);
                        } else
                        {
                            window.location.reload();
                        }
                    },
                    error: function () {
                        //window.location.href=base_url+'admin/dashboard';
                    }

                });
            }));



            $.ajax({
                type: "GET",
                url: base_url + "ajax/get_country",
                data: {id: $(this).val()},
                beforeSend: function () {
                    $('#country').find("option:eq(0)").html("please wait");
                },
                success: function (data) {
                    /*get response as json */
                    $('#country').find("option:eq(0)").html("select country");
                    var obj = jQuery.parseJSON(data);
                    $(obj).each(function ()
                    {
                        var option = $('<option />');
                        option.attr('value', this.value).text(this.label);
                        $('#country').append(option);

                    });


                    $('#country').val(country);


                    /*ends */

                }
            });

            $.ajax({
                type: "GET",
                url: base_url + "ajax/get_country",
                data: {id: $(this).val()},
                beforeSend: function () {
                    $('#country1').find("option:eq(0)").html("please wait");
                },
                success: function (data) {
                    /*get response as json */
                    $('#country1').find("option:eq(0)").html("select country");
                    var obj = jQuery.parseJSON(data);
                    $(obj).each(function ()
                    {
                        var option = $('<option />');
                        option.attr('value', this.value).text(this.label);
                        $('#country1').append(option);

                    });


                    $('#country1').val(country);


                    /*ends */

                }
            });






            $('body').on('change', '.country', function () {


                $.ajax({
                    type: "POST",
                    url: base_url + "ajax/get_state",
                    data: {id: $(this).val()},
                    beforeSend: function () {
                        $("#state option:gt(0)").remove();

                        $('#state').find("option:eq(0)").html("Please wait");

                    },
                    success: function (data) {
                        /*get response as json */
                        $('#state').find("option:eq(0)").html("Select state");
                        var obj = jQuery.parseJSON(data);
                        $(obj).each(function ()
                        {
                            var option = $('<option />');
                            option.attr('value', this.value).text(this.label);
                            $('#state').append(option);
                        });

                        /*ends */

                    }
                });
            });





        });


    }

}




if (modules == 'profile')
{
    $(document).ready(function () {
        $("#profile_form").validate({
            rules: {
                name: "required",
                email: {
                    required: true,
                    email: true
                },
                country: "required",
                city: "required",
                biography: "required"

            },
            messages: {
                name: "Please enter name",
                email: {
                    required: "Please enter email",
                    email: "Please enter valid email address"
                },
                country: "Please enter country",
                city: "Please enter city",
                biography: "Please enter biography",
            },
            submitHandler: function (form) {

                $.ajax({
                    url: base_url + 'admin/profile/update_profile',
                    data: $("#profile_form").serialize(),
                    type: "POST",
                    beforeSend: function () {
                        $('#profile_btn').attr('disabled', true);
                        $('#profile_btn').html('<div class="spinner-border text-light" role="status"></div>');
                    },
                    success: function (res) {
                        $('#profile_btn').attr('disabled', false);
                        $('#profile_btn').html('Update');
                        var obj = JSON.parse(res);

                        if (obj.status === 200)
                        {
                            $('.admin_name').html($('#name').val());
                            $('.admin_email').html($('#email').val());
                            $('.admin_location').html($('#city').val() + ', ' + $('#country').val());
                            $('.admin_biography').html($('#biography').val());
                            $('#profile_modal').modal('hide');
                            toastr.success(obj.msg);

                        } else
                        {
                            toastr.error(obj.msg);
                        }
                    }
                });
                return false;
            }

        });

        $("#change_password").validate({
            rules: {
                currentpassword: {
                    required: true,
                    remote: {
                        url: base_url + "admin/profile/check_currentpassword",
                        type: "post",
                        data: {
                            currentpassword: function () {
                                return $("#currentpassword").val();
                            }
                        }
                    }
                },

                password: {
                    required: true,
                    minlength: 6
                },
                confirm_password: {
                    required: true,
                    equalTo: "#password"
                },
            },
            messages: {
                currentpassword: {
                    required: "Please enter current password",
                    remote: "Your current password is invalid"
                },
                password: {
                    required: "Please enter password",
                    minlength: "Your password must be 6 characters"
                },
                confirm_password: {
                    required: "Please enter confirm password",
                    equalTo: "Your password does not match"
                },

            },
            submitHandler: function (form) {

                $.ajax({
                    url: base_url + 'admin/profile/change_password',
                    data: $("#change_password").serialize(),
                    type: "POST",
                    beforeSend: function () {
                        $('#password_btn').attr('disabled', true);
                        $('#password_btn').html('<div class="spinner-border text-light" role="status"></div>');
                    },
                    success: function (res) {
                        $('#password_btn').attr('disabled', false);
                        $('#password_btn').html('Update');
                        var obj = JSON.parse(res);

                        if (obj.status === 200)
                        {
                            $('#change_password')[0].reset();
                            toastr.success(obj.msg);

                        } else
                        {
                            toastr.error(obj.msg);
                        }
                    }
                });
                return false;
            }

        });



    });
}

if (modules == 'language') {

    if (pages == 'index')
    {
        function change_status(id)
        {
            var status = $('#lang_status' + id).attr('data-status');
            if (status == 1)
            {
                update_language = '2';
            }
            if (status == 2)
            {
                update_language = '1';
            }

            $.ajax({
                type: 'POST',
                url: base_url + 'admin/language/update_language_status',
                data: {id: id, update_language: update_language},
                success: function (response)
                {
                    if (response == 0)
                    {

                        toastr.error('Default Language cannot be inactive.');
                    } else if (response == 1)
                    {
                        if (status == 1)
                        {
                            $('#lang_status' + id).attr('data-status', 2);
                            $('#lang_status' + id).removeClass("label label-success").addClass("label label-danger");
                            $('#texts' + id).text('In Active');
                            $('#default_language' + id).hide();

                        }
                        if (status == 2)
                        {
                            $('#lang_status' + id).attr('data-status', 1);
                            $('#lang_status' + id).removeClass("label label-danger").addClass("label label-success");
                            $('#texts' + id).text('Active');
                            $('#default_language' + id).show();
                        }
                    } else
                    {

                    }
                }
            });

        }


        $(document).ready(function () {
            $('.active_lang').on('change', function (e, data) {
                var update_language = '';
                var sts_str = '';
                var id = $(this).attr('data-id');
                if ($(this).is(':checked')) {
                    update_language = '1';
                    sts_str = 'Active';
                } else {
                    update_language = '2';
                    sts_str = 'InActive';
                }
                if (update_language != '') {

                }
            })

            $('.default_lang').on('change', function (e, data) {
                var id = $(this).attr('data-id');
                $.ajax({
                    type: 'POST',
                    url: base_url + 'admin/language/update_language_default',
                    data: {id: id},
                    success: function (response)
                    {
                        if (response == 0)
                        {
                            $('#default_language' + id).attr('checked', false);
                        } else
                        {
                            $('#default_language' + id).attr('checked', true);
                        }
                    }
                });

            });

            $("#add_language").validate({
                rules: {
                    language: {
                        required: true,
                        remote: {
                            url: base_url + "admin/language/check_language",
                            type: "post",
                            data: {
                                language: function () {
                                    return $("#language").val();
                                }
                            }
                        }
                    },

                    language_value: {
                        required: true,
                        remote: {
                            url: base_url + "admin/language/check_language_value",
                            type: "post",
                            data: {
                                language_value: function () {
                                    return $("#language_value").val();
                                }
                            }
                        }
                    },
                    tag: "required"
                },
                messages: {
                    language: {
                        required: "Please enter language",
                        remote: "Your language is already exits"
                    },
                    language_value: {
                        required: "Please enter language value",
                        remote: "Your language value is already exit"
                    },
                    tag: "Please select tag",

                }

            });



        });
    }



    if (pages == 'keywords') {
        var language_table;
        language_table = $('#language_table').DataTable({
            'ordering': false,
            "processing": true, //Feature control the processing indicator.
            'bnDestroy': true,
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.

            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": base_url + 'admin/language/language_list',
                "type": "POST",
                "data": function (data) {
                },
                error: function () {

                }
            },

            //Set column definition initialisation properties.
            "columnDefs": [
                {
                    "targets": [0], //first column / numbering column
                    "orderable": false, //set not orderable
                },
            ],

        });


        function update_language(lang_key, lang)
        {
            var cur_val = $('input[name="' + lang_key + '[' + lang + ']"]').val();
            var prev_val = $('input[name="prev_' + lang_key + '[' + lang + ']"]').val();

            $.post(base_url + 'admin/language/update_language', {lang_key: lang_key, lang: lang, cur_val: cur_val}, function (data) {
                if (data == 1)
                {

                } else if (data == 0)
                {
                    $('input[name="' + lang_key + '[' + lang + ']"]').val(prev_val);
                    toastr.error('Sorry, This keyword already exist!');

                } else if (data == 2)
                {
                    $('input[name="' + lang_key + '[' + lang + ']"]').val(prev_val);
                    toastr.error('Sorry, This field should not be empty!');

                }
            });

        }

    }


    if (pages == 'add_page')
    {
        function keyword_validation() {

            var error = 0;
            var page_name = $('#page_name').val().trim();


            if (page_name == "") {
                $('.keyword_error').show();
                error = 1;
            } else {
                $('.keyword_error').hide();

            }

            if (error == 0) {
                return true;
            } else {
                return false;
            }
        }
    }

    if (pages == 'add_app_keywords')
    {
        function keyword_validation() {

            var error = 0;
            var field_name = $('#field_name').val().trim();
            var name = $('#name').val().trim();


            if (field_name == "") {
                $('.field_name_error').show();
                error = 1;
            } else {
                $('.field_name_error').hide();

            }

            if (name == "") {
                $('.name_error').show();
                error = 1;
            } else {
                $('.name_error').hide();

            }

            if (error == 0) {
                return true;
            } else {
                return false;
            }
        }

    }


    if (pages == 'app_keywords') {
        var language_table;
        language_table = $('#app_language_table').DataTable({
            'ordering': false,
            "processing": true, //Feature control the processing indicator.
            'bnDestroy': true,
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.

            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": base_url + 'admin/language/app_language_list',
                "type": "POST",
                "data": function (data) {
                    data.page_key = $('#page_key').val();
                },
                error: function () {

                }
            },

            //Set column definition initialisation properties.
            "columnDefs": [
                {
                    "targets": [0], //first column / numbering column
                    "orderable": false, //set not orderable
                },
            ],

        });


        function update_multi_lang()
        {

            $("#form_id").submit();
        }

    }


}
if (modules == 'questions') {

    if (pages == 'index') {

        var questions_table;
        questions_table = $('#questions_table').DataTable({
            'ordering': false,
            "processing": true, //Feature control the processing indicator.
            'bnDestroy': true,
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.

            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": base_url + 'admin/questions/questions_list',
                "type": "POST",
                "data": function (data) {
                },
                error: function () {

                }
            },

            //Set column definition initialisation properties.
            "columnDefs": [
                {
                    "targets": [0], //first column / numbering column
                    "orderable": false, //set not orderable
                },
            ],

        });

        function questions_tables()
        {
            questions_table.ajax.reload(null, false);
        }

        function delete_questions(id)
        {
            $('#questions_id').val(id);
            $('#delete_questions').modal('show');
        }
        function questions_delete()
        {
            var questions_id = $('#questions_id').val();
            $('#change_btn').attr('disabled', true);
            $('#change_btn').html('<div class="spinner-border text-light" role="status"></div>');
            $.post(base_url + 'admin/questions/questions_delete', {questions_id: questions_id}, function (res) {

                questions_tables();

                $('#change_btn').attr('disabled', false);
                $('#change_btn').html('Yes');
                $('#delete_questions').modal('hide');


            });
        }

        function approve_questions(id)
        {
            $('#questions_id').val(id);
            $('#approve_questions').modal('show');
        }
        function questions_approve()
        {
            var questions_id = $('#questions_id').val();
            $('#change_btn').attr('disabled', true);
            $('#change_btn').html('<div class="spinner-border text-light" role="status"></div>');
            $.post(base_url + 'admin/questions/questions_approve', {questions_id: questions_id}, function (res) {

                questions_tables();

                $('#change_btn').attr('disabled', false);
                $('#change_btn').html('Yes');
                $('#approve_questions').modal('hide');


            });
        }
    }

    if (pages == 'answers') {

        var answers_table;
        answers_table = $('#answers_table').DataTable({
            'ordering': false,
            "processing": true, //Feature control the processing indicator.
            'bnDestroy': true,
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.

            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": base_url + 'admin/questions/answers_list',
                "type": "POST",
                "data": function (data) {
                },
                error: function () {

                }
            },

            //Set column definition initialisation properties.
            "columnDefs": [
                {
                    "targets": [0], //first column / numbering column
                    "orderable": false, //set not orderable
                },
            ],

        });

        function answers_tables()
        {
            answers_table.ajax.reload(null, false);
        }

        function delete_answers(id)
        {
            $('#answers_id').val(id);
            $('#delete_answers').modal('show');
        }
        function answers_delete()
        {
            var answers_id = $('#answers_id').val();
            $('#change_btn').attr('disabled', true);
            $('#change_btn').html('<div class="spinner-border text-light" role="status"></div>');
            $.post(base_url + 'admin/questions/answers_delete', {answers_id: answers_id}, function (res) {

                answers_tables();

                $('#change_btn').attr('disabled', false);
                $('#change_btn').html('Yes');
                $('#delete_answers').modal('hide');


            });
        }

        function approve_answers(id)
        {
            $('#answers_id').val(id);
            $('#approve_answers').modal('show');
        }
        function answers_approve()
        {
            var answers_id = $('#answers_id').val();
            $('#change_btn').attr('disabled', true);
            $('#change_btn').html('<div class="spinner-border text-light" role="status"></div>');
            $.post(base_url + 'admin/questions/answers_approve', {answers_id: answers_id}, function (res) {

                answers_tables();

                $('#change_btn').attr('disabled', false);
                $('#change_btn').html('Yes');
                $('#approve_answers').modal('hide');


            });
        }


    }

}

