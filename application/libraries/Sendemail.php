<?php
defined('BASEPATH') OR exit('No direct script access allowed');

 // Load Composer's autoloader
        require 'vendor/autoload.php';

        use PHPMailer\PHPMailer\PHPMailer;
        use PHPMailer\PHPMailer\SMTP;
        use PHPMailer\PHPMailer\Exception;

class Sendemail {



   
    public function __construct() {

        $this->ci = & get_instance();

        $this->ci->load->helper('file');  
        $this->ci->load->library('email');
        $this->ci->email->initialize(smtp_mail_config());
        $this->ci->email->set_newline("\r\n");
        $this->ci->email->from(!empty(settings("email_address"))?settings("email_address"):"info@doccure.com",!empty(settings("email_tittle"))?settings("email_tittle"):"Doccure"); 
        $this->title=!empty(settings("website_name"))?settings("website_name"):"Doccure";

    }


   public function send_email_verification($data)
   {    
        $message='';

        $email_templates=email_template(1);
	
	$user_id = $data['id'] > 0 ? $data['id'] : $data['userid'];
	
        $body=$email_templates['template_content'];

        $body = str_replace('{{site_url}}', base_url(), $body);
        $body = str_replace('{{site_logo}}',!empty(base_url().settings("logo_front"))?base_url().settings("logo_front"):base_url()."assets/img/logo.png", $body);
        $body = str_replace('{{user_name}}', ucfirst($data['first_name'].' '.$data['last_name']), $body);
        $body = str_replace('{{website_name}}', !empty(settings("website_name"))?settings("website_name"):"Doccure", $body);
        $body = str_replace('{{verify_url}}', base_url().'activate/'.md5($user_id), $body);
        $body = str_replace('{{date}}', date('Y'), $body);

        $message=$body;

        $this->ci->email->to($data['email']); 
        $this->ci->email->subject('Welcome and thank you for joining '.$this->title);
        $this->ci->email->message($message);
        $this->ci->email->send();
        //echo $this->ci->email->print_debugger();

                
   }

    public function send_resetpassword_email($data)
   {    
        $message='';

        $email_templates=email_template(3);
        $body=$email_templates['template_content'];

        $body = str_replace('{{site_url}}', base_url(), $body);
        $body = str_replace('{{site_logo}}',!empty(base_url().settings("logo_front"))?base_url().settings("logo_front"):base_url()."assets/img/logo.png", $body);
        $body = str_replace('{{user_name}}', ucfirst($data['first_name'].' '.$data['last_name']), $body);
        $body = str_replace('{{website_name}}', !empty(settings("website_name"))?settings("website_name"):"Doccure", $body);
        $body = str_replace('{{email}}', $data['email'], $body);
        $body = str_replace('{{reset_url}}', base_url().'reset/'.$data['url'], $body);
        $body = str_replace('{{date}}', date('Y'), $body);

        $message=$body;

        $this->ci->email->to($data['email']); 
        $this->ci->email->subject('Password reset request');
        $this->ci->email->message($message);
        $this->ci->email->send();

                
   }

   public function send_appoinment_email($data)
   {    
        $message='';

        $email_templates=email_template(2);
        $body=$email_templates['template_content'];

        $body = str_replace('{{site_url}}', base_url(), $body);
        $body = str_replace('{{site_logo}}',!empty(base_url().settings("logo_front"))?base_url().settings("logo_front"):base_url()."assets/img/logo.png", $body);
        $body = str_replace('{{doctor_name}}', ucfirst($data['doctor_name']), $body);
        $body = str_replace('{{website_name}}', !empty(settings("website_name"))?settings("website_name"):"Doccure", $body);
        $body = str_replace('{{patient_name}}', $data['patient_name'], $body);
        $body = str_replace('{{date}}', date('Y'), $body);

        $message=$body;

        $this->ci->email->to($data['doctor_email']); 
        $this->ci->email->subject('New Appointment Booked!');
        $this->ci->email->message($message);
        $this->ci->email->send();

                
   }

   public function send_appoinment_cancelemail($data)
   {    
        $message='';

        $email_templates=email_template(4);
        $body=$email_templates['template_content'];

        $body = str_replace('{{site_url}}', base_url(), $body);
        $body = str_replace('{{site_logo}}',!empty(base_url().settings("logo_front"))?base_url().settings("logo_front"):base_url()."assets/img/logo.png", $body);
        $body = str_replace('{{doctor_name}}', ucfirst($data['doctor_name']), $body);
        $body = str_replace('{{website_name}}', !empty(settings("website_name"))?settings("website_name"):"Doccure", $body);
        $body = str_replace('{{patient_name}}', $data['patient_name'], $body);
        $body = str_replace('{{date}}', date('Y'), $body);

        $message=$body;

        $this->ci->email->to($data['patient_email']); 
        $this->ci->email->subject('Your Appointment Cancelled!');
        $this->ci->email->message($message);
        $this->ci->email->send();

                
   }

   public function send_appoinment_acceptemail($data)
   {    
        $message='';

        $email_templates=email_template(5);
        $body=$email_templates['template_content'];

        $body = str_replace('{{site_url}}', base_url(), $body);
        $body = str_replace('{{site_logo}}',!empty(base_url().settings("logo_front"))?base_url().settings("logo_front"):base_url()."assets/img/logo.png", $body);
        $body = str_replace('{{doctor_name}}', ucfirst($data['doctor_name']), $body);
        $body = str_replace('{{website_name}}', !empty(settings("website_name"))?settings("website_name"):"Doccure", $body);
        $body = str_replace('{{patient_name}}', $data['patient_name'], $body);
        $body = str_replace('{{date}}', date('Y'), $body);

        $message=$body;

        $this->ci->email->to($data['patient_email']); 
        $this->ci->email->subject('Your Appointment Accepted!');
        $this->ci->email->message($message);
        $this->ci->email->send();

                
   }



   public function contact_form_email($data)
   {    
        $message='';

        $email_templates=email_template(6);
        $body=$email_templates['template_content'];

        $body = str_replace('{{site_url}}', base_url(), $body);
        $body = str_replace('{{site_logo}}',!empty(base_url().settings("logo_front"))?base_url().settings("logo_front"):base_url()."assets/img/logo.png", $body);
        
        $body = str_replace('{{website_name}}', !empty(settings("website_name"))?settings("website_name"):"Doccure", $body);
        $body = str_replace('{{name}}', ucfirst($data['name']), $body);
        $body = str_replace('{{email}}', $data['email'], $body);
        $body = str_replace('{{mobile}}', $data['mobile'], $body);
        $body = str_replace('{{concern}}', $data['concern'], $body);
        $body = str_replace('{{explain}}', $data['explain'], $body);
        $body = str_replace('{{date}}', date('Y'), $body);

        $message=$body;

        $this->ci->email->to(settings("email")); 
        $this->ci->email->subject('Contact Form Submission');
        $this->ci->email->message($message);
        $this->ci->email->send();

                
   }




   


}



/* End of file Common.php */

