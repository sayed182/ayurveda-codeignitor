
    <body>
  
    <!-- Main Wrapper -->
        <div class="main-wrapper">
    
      <!-- Header -->
            <div class="header">
      
        <!-- Logo -->
                <div class="header-left">
                    <a href="<?php echo base_url();?>admin/dashboard" class="logo">
            <img src="<?php echo !empty(base_url().settings("logo_front"))?base_url().settings("logo_front"):base_url()."assets/img/logo.png";?>" alt="Logo">
          </a>
          <a href="<?php echo base_url();?>admin/dashboard" class="logo logo-small">
            <img src="<?php echo !empty(base_url().settings("logo_front"))?base_url().settings("logo_front"):base_url()."assets/img/logo.png";?>" alt="Logo" width="30" height="30">
          </a>
                </div>
        <!-- /Logo -->
        
        <a href="javascript:void(0);" id="toggle_btn">
          <i class="fe fe-text-align-left"></i>
        </a>
        
        
        
        <!-- Mobile Menu Toggle -->
        <a class="mobile_btn" id="mobile_btn">
          <i class="fa fa-bars"></i>
        </a>
        <!-- /Mobile Menu Toggle -->
        
        <!-- Header Right Menu -->
        <ul class="nav user-menu">

         
         <?php 

         $admin_detail=admin_detail($this->session->userdata('admin_id'));
          $profile_image=(!empty($admin_detail['profileimage']))?base_url().$admin_detail['profileimage']:base_url().'assets/img/user.png';

          ?>
          
          <!-- User Menu -->
          <li class="nav-item dropdown has-arrow">
            <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
              <span class="user-img"><img class="rounded-circle avatar-view-img" src="<?php echo $profile_image;?>" width="31" alt="Ryan Taylor"></span>
            </a>
            <div class="dropdown-menu">
              <div class="user-header">
                <div class="avatar avatar-sm">
                  <img src="<?php echo $profile_image;?>" alt="User Image" class="avatar-img rounded-circle avatar-view-img">
                </div>
                <div class="user-text">
                  <h6 class="admin_name"><?php echo ucfirst($admin_detail['name']);?></h6>
                  <p class="text-muted mb-0">Administrator</p>
                </div>
              </div>
              <a class="dropdown-item" href="<?php echo base_url();?>admin/profile">My Profile</a>
              <a class="dropdown-item" href="<?php echo base_url();?>admin/settings">Settings</a>
              <a class="dropdown-item" href="<?php echo base_url();?>admin/login/logout">Logout</a>
            </div>
          </li>
          <!-- /User Menu -->
          
        </ul>
        <!-- /Header Right Menu -->
        
            </div>
      <!-- /Header -->