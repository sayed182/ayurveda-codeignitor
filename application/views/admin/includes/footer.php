


   <script type="text/javascript">
           var base_url='<?php echo base_url();?>';
           var modules='<?php echo $module;?>';
           var pages='<?php echo $page;?>';
       </script>

   <!-- jQuery -->
     <?php if($page=='add_post' || $page=='edit_post') { ?>
        <script src="<?php echo base_url();?>assets/js/jquery2.js"></script>
    <?php }  else { ?>
       <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
    <?php }  ?>
    <!-- Bootstrap Core JS -->
        <script src="<?php echo base_url();?>assets/js/popper.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/select2/js/select2.min.js"></script>
    <!-- Slimscroll JS -->
      <script src="<?php echo base_url();?>assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
     <?php if($module=='appointments' || $module=='users' || $module=='specialization' || $module=='reviews'||$module=='email_template' || $module=='categories' ||  $module=='subcategories'  ||  $module=='post' || $module=='language' ||  $module=='payment_requests' ||  $module=='country' || $module=='questions') { ?>

    <script src="<?php echo base_url();?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/datatables/datatables.min.js"></script>

  <?php } if($module=='profile' || $module=='users' || $page=='add_post' || $page=='edit_post' || $module=='language' || $module=='country') { ?>

  <script src="<?php echo base_url();?>assets/js/jquery.validate.js" type="text/javascript"></script>
  <script src="<?php echo base_url();?>assets/js/jquery.password-validation.js" type="text/javascript"></script>

    <script type="text/javascript" src="<?php echo base_url();?>assets/js/cropper_profile.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/cropper.min.js"></script>
  <?php } ?>
    <!-- Custom JS -->
    <script  src="<?php echo base_url();?>assets/js/admin2.js"></script>
    
   
     <script src="<?php echo base_url();?>assets/js/toastr.js"></script>
     <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jstz-1.0.7.min.js"></script>
     <script src="<?php echo base_url();?>assets/js/admin.js"></script>

     <?php if($theme=='blog') {  if($module=='post') { if($page=='add_post' || $page=='edit_post') { ?>
       <script src="<?php echo base_url();?>assets/plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.js"></script>
       <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.cropit.js"></script>
       <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/cropper_image.js"></script> 
     <?php } } ?>
       <script src="<?php echo base_url();?>assets/js/blog.js"></script>
     <?php } ?>
    <?php if($this->session->flashdata('error_message')) {  ?>
             <script>
               toastr.error('<?php echo $this->session->flashdata('error_message');?>');
            </script>
        <?php $this->session->unset_userdata('error_message');
        } if($this->session->flashdata('success_message')) {  ?>

            <script>
               toastr.success('<?php echo $this->session->flashdata('success_message');?>');
            </script>
            
      <?php $this->session->unset_userdata('success_message'); } ?>

</body>

</html>