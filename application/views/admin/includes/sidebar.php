
      
      <!-- Sidebar -->
            <div class="sidebar" id="sidebar">
                <div class="sidebar-inner slimscroll">
          <div id="sidebar-menu" class="sidebar-menu">
            <ul>
              <li class="menu-title"> 
                <span>Main</span>
              </li>
              <li <?php echo ($module=='dashboard')?'class="active"':'';?>> 
                <a href="<?php echo base_url();?>admin/dashboard"><i class="fe fe-home"></i> <span>Dashboard</span></a>
              </li>
              <li <?php echo ($module=='appointments')?'class="active"':'';?>> 
                <a href="<?php echo base_url();?>admin/appointments"><i class="fe fe-layout"></i> <span>Appointments</span></a>
              </li>
              <li <?php echo ($module=='specialization')?'class="active"':'';?>> 
                <a href="<?php echo base_url();?>admin/specialization"><i class="fe fe-users"></i> <span>Specialization</span></a>
              </li>
              <li <?php echo ($module=='users' && $page=='doctors' )?'class="active"':'';?>>  
                <a href="<?php echo base_url();?>admin/users/doctors"><i class="fe fe-user-plus"></i> <span>Doctors</span></a>
              </li>
              <li <?php echo ($module=='users' && $page=='patients' )?'class="active"':'';?>>
                <a href="<?php echo base_url();?>admin/users/patients"><i class="fe fe-user"></i> <span>Patients</span></a>
              </li>

              <li class="submenu">
                <a href="#"><i class="fe fe-question"></i> <span>Questions & Answer </span> <span class="menu-arrow"></span></a>
                <ul style="display: none;">
                  <li ><a href="<?php echo base_url();?>admin/questions">Questions</a></li>
                  <li><a href="<?php echo base_url();?>admin/questions/answers">Answers</a></li>
                  </ul>
              </li>

              <li <?php echo ($module=='payment_requests' && $page=='index')?'class="active"':'';?>> 
                <a href="<?php echo base_url();?>admin/payment_requests"><i class="fe fe-activity"></i> <span>Payment Requests</span></a>
              </li>
              <li <?php echo ($module=='reviews')?'class="active"':'';?>> 
                <a href="<?php echo base_url();?>admin/reviews"><i class="fe fe-star-o"></i> <span>Reviews</span></a>
              </li>
              <li <?php echo ($module=='settings')?'class="active"':'';?>>
                <a href="<?php echo base_url();?>admin/settings"><i class="fe fe-vector"></i> <span>Settings</span></a>
              </li>
               <li <?php echo ($module=='email_template')?'class="active"':'';?>>
                <a href="<?php echo base_url();?>admin/email_template"><i class="fe fe-vector"></i> <span>Email Template</span></a>
              </li>
               <li <?php echo ($module=='cms')?'class="active"':'';?>>
                <a href="<?php echo base_url();?>admin/cms"><i class="fe fe-vector"></i> <span>CMS</span></a>
              </li>

              <li class="submenu">
                <a href="#"><i class="fe fe-vector"></i> <span> Language </span> <span class="menu-arrow"></span></a>
                <ul style="display: none;">
                  <li><a href="<?php echo base_url();?>admin/language"> Language </a></li>
                  <li><a href="<?php echo base_url();?>admin/language/keywords">Language Keywords </a></li>
                  <li><a href="<?php echo base_url();?>admin/language/pages"> App Language Keywords </a></li>
                  </ul>
              </li>
             
              <li <?php echo ($module=='profile')?'class="active"':'';?>>
                <a href="<?php echo base_url();?>admin/profile"><i class="fe fe-user-plus"></i> <span>Profile</span></a>
              </li>
              <li class="menu-title"> 
                <span>Blogs</span>
              </li>
              <li class="submenu">
                <a href="#"><i class="fe fe-folder-open"></i> <span> Categories </span> <span class="menu-arrow"></span></a>
                <ul style="display: none;">
                  <li><a href="<?php echo base_url();?>blog/categories"> Categories </a></li>
                  <li><a href="<?php echo base_url();?>blog/subcategories"> Sub Categories </a></li>
                  </ul>
              </li>
              <li class="submenu">
                <a href="#"><i class="fe fe-folder-open"></i> <span> Post </span> <span class="menu-arrow"></span></a>
                <ul style="display: none;">
                  <li><a href="<?php echo base_url();?>blog/post"> Post </a></li>
                  <li><a href="<?php echo base_url();?>blog/pending-post">Pending Post </a></li>
                  <li><a href="<?php echo base_url();?>blog/add-post"> Add Post </a></li>
                  </ul>
              </li>

              <li class="submenu">
                <a href="#"><i class="fe fe-folder-open"></i> <span> Country Config </span> <span class="menu-arrow"></span></a>
                <ul style="display: none;">
                  <li><a href="<?php echo base_url();?>admin/country/country"> Add Country </a></li>
                  <li><a href="<?php echo base_url();?>admin/country/state">Add State </a></li>
                  <li><a href="<?php echo base_url();?>admin/country/city"> Add City </a></li>
                  </ul>
              </li>
              
            </ul>
          </div>
                </div>
            </div>
      <!-- /Sidebar -->