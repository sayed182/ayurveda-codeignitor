<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
         <title><?php echo !empty(settings("meta_title"))?settings("meta_title"):"Doccure";?></title>
    <meta content="<?php echo !empty(settings("meta_keywords"))?settings("meta_keywords"):"";?>" name="keywords">
    <meta content="<?php echo !empty(settings("meta_description"))?settings("meta_description"):"";?>" name="description">
        <!-- Favicons -->
    <link href="<?php echo !empty(base_url().settings("favicon"))?base_url().settings("favicon"):base_url()."assets/img/favicon.png";?>" rel="icon">

    <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
    
    <!-- Fontawesome CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/fontawesome/css/fontawesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/fontawesome/css/all.min.css">
    
    <!-- Main CSS -->
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/toastr.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/admin.css">
    
    <!--[if lt IE 9]>
      <script src="assets/js/html5shiv.min.js"></script>
      <script src="assets/js/respond.min.js"></script>
    <![endif]-->
    </head>
    <body>
  
    <!-- Main Wrapper -->
        <div class="main-wrapper login-body">
            <div class="login-wrapper">
              <div class="container">
                  <div class="loginbox">
                      <div class="login-left">
              <img class="img-fluid" src="<?php echo !empty(base_url().settings("logo_front"))?base_url().settings("logo_front"):base_url()."assets/img/logo.png";?>" alt="Logo">
                        </div>
                        <div class="login-right">
              <div class="login-right-wrap">
                <h1>Login</h1>
                <p class="account-subtitle">Access to our dashboard</p>
                
                <!-- Form -->
                <form method="post" action="<?php echo base_url();?>admin/login/is_valid_login">
                  <div class="form-group">
                    <input class="form-control" name="email" type="email" placeholder="Email">
                  </div>
                  <div class="form-group">
                    <input class="form-control" name="password" type="password" placeholder="Password">
                  </div>
                  <div class="form-group">
                    <button class="btn btn-primary btn-block" type="submit">Login</button>
                  </div>
                </form>
                <!-- /Form -->
                
                <!-- <div class="text-center forgotpass"><a href="forgot-password.html">Forgot Password?</a></div>
                <div class="login-or">
                  <span class="or-line"></span>
                  <span class="span-or">or</span>
                </div> -->
                  
            
              </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <!-- /Main Wrapper -->
    
    <!-- jQuery -->
        <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
    
    <!-- Bootstrap Core JS -->
        <script src="<?php echo base_url();?>assets/js/popper.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    
    <!-- Custom JS -->
    <script src="<?php echo base_url();?>assets/js/admin2.js"></script>

    <script src="<?php echo base_url();?>assets/js/toastr.js"></script>

    <?php if($this->session->flashdata('error_message')) {  ?>
             <script>
               toastr.error('<?php echo $this->session->flashdata('error_message');?>');
            </script>
        <?php $this->session->unset_userdata('error_message');
        } if($this->session->flashdata('success_message')) {  ?>

            <script>
               toastr.success('<?php echo $this->session->flashdata('success_message');?>');
            </script>
            
      <?php $this->session->unset_userdata('success_message'); } ?>
    
    </body>
</html>

        

