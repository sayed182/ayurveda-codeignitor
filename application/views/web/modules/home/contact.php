<style>

  a{

    color:white !important;
  }

  .main-nav li a.header-login {
    border: 2px solid white;
    border-radius: 25px;
    padding: 7px 15px !important;
    text-align: center;
    font-size: 15px;
    color: white;
    text-transform: uppercase;
    font-weight: 500;
}

.dropdown-content {
    display: none;
    position: absolute;
    background-color: #171515;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgb(21 19 19 / 20%);
    z-index: 1;
}
.bar-icon span{
  background-color: #fff !important;
}
.main-nav li a.header-login:hover{
  border-color: #fff !important;
}
</style>
<section class="py-5 contact-sec">
	<img src="<?php echo base_url();?>/assets/img/leaves.png" alt="Image" class="leaves" style="top: 50%;">
 
	<img src="<?php echo base_url();?>/assets/img/ct-bg.png" alt="Image" class="ct-bg">
	<img src="<?php echo base_url();?>/assets/img/contact-bg.png" class="contact-bg">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<h1>Tell Us Your Concern & We’ll Get In Touch With You!</h1>
				<div class="loc">
					<h4 class="provide">Contact Us</h4>
					<ul class="list-unstyled pt-3">
						<li><i class="fas fa-map-marker-alt"></i>&nbsp;&nbsp;H No 3-9-249, D No 401, Sri sai residency, Saraswathi Nagar Colony, L B Nagar, Hyderabad - 500074</li>
						<li><i class="fas fa-phone-alt"></i>&nbsp;&nbsp;98495 69365</li>
						<li><i class="far fa-envelope"></i>&nbsp;&nbsp;ayurwaydic@gmail.com</li>
					</ul>
					<a target="_blank" href="http://www.google.com/maps/place/17.346890,78.544090" style="background: #3dac7b !important"  class="directions"><i class="far fa-map"></i>&nbsp;&nbsp;Get Directions</a>
				</div>
        
			</div>
			<div class="col-md-6">
				<div class="tab-content add_ques">
                      <div id="login" class="container tab-pane active">
                        <div class="card">
                            <div class="card-body">
                            	<h4 class="provide pb-3">Please provide details to contact you</h4>
                                <form action="#" id="contact_form" method="post" autocomplete="off" novalidate="novalidate">
				                          <input type="hidden" id="country_id" value="+91">
                                  <div id="username_div">
                        <div class="form-group form-focus">
                  			   <label class="round-input-container w-100">
                  			    <div class="round-input-decorator">
                  			      <div class="round-input-border-left"></div>
                  			      <span class="round-input-label-text">Name</span>
                  			      <div class="round-input-border-right"></div>
                  			    </div>
                            
                  			    <input type="text" name="name" id="name" class="form-control floating">
                            </label>
                        </div>

                        <div class="form-group form-focus">
                  			   <label class="round-input-container w-100">
                  			    <div class="round-input-decorator">
                  			      <div class="round-input-border-left"></div>
                  			      <span class="round-input-label-text">Mobile No</span>
                  			      <div class="round-input-border-right"></div>
                  			    </div>
                  			    <input type="text" name="mobile" id="mobile" class="form-control floating">
                            </label>
                        </div>

                        <div class="form-group form-focus">
                  			   <label class="round-input-container w-100">
                  			    <div class="round-input-decorator">
                  			      <div class="round-input-border-left"></div>
                  			      <span class="round-input-label-text">Email ID</span>
                  			      <div class="round-input-border-right"></div>
                  			    </div>
                  			    <input type="text" name="email" id="email" class="form-control floating">
                            </label>
                        </div>
                        <div class="form-group form-focus">
                  			   <label class="round-input-container w-100">
                  			    <div class="round-input-decorator">
                  			      <div class="round-input-border-left"></div>
                  			      <span class="round-input-label-text">Your Concern</span>
                  			      <div class="round-input-border-right"></div>
                  			    </div>
                  			    <input type="text" name="concern" id="concern" class="form-control floating">
                            </label>
                        </div>
                         <div class="form-group form-focus">
                  			   <label class="round-input-container w-100">
                  			    <div class="round-input-decorator">
                  			      <div class="round-input-border-left"></div>
                  			      <span class="round-input-label-text">Explain (optional)</span>
                  			      <div class="round-input-border-right"></div>
                  			    </div>
                  			    <textarea rows="5" name="explain" id="explain" class="form-control floating" style="height: 70px"></textarea>
                            </label>
                        </div>
                                   
                                    
                                  </div>

                                  
                                     
                                      
                                    <div class="view-all text-left d-inline-block mt-4 w-100"> 
                                        <button type="submit" id="contact_button" class="btn btn-primary mt-0 w-100" style="display: inline-block;margin-top: 28px !important">Submit</button>
                                      </div>
                                  </form>
                            </div>
                          </div>
                      </div>
                   
                     
                    </div>
			</div>
		</div>
   
	</div>
</section>

<section class="pb-5" style="position: relative;">
  <img src="<?php echo base_url();?>/assets/img/leaves1.png" alt="Image" class="leaves" style="bottom: 0;right: -100px;left: unset;width: 15%">
  <div class="container">
   <div class="row pt-5 about-sec" style="position: relative;">
       <div class="col-md-12 text-center">
         <h2 class="py-4" style="font-size: 32px;font-weight: 800;font-style: inherit;">Consult A Doctor Online With Ayurwayda</h2>
         <p>At Ayurwayda, we aim at providing <span>quality healthcare services</span> to all. Situations such as busy schedules, traffic, long queues, unaffordability, etc can get in the way of seeking proper medical aid. Hence, Ayurwayda partners with top hospitals and leading healthcare institutions across the country to make sure that expert doctors are just a tap away. Our website and doctor consultation app will enable you to consult a doctor from any location or situation. We have doctors in every city of many specialities and super specialities, who treat numerous medical conditions.</p>
         <p>Make life easier for you and your loved ones. Get your <span>first free consultation</span> today by chatting with our doctor to diagnose and symptoms and conditions treat any you may be experiencing.</p>
       </div>
    </div>
  </div>
</section>

