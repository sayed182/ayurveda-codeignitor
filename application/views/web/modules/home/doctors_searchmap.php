	<div class="breadcrumb-bar">
				<div class="container-fluid">
					<div class="row align-items-center">
						<div class="col-md-8 col-12">
							<nav aria-label="breadcrumb" class="page-breadcrumb">
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="<?php echo base_url();?>"><?php echo $language['lg_home'];?></a></li>
									<li class="breadcrumb-item active" aria-current="page"><?php echo $language['lg_doctor_search'];?></li>
								</ol>
							</nav>
							<h2 class="breadcrumb-title search-results"></h2>
						</div>
						<div class="col-md-4 col-12 d-md-block d-none">
							<div class="sort-by">
								<span class="sort-title"><?php echo $language['lg_sort_by'];?></span>
								<span class="sortby-fliter">
									<select class="select form-control" id="orderby" onchange="search_doctor(0)">
										<option value=""><?php echo $language['lg_select'];?></option>
										<option class="sorting" value="Rating"><?php echo $language['lg_rating'];?></option>
										<option class="sorting" value="Popular"><?php echo $language['lg_popular'];?></option>
										<option class="sorting" value="Latest"><?php echo $language['lg_latest'];?></option>
										<option class="sorting" value="Free"><?php echo $language['lg_free'];?></option>
									</select>
								</span>
							</div>
						</div>
					</div>
				</div>
			</div>

<div class="content">
				<div class="container-fluid">

	            <div class="row">

	            		<div class="col-md-12 col-lg-4 col-xl-3 theiaStickySidebar">
						
							<!-- Search Filter -->
							<div class="card search-filter">
								<div class="card-header">
									<h4 class="card-title mb-0"><?php echo $language['lg_search_filter'];?> <a href="javascript:void(0);" onclick="reset_doctor()" class="text-danger text-sm float-right"><?php echo $language['lg_reset'];?></a></h4>
								</div>
								<div class="card-body">
									<form id="search_doctor_form">

								<div class="filter-widget">
									<input type="text" class="form-control filter-form ft-search"  name="keywords"  type="text" placeholder="<?php echo $language['lg_keywords'];?>" value="<?php echo !empty($keywords)?$keywords:'';?>" autocomplete="off" id="keywords">
								</div>
								
								<div class="filter-widget">
									<select class="form-control " name="appointment_type" id="appointment_type">
										<option value=""> <?php echo $language['lg_booking_type'];?></option>
										<option value="online"><?php echo $language['lg_online'];?></option>
										<option value="clinic"><?php echo $language['lg_clinic'];?></option>
									</select>					
						       </div>
								<div class="filter-widget">
									<select class="form-control " name="gender" id="gender" >
										<option value=""> <?php echo $language['lg_select_gender'];?></option>
										<option value="Male"><?php echo $language['lg_male'];?></option>
										<option value="Female"><?php echo $language['lg_female'];?></option>
									</select>							
								</div>

								<div class="filter-widget">
									<select name="specialization" class="form-control " id="specialization">
				                        <option value=""><?php echo $language['lg_select_speciali'];?></option>
				                     </select>
								</div>
								<div class="filter-widget">
									<select name="country" class="form-control " id="country">
				                        <option value=""><?php echo $language['lg_select_country'];?></option>
				                     </select>
		                       </div>
								<div class="filter-widget">
									<select name="state" class="form-control " id="state">
			                        <option value=""><?php echo $language['lg_select_state'];?></option>
			                        </select>
		                       </div>
		                       <div class="filter-widget">
									<select name="city" class="form-control " id="city">
			                        <option value=""><?php echo $language['lg_select_city'];?></option>
			                        </select>
		                       </div>
									<div class="btn-search">
										<button type="button" onclick="search_doctor(0)" class="btn btn-block"><?php echo $language['lg_search3'];?></button>
									</div>
									</form>	
								</div>
							</div>
							<!-- /Search Filter -->
							
						</div>
					<div class="col-xl-5 col-lg-12 ">
				
						
							<input type="hidden" name="page" id="page_no_hidden" value="1" >

							<div id="doctor-list"></div>

							<div class="load-more text-center d-none" id="load_more_btn">
								<a class="btn btn-primary btn-sm" href="javascript:void(0);"><?php echo $language['lg_load_more'];?></a>	
							</div>
						<!-- Doctor Widget -->
						
	            </div>
	            <!-- /content-left-->
	            <div class="col-xl-4 col-lg-6 ">
	                <div id="map" class="map-listing"></div>
	                <!-- map-->
	            </div>
	            <!-- /map-right-->
	        </div>
	        <!-- /row-->
	   
				</div>

			</div>		

			<script type="text/javascript">
				var country='';
			    var state='<?php echo $state;?>';
			    var city='<?php echo $city;?>';
			    var specialization='';
			</script>
			<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA_QD2_rlwEFGhCK0oj2n6cixsvX0D3zgk"></script>