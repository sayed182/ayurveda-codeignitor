<!-- Breadcrumb -->
			<div class="breadcrumb-bar">
				<div class="container">
					<div class="row align-items-center">
						<div class="col-md-8 col-12">
							<nav aria-label="breadcrumb" class="page-breadcrumb">
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="<?php echo base_url();?>"><?php echo $language['lg_home'];?></a></li>
									<li class="breadcrumb-item active" aria-current="page"><?php echo $language['lg_doctor_search'];?></li>
								</ol>
							</nav>
							<h2 class="breadcrumb-title search-results"></h2>
						</div>
						<div class="col-md-4 col-12 d-md-block d-none">
							<div class="sort-by">
								<span class="sort-title"><?php echo $language['lg_sort_by'];?></span>
								<span class="sortby-fliter">
									<select class="select form-control" id="orderby" onchange="search_doctor(0)">
										<option value=""><?php echo $language['lg_select'];?></option>
										<option class="sorting" value="Rating"><?php echo $language['lg_rating'];?></option>
										
										<option class="sorting" value="Latest"><?php echo $language['lg_latest'];?></option>
										<option class="sorting" value="Free"><?php echo $language['lg_free'];?></option>
									</select>
								</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /Breadcrumb -->
			
			<!-- Page Content -->
			<div class="content">
				<div class="container">

					<div class="row">
						<div class="col-md-12 col-lg-4 col-xl-3 theiaStickySidebar">
						
							<!-- Search Filter -->
							<div class="card search-filter">
								<div class="card-header">
									<h4 class="card-title mb-0"><?php echo $language['lg_search_filter'];?> <a href="javascript:void(0);" onclick="reset_doctor()" class="text-danger text-sm float-right"><?php echo $language['lg_reset'];?></a></h4>
								</div>
								<div class="card-body">
									<form id="search_doctor_form">

								<div class="filter-widget">
									<input type="text" class="form-control filter-form ft-search"  name="keywords"  type="text" placeholder="<?php echo $language['lg_keywords'];?>" value="<?php echo !empty($keywords)?$keywords:'';?>" autocomplete="off" id="keywords">
								</div>
								
								<div class="filter-widget">
									<select class="form-control " name="appointment_type" id="appointment_type">
										<option value=""> <?php echo $language['lg_booking_type'];?></option>
										<option value="online"><?php echo $language['lg_online'];?></option>
										<option value="clinic"><?php echo $language['lg_clinic'];?></option>
									</select>					
						       </div>
								

								<div class="filter-widget">
									<select name="specialization" class="form-control " id="specialization">
				                        <option value=""><?php echo $language['lg_select_speciali'];?></option>
				                     </select>
								</div>
									<div class="btn-search">
										<button type="button" onclick="search_doctor(0)" class="btn btn-block"><?php echo $language['lg_search3'];?></button>
									</div>
									</form>	
								</div>
							</div>
							<!-- /Search Filter -->
							
						</div>
						
						<div class="col-md-12 col-lg-8 col-xl-9">
							<input type="hidden" name="page" id="page_no_hidden" value="1" >
							<div id="doctor-list"></div>

							<div class="load-more text-center d-none" id="load_more_btn">
								<a class="btn btn-primary btn-sm" href="javascript:void(0);"><?php echo $language['lg_load_more'];?></a>	
							</div>

							<!-- Doctor Widget -->
							
							<!-- /Doctor Widget -->

								
						</div>
					</div>

				</div>

			</div>		
			<!-- /Page Content -->

			<script type="text/javascript">
				var country='';
				var country_code='';
			    var state='';
			    var city='';
			  	var citys='<?php if(isset($_GET['location'])) { echo ($_GET['location']); } ?>';
			    var specialization='';
			</script>