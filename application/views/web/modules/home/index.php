<style>
    .bg {
        /* The image used */
        background-image: url("assets/img/login-banner-005-a.jpg");

        /* Full height */
        height: 100%;

        /* Center and scale the image nicely */
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;

    }

    .background-multiply .content {
        background-color: #636363 !important;
        background-blend-mode: multiply;
    }
</style>

<style>
    .inner-addon {
        position: relative;
    }


    #locationClickableIcon {
        cursor: pointer;
        width: auto;
        height: 6px;
        color: #3dac7b;
    }

    .search-location {
        padding: 15px;
    }


</style>

<script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA_QD2_rlwEFGhCK0oj2n6cixsvX0D3zgk&callback=locate&libraries=places&v=weekly"
        defer>
</script>

<script>

    let placeSearch;
    let autocomplete;
    const componentForm = {
        street_number: "short_name",
        route: "long_name",
        locality: "long_name",
        administrative_area_level_1: "long_name",
        country: "long_name",
        postal_code: "short_name",
    };

    function initAutocomplete() {

        // Create the autocomplete object, restricting the search predictions to
        // geographical location types.
        autocomplete = new google.maps.places.Autocomplete(
            document.getElementById("search_location"),
            {types: ['(cities)']}
        );

        // Set initial restrict to the greater list of countries.
        autocomplete.setComponentRestrictions({
            country: ["ind"],
        });
        // Avoid paying for data that you don't need by restricting the set of
        // place fields that are returned to just the address components.
        autocomplete.setFields(["address_component"]);
        // When the user selects an address from the drop-down, populate the
        // address fields in the form.
        autocomplete.addListener("place_changed", fillInAddress);
    }

    function fillInAddress() {

        // Get the place details from the autocomplete object.
        const place = autocomplete.getPlace();

        for (const component in componentForm) {
            //console.log(component);
            document.getElementById(component).value = "";
            document.getElementById(component).disabled = false;
        }

        // Get each component of the address from the place details,
        // and then fill-in the corresponding field on the form.
        const component = place.address_components[0];
        const addressType = component.types[0];
        // map changes
        if (componentForm[addressType]) {
            const val = component[componentForm['administrative_area_level_1']];

            document.getElementById('search_location').value = val;
        }
        //   for (const component of place.address_components) {
        // console.log(val);
        //     const addressType = component.types[0];

        //    if (componentForm[addressType]) {
        //      const val = component[componentForm['administrative_area_level_1']];

        //       document.getElementById('search_location').value = val;
        //    }
        //  }
        var address = $.trim($('#search_location').val());
        get_lat_long(address);
    }

    // Bias the autocomplete object to the user's geographical location,
    // as supplied by the browser's 'navigator.geolocation' object.
    function geolocate() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition((position) => {
                const geolocation = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude,
                };
                const circle = new google.maps.Circle({
                    center: geolocation,
                    radius: position.coords.accuracy,
                });
                autocomplete.setBounds(circle.getBounds());
            });
        }
    }

    function locate() {
        geocoder = new google.maps.Geocoder();
        if ("geolocation" in navigator) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var currentLatitude = position.coords.latitude;
                var currentLongitude = position.coords.longitude;

                var infoWindowHTML = "Latitude: " + currentLatitude + "<br>Longitude: " + currentLongitude;
                var infoWindow = new google.maps.InfoWindow({map: map, content: infoWindowHTML});
                var currentLocation = {lat: currentLatitude, lng: currentLongitude};
                infoWindow.setPosition(currentLocation);
                var lat = currentLocation.lat;
                var lng = currentLocation.lng;
                //lat = '34.052235';
                //lng = '-118.243683';
                $('#s_lat').val(lat);
                $('#s_long').val(lng);
                var latlng = {lat: parseFloat(lat), lng: parseFloat(lng)};
                //alert(lat + '---' + lng);
                geocoder.geocode({location: latlng}, function (results, status) {
                    var res = (results[0].formatted_address);
                    //console.log(res);
                    var arr = res.split(",");

                    var city = $.trim(arr.slice(-4, -2).pop().toLowerCase());

                    $('#search_location').val(city);
                });

            });
        }
        initAutocomplete();
    }

    function get_lat_long(address) {
        //alert(address);
        geocoder = new google.maps.Geocoder();

        geocoder.geocode({'address': address}, function (results, status) {

            if (status == google.maps.GeocoderStatus.OK) {
                var latitude = results[0].geometry.location.lat();
                var longitude = results[0].geometry.location.lng();
                $('#s_lat').val(latitude);
                $('#s_long').val(longitude);
                //alert(latitude);
            }
        });
    }

    function setCardLocation(ele){
        console.log(ele);
    }
</script>
<!-- Home Banner -->
<div class="bg_img1">
    <section class="section section-search">
        <div class="container">
            <div class="banner-wrapper w-100">
                <div class="row">
                    <div class="col-md-7">
                        <div class="banner-header">
                            <!-- <h1><?php echo settings('banner_title'); ?></h1> -->
                            <h1>
                                Got A Health Issue? <br/>Consult Top <span>Ayurvedic Doctors</span>
                            </h1>
                            <p><?php echo settings('banner_sub_title'); ?></p>
                        </div>
                        <!-- map changes -->
                        <input id="street_number" type="hidden"/>
                        <input disabled="disabled" id="route" type="hidden"/>
                        <input disabled="disabled" id="locality" type="hidden"/>
                        <input disabled="disabled" id="administrative_area_level_1" type="hidden"/>
                        <input disabled="disabled" id="postal_code" type="hidden"/>
                        <input disabled="disabled" id="country" type="hidden"/>
                        <!-- map changes -->
                        <!-- Search -->
                        <div class="search-box">
                            <form method="post" action="#" style="position: relative;">
                                <div class="form-group mb-0 search-info input-container">
                                    <input type="text" class="form-control" autocomplete="off"
                                           onkeyup="search_keyword()" id="search_keywords" name="keywords"
                                           placeholder="<?php echo $language['lg_search_doctorsd']; ?>">
                                    <!-- <span class="form-text"><?php echo $language['lg_ex__dental_or_s']; ?></span> -->
                                    <div class="keywords_result"></div>
                                </div>
                                <button type="button" class="btn search-btn" id="search_button"><i
                                            class="fa fa-search fa-lg" id="autolocate" style="color: #3dac7b"></i>
                                    <span><?php echo $language['lg_search3']; ?></span></button>
                            </form>

                        </div>

                        <div class="mb-0 mt-2 search-location">
                            <i class="fas fa-map-marker-alt fa-lg for-mob" id="autolocate" style="color: #3dac7b"></i>
                            <span id="locationClickableIcon" onclick="locate()"><i
                                        class="fas fa-map-marker-alt fa-lg for-web" id='autolocate'
                                        style="color: #3dac7b; padding-right: 15px;"></i>Detect Location</span>
                            <input type="hidden" class="form-control" autocomplete="off" id="search_location"
                                   name="location" placeholder="<?php echo $language['lg_search_location']; ?>">
                            <!-- map changes -->

                            <!-- map changes -->
                            <!-- <span class="form-text"><?php echo $language['lg_based_on_your_l']; ?></span> -->


                        </div>
                        <!-- /Search -->
                    </div>

                </div>
                <!-- <div class="row">
                   <div class="col-md-12">
                       <form class="form-inline" action="/action_page.php">

                         <input type="email" class="form-control" placeholder="Enter email" id="email">

                         <input type="password" class="form-control" placeholder="Enter password" id="pwd">

                         <button type="submit" class="btn btn-primary">Submit</button>
                       </form>

                   </div>
                </div> -->

                <div class="row pt-4">
                    <div class="col-md-3">
                        <ul class="list-unstyled mob-0">
                            <li>Book an appointment</li>
                            <li>Convenient and easy</li>

                        </ul>
                    </div>
                    <div class="col-md-3">
                        <ul class="list-unstyled">
                            <li>Consult with verified doctors</li>
                            <li>Free Follow up</li>

                        </ul>
                    </div>
                </div>


            </div>
        </div>
    </section>
    <br/><br/><br/>
    <!-- /Home Banner -->

    <section class="py-5 looking-for">
        <div class="container for-web">
            <div class="row">
                <div class="col-md-3">
                    <div class="pt-5 pb-5 pr-3">
                        <h2>What Are You Looking For?</h2>
                        <p>Ayurvedic treatment has proven the best result in healthcare and we are providing you every
                            service at home.</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card text-center h-100">
                        <div class="card-body">
                            <img src="assets/img/icon-1.png" alt="image" class="">
                            <h5>Visit A Doctor</h5>
                            <p>Select preferred ayurvedic doctor and book an appointment.</p>
                            <a href="<?php echo base_url(); ?>doctors-search"
                               class="text-decoration-none d-inline-block mt-2">Book Now</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card text-center h-100">
                        <div class="card-body">
                            <img src="assets/img/icon-2.png" alt="image" class="">
                            <h5>Find A Pharmacy</h5>
                            <p>Browse through our listed pharmacies to get your ayurvedic medicines.</p>
                            <a href="#" class="text-decoration-none d-inline-block mt-2">Coming soon</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card text-center h-100">
                        <div class="card-body">
                            <img src="assets/img/icon-3.png" alt="image" class="">
                            <h5>Find A Lab</h5>
                            <p>Get accurate tests done at the ease of your home.</p>
                            <a href="#" class="text-decoration-none d-inline-block mt-2">Coming soon</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--  <div class="container for-mob">
            <div class="row blog p-0">
                <div class="col-md-12">
                    <div id="blogCarousel" class="carousel slide" data-ride="carousel">

                      
                        <div class="carousel-inner">

                            <div class="carousel-item active">
                                <div class="row">
                                    <div class="col-md-8 offset-md-2">
                                        <div class="card text-center">
                                           <div class="card-body">
                                              <img src="assets/img/icon-1.png" alt="image">
                                              <h5>Visit A Doctor</h5>
                                              <p>Select preferred ayurvedic doctor and book an appointment.</p>
                                              <a href="<?php echo base_url(); ?>doctors-search" class="text-decoration-none d-inline-block mt-2">Book Now</a>
                                           </div>
                                        </div>
                                    </div>
                                    
                                </div>
                               
                            </div>
                        

                            <div class="carousel-item">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="card text-center">
                                           <div class="card-body">
                                              <img src="assets/img/icon-2.png" alt="image">
                                              <h5>Visit A Doctor</h5>
                                              <p>Select preferred ayurvedic doctor and book an appointment.</p>
                                              <a href="<?php echo base_url(); ?>doctors-search" class="text-decoration-none d-inline-block mt-2">Book Now</a>
                                           </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            
                            <div class="carousel-item">
                                <div class="row">
                                     <div class="col-md-12">
                                        <div class="card text-center">
                                           <div class="card-body">
                                              <img src="assets/img/icon-2.png" alt="image">
                                              <h5>Visit A Doctor</h5>
                                              <p>Select preferred ayurvedic doctor and book an appointment.</p>
                                              <a href="<?php echo base_url(); ?>doctors-search" class="text-decoration-none d-inline-block mt-2">Book Now</a>
                                           </div>
                                        </div>
                                    </div>
                                </div>
                               
                            </div>
                           

                            <div class="carousel-item">
                                <div class="row">
                                     <div class="col-md-12">
                                        <div class="card text-center">
                                           <div class="card-body">
                                              <img src="assets/img/icon-3.png" alt="image">
                                              <h5>Visit A Doctor</h5>
                                              <p>Select preferred ayurvedic doctor and book an appointment.</p>
                                              <a href="<?php echo base_url(); ?>doctors-search" class="text-decoration-none d-inline-block mt-2">Book Now</a>
                                           </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                          
                             <a class="carousel-control-prev" href="#blogCarousel" role="button" data-slide="prev">
                                
                              </a>
                              <a class="carousel-control-next" href="#blogCarousel" role="button" data-slide="next">
                                
                              </a>

                        </div>
                        
                    </div>
                   

                </div>
            </div>
          </div> -->
        <div class="container for-mob">
            <div class="row pt-3">
                <div class="col-md-12 text-center">
                    <h2>What Are You Looking For?</h2>
                    <p>Ayurvedic treatment has proven the best result in healthcare and we are providing you every
                        service at home.</p>
                </div>
            </div>

            <div id="testimonial_4"
                 class="carousel slide testimonial4_indicators testimonial4_control_button thumb_scroll_x swipe_x"
                 data-ride="carousel" data-pause="hover" data-interval="5000" data-duration="2000">

                <div class="carousel-inner" role="listbox">
                    <div class="carousel-item active">
                        <div class="testimonial4_slide mob-slider">

                            <div class="card text-center">
                                <div class="card-body">
                                    <img src="assets/img/icon-1.png" alt="image">
                                    <h5>Visit A Doctor</h5>
                                    <p>Select preferred ayurvedic doctor and book an appointment.</p>
                                    <a href="<?php echo base_url(); ?>doctors-search"
                                       class="text-decoration-none d-inline-block mt-2">Book Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="testimonial4_slide mob-slider">
                            <div class="card text-center">
                                <div class="card-body">
                                    <img src="assets/img/icon-2.png" alt="image">
                                    <h5>Find A Pharmacy</h5>
                                    <p>Browse through our listed pharmacies to get your ayurvedic medicines.</p>
                                    <a href="<?php echo base_url(); ?>doctors-search"
                                       class="text-decoration-none d-inline-block mt-2">Coming soon</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="testimonial4_slide mob-slider">

                            <div class="card text-center">
                                <div class="card-body">
                                    <img src="assets/img/icon-3.png" alt="image">
                                    <h5>Find A Lab</h5>
                                    <p>Get accurate tests done at the ease of your home.</p>
                                    <a href="<?php echo base_url(); ?>doctors-search"
                                       class="text-decoration-none d-inline-block mt-2">Coming soon</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#testimonial_4" data-slide="prev">
                    <span class="carousel-control-prev-icon">
                      <img src="assets/img/left-arrow.png" alt="image">
                    </span>
                </a>
                <a class="carousel-control-next" href="#testimonial_4" data-slide="next">
                    <span class="carousel-control-next-icon">
                      <img src="assets/img/right-arrow.png" alt="image">
                    </span>
                </a>
            </div>
        </div>

    </section>

    <img src="assets/img/leaves.png" alt="Image" class="leaves">
</div>


<!--  <section class="py-5 treatment">
   <div class="container">
       <div class="row">
           <div class="col-md-12 text-center">
               <h1>Treatments</h1>
               <p>Our doctors specialise in treating some specific illness through the practice of Ayurveda.</p>
           </div>
       </div>
       <div class="row pt-5">
           <div class="col" id="p1">
              <div class="card text-center">
                 <div class="card-body">
                     <img src="assets/img/tr-icon1.png" alt="image">
                     <h4>Stress & Anxiety</h4>
                 </div>
              </div>
           </div>
           <div class="col col-half-offset" id="p2">
               <div class="card text-center">
                 <div class="card-body">
                     <img src="assets/img/tr-icon1.png" alt="image">
                     <h4>Stress & Anxiety</h4>
                 </div>
              </div>
           </div>
           <div class="col col-half-offset" id="p3">
               <div class="card text-center">
                 <div class="card-body">
                     <img src="assets/img/tr-icon3.png" alt="image">
                     <h4>Arthritis</h4>
                 </div>
              </div>
           </div>
           <div class="col col-half-offset" id="p4">
               <div class="card text-center">
                 <div class="card-body">
                     <img src="assets/img/tr-icon4.png" alt="image">
                     <h4>Respiratory Disorders </h4>
                 </div>
              </div>
           </div>
           <div class="col col-half-offset" id="p5">
               <div class="card text-center">
                 <div class="card-body">
                     <img src="assets/img/tr-icon5.png" alt="image">
                     <h4>Weight Management</h4>
                 </div>
              </div>
           </div>

       </div>

       <div class="row pt-3">
           <div class="col" id="p1">
              <div class="card text-center">
                 <div class="card-body">
                     <img src="assets/img/tr-icon6.png" alt="image">
                     <h4>Skin Problems</h4>
                 </div>
              </div>
           </div>
           <div class="col col-half-offset" id="p2">
               <div class="card text-center">
                 <div class="card-body">
                     <img src="assets/img/tr-icon7.png" alt="image">
                     <h4>Infertility</h4>
                 </div>
              </div>
           </div>
           <div class="col col-half-offset" id="p3">
               <div class="card text-center">
                 <div class="card-body">
                     <img src="assets/img/tr-icon8.png" alt="image">
                     <h4>Diabetes</h4>
                 </div>
              </div>
           </div>
           <div class="col col-half-offset" id="p4">
               <div class="card text-center">
                 <div class="card-body">
                     <img src="assets/img/tr-icon9.png" alt="image">
                     <h4>Chronic Disorders</h4>
                 </div>
              </div>
           </div>
           <div class="col col-half-offset" id="p5">
               <div class="card text-center">
                 <div class="card-body">
                     <img src="assets/img/tr-icon10.png" alt="image">
                     <h4>Insomnia</h4>
                 </div>
              </div>
           </div>

       </div>



   </div>
 </section> -->

<?php if (!empty($specialization)) { ?>

    <section class="section section-specialities treatment py-5">
        <div class="container">
            <div class="section-header text-center mb-4">
                <h1><?php echo settings('specialities_title'); ?></h1>
                <p class="sub-title"><?php echo settings('specialities_content'); ?></p>
            </div>
            <div class="row justify-content-center row-cols-md-5 row-cols-sm-2">


                <?php foreach ($specialization as $srows) { ?>

                    <div class="col mb-4">
                        <div class="speicality-item text-center">
                            <div class="speicality-img pb-3">
                                <!-- <img src="<?php echo base_url() . $srows['specialization_img']; ?>" class="img-fluid" alt="Speciality"> -->
                                <img src="<?php echo base_url() . $srows['specialization_img']; ?>" alt="image"
                                     class="">

                            </div>
                            <h4><?php echo $srows['specialization']; ?></h4>
                        </div>
                    </div>


                <?php } ?>


            </div>
        </div>
    </section>

<?php } ?>

<!-- Popular Section -->
<section class="section section-doctor">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="section-header ">
                    <h1><?php echo settings('doctor_title'); ?></h1>

                </div>
                <div class="about-content">
                    <p><?php echo settings('doctor_content'); ?></p>
                    <!-- <a href="javascript:;"><?php echo $language['lg_read_more']; ?></a> -->
                </div>
            </div>
            <div class="col-lg-12 pt-4">
                <div class="doctor-slider slider">

                    <?php if (!empty($doctors)) {
                        foreach ($doctors as $rows) {

                            $profileimage = (!empty($rows['profileimage'])) ? base_url() . $rows['profileimage'] : base_url() . 'assets/img/doctors/doctor-02.jpg';

                            $where = array('patient_id' => $this->session->userdata('user_id'), 'doctor_id' => $rows['user_id']);
                            $favourites = '';
                            $is_favourite = $this->db->get_where('favourities', $where)->result_array();
                            if (count($is_favourite) > 0) {
                                $favourites = 'fav-btns';
                            }
                            ?>
                            <!-- Doctor Widget -->
<!--                            <a href="--><?php //echo base_url(); ?><!--doctor/--><?php //echo $rows['cityname']; ?><!--/--><?php //echo $rows['speciality']; ?><!--/--><?php //echo $rows['username']; ?><!--">-->
                            <div class="profile-widget dr-profile" onclick="setCardLocation(this);">
                                <div class="doc-img">
                                    <a href="<?php echo base_url(); ?>doctor/<?php echo $rows['cityname']; ?>/<?php echo $rows['speciality']; ?>/<?php echo $rows['username']; ?>">
                                        <img width="228" height="152" class="img-fluid" alt="User Image"
                                             src="<?php echo $profileimage; ?>">
                                    </a>
                                    <a href="javascript:void(0)" id="favourities_<?php echo $rows['user_id']; ?>"
                                       onclick="add_favourities('<?php echo $rows['user_id']; ?>')"
                                       class="fav-btn <?php echo $favourites; ?>">
                                        <i class="fas fa-heart"></i>
                                    </a>
                                </div>
                                <a href="<?php echo base_url(); ?>doctor/<?php echo $rows['cityname']; ?>/<?php echo $rows['speciality']; ?>/<?php echo $rows['username']; ?>">
                                    <div class="pro-content">
                                        <h3 class="title">
                                            <span class="font-weight-bold"
                                               ><?php echo $language['lg_dr']; ?><?php echo ucfirst($rows['first_name'] . ' ' . $rows['last_name']); ?></span>
                                            <i class="fas fa-check-circle verified"></i>
                                        </h3>
                                        <!-- <p class="speciality"><?php echo ucfirst($rows['speciality']); ?></p> -->
                                        <p class="speciality"><?php echo ucfirst($rows['speciality']); ?></p>
                                        <!-- <p class="speciality">5 Years Experience</p> -->
                                        <div class="rating">
                                            <span class="d-inline-block average-rating"><?php echo $rows['rating_count']; ?></span>
                                            <?php
                                            $rating_value = $rows['rating_value'];
                                            for ($i = 1; $i <= 1; $i++) {
                                                if ($i <= $rating_value) {
                                                    echo '<i class="fas fa-star filled"></i>';
                                                } else {
                                                    echo '<i class="fas fa-star"></i>';
                                                }
                                            }
                                            ?>

                                        </div>
                                        <ul class="available-info">

                                            <li>
                                                <i class="fas fa-map-marker-alt"></i> <?php echo $rows['statename'] . ' ' . $rows['countryname']; ?>
                                            </li>
                                            <!-- <li>
                                              <i class="far fa-clock"></i> Open 11:00am - 5:00pm
                                            </li> -->
                                            <li>

                                                <?php

                                                if ($rows['price_type'] == 'Custom Price') {

                                                    $user_currency = get_user_currency();
                                                    $user_currency_code = $user_currency['user_currency_code'];
                                                    $user_currency_rate = $user_currency['user_currency_rate'];

                                                    $currency_option = (!empty($user_currency_code)) ? $user_currency_code : $rows['currency_code'];
                                                    $rate_symbol = currency_code_sign($currency_option);

                                                    if (!empty($this->session->userdata('user_id'))) {
                                                        $rate = get_doccure_currency($rows['amount'], $rows['currency_code'], $user_currency_code);
                                                    } else {
                                                        $rate = $rows['amount'];
                                                    }
                                                    $amount = $rate_symbol . '' . $rate;

                                                } else {

                                                    $amount = "Free";
                                                }

                                                ?>

                                                <i class="far fa-money-bill-alt"></i> <?php echo $amount; ?>

                                            </li>
                                        </ul>
                                        <div class="row row-sm">
                                            <div class="col-12">
                                                <a href="<?php echo base_url() . 'book-appoinments/' . $rows['username']; ?>"
                                                   class="btn book-btn"><?php echo $language['lg_book_now']; ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>

                            <!-- /Doctor Widget -->
                        <?php }
                    } ?>


                </div>
            </div>
        </div>
    </div>
</section>
<!-- /Popular Section -->

<section class="py-5 green-bg">
    <div class="container">
        <div class="row py-4">
            <div class="col-3">
                <div class="d-flex quick-service-list">
                    <div>
                        <img src="assets/img/gr-icon1.png" alt="Image">
                    </div>
                    <div>
                        <h5 class="py-3 ml-2">Cut Down On<br/> Travel Time</h5>
                    </div>
                </div>

            </div>
            <div class="col-3">
                <div class="d-flex quick-service-list">
                    <div>
                        <img src="assets/img/gr-icon2.png" alt="Image">
                    </div>
                    <div>
                        <h5 class="py-3 ml-2">Zero Waiting <br/>Period</h5>
                    </div>
                </div>

            </div>
            <div class="col-3">
                <div class="d-flex quick-service-list">
                    <div>
                        <img src="assets/img/gr-icon3.png" alt="Image">
                    </div>
                    <div>
                        <h5 class=" py-3 ml-2">Certified<br/> Experts</h5>
                    </div>
                </div>

            </div>
            <div class="col-3">
                <div class="d-flex quick-service-list">
                    <div>
                        <img src="assets/img/gr-icon4.png" alt="Image">
                    </div>
                    <div>
                        <h5 class="py-3 ml-2">24x7 Pharmacy<br/> Availability</h5>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>

<!-- Availabe Features -->
<!-- <section class="section section-features">
        <div class="container-fluid">
           <div class="row">
            <div class="col-md-5 features-img">
              <img src="<?php echo !empty(base_url() . settings("feature_image")) ? base_url() . settings("feature_image") : base_url() . "assets/img/features/feature.png"; ?>" class="img-fluid" alt="Feature">
            </div>
            <div class="col-md-7">
              <div class="section-header">  
                <h2 class="mt-2"><?php echo settings('feature_title'); ?></h2>
                <p><?php echo settings('feature_sub_title'); ?> </p>
              </div>  
              <div class="features-slider slider">
               
                <div class="feature-item text-center">
                  <img src="<?php echo base_url(); ?>assets/img/features/feature-01.jpg" class="img-fluid" alt="Feature">
                  <p><?php echo $language['lg_patient_ward']; ?></p>
                </div>
                
                <div class="feature-item text-center">
                  <img src="<?php echo base_url(); ?>assets/img/features/feature-02.jpg" class="img-fluid" alt="Feature">
                  <p><?php echo $language['lg_test_room']; ?></p>
                </div>
                >
                <div class="feature-item text-center">
                  <img src="<?php echo base_url(); ?>assets/img/features/feature-03.jpg" class="img-fluid" alt="Feature">
                  <p><?php echo $language['lg_icu']; ?></p>
                </div>
               
                <div class="feature-item text-center">
                  <img src="<?php echo base_url(); ?>assets/img/features/feature-04.jpg" class="img-fluid" alt="Feature">
                  <p><?php echo $language['lg_laboratory']; ?></p>
                </div>
                
                <div class="feature-item text-center">
                  <img src="<?php echo base_url(); ?>assets/img/features/feature-05.jpg" class="img-fluid" alt="Feature">
                  <p><?php echo $language['lg_operation']; ?></p>
                </div>
                
                <div class="feature-item text-center">
                  <img src="<?php echo base_url(); ?>assets/img/features/feature-06.jpg" class="img-fluid" alt="Feature">
                  <p><?php echo $language['lg_medical']; ?></p>
                </div>
               
              </div>
            </div>
           </div>
        </div>
      </section> -->
<!-- /Availabe Features -->


<!-- /blog Section starts-->
<section class="section section-doctor for-web" style="position: relative;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="section-header ">
                    <h1>Read Articles From Health Experts</h1>
                    <p class="sub-title w-100">Some of the most recent articles on trending topics that help you
                        maintain a healthy Ayurvedic lifestyle all year round.</p>

                </div>

            </div>
            <div class="col-lg-12 pt-4">
                <div class="doctor-slider slider read-article">

                    <?php foreach ($blogs as $brows) {
                        $image_url = explode(',', $brows['upload_image_url']);
                        ?>

                        <div class="">
                            <!-- Blog Post -->
                            <div class="blog grid-blog">
                                <div class="blog-image">
                                    <a href="<?php echo base_url() . 'blog/' . str_replace(" ", "_", $brows['title']); ?>"><img
                                                class="img-fluid" src="<?php echo $image_url[0]; ?>"
                                                alt="Post Image"></a>
                                </div>
                                <div class="blog-content">
                                    <span class="blog-tag"><?php echo $brows['keywords']; ?></span>

                                    <h3 class="blog-title"><a
                                                href="<?php echo base_url() . 'blog/' . str_replace(" ", "_", $brows['title']); ?>"><?php echo $brows['title']; ?></a>
                                    </h3>
                                    <!--  <p class="mb-0"><?php echo character_limiter($brows['description'], 70, '...'); ?></p> -->

                                    <ul class="entry-meta meta-item">
                                        <li>
                                            <div class="post-author">
                                                <a href="<?php echo ($brows['post_by'] == 'Admin') ? 'javascript:void(0);' : base_url() . 'doctor-preview/' . $brows['username']; ?>"><img
                                                            src="<?php echo (!empty($brows['profileimage'])) ? base_url() . $brows['profileimage'] : base_url() . 'assets/img/user.png'; ?>"
                                                            alt="Post Author">
                                                    <span><?php echo ($brows['post_by'] == 'Admin') ? ucfirst($brows['name']) : $language['lg_dr'] . ' ' . ucfirst($brows['name']); ?></span></a>
                                            </div>
                                        </li>
                                        <li>
                                            <i class="far fa-calendar-alt"></i> <?php echo date('d M ,Y', strtotime($brows['created_date'])); ?>
                                        </li>

                                        <li>

                                            <i class="far fa-clock"></i> <?php
                                            $word_count = str_word_count( $brows["content"] );
                                            $time = ceil( $word_count / 250 );
                                            echo $time ?> min, Read
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!-- /Blog Post -->

                        </div>

                    <?php } ?>


                </div>
            </div>
        </div>
        <div class="view-all text-center">
            <a href="<?php echo base_url(); ?>blog" class="btn btn-primary"><?php echo $language['lg_view_all']; ?></a>
        </div>
    </div>
    <img src="assets/img/leaves1.png" alt="Image" class="leaves" style="bottom: 0;right: -54px;left: unset">
</section>
<!-- /blog Section ends-->


<!-- <?php if (!empty($blogs)) { ?>
      
      
       <section class="section section-blogs for-web">
        <div class="container">
        
         
          <div class="section-header text-center">
            <h1><?php echo $language['lg_blogs_and_news']; ?></h1>
            <h1>Read Articles From Health Experts</h1>
            <p class="sub-title w-100">Some of the most recent articles on trending topics that help you maintain a healthy Ayurvedic lifestyle all year round.</p>
          </div>
         
          <div class="row blog-grid-row">

            <?php foreach ($blogs as $brows) {
    $image_url = explode(',', $brows['upload_image_url']);
    ?>

            <div class="col-md-6 col-lg-4 col-sm-12">
                        
              <div class="blog grid-blog">
                <div class="blog-image">
                  <a href="<?php echo base_url() . 'blog/blog-details/' . $brows['slug']; ?>"><img class="img-fluid" src="<?php echo $image_url[0]; ?>" alt="Post Image"></a>
                </div>
                <div class="blog-content">
                  <span class="blog-tag"><?php echo $brows['keywords']; ?></span>
                  
                  <h3 class="blog-title"><a href="<?php echo base_url() . 'blog/blog-details/' . $brows['slug']; ?>"><?php echo $brows['title']; ?></a></h3>
                 

                     <ul class="entry-meta meta-item">
                        <li>
                          <div class="post-author">
                            <a href="<?php echo ($brows['post_by'] == 'Admin') ? 'javascript:void(0);' : base_url() . 'doctor-preview/' . $brows['username']; ?>"><img src="<?php echo (!empty($brows['profileimage'])) ? base_url() . $brows['profileimage'] : base_url() . 'assets/img/user.png'; ?>" alt="Post Author"> <span><?php echo ($brows['post_by'] == 'Admin') ? ucfirst($brows['name']) : $language['lg_dr'] . ' ' . ucfirst($brows['name']); ?></span></a>
                          </div>
                        </li>
                      

                        <li><i class="far fa-clock"></i> <?php echo date('d M Y', strtotime($brows['created_date'])); ?></li>
                      </ul>
                </div>
              </div>
             
              
            </div>

            <?php } ?>
          
            
          </div>
          <div class="view-all text-center"> 
            <a href="<?php echo base_url(); ?>blog" class="btn btn-primary"><?php echo $language['lg_view_all']; ?></a>
          </div>
        </div>
      </section> -->


    <section class="section section-blogs for-mob">
        <div class="container">

            <!-- Section Header -->
            <div class="section-header text-center">
                <!-- <h1><?php echo $language['lg_blogs_and_news']; ?></h1> -->
                <h1>Read Articles From Health Experts</h1>
                <p class="sub-title w-100">Some of the most recent articles on trending topics that help you maintain a
                    healthy Ayurvedic lifestyle all year round.</p>
            </div>
            <!-- /Section Header -->

            <div class="row justify-content-center">
                <div class="col-md-12">

                    <div class="doctor-slider slider">

                        <?php foreach ($blogs as $brows) {
                            $image_url = explode(',', $brows['upload_image_url']);
                            ?>


                            <div class="">
                                <!-- Blog Post -->
                                <div class="blog grid-blog">
                                    <div class="blog-image">
                                        <a href="<?php echo base_url() . 'blog/' . str_replace(" ", "_", $brows['title']); ?>"><img
                                                    class="img-fluid" src="<?php echo $image_url[0]; ?>"
                                                    alt="Post Image"></a>
                                    </div>
                                    <div class="blog-content">
                                        <span class="blog-tag"><?php echo $brows['keywords']; ?></span>

                                        <h3 class="blog-title"><a
                                                    href="<?php echo base_url() . 'blog/' . url_title($brows['title']); ?>"><?php echo $brows['title']; ?></a>
                                        </h3>


                                        <ul class="entry-meta meta-item">
                                            <li>
                                                <div class="post-author">
                                                    <a href="<?php echo ($brows['post_by'] == 'Admin') ? 'javascript:void(0);' : base_url() . 'doctor-preview/' . $brows['username']; ?>"><img
                                                                src="<?php echo (!empty($brows['profileimage'])) ? base_url() . $brows['profileimage'] : base_url() . 'assets/img/user.png'; ?>"
                                                                alt="Post Author">
                                                        <span><?php echo ($brows['post_by'] == 'Admin') ? ucfirst($brows['name']) : $language['lg_dr'] . ' ' . ucfirst($brows['name']); ?></span></a>
                                                </div>
                                            </li>


                                            <li>
                                                <i class="far fa-clock"></i> <?php echo date('d M Y', strtotime($brows['created_date'])); ?>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- /BLog Post -->

                            </div>

                        <?php } ?>

                    </div>


                </div>
                <div class="view-all text-center col-md-12">
                    <a href="<?php echo base_url(); ?>blog"
                       class="btn btn-primary"><?php echo $language['lg_view_all']; ?></a>
                </div>
            </div>
        </div>
    </section>
<!-- /Blog Section -->


<!--testimonial section-->

    <section class="testimonial text-center">
        <div class="container">

            <div class="heading">
                <h1>What They Believe</h1>
                <p>What our users have to say about their experience.</p>
            </div>
            <div id="testimonial4"
                 class="carousel slide testimonial4_indicators testimonial4_control_button thumb_scroll_x swipe_x"
                 data-ride="carousel" data-pause="hover" data-interval="5000" data-duration="2000">

                <div class="carousel-inner" role="listbox">
                    <div class="carousel-item active">
                        <div class="testimonial4_slide">

                            <p class="pb-3">Very good site. Well thought out about booking/rescheduling/canceling an
                                appointment. Also, Doctor’s feedback mechanism is good and describes all the basics in a
                                good way</p>
                            <div class="client_img">
                                <img src="https://i.ibb.co/8x9xK4H/team.jpg" class="img-circle img-responsive"/>
                                <h5 class="pt-3">Ankur Singh</h5>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="testimonial4_slide">
                            <p class="pb-3">Very good site. Well thought out about booking/rescheduling/canceling an
                                appointment. Also, Doctor’s feedback mechanism is good and describes all the basics in a
                                good way</p>
                            <div class="client_img">
                                <img src="https://i.ibb.co/8x9xK4H/team.jpg" class="img-circle img-responsive"/>
                                <h5 class="pt-3">Ankur Singh</h5>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="testimonial4_slide">

                            <p class="pb-3">Very good site. Well thought out about booking/rescheduling/canceling an
                                appointment. Also, Doctor’s feedback mechanism is good and describes all the basics in a
                                good way</p>
                            <div class="client_img">
                                <img src="https://i.ibb.co/8x9xK4H/team.jpg" class="img-circle img-responsive"/>
                                <h5 class="pt-3">Ankur Singh</h5>
                            </div>
                        </div>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#testimonial4" data-slide="prev">
                    <span class="carousel-control-prev-icon">
                      <img src="assets/img/left-arrow.png" alt="image">
                    </span>
                </a>
                <a class="carousel-control-next" href="#testimonial4" data-slide="next">
                    <span class="carousel-control-next-icon">
                      <img src="assets/img/right-arrow.png" alt="image">
                    </span>
                </a>
            </div>
        </div>
    </section>


<!--have doubts-->
    <section class="py-5 have_doubts">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <img src="assets/img/doubt.png" alt="image" class="img-fluid">
                </div>
                <div class="col-md-6">
                    <h1>Have Doubts? <br/>Here’s Your Answers</h1>

                    <div class="bs-example">
                        <div class="accordion" id="accordionExample">
                            <?php $sno = 0;
                            foreach ($qa as $key => $value) {
                                $sno++;

                                ?>
                                <div class="card">
                                    <div class="card-header" id="headingOne_<?php echo $sno; ?>">
                                        <h2 class="mb-0">
                                            <button type="button" class="btn btn-link" data-toggle="collapse"
                                                    data-target="#collapseOne_<?php echo $sno; ?>">
                                                <span><?php echo "0" . $sno; ?></span><i
                                                        class="fa fa-plus"></i> <?php echo $value['question']; ?>
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="collapseOne_<?php echo $sno; ?>" class="collapse"
                                         aria-labelledby="headingOne_<?php echo $sno; ?>"
                                         data-parent="#accordionExample">
                                        <div class="card-body">
                                            <p><?php echo $value['answers']; ?></p>
                                        </div>
                                    </div>
                                </div>

                            <?php } ?>

                        </div>
                    </div>
                    <div class="add_ques mt-5">
                        <div class="card">
                            <div class="card-body">

                                <form>
                                    <!--  <div class="form-group">
                                        <label class="round-input-container w-100">
                                           <div class="round-input-decorator">
                                             <div class="round-input-border-left"></div>
                                             <span class="round-input-label-text">Have any doubts in your mind? Ask your question here.</span>
                                             <div class="round-input-border-right"></div>
                                           </div>
                                         <input type="text" class="round-input form-control"/>
                                       </label>
                                     </div> -->
                                    <!--    <div class="form-group form-focus">
                                      <label class="round-input-container w-100">
                                       <div class="round-input-decorator">
                                         <div class="round-input-border-left"></div>
                                         <span class="round-input-label-text">Have any doubts in your mind? Ask your question here.</span>
                                         <div class="round-input-border-right"></div>
                                       </div>

                                       <input type="text" name="email" id="email" class="form-control floating">
                                       </label>
                                   </div>
                                   <div class="form-group form-focus">
                                      <label class="round-input-container w-100">
                                       <div class="round-input-decorator">
                                         <div class="round-input-border-left"></div>
                                         <span class="round-input-label-text">Enter your email ID</span>
                                         <div class="round-input-border-right"></div>
                                       </div>

                                       <input type="text" name="email" id="email" class="form-control floating">
                                       </label>
                                   </div> -->
                                    <!--  <div class="form-group">
                                        <label class="round-input-container w-100">
                                           <div class="round-input-decorator">
                                             <div class="round-input-border-left"></div>
                                             <span class="round-input-label-text">Enter your email ID</span>
                                             <div class="round-input-border-right"></div>
                                           </div>
                                         <input type="text" class="round-input form-control"/>
                                       </label>
                                     </div> -->
                                    <!-- <div class="view-all text-left">
                                        <button type="submit" class="btn btn-primary mt-2">Add Question</button>
                                      </div> -->
                                </form>

                                <div class="view-all text-left">


                                    <?php if ($this->session->userdata('role') == '2') { ?>
                                        <a href="<?php echo base_url(); ?>qa">
                                            <button type="button" class="btn btn-primary mt-0">Add Question</button>
                                        </a>
                                    <?php } elseif (!$this->session->userdata('user_id')) { ?>

                                        <a href="<?php echo base_url(); ?>signin">
                                            <button type="button" class="btn btn-primary mt-0">Add your
                                                Question
                                            </button>
                                        </a>

                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/61408ec9d326717cb6816277/1ffi2rhte';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->


<?php } ?>
     