<!-- Breadcrumb -->
			<div class="breadcrumb-bar">
				<div class="container-fluid">
					<div class="row align-items-center">
						<div class="col-md-12 col-12">
							<nav aria-label="breadcrumb" class="page-breadcrumb">
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="<?php echo base_url();?>dashboard"><?php echo $language['lg_dashboard'];?></a></li>
									<li class="breadcrumb-item active" aria-current="page"><?php echo $language['lg_appointments'];?></li>
								</ol>
							</nav>
							<h2 class="breadcrumb-title"><?php echo $language['lg_appointments'];?></h2>
						</div>
					</div>
				</div>
			</div>
			<!-- /Breadcrumb -->
			
			<!-- Page Content -->
			<div class="content">
				<div class="container-fluid">

					<div class="row">
						<div class="col-md-5 col-lg-4 col-xl-3 theiaStickySidebar">
						
							<!-- Profile Sidebar -->
							<?php $this->load->view('web/includes/doctor_sidebar');	?>
							<!-- /Profile Sidebar -->
							
						</div>
						
						<div class="col-md-7 col-lg-8 col-xl-9">
                            <!-- Test -->
                            <?php $appointment_count = 0; ?>
                            <?php if($appointment_count >0){?>
							<input type="hidden" name="page" id="page_no_hidden" value="1" >
							<div class="appointments" id="appointment-list">
							
							</div>

							<div class="load-more text-center d-none" id="load_more_btn">
									<a class="btn btn-primary btn-sm" href="javascript:void(0);"><?php echo $language['lg_load_more'];?></a>	
							</div>
                            <?php }else {?>
                                <div class="d-flex flex-column w-100 h-100 justify-content-center align-items-center">
                                    <div class="p-5 mb-4">
                                        <img src="<?php echo base_url('/assets/img/no_appointments.png')?>" alt="" class="img-responsive">
                                    </div>

                                    <h4 style="color: #979797;">No Appointments Found</h4>
                                    <a class="btn btn-primary btn-big mt-3" href="javascript:void(0);">Book Now</a>
                                </div>

                            <?php } ?>
						</div>
					</div>

				</div>

			</div>		
			<!-- /Page Content -->