<!-- Breadcrumb -->
<div class="breadcrumb-bar">
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col-md-12 col-12">
                <nav aria-label="breadcrumb" class="page-breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>dashboard"><?php echo $language['lg_dashboard']; ?></a></li>
                        <li class="breadcrumb-item active" aria-current="page"><?php echo $language['lg_schedule_timing']; ?></li>
                    </ol>
                </nav>
                <h2 class="breadcrumb-title"><?php echo $language['lg_schedule_timing']; ?></h2>
            </div>
        </div>
    </div>
</div>
<!-- /Breadcrumb -->

<!-- Page Content -->
<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-5 col-lg-4 col-xl-3 theiaStickySidebar">

                <!-- Profile Sidebar -->
                <?php $this->load->view('web/includes/doctor_sidebar'); ?>
                <!-- /Profile Sidebar -->

            </div>

            <div class="col-md-7 col-lg-8 col-xl-9">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title"><?php echo $language['lg_schedule_timing']; ?></h4>
                                <div class="track-dates-container">
                                    <h3 class="text-center"><?php echo date('F ,Y'); ?></h3>
                                    <div class="d-flex" id="scheduling-dates">
                                        <div class="date-block" id="scheduling-dates-prev">
                                            <span class="fas fa-chevron-left"></span>
                                        </div>
                                        <div id="scheduling-dates-wrapper">
                                            <div class="schedule-dates-track">

                                            </div>
                                        </div>
                                        <div class="date-block" id="scheduling-dates-next">
                                            <span class="fas fa-chevron-right"></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="profile-box">
                                    <form id="schedule_form" name="schedule_form" method="post" autocomplete="off">
                                        <div class="hours-info">
                                            <div class="row form-row hours-cont">
                                                <div class="col-12 col-md-10">
                                                    <div class="row">
                                                        <div class="col-lg-4">
                                                            <div class="form-group">
                                                                <label><?php echo $language['lg_timing_slot_dur']; ?></label>
                                                                <select class="select form-control" id="slots">
                                                                    <option value=""><?php echo $language['lg_select8']; ?></option>
                                                                    <option value="5" <?php echo ($slots['slot'] == '5') ? 'selected' : ''; ?>>5 mins</option>
                                                                    <option value="10" <?php echo ($slots['slot'] == '10') ? 'selected' : ''; ?>>10 mins</option>
                                                                    <option value="15" <?php echo ($slots['slot'] == '15') ? 'selected' : ''; ?>>15 mins</option>
                                                                    <option value="20" <?php echo ($slots['slot'] == '20') ? 'selected' : ''; ?>>20 mins</option>
                                                                    <option value="30" <?php echo ($slots['slot'] == '30') ? 'selected' : ''; ?>>30 mins</option>
                                                                    <option value="45" <?php echo ($slots['slot'] == '45') ? 'selected' : ''; ?>>45 mins</option>
                                                                    <option value="1" <?php echo ($slots['slot'] == '1') ? 'selected' : ''; ?>>1 Hour</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="row form-row">
                                                        <input type="hidden" name="slot" id="slot">
                                                        <input type="hidden" name="day_id" id="day_id" value="1">
                                                        <input type="hidden" name="day_name" id="day_name" value="Sunday">
                                                        <input type="hidden" name="id_value" id="id_value" value="">
                                                        <div class="col-12">
                                                            <label><?php echo html_escape('Time'); ?></label>
                                                        </div>

                                                        <div class="col-12 col-md-6">
                                                            <div class="form-group">
                                                                <select class="form-control" id="from_time" name="from_time" onchange="get_to_time()">
                                                                    <option value=""><?php echo $language['lg_select_time']; ?></option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-12 col-md-6">
                                                            <div class="form-group">
                                                                <select class="form-control" id="to_time" name="to_time">
                                                                    <option value=""><?php echo $language['lg_select_time']; ?></option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <label>Repeat</label>
                                                        </div>

                                                        <div class="col-lg-4">
                                                            <div class="form-group">

                                                                <select class="select form-control" id="slots">
                                                                    <option value=""></option>
                                                                    <option value="5">5 mins</option>
                                                                    <option value="10">10 mins</option>
                                                                    <option value="15" <?php echo ($slots['slot'] == '15') ? 'selected' : ''; ?>>15 mins</option>
                                                                    <option value="20" <?php echo ($slots['slot'] == '20') ? 'selected' : ''; ?>>20 mins</option>
                                                                    <option value="30" <?php echo ($slots['slot'] == '30') ? 'selected' : ''; ?>>30 mins</option>
                                                                    <option value="45" <?php echo ($slots['slot'] == '45') ? 'selected' : ''; ?>>45 mins</option>
                                                                    <option value="1" <?php echo ($slots['slot'] == '1') ? 'selected' : ''; ?>>1 Hour</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-12 col-md-6">
                                                            <div class="form-group">
                                                                <label class="d-block mb-3"><?php echo $language['lg_booking_system']; ?></label>
                                                                <div class="custom-control custom-radio custom-control-inline">
                                                                    <input type="radio" id="online" value="online" name="type" class="custom-control-input">
                                                                    <label class="custom-control-label" for="online"> <?php echo $language['lg_online']; ?></label>
                                                                </div>
                                                                <div class="custom-control custom-radio custom-control-inline">
                                                                    <input type="radio" id="clinic" value="clinic" name="type" class="custom-control-input">
                                                                    <label class="custom-control-label" for="clinic"><?php echo $language['lg_clinic']; ?></label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="submit-section text-center">
                                            <button type="submit" id="submit_btn" class="btn btn-primary submit-btn"><?php echo $language['lg_add10']; ?></button>
                                        </div>
                                    </form>
                                    <!--<div class="row">
                                                    <div class="col-md-12">
                                                        <div class="card schedule-widget mb-0">

                                                             // Schedule Header
                                                            <div class="schedule-header">

                                                                 // Schedule Nav
                                                                <div class="schedule-nav timingsnav">
                                                                    <ul class="nav nav-tabs nav-justified">
                                                                        <li class="nav-item">
                                                                            <a class="nav-link active" data-toggle="tab" id="sunday" data-day-value="1" data-append-value="sunday" href="#slot_sunday">Sunday</a>
                                                                        </li>
                                                                        <li class="nav-item">
                                                                            <a class="nav-link" data-toggle="tab" id="monday" data-day-value="2" data-append-value="monday" href="#slot_monday">Monday</a>
                                                                        </li>
                                                                        <li class="nav-item">
                                                                            <a class="nav-link" data-toggle="tab" id="tuesday" data-day-value="3" data-append-value="tuesday" href="#slot_tuesday">Tuesday</a>
                                                                        </li>
                                                                        <li class="nav-item">
                                                                            <a class="nav-link" data-toggle="tab" id="wednesday" data-day-value="4" data-append-value="wednesday" href="#slot_wednesday">Wednesday</a>
                                                                        </li>
                                                                        <li class="nav-item">
                                                                            <a class="nav-link" data-toggle="tab" id="thursday" data-day-value="5" data-append-value="thursday" href="#slot_thursday">Thursday</a>
                                                                        </li>
                                                                        <li class="nav-item">
                                                                            <a class="nav-link" data-toggle="tab" id="friday" data-day-value="6" data-append-value="friday" href="#slot_friday">Friday</a>
                                                                        </li>
                                                                        <li class="nav-item">
                                                                            <a class="nav-link" data-toggle="tab" id="saturday" data-day-value="7" data-append-value="saturday" href="#slot_saturday">Saturday</a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                 /Schedule Nav

                                                            </div>
                                                             /Schedule Header

                                                             Schedule Content
                                                            <div class="tab-content schedule-cont">

                                                                 Sunday Slot
                                                                <div id="slot_sunday" class="tab-pane fade show active"></div>
                                                                 /Sunday Slot

                                                                 Monday Slot
                                                                <div id="slot_monday" class="tab-pane fade"></div>
                                                                 /Monday Slot

                                                                 Tuesday Slot
                                                                <div id="slot_tuesday" class="tab-pane fade"></div>
                                                                 /Tuesday Slot

                                                                 Wednesday Slot
                                                                <div id="slot_wednesday" class="tab-pane fade"></div>
                                                                 /Wednesday Slot

                                                                 Thursday Slot
                                                                <div id="slot_thursday" class="tab-pane fade"></div>
                                                                 /Thursday Slot

                                                                 Friday Slot
                                                                <div id="slot_friday" class="tab-pane fade"></div>
                                                                 /Friday Slot

                                                                 Saturday Slot
                                                                <div id="slot_saturday" class="tab-pane fade"></div>
                                                                 /Saturday Slot

                                                            </div>
                                                             /Schedule Content

                                                        </div>
                                                    </div>
                                                </div>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>

</div>
<!-- /Page Content -->