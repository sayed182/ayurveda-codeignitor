<!-- Breadcrumb -->
<div class="breadcrumb-bar">
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col-md-12 col-12">
                <nav aria-label="breadcrumb" class="page-breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a
                                    href="<?php echo base_url(); ?>"><?php echo $language['lg_home']; ?></a></li>
                        <li class="breadcrumb-item active"
                            aria-current="page"><?php echo $language['lg_dashboard']; ?></li>
                    </ol>
                </nav>
                <h2 class="breadcrumb-title"><?php echo $language['lg_dashboard']; ?></h2>
            </div>
        </div>
    </div>
</div>
<!-- /Breadcrumb -->

<!-- Page Content -->
<div class="content">
    <div class="container-fluid">

        <div class="row">
            <div class="col-md-5 col-lg-4 col-xl-3 theiaStickySidebar">

                <!-- Profile Sidebar -->
                <?php
                $this->load->view('web/includes/doctor_sidebar');
                $user_detail = user_detail($this->session->userdata('user_id'));
                ?>
                <!-- /Profile Sidebar -->


            </div>

            <div class="col-md-7 col-lg-8 col-xl-9">

                <?php

                if ($user_detail['is_verified'] == '0') {?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card verify-notification">

                                <div class="col-md-2 col-sm-3">
                                                <span class="fa-envelope-check">
                                                    <svg width="74" height="58" viewBox="0 0 74 58" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
<path d="M32.6966 30.5802C32.1127 30.9451 31.4559 31.0911 30.872 31.0911C30.2882 31.0911 29.6313 30.9451 29.0474 30.5802L0 12.8452V36.4189C0 41.4547 4.08708 45.5418 9.12294 45.5418H52.6941C57.7299 45.5418 61.817 41.4547 61.817 36.4189V12.8452L32.6966 30.5802Z"
      fill="#60A2E8"/>
<path d="M52.6946 0H9.12348C4.81746 0 1.16828 3.06531 0.29248 7.15238L30.9455 25.8362L61.5256 7.15238C60.6498 3.06531 57.0007 0 52.6946 0Z"
      fill="#60A2E8"/>
<circle cx="58.9093" cy="42.4806" r="14.0909" fill="#3DAC7B" stroke="#F2F9FF" stroke-width="2"/>
<path d="M55.418 42.2622L58.0362 44.8804L62.618 40.2986" stroke="white" stroke-width="2" stroke-linecap="round"/>
</svg>
                                                </span>
                                </div>
                                <div class="col-md-7">
                                    <h4>Verify your Email/Mobile No.</h4>
                                </div>
                                <div class="col-md-3">
                                    <button class="btn btn-block verify-notification__btn" data-toggle="modal" data-target="verifyModal">Verify Now</button>
                                </div>


                            </div>
                        </div>
                    </div>
                <?php }?>

                <div class="row">

                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-4 col-sm-12 d-flex justify-content-between">
                                <h4 class="text-uppercase">PROFILE COMPLETED</h4>
                                <h4 style="color: #3c763d;"> 70% </h4>
                            </div>
                            <div class="col-md-8 col-sm-12">
                                <div class="profile_progress">
                                    <span class=""><span class="profile_progress-bar completed"></span><a class="far fa-check-circle"></a></span>
                                    <span><span class="profile_progress-bar completed"></span><a class="far fa-check-circle"></a></span>
                                    <span><span class="profile_progress-bar completed"></span><a class="far fa-check-circle"></a></span>
                                    <span><span class="profile_progress-bar"></span><a class="far fa-check-circle"></a></span>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="card dash-card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12 col-lg-4">
                                        <div class="dash-widget dct-border-rht">
                                            <div class="circle-bar circle-bar1">
                                                <div class="circle-graph1" data-percent="75">
                                                    <img src="<?php echo base_url(); ?>assets/img/icon01.png"
                                                         class="img-fluid" alt="patient">
                                                </div>
                                            </div>
                                            <div class="dash-widget-info">
                                                <h6><?php echo $language['lg_total_patient']; ?></h6>
                                                <h3><?php echo $total_patient; ?></h3>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-lg-4">
                                        <div class="dash-widget dct-border-rht">
                                            <div class="circle-bar circle-bar2">
                                                <div class="circle-graph2" data-percent="65">
                                                    <img src="<?php echo base_url(); ?>assets/img/icon02.png"
                                                         class="img-fluid" alt="Patient">
                                                </div>
                                            </div>
                                            <div class="dash-widget-info">
                                                <h6><?php echo $language['lg_today_patient']; ?></h6>
                                                <h3><?php echo $today_patient; ?></h3>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-lg-4">
                                        <div class="dash-widget">
                                            <div class="circle-bar circle-bar3">
                                                <div class="circle-graph3" data-percent="50">
                                                    <img src="<?php echo base_url(); ?>assets/img/icon03.png"
                                                         class="img-fluid" alt="Patient">
                                                </div>
                                            </div>
                                            <div class="dash-widget-info">
                                                <h6><?php echo $language['lg_appoinments']; ?></h6>
                                                <h3><?php echo $recent; ?></h3>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <h4 class="mb-4"><?php echo $language['lg_patient_appoinm']; ?></h4>
                        <div class="appointment-tab">

                            <!-- Appointment Tab -->
                            <ul class="nav nav-tabs nav-tabs-solid nav-tabs-rounded">
                                <li class="nav-item">
                                    <a class="nav-link active" href="#appointments" data-toggle="tab"
                                       onclick="appoinments_table(1)"><?php echo $language['lg_today']; ?></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#appointments" data-toggle="tab"
                                       onclick="appoinments_table(2)"><?php echo $language['lg_upcoming']; ?></a>
                                </li>
                            </ul>
                            <!-- /Appointment Tab -->

                            <div class="tab-content">


                                <div class="tab-pane show active" id="appointments">
                                    <div class="card card-table mb-0">
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <input type="hidden" id="type">
                                                <table id="appoinments_table"
                                                       class="table table-hover table-center mb-0">
                                                    <thead>
                                                    <tr>
                                                        <th><?php echo $language['lg_patient_name']; ?></th>
                                                        <th><?php echo $language['lg_appoinment_date']; ?></th>
                                                        <th><?php echo $language['lg_appoinment_type']; ?></th>
                                                        <th><?php echo $language['lg_action']; ?></th>

                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>

</div>

<!-- /Page Content -->
   
		