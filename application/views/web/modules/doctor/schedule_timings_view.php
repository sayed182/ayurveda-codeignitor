<?php if(!empty($available_time)){ ?>
<h4 class="card-title d-flex justify-content-between">
	<span><?php echo $language['lg_time_slots'];?></span> 
	<a class="edit-link" data-toggle="modal" onclick="add_slot();" href="javascript:void(0);"><i class="fa fa-edit mr-1"></i><?php echo $language['lg_add_slot'];?></a>
</h4>
<div class="doc-times">
  	<?php foreach ($available_time as $key => $value){ ?>
			<div class="doc-slot-list">
			<?php echo date('g:i a',strtotime($value['time_start'])); ?> - <?php echo date('g:i a',strtotime($value['time_end'])); ?> 
				<a href="javascript:void(0)" class="delete_schedule" data-delete-val="<?php echo $value['id']; ?>">
				(<?php echo $value['type']; ?>) <i class="fa fa-times"></i>
				</a>
			</div>
    <?php } ?>

</div>
<?php } else { ?>

	<h4 class="card-title d-flex justify-content-between">
		<span><?php echo $language['lg_time_slots'];?></span> 
		<a class="edit-link" data-toggle="modal" onclick="add_slot();" href="javascript:void(0);"><i class="fa fa-plus-circle"></i> <?php echo $language['lg_add_slot'];?></a>
	</h4>
	<p class="text-muted mb-0"><?php echo $language['lg_not_available'];?></p>

<?php } ?>


