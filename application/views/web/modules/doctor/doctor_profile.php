<!-- Page Content -->
<!-- Page Content -->
			<div class="content">
				<div class="container-fluid">

					<div class="row">
						<div class="col-md-5 col-lg-4 col-xl-3 theiaStickySidebar">
						
							<!-- Profile Sidebar -->
							<?php $this->load->view('web/includes/doctor_sidebar');?>
							<!-- /Profile Sidebar -->
							
						</div>
						<div class="col-md-7 col-lg-8 col-xl-9">

							<form method="post" action="#" id="doctor_profile_form" autocomplete="off">

								<input type="hidden" value="<?php echo date('d/m/Y', strtotime('-20 years')); ?>" id="maxDate">
								

							<?php

							$user_profile_image=(!empty($profile['profileimage']))?base_url().$profile['profileimage']:base_url().'assets/img/user.png';
							 ?>
						
							<!-- Basic Information -->
							<div class="card">
								<div class="card-body">
									<h4 class="card-title"><?php echo $language['lg_basic_informati'];?></h4>
									<div class="row form-row">
										<div class="col-md-12">
											<div class="form-group">
												<div class="change-avatar">
													<div class="profile-img">
														<img src="<?php echo $user_profile_image;?>" alt="User Image" class="avatar-view-img">
													</div>
													<div class="upload-img">
														<div class="change-photo-btn avatar-view-btn">
															<span><i class="fa fa-upload"></i> <?php echo $language['lg_upload_photo'];?></span>
															<input type="hidden" id="crop_prof_img" name="profile_image">
														</div>
														<small class="form-text text-muted"><?php echo $language['lg_allowed_jpg_gif'];?></small>
													</div>
												</div>
											</div>
										</div>
										
										
										<div class="col-md-6">
											<div class="form-group">
												<label><?php echo $language['lg_first_name'];?> <span class="text-danger">*</span></label>
												<input type="text" name="first_name" id="first_name" value="<?php echo $profile['first_name'];?>" class="form-control">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label><?php echo $language['lg_last_name'];?> <span class="text-danger">*</span></label>
												<input type="text" name="last_name" id="last_name" value="<?php echo $profile['last_name'];?>" class="form-control">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label><?php echo $language['lg_email'];?> <span class="text-danger">*</span></label>
												<input type="email"  value="<?php echo $profile['email'];?>" class="form-control" disabled>
											</div>
										</div>
										<!--  <div class="col-md-6">
										 	<div class="form-group">
												<label>Country Code<span class="text-danger">*</span></label>
                          						<select name="country_code" class="form-control" id="country_code">
                          						</select>
                          					</div>
                        				</div> -->
										<div class="col-md-6">
											<div class="form-group">
												<label><?php echo $language['lg_mobile_number'];?> <span class="text-danger">*</span></label>
												<input type="text" name="mobileno" id="mobileno" value="<?php echo $profile['mobileno'];?>" class="form-control">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label><?php echo $language['lg_select_gender'];?> <span class="text-danger">*</span></label>
												<select class="form-control select" name="gender" id="gender">
													<option value=""><?php echo $language['lg_select'];?></option>
													<option value="Male" <?php echo ($profile['gender']=='Male')?'selected':'';?>><?php echo $language['lg_male'];?></option>
													<option value="Female" <?php echo ($profile['gender']=='Female')?'selected':'';?>><?php echo $language['lg_female'];?></option>
												</select>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group mb-0">
												<label><?php echo $language['lg_date_of_birth'];?> <span class="text-danger">*</span></label>
												<input type="text" name="dob" id="dob" value="<?php echo !empty($profile['dob'])?date('d/m/Y',strtotime(str_replace('-', '/', $profile['dob']))):'';?>" class="form-control">
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- /Basic Information -->
							
							<!-- About Me -->
							<div class="card">
								<div class="card-body">
									<h4 class="card-title"><?php echo $language['lg_about_me'];?></h4>
									<div class="form-group mb-0">
										<label><?php echo $language['lg_biography'];?></label>
										<textarea class="form-control" name="biography" id="biography" rows="5"><?php echo $profile['biography'];?></textarea>
									</div>
								</div>
							</div>
							<!-- /About Me -->
							
							<!-- Clinic Info -->
							<div class="card">
								<div class="card-body">
									<h4 class="card-title"><?php echo $language['lg_clinic_info'];?></h4>
									<div class="row form-row">
										<div class="col-md-6">
											<div class="form-group">
												<label><?php echo $language['lg_clinic_name'];?></label>
												<input type="text" value="<?php echo $profile['clinic_name'];?>" name="clinic_name" id="clinic_name" class="form-control">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label><?php echo $language['lg_clinic_address'];?></label>
												<input type="text" value="<?php echo $profile['clinic_address'];?>" name="clinic_address" id="clinic_address" class="form-control">
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group">
												<label><?php echo $language['lg_clinic_images'];?></label>
												<div action="<?php echo base_url(); ?>Profile/upload_files" class="dropzone"></div>
												
											</div>
											<div class="upload-wrap">
												<?php 

							                    if(!empty($clinic_images)){
							                      $i=1;
							                      foreach ($clinic_images as $c) {
							                          echo '
													<div class="upload-images" id="clinic_'.$c['id'].'">
													<img src="'.base_url().'uploads/clinic_uploads/'.$c['user_id'].'/'.$c['clinic_image'].'" alt="">
														<a href="javascript:void(0);" class="btn btn-icon btn-danger btn-sm" onclick="delete_clinic_image('.$c['id'].')"><i class="far fa-trash-alt"></i></a>
													</div>';
	                                              }
							                    }

							                    ?>    
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- /Clinic Info -->

							<!-- Contact Details -->
							<div class="card contact-card">
								<div class="card-body">
									<h4 class="card-title"><?php echo $language['lg_contact_details'];?></h4>
									<div class="row form-row">
										<div class="col-md-6">
											<div class="form-group">
												<label><?php echo $language['lg_address_line_1'];?> <span class="text-danger">*</span></label>
												<input type="text" name="address1" id="address1" value="<?php echo $profile['address1'];?>" class="form-control">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label"><?php echo $language['lg_address_line_2'];?> <span class="text-danger">*</span></label>
												<input type="text" name="address2" id="address2" value="<?php echo $profile['address2'];?>" class="form-control">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label"><?php echo $language['lg_country'];?> <span class="text-danger">*</span></label>
												<select class="form-control select" name="country" id="country">
													<option value=""><?php echo $language['lg_select_country'];?></option>
												</select>
											</div>
										</div>

										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label"><?php echo $language['lg_state__province'];?> <span class="text-danger">*</span></label>
												<select class="form-control select" name="state" id="state">
													<option value=""><?php echo $language['lg_select_state'];?></option>
												</select>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label"><?php echo $language['lg_city'];?> <span class="text-danger">*</span></label>
												<select class="form-control select" name="city" id="city">
													<option value=""><?php echo $language['lg_select_city'];?></option>
												</select>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label"><?php echo $language['lg_postal_code'];?> <span class="text-danger">*</span></label>
												<input type="text" name="postal_code" id="postal_code" value="<?php echo $profile['postal_code'];?>" class="form-control">
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- /Contact Details -->
							
							<!-- Pricing -->
							<div class="card">
								<div class="card-body">
									<h4 class="card-title"><?php echo $language['lg_pricing'];?> <span class="text-danger">*</span></h4>
									
									<div class="form-group mb-0">
										<div id="pricing_select">
											<div class="custom-control custom-radio custom-control-inline">
												<input type="radio" id="price_type" name="price_type" class="custom-control-input" value="Free" <?php echo ($profile['price_type']=='Free')?'checked':'';?>>
												<label class="custom-control-label" for="price_type"><?php echo $language['lg_free'];?></label>
											</div>
											<div class="custom-control custom-radio custom-control-inline">
												<input type="radio" id="price_type1" name="price_type" value="Custom Price" class="custom-control-input" <?php echo ($profile['price_type']=='Custom Price')?'checked':'';?>>
												<label class="custom-control-label" for="price_type1"><?php echo $language['lg_custom_price1'];?> (<?php echo $language['lg_per_slot'];?>)</label>
											</div>
										</div>

									</div>
									
									<div class="row custom_price_cont" id="custom_price_cont" style="display: <?php echo ($profile['price_type']=='Free')?'none':'block';?>;">
										<div class="col-md-4">
											<input type="text" class="form-control" id="amount"  name="amount" value="<?php echo $profile['amount'];?>" placeholder="20">
											<small class="form-text text-muted"><?php echo $language['lg_custom_price_yo'];?></small>
										</div>
									</div>
									
								</div>
							</div>
							<!-- /Pricing -->
							
							<!-- Services and Specialization -->
							<div class="card services-card">
								<div class="card-body">
									<h4 class="card-title"><?php echo $language['lg_services_and_sp'];?></h4>
									<div class="form-group">
										<label><?php echo $language['lg_services'];?> <span class="text-danger">*</span></label>
										<input type="text" data-role="tagsinput" class="input-tags form-control" placeholder="Enter Services" name="services" value="<?php echo $profile['services'];?>" id="services">
										<small class="form-text text-muted"><?php echo $language['lg_note__type__pre'];?></small>
									</div> 
									<div class="form-group mb-0">
										<label><?php echo $language['lg_specialization1'];?> <span class="text-danger">*</span></label>
										<select class="form-control select" name="specialization" id="specialization">
													<option value=""><?php echo $language['lg_select_speciali'];?></option>
									    </select>
									</div> 
								</div>              
							</div>
							<!-- /Services and Specialization -->
						 
							<!-- Education -->
							<div class="card">
								<div class="card-body">
									<h4 class="card-title"><?php echo $language['lg_education'];?></h4>
									<div class="education-info">
										<?php
										
										if(!empty($education)){
											$i=1;
											foreach ($education as $erows) {
										echo'<div class="row form-row education-cont">
											<div class="col-12 col-md-10 col-lg-11">
												<div class="row form-row">
													<div class="col-12 col-md-6 col-lg-4">
														<div class="form-group">
															<label>'.$language['lg_degree'].' <span class="text-danger">*</span></label>
															<input type="text" name="degree[]" value="'.$erows['degree'].'" class="form-control degree">
														</div> 
													</div>
													<div class="col-12 col-md-6 col-lg-4">
														<div class="form-group">
															<label>'.$language['lg_collegeinstitut'].' <span class="text-danger">*</span></label>
															<input type="text" name="institute[]" value="'.$erows['institute'].'" class="form-control institute">
														</div> 
													</div>
													<div class="col-12 col-md-6 col-lg-4">
														<div class="form-group">
															<label>'.$language['lg_year_of_complet'].' <span class="text-danger">*</span></label>
															<input type="text" name="year_of_completion[]" value="'.$erows['year_of_completion'].'" readonly class="form-control years year_of_completion">
														</div> 
													</div>
												</div>
											</div>
                                            <div class="col-md-6">
                                                <label>Certificate</label>
                                                <input type="file" name="cer_files[]" class="form-control">
                                            </div>
											';
											if($i!=1){ 
											echo'<div class="col-12 col-md-2 col-lg-1"><label class="d-md-block d-sm-none d-none">&nbsp;</label><a href="#" class="btn btn-danger trash"><i class="far fa-trash-alt"></i></a></div>';
										   }
										   echo'</div>';
										$i++; } }
										?>
										<div class="row form-row education-cont">
											<div class="col-12 col-md-10 col-lg-11">
												<div class="row form-row">
													<div class="col-12 col-md-6 col-lg-4">
														<div class="form-group">
															<label><?php echo $language['lg_degree'];?> <span class="text-danger">*</span></label>
															<input type="text" name="degree[]" class="form-control degree">
														</div> 
													</div>
													<div class="col-12 col-md-6 col-lg-4">
														<div class="form-group">
															<label><?php echo $language['lg_collegeinstitut'];?> <span class="text-danger">*</span></label>
															<input type="text" name="institute[]" class="form-control institute">
														</div> 
													</div>
													<div class="col-12 col-md-6 col-lg-4">
														<div class="form-group">
															<label><?php echo $language['lg_year_of_complet'];?> <span class="text-danger">*</span></label>
															<input type="text" name="year_of_completion[]" readonly class="form-control years year_of_completion">
														</div> 
													</div>
												</div>

											</div>
                                            <div class="col-md-6">
                                                <label>Certificate</label>
                                                <input type="file" name="cer_files[]" class="form-control">
                                            </div>
											<?php 
												if(count($education)>=1){ 
											echo'<div class="col-12 col-md-2 col-lg-1"><label class="d-md-block d-sm-none d-none">&nbsp;</label><a href="#" class="btn btn-danger trash"><i class="far fa-trash-alt"></i></a></div>';
										   }
											 ?>
										</div>
									</div>


									<div class="add-more">
										<a href="javascript:void(0);" class="add-education"><i class="fa fa-plus-circle"></i><?php echo $language['lg_add_more'];?> </a>
									</div>
								</div>
							</div>
							<!-- /Education -->
						
							<!-- Experience -->
							<div class="card">
								<div class="card-body">
									<h4 class="card-title"><?php echo $language['lg_experience'];?></h4>
									<div class="experience-info">
										<?php
										
										if(!empty($experience)){
											$j=1;
										foreach ($experience as $exrows) {
											echo'<div class="row form-row experience-cont">
											<div class="col-12 col-md-10 col-lg-11">
												<div class="row form-row">
													<div class="col-12 col-md-6 col-lg-4">
														<div class="form-group">
															<label>'.$language['lg_hospital_name'].'</label>
															<input type="text" name="hospital_name[]" value="'.$exrows['hospital_name'].'" class="form-control">
														</div> 
													</div>
													<div class="col-12 col-md-6 col-lg-4">
														<div class="form-group">
															<label>'.$language['lg_from'].'From</label>
															<input type="text" name="from[]" id="from" value="'.$exrows['from'].'" readonly class="form-control years">
														</div> 
													</div>
													<div class="col-12 col-md-6 col-lg-4">
														<div class="form-group">
															<label>'.$language['lg_to3'].'</label>
															<input type="text" name="to[]" id="to" value="'.$exrows['to'].'" readonly class="form-control years">
														</div> 
													</div>
													<div class="col-12 col-md-6 col-lg-4">
														<div class="form-group">
															<label>'.$language['lg_designation'].'</label>
															<input type="text" name="designation[]" value="'.$exrows['designation'].'" class="form-control">
														</div> 
													</div>
												</div>
											</div>';
											if($j!=1){ 
											echo'<div class="col-12 col-md-2 col-lg-1"><label class="d-md-block d-sm-none d-none">&nbsp;</label><a href="#" class="btn btn-danger trash"><i class="far fa-trash-alt"></i></a></div>';
										   }
										   echo'</div>';
										 $j++; } } ?>
											<div class="row form-row experience-cont">
											<div class="col-12 col-md-10 col-lg-11">
												<div class="row form-row">
													<div class="col-12 col-md-6 col-lg-4">
														<div class="form-group">
															<label><?php echo $language['lg_hospital_name'];?></label>
															<input type="text" name="hospital_name[]" class="form-control">
														</div> 
													</div>
													<div class="col-12 col-md-6 col-lg-4">
														<div class="form-group">
															<label><?php echo $language['from'];?>From</label>
															<input type="text" name="from[]" id="from" readonly class="form-control years">
														</div> 
													</div>
													<div class="col-12 col-md-6 col-lg-4">
														<div class="form-group">
															<label><?php echo $language['lg_to3'];?></label>
															<input type="text" name="to[]" id="to" readonly class="form-control years">
														</div> 
													</div>
													<div class="col-12 col-md-6 col-lg-4">
														<div class="form-group">
															<label><?php echo $language['lg_designation'];?></label>
															<input type="text" name="designation[]" class="form-control">
														</div> 
													</div>
												</div>
											</div>
											<?php 
												if(count($experience)>=1){ 
											echo'<div class="col-12 col-md-2 col-lg-1"><label class="d-md-block d-sm-none d-none">&nbsp;</label><a href="#" class="btn btn-danger trash"><i class="far fa-trash-alt"></i></a></div>';
										   }
											 ?>
											 <input type="hidden" id="experience_count" value="<?php echo count($experience);?>">
										</div>
									</div>
									<div class="add-more">
										<a href="javascript:void(0);" class="add-experience"><i class="fa fa-plus-circle"></i><?php echo $language['lg_add_more'];?></a>
									</div>
								</div>
							</div>
							<!-- /Experience -->
							
							<!-- Awards -->
							<div class="card">
								<div class="card-body">
									<h4 class="card-title"><?php echo $language['lg_awards'];?></h4>
									<div class="awards-info">
										<?php
										if(!empty($awards)){
											$k=1;
										foreach ($awards as $arows) {
										echo'<div class="row form-row awards-cont">
											<div class="col-12 col-md-5">
												<div class="form-group">
													<label>'.$language['lg_awards'].'</label>
													<input type="text" name="awards[]" value="'.$arows['awards'].'" class="form-control">
												</div> 
											</div>
											<div class="col-12 col-md-5">
												<div class="form-group">
													<label>'.$language['lg_year'].'</label>
													<input type="text" name="awards_year[]" value="'.$arows['awards_year'].'" readonly class="form-control years">
												</div> 
											</div>';
											if($k!=1){ 
											echo'<div class="col-12 col-md-2 col-lg-1"><label class="d-md-block d-sm-none d-none">&nbsp;</label><a href="#" class="btn btn-danger trash"><i class="far fa-trash-alt"></i></a></div>';
										   }
										   echo'</div>';
										 $k++; } } ?>
										<div class="row form-row awards-cont">
											<div class="col-12 col-md-5">
												<div class="form-group">
													<label><?php echo $language['lg_awards'];?></label>
													<input type="text" name="awards[]" class="form-control">
												</div> 
											</div>
											<div class="col-12 col-md-5">
												<div class="form-group">
													<label><?php echo $language['lg_year'];?></label>
													<input type="text" name="awards_year[]" readonly class="form-control years">
												</div> 
											</div>
											<?php 
												if(count($awards)>=1){ 
											echo'<div class="col-12 col-md-2 col-lg-1"><label class="d-md-block d-sm-none d-none">&nbsp;</label><a href="#" class="btn btn-danger trash"><i class="far fa-trash-alt"></i></a></div>';
										   }
											 ?>
										</div>
									</div>
									<div class="add-more">
										<a href="javascript:void(0);" class="add-award"><i class="fa fa-plus-circle"></i><?php echo $language['lg_add_more'];?></a>
									</div>
								</div>
							</div>
							<!-- /Awards -->
							
							<!-- Memberships -->
							<div class="card">
								<div class="card-body">
									<h4 class="card-title"><?php echo $language['lg_memberships'];?></h4>
									<div class="membership-info">
										<?php
										
										if(!empty($memberships)){
											$l=1;
										foreach ($memberships as $mrows) {
										echo'<div class="row form-row membership-cont">
											<div class="col-12 col-md-10 col-lg-5">
												<div class="form-group">
													<label>'.$language['lg_memberships'].'</label>
													<input type="text" name="memberships[]" value="'.$mrows['memberships'].'" class="form-control">
												</div> 
											</div>';
											if($l!=1){ 
											echo'<div class="col-12 col-md-2 col-lg-1"><label class="d-md-block d-sm-none d-none">&nbsp;</label><a href="#" class="btn btn-danger trash"><i class="far fa-trash-alt"></i></a></div>';
										   }
										   echo'</div>';
										 $l++; } } ?>
										<div class="row form-row membership-cont">
											<div class="col-12 col-md-10 col-lg-5">
												<div class="form-group">
													<label><?php echo $language['lg_memberships'];?></label>
													<input type="text" name="memberships[]" class="form-control">
												</div> 
											</div>
											<?php 
												if(count($memberships)>=1){ 
											echo'<div class="col-12 col-md-2 col-lg-1"><label class="d-md-block d-sm-none d-none">&nbsp;</label><a href="#" class="btn btn-danger trash"><i class="far fa-trash-alt"></i></a></div>';
										   }
											 ?>
										</div>
									</div>
									<div class="add-more">
										<a href="javascript:void(0);" class="add-membership"><i class="fa fa-plus-circle"></i><?php echo $language['lg_add_more'];?> </a>
									</div>
								</div>
							</div>
							<!-- /Memberships -->
							
							<!-- Registrations -->
							<div class="card">
								<div class="card-body">
									<h4 class="card-title"><?php echo $language['lg_registrations'];?></h4>
									<div class="registrations-info">
										<?php
										if(!empty($registrations)){
											$m=1;
										foreach ($registrations as $rrows) {
											echo'<div class="row form-row reg-cont">
											<div class="col-12 col-md-5">
												<div class="form-group">
													<label>'.$language['lg_registrations'].'</label>
													<input type="text" name="registrations[]" value="'.$rrows['registrations'].'" class="form-control">
												</div> 
											</div>
											<div class="col-12 col-md-5">
												<div class="form-group">
													<label>'.$language['lg_year'].'</label>
													<input type="text" readonly name="registrations_year[]" value="'.$rrows['registrations_year'].'" class="form-control years">
												</div> 
											</div>';
											if($m!=1){ 
											echo'<div class="col-12 col-md-2 col-lg-1"><label class="d-md-block d-sm-none d-none">&nbsp;</label><a href="#" class="btn btn-danger trash"><i class="far fa-trash-alt"></i></a></div>';
										   }
										   echo'</div>';
										 } } ?>
										<div class="row form-row reg-cont">
											<div class="col-12 col-md-5">
												<div class="form-group">
													<label><?php echo $language['lg_registrations'];?></label>
													<input type="text" name="registrations[]" class="form-control">
												</div> 
											</div>
											<div class="col-12 col-md-5">
												<div class="form-group">
													<label><?php echo $language['lg_year'];?></label>
													<input type="text" readonly name="registrations_year[]" class="form-control years">
												</div> 
											</div>
											<?php 
												if(count($registrations)>=1){ 
											echo'<div class="col-12 col-md-2 col-lg-1"><label class="d-md-block d-sm-none d-none">&nbsp;</label><a href="#" class="btn btn-danger trash"><i class="far fa-trash-alt"></i></a></div>';
										   }
											 ?>
										</div>
									</div>
									<div class="add-more">
										<a href="javascript:void(0);" class="add-reg"><i class="fa fa-plus-circle"></i><?php echo $language['lg_add_more'];?> </a>
									</div>
								</div>
							</div>
							<!-- /Registrations -->

							<!-- Documents -->
							<div class="card">
								<div class="card-body">
									<h4 class="card-title">Documents</h4>
									<div class="projects-info">
										<?php
										$i=1;
										if(!empty($document)){
											foreach ($document as $documents) { ?>

												<a href="<?php echo base_url().$documents['doc_file'] ?>" target="_blank"><?php echo $i.'. '.ucwords($documents['doc_name']); ?></a> 
												<a href="javascript:;" style="color: red"><i class="far fa-trash-alt doc_delete" data-id="<?php echo $documents['id'] ?>"></i></a> 
												<br>

										<?php
											$i++;
											}
										}
										?>
									</div>
									<div class="document-info" style="margin-bottom: 15px;margin-top: 15px;">
				                        <div class="row document-cont">
				                          <div class="col-md-9">
				                            <label>Document Name</label>
				                            <input type="text" name="doc_name[]" class="form-control">
				                          </div>
				                          <div class="col-md-9">
				                            <label>Document</label>
				                            <input type="file" name="doc_files[]" class="form-control">
				                          </div>
				                        </div>
				                    </div>

			                      	<div class="add-more" style="margin-bottom: 15px;">
			                          <a href="javascript:void(0);" class="add-document"><i class="fa fa-plus-circle"></i><?php echo $language['lg_add_more'];?> </a>
			                      	</div>
								</div>
							</div>
							<!-- /Documents -->

							<!-- Business Hours-->
              <div class="card">
              <div class="card-body">
                <h4 class="card-title"><?php echo $language['lg_business_hours'];?></h4>
                  
                  <div class="registrations_ct_01">
                     <div class="table-responsice">
                      <table class="table availability-table">
                        <tbody>
                          <?php

                          $business_hours_details = (!empty($business_hours['business_hours']))?$business_hours['business_hours']:'';

                if(!empty($business_hours_details)){
                  $business_hours_details = json_decode($business_hours_details,true);

                

                }
                          

                    $days = array();
                    $Monday_from_time = '';
                    $Monday_to_time = '';
                    $Tuesday_from_time = '';
                    $Tuesday_to_time = '';
                    $Wednesday_from_time = '';
                    $Wednesday_to_time = '';
                    $Thursday_from_time = '';
                    $Thursday_to_time = '';
                    $Friday_from_time = '';
                    $Friday_to_time = '';
                    $Saturday_from_time = '';
                    $Saturday_to_time = '';
                    $Sunday_from_time = '';
                    $Sunday_to_time = '';
                    $Monday_checked = '';
                    $Tuesday_checked = '';
                    $Wednesday_checked = '';
                    $Thursday_checked = '';
                    $Friday_checked = '';
                    $Saturday_checked = '';
                    $Sunday_checked = '';
                    foreach ($business_hours_details as $availability) {
                      $days[] = $availability['day'];
                      if($availability['day'] == 1){
                        $Monday = '';
                        $Monday_checked = 'checked';
                        $Monday_from_time = $availability['from_time'];
                        $Monday_to_time = $availability['to_time'];
                      }
                      if($availability['day'] == 2){
                        $Tuesday = '';
                        $Tuesday_checked = 'checked';
                        $Tuesday_from_time = $availability['from_time'];
                        $Tuesday_to_time = $availability['to_time'];
                      }
                      if($availability['day'] == 3){
                        $Wednesday = '';
                        $Wednesday_checked = 'checked';
                        $Wednesday_from_time = $availability['from_time'];
                        $Wednesday_to_time = $availability['to_time'];
                      }
                      if($availability['day'] == 4){
                        $Thursday = '';
                        $Thursday_checked = 'checked';
                        $Thursday_from_time = $availability['from_time'];
                        $Thursday_to_time = $availability['to_time'];
                      }
                      if($availability['day'] == 5){
                        $Friday = '';
                        $Friday_checked = 'checked';
                        $Friday_from_time = $availability['from_time'];
                        $Friday_to_time = $availability['to_time'];
                      }
                      if($availability['day'] == 6){
                        $Saturday = '';
                        $Saturday_checked = 'checked';
                        $Saturday_from_time = $availability['from_time'];
                        $Saturday_to_time = $availability['to_time'];
                      }
                      if($availability['day'] == 7){
                        $Sunday = '';
                        $Sunday_checked = 'checked';
                        $Sunday_from_time = $availability['from_time'];
                        $Sunday_to_time = $availability['to_time'];
                      }
                    }
                    $day_count = count($days);
                    $checked = '';
                    $Monday = '';
                    $Tuesday = '';
                    $Wednesday = '';
                    $Thursday = '';
                    $Friday = '';
                    $Saturday = '';
                    $Sunday = '';
                    if($day_count == 7)
                    {
                      $checked = 'checked';
                      $Monday = 'disabled';
                      $Tuesday = 'disabled';
                      $Wednesday = 'disabled';
                      $Thursday = 'disabled';
                      $Friday = 'disabled';
                      $Saturday = 'disabled';
                      $Sunday = 'disabled';

                      $All_from_time = $Monday_from_time;
                      $All_to_time = $Monday_to_time;
                      $Monday_checked = '';
                      $Tuesday_checked = '';
                      $Wednesday_checked = '';
                      $Thursday_checked = '';
                      $Friday_checked = '';
                      $Saturday_checked = '';
                      $Sunday_checked = '';
                    }
                    else {
                      $checked = '';
                      $All_from_time = '-';
                      $All_to_time = '-';
                    }
                         ?>
                  <tr>
                    <td>
                      <input type="checkbox" class="days_check" name="availability[0][day]" value="1" <?php echo $checked; ?>> <?php echo $language['lg_all_days'];?>
                    </td>
                    <td style="width:180px;">
                      <?php echo $language['lg_from'];?> <span class="time-select">
                        <select class="form-control daysfromtime_check"  name="availability[0][from_time]">
                          <?php echo from_time(1, $All_from_time); ?>
                        </select></span>
                    </td>
                    <td style="width:155px;">
                      <?php echo $language['lg_to3'];?> <span class="time-select">
                        <select class="form-control daystotime_check" name="availability[0][to_time]">
                          <?php echo to_time(1, $All_to_time); ?>
                        </select></span>
                    </td>
                  </tr>
          <?php
            $day_name = array();
            $day_name[1] = 'Monday';
            $day_name[2] = 'Tuesday';
            $day_name[3] = 'Wednesday';
            $day_name[4] = 'Thursday';
            $day_name[5] = 'Friday';
            $day_name[6] = 'Saturday';
            $day_name[7] = 'Sunday';

            $day_name_checked = array();
            $day_name_checked[1] = 'Monday_checked';
            $day_name_checked[2] = 'Tuesday_checked';
            $day_name_checked[3] = 'Wednesday_checked';
            $day_name_checked[4] = 'Thursday_checked';
            $day_name_checked[5] = 'Friday_checked';
            $day_name_checked[6] = 'Saturday_checked';
            $day_name_checked[7] = 'Sunday_checked';

            $var_from_time = array();
            $var_from_time[1] = 'Monday_from_time';
            $var_from_time[2] = 'Tuesday_from_time';
            $var_from_time[3] = 'Wednesday_from_time';
            $var_from_time[4] = 'Thursday_from_time';
            $var_from_time[5] = 'Friday_from_time';
            $var_from_time[6] = 'Saturday_from_time';
            $var_from_time[7] = 'Sunday_from_time';

            $var_to_time = array();
            $var_to_time[1] = 'Monday_to_time';
            $var_to_time[2] = 'Tuesday_to_time';
            $var_to_time[3] = 'Wednesday_to_time';
            $var_to_time[4] = 'Thursday_to_time';
            $var_to_time[5] = 'Friday_to_time';
            $var_to_time[6] = 'Saturday_to_time';
            $var_to_time[7] = 'Sunday_to_time';
             for ($i=1; $i <= 7; $i++) {  ?>
            <tr>
              <td>
                <input type="checkbox" class="eachdays eachdays<?php echo $i; ?>"  name="availability[<?php echo $i; ?>][day]" value="<?php echo $i; ?>" <?php echo $$day_name_checked[$i]; ?> <?php echo $$day_name[$i]; ?>> <?php echo $day_name[$i]; ?>
              </td>
              <td style="width:180px;">
                <?php echo $language['lg_from'];?> <span class="time-select">
                  <select class="form-control eachdayfromtime  eachdayfromtime<?php echo $i; ?>" name="availability[<?php echo $i; ?>][from_time]" <?php echo $$day_name[$i]; ?>>
                    <?php echo from_time(2, $$var_from_time[$i]); ?>
                  </select></span>
              </td>
              <td style="width:155px;">
                <?php echo $language['lg_to3'];?> <span class="time-select" >
                  <select class="form-control  eachdaytotime eachdaytotime<?php echo $i; ?>" name="availability[<?php echo $i; ?>][to_time]" <?php echo $$day_name[$i]; ?>>
                    <?php echo to_time(1, $$var_to_time[$i]); ?>
                  </select>
                </span>
              </td>
            </tr>
        <?php }

          function from_time($selected='', $my_from_time)
          {
            if($my_from_time == '-'){
              $time_from = '<option value="" selected>-</option>';
            }
            else{
              $time_from = '<option value="">-</option>';
            }
            for ($j=0; $j <=23 ; $j++) {
              if($j <10){
                $k = '0'.$j;
              }else{
                $k = $j;
              }
              if($j <12){
                if($j <10){
                  $label1 = '0'.$j.':00 AM';
                  $label2 = '0'.$j.':30 AM';
                }else{
                  $label1 = $j.':00 AM';
                  $label2 = $j.':30 AM';
                }

              }else{
                $s = ($j-12);

                if($s ==0){ $s  = 12; }

                if($s <10){
                  $label1 = '0'.$s.':00 PM';
                  $label2 = '0'.$s.':30 PM';
                }else{
                  $label1 = $s.':00 PM';
                  $label2 = $s.':30 PM';
                }
              }
              if($my_from_time == $label1){
                $time_from .= '<option value="'.$label1.'" selected>'.$label1.'</option>';
              }else{
                $time_from .= '<option value="'.$label1.'">'.$label1.'</option>';
              }

              if($my_from_time == $label2){
                $time_from .= '<option value="'.$label2.'" selected>'.$label2.'</option>';
              }else{
                $time_from .= '<option value="'.$label2.'">'.$label2.'</option>';
              }
            }
            return $time_from;
          }
          function to_time($selected='', $my_to_time)
          {
            if($my_to_time == '-'){
              $to_time = '<option value="" selected>-</option>';
            }
            else{
              $to_time = '<option value="">-</option>';
            }
            for ($j=0; $j <=23 ; $j++) {

              if($j <10){
                $k = '0'.$j;
              }else{
                $k = $j;
              }

              if($j <12){
                if($j <10){
                  $label1 = '0'.$j.':00 AM';
                  $label2 = '0'.$j.':30 AM';
                }else{
                  $label1 = $j.':00 AM';
                  $label2 = $j.':30 AM';
                }

              }else{
                $s = ($j-12);

                if($s ==0){ $s  = 12; }

                if($s <10){
                  $label1 = '0'.$s.':00 PM';
                  $label2 = '0'.$s.':30 PM';
                }else{
                  $label1 = $s.':00 PM';
                  $label2 = $s.':30 PM';
                }
              }
              if($my_to_time == $label1){
                $to_time .= '<option value="'.$label1.'" selected>'.$label1.'</option>';
              }else{
                $to_time .= '<option value="'.$label1.'">'.$label1.'</option>';
              }

              if($my_to_time == $label2){
                $to_time .= '<option value="'.$label2.'" selected>'.$label2.'</option>';
              }else{
                $to_time .= '<option value="'.$label2.'">'.$label2.'</option>';
              }
            }
          return $to_time;
          }
        ?>


                        </tbody>
                      </table>
                    </div>                   
                     
                   </div> 
              </div>              
              </div>              
              <!-- Business Hours -->
							
							<div class="submit-section submit-btn-bottom">
								<button type="submit" id="save_btn" class="btn btn-primary submit-btn"><?php echo $language['lg_save_changes'];?></button>
							</div>
							</form>
						</div>
					</div>

				</div>

			</div>		
			<!-- /Page Content -->



			<script type="text/javascript">
				var country='<?php echo $profile['country'];?>';
			    var state='<?php echo $profile['state'];?>';
			    var city='<?php echo $profile['city'];?>';
			    var specialization='<?php echo $profile['specialization'];?>';
			    var country_code='<?php echo $profile['country_code'];?>';
			</script>