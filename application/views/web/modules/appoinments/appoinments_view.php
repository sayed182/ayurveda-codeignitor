<?php 
if(!empty($appoinments_list)) { 



  foreach ($appoinments_list as $rows) { 

             if($rows['payment_method']=='Pay on Arrive') {
                $hourly_rate='Pay on Arrive';
              }
              else {
                $hourly_rate=!empty($rows['per_hour_charge'])?$rows['per_hour_charge']:'Free';
              }

              $current_timezone = $rows['time_zone'];               
              $old_timezone = $this->session->userdata('time_zone');               
              $invite_date  = converToTz($rows['appointment_date'].' '.$rows['appointment_time'],$old_timezone,$current_timezone);
              $invite_to  = converToTz($rows['appointment_date'].' '.$rows['appointment_end_time'],$old_timezone,$current_timezone);     
              $invite_to_end_time =  date('Y-m-d H:i:s', strtotime($invite_to));
              $invite_date = date('Y-m-d H:i:s', strtotime($invite_date));

              

            $appointment_date=date('d M Y',strtotime(converToTz($rows['appointment_date'],$old_timezone,$current_timezone)));
            $appointment_time=date('h:i A',strtotime(converToTz($rows['from_date_time'],$old_timezone,$current_timezone)));
            $appointment_end_time=date('h:i A',strtotime(converToTz($rows['to_date_time'],$old_timezone,$current_timezone)));
            $created_date=date('d M Y',strtotime(converToTz($rows['created_date'],$old_timezone,$current_timezone)));
            $hourly_rate=$hourly_rate;
            $type=$rows['type'];

            // Declare and define two dates 
     

      $start_date = new DateTime(date('Y-m-d H:i:s'));
      $start_diff = $start_date->diff(new DateTime($invite_date));

      $start_days= str_replace('+', '', $start_diff->format('%R%d'));
      $start_hours= str_replace('+', '', $start_diff->format('%R%h'));
      $start_minutes= str_replace('+', '', $start_diff->format('%R%i'));
      $start_seconds= str_replace('+', '', $start_diff->format('%R%s'));


      $end_date = new DateTime(date('Y-m-d H:i:s'));
      $end_diff = $end_date->diff(new DateTime($invite_to_end_time));

      $end_days= str_replace('+', '', $end_diff->format('%R%d'));
      $end_hours= str_replace('+', '', $end_diff->format('%R%h'));
      $end_minutes= str_replace('+', '', $end_diff->format('%R%i'));
      $end_seconds= str_replace('+', '', $end_diff->format('%R%s'));

 
            
    if($app_type=='doctor')
    {
      $preview_url=base_url().'mypatient-preview/'.base64_encode($rows['userid']);
    }       

     if($app_type=='patient')
    {
      $preview_url=base_url().'doctor-preview/'.$rows['username'];
    }  

  ?>

  
  <div class="appointment-list">
  <div class="profile-info-widget">
  
   <div class="profile-det-info">
      <h3><a target="_blank" href="<?php echo $preview_url;?>"><?php echo $rows['first_name'].' '.$rows['last_name'];?></a></h3>
      <div class="patient-details">
        <h5><i class="far fa-clock"></i> <?php echo $appointment_date.', '.$appointment_time.' - '.$appointment_end_time;?> </h5>
        <h5><i class="fas fa-map-marker-alt"></i> <?php echo $rows['cityname'].', '.$rows['countryname'];?></h5><h5><i class="fas fa-envelope"></i> <?php echo $rows['email'];?></h5>
        <h5 class="mb-0"><i class="fas fa-phone"></i> <?php echo $rows['mobileno'];?></h5>
      </div>
    </div>
  </div>
  <div class="appointment-action">
    <a href="#" class="btn btn-sm bg-info-light" onclick="show_appoinments_modal('<?php echo $appointment_date;?>','<?php echo $appointment_time.' - '.$appointment_end_time;?>','<?php echo $created_date;?>','<?php echo $hourly_rate;?>','<?php echo $type;?>')" ><i class="far fa-eye"></i><?php echo $language['lg_view1'];?> </a>
    <?php 
    if($app_type=='doctor'){
    if($rows['approved']==0) {
      echo'<a href="javascript:void(0);" onclick="conversation_status(\''.$rows['id'].'\',\'1\')" class="btn btn-sm bg-success-light"><i class="fas fa-check"></i> '.$language['lg_accept'].'</a>';
      }
    }
    if($app_type=='patient'){
    if($rows['approved']==0) {
      echo'<a href="javascript:void(0);" class="btn btn-sm bg-danger-light"><i class="fas fa-check"></i> '.$language['lg_cancelled'].'</a>';
      }
    }
      if($rows['approved']==1 && $rows['appointment_status']==0) {
         if($app_type=='doctor'){
      echo'<a href="javascript:void(0);" onclick="conversation_status(\''.$rows['id'].'\',\'0\')"  class="btn btn-sm bg-danger-light"><i class="fas fa-times"></i> '.$language['lg_cancel'].'</a>';
       }
     echo'<div class="conv-list conversation_right conversation_start">';

                   if($start_days>0){ // More than Today 

                        if($start_days == 1){
                            $day = $language['lg_day1'];
                        }else{
                            $day = $language['lg_days'];
                        }                                                 

                        echo'<ul>
                             <li><a class="btn bg-success border-0 rounded-circle"><i class="fas fa-phone"></i></a></li>
                             <li><a class="btn bg-success border-0 rounded-circle"><i class="fas fa-video"></i></a></li>
                             <li><a href="'.base_url().'messages" class="conv_messages"><i class="fas fa-comments"></i></li>
                             </ul> 
                             <div class="remainingtime">'.$language['lg_remaining_time_'].' - '.$start_days .$day.'</div>'; 

                    }
                    else if($start_days < 1 && ( $start_hours > 0 || $start_minutes > 0 || $start_seconds > 0)){ 

                       echo'<ul>
                             <li><a class="btn bg-success border-0  rounded-circle"><i class="fas fa-phone"></i></a></li>
                             <li><a class="btn bg-success border-0 rounded-circle"><i class="fas fa-video"></i></a></li>
                             <li><a href="'.base_url().'messages" class="conv_messages"><i class="fas fa-comments"></i></li>
                             </ul> 
                             <div class="remainingtime">'.$language['lg_remaining_time_'].' - '.sprintf("%02d",$start_hours) . ":" .sprintf("%02d",$start_minutes) .":" .sprintf("%02d",$start_seconds).'</div>'; 

                   } 
                 else{

                   if($end_days < 0 && $end_hours < 0 || $end_minutes < 0 || $end_seconds < 0 ){

                         expired_appoinments($rows['id']); 

                                       
                }else{

                  echo'<ul>
                             <li><a target="_blank" onclick="outgoing_call(\''.md5($rows['id']).'\')" href="javascript:void(0);" class="conv_videocall startVideo btn bg-success border-0 rounded-circle"><i class="fas fa-phone"></i></a></li>
                             <li><a target="_blank" onclick="outgoing_video_call(\''.md5($rows['id']).'\')" href="javascript:void(0);" class="conv_videocall startVideo border-0 btn bg-success rounded-circle"><i class="fas fa-video"></i></a></li>
                             <li><a href="'.base_url().'messages" class="conv_messages"><i class="fas fa-comments"></i></li>
                             </ul> 
                             <div class="remainingtime">'.$language['lg_conversation_wi'].' - '.sprintf("%02d",$end_hours) . ":" .sprintf("%02d",$end_minutes) .":" .sprintf("%02d",$end_seconds).'</div>';

                }

            }

        echo'</div>';
      }
      ?>

  </div>
  </div>

  

<?php } } else {
                 echo '<div class="appointment-list">
                        <div class="profile-info-widget">
                        <p>'.$language['lg_no_appoinments_'].'</p>
                        </div>
                        </div>';
}  ?>

