

			<!-- Clinic and Specialities -->
			<section class="section section-specialities" style="padding-top:135px 0;">
				<div class="container">
					<div class="row">
                        <?php if($this->session->userdata('role')==2){ ?>

                        <div class="col-md-12">
                            <div class="card verify-notification">
                                <div class="col-md-8" >
                                    <h4 style="color:#1D72CD">Want to share queries & get answers?</h4>
                                    <h6 style="color:#1D72CD; font-weight: 300;">Get solutions & advice from Doctors</h6>
                                </div>
                                <div class="col-md-2">
                                    <a href="<?php echo base_url();?>qa/my_questions"><button class="btn btn-info" style="background: #1D72CD;">Ask a Question</button></a>
                                </div>


                            </div>
                        </div>
                        <?php } ?>
						<div class="col-md-12">
							<div class="card border-0">
								<div class="card-body">
									<div class="form-inline">
										<div class="form-group col-md-7 mb-2">
										    <label for="keywords" class="sr-only">Keywords</label>
										    <input type="text" class="form-control w-100" id="keywords" placeholder="Search">
										</div>
										<div class="form-group col-md-3 mb-2">
										    <label for="specialization" class="sr-only">Specialization</label>
										    <select class="form-control w-100" id="specilization">
										    	<option value="">Category</option>
										    	<?php if(!empty($specialization)) {
									 foreach ($specialization as $crows) { ?>
                                          <option value="<?php echo $crows['id']; ?>"><?php echo $crows['specialization']; ?></option>

									 <?php } } ?>
										    	
										    </select>
										</div>
										<button onclick="get_questions(0)"  class="btn btn-primary form-control col-md-2 w-100 mb-2 ">Search</button>

										</div>
								</div>
							</div>
						
                       <div class="ques-list">
							<input type="hidden" name="page" id="page_no_hidden" value="1" >
							<ul class="list-group" id="questions-list">
							
							</ul>

							<div class="load-more text-center d-none" id="load_more_btn">
								<a class="btn btn-primary btn-sm" href="javascript:void(0);">Load More</a>	
							</div>
						</div>
							
						</div>
					</div>
					<!-- <div class="section-header text-center mb-3">
						<h3>Choose Specilization</h3>
					</div> -->
					<!-- <div class="row justify-content-center">
						<div class="col-md-12">
							
							
							<div class="row">

								<?php if(!empty($specialization)) {
									 foreach ($specialization as $crows) { ?>
								
								<div class="col-lg-2">
								<div class="health-item text-center">
									<div class="health-img">
										<a href="<?php base_url();?>qa/questions_answers/<?php echo $crows['id'];?>">
									  
										
										<p class="mt-2 specilization_tags"><b><?php echo $crows['specialization'];?></b></p>
										
									</a>
									</div>
								</div>
							</div>
								<?php   }  } ?>						
								
								
								
								
								
							</div>
							
							
						</div>
												
					</div> -->
				</div>   
			</section>	 
			<!-- Clinic and Specialities -->


<!--answer popup-->
 

  


			