<?php
if (!empty($questions)) {

    foreach ($questions as $rows) {

        $profile_image = (!empty($rows['profileimage'])) ? base_url() . $rows['profileimage'] : base_url() . 'assets/img/user.png';
        $doctor_image = (!empty($rows['doctor_image'])) ? base_url() . $rows['doctor_image'] : base_url() . 'assets/img/user.png';

        ?>

        <div class="card" style="border-radius: 15px;">
            <!--								<div class="card-header">-->
            <!--									<h5>Here is a question for you</h5>-->
            <!--								</div>-->
            <div class="card-body">
                <div class="row">
                    <div class="col-1">
                        <span><img src="<?php echo $doctor_image; ?>" alt="Profile"
                                   class="d-block img-responsive w-100 rounded-circle"></span>
                    </div>
                    <div class="col ">
                        <div class="d-flex justify-content-between align-items-center">

                            <span class="pr-3"><?php echo ucfirst($rows['name']); ?> <span
                                        style="color:#1D72CD; text-decoration: underline;">Stress & Anxiety</span></span>

                            <span class=""><?php echo time_elapsed_string($rows['created_date']); ?></span>
                        </div>


                        <h4 style="clear: both" class="pt-3"><?php echo $rows['questions']; ?></h4>
                        <div class="question-images">
                            <img src="<?php echo base_url('assets/img/stress.png') ?>" alt="" style="max-width: 80px"
                                 class="rounded">
                            <img src="<?php echo base_url('assets/img/stress-1.png') ?>" alt="" style="max-width: 80px"
                                 class="rounded">
                        </div>
                        <!--									<span class="label label-success" style="background: #3dac7b !important;color:white;padding: 5px;border-radius: 6px;display: inline-block;margin-top: 15px;">-->
                        <?php //echo ($rows['specialization']); ?><!--</span>-->
                        <!--<div class="d-flex">
                            <?php if ($this->session->userdata('role') == 1) { ?>
                                <a href="javascript:void(0);" onclick="show_answer('<?php echo $rows['id']; ?>')"
                                   class="p-3 pl-0"><span style="color:#3dac7b"><i class="fas fa-plus"></i>&nbsp;&nbsp; Add Answer</span></a>
                            <?php } ?>

                        </div>-->
                    </div>


                </div>
            </div>
            <div class="card-footer bg-transparent">
                <?php $answer_count = $rows['answer_count'];

                if ($answer_count > 0) {

                ?>

                <div class="pb-2 row">
                    <div class="col-1">
                        <img src="<?php echo $doctor_image; ?>" alt="Profile"
                             class="d-block img-responsive w-100 rounded-circle">
                    </div>
                    <div class="col d-flex justify-content-between align-items-center">
                                            <span class="">

                                            <span><?php echo "Dr. " . $rows['doctor_name']; ?> <span
                                                        class="label label-success"
                                                        style="background: #3dac7b !important;color:white;padding: 5px;border-radius: 6px;display: inline-block;margin-left: 15px;">Consult</span><br>
                                            </span>
										</span>

                        <span class=""><?php echo time_elapsed_string($rows['answer_date']); ?></span>
                    </div>


                </div>
                <div class="pb-3 row">
                    <div class="col-1"></div>
                    <div class="col">
                        <h5 style="clear: both" class="pt-2"><?php echo($rows['answer']); ?> </h5>
                        <div class="question-images">
                            <img src="<?php echo base_url('assets/img/stress.png') ?>" alt="" style="max-width: 80px"
                                 class="rounded">
                            <img src="<?php echo base_url('assets/img/stress-1.png') ?>" alt="" style="max-width: 80px"
                                 class="rounded">
                        </div>

                        <div class="d-flex answer_review">
                            <a href="" class=" mr-3 liked"><i class="fas fa-thumbs-up"></i><h6 class="">Useful</h6></a>
                            <a href="" class=""><i class="far fa-thumbs-down"></i> <h6 class="">Not Useful</h6></a>
                        </div>
                        <div class="row">
                            <div class="accordion w-100" id="accordionExample">
                                <div class="card border-0">
                                    <div class="card-header" id="headingOne">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link btn-block text-left" type="button"
                                                    data-toggle="collapse" data-target="#collapseOne"
                                                    aria-expanded="true" aria-controls="collapseOne">
                                                <i class="fa fa-chevron-up"></i> <span class="ml-3">Feedback</span>
                                            </button>
                                        </h2>
                                    </div>

                                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                                         data-parent="#accordionExample">
                                        <div class="list-group border-0 rounded p-3" style="background: #F9FBFD;">
                                            <a href="#" class="list-group-item border-0 bg-transparent" >
                                                <div class="d-flex w-100 mb-3">
                                                    <span class="d-flex justify-content-center align-items-center rounded-circle" style="color: #1D72CD; background: rgba(29,114,205, 0.15); height: 30px; width: 30px;" ><i class="fa fa-user"></i></span> <span class="d-flex align-items-center ml-3 h6">You</span>
                                                </div>
                                                <h5 class="mb-1">Some placeholder content in a paragraph.</h5>

                                            </a>

                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <input type="text" class="form-control" placeholder="Answer Here" id="qa-answers"
                                       data-questionID="<?php echo $rows['id']; ?>">
                                <small class="d-block">Press SHIFT + Enter to send</small>
                            </div>
                            <div class="col-md-6">
                                <ul class="d-flex justify-content-end align-items-center h-100 list-unstyled">
                                    <li class="mr-3">Share To:</li>
                                    <li class="bg-secondary text-white rounded-circle mr-2 h5 d-flex justify-content-center align-items-center"
                                        style="width: 30px; height: 30px;"><i class="fab fa-facebook"></i></li>
                                    <li class="bg-secondary text-white rounded-circle mr-2 h5 d-flex justify-content-center align-items-center"
                                        style="width: 30px; height: 30px;"><i class="fab fa-instagram"></i></li>
                                    <li class="bg-secondary text-white rounded-circle h5 d-flex justify-content-center align-items-center"
                                        style="width: 30px; height: 30px;"><i class="fab fa-twitter"></i></li>
                                </ul>
                            </div>
                        </div>

                        <?php if ($answer_count > 1) { ?><a
                            href="<?php echo base_url(); ?>qa/view_answers/<?php echo $rows['id']; ?>"><?php echo $answer_count - 1; ?>
                            More answer</a> <?php } ?>


                        <?php } else { ?>
                            <div class="row">
                                <div class="col-md-6">
                                    <input type="text" class="form-control" placeholder="Answer Here" id="qa-answers"
                                           data-questionID="<?php echo $rows['id']; ?>">
                                    <br>
                                    <small>Press SHIFT + Enter to send</small>
                                </div>
                                <div class="col-md-6">
                                    <ul class="d-flex justify-content-end align-items-center h-100 list-unstyled">
                                        <li class="mr-3">Share To:</li>
                                        <li class="bg-secondary text-white rounded-circle mr-2 h5 d-flex justify-content-center align-items-center"
                                            style="width: 30px; height: 30px;"><i class="fab fa-facebook"></i></li>
                                        <li class="bg-secondary text-white rounded-circle mr-2 h5 d-flex justify-content-center align-items-center"
                                            style="width: 30px; height: 30px;"><i class="fab fa-instagram"></i></li>
                                        <li class="bg-secondary text-white rounded-circle h5 d-flex justify-content-center align-items-center"
                                            style="width: 30px; height: 30px;"><i class="fab fa-twitter"></i></li>
                                    </ul>
                                </div>
                            </div>
                        <?php } ?>
                    </div>

                </div>
            </div>
        </div>


    <?php }
} else {
    echo '<li class="list-group-item">
                    <div class="user-que-name d-inline-block">
                    <h5 class="table-avatar">No Questions Found<h5>
                    </div>
                    </li>';
} ?>