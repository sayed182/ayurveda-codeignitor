<section>
   		<div class="container">
   			<div class="row">
						<div class="col-md-12">
							<div class="card">
								
							</div>
							<div class="card">
								
								<div class="card-body">
									<div class="pb-3 mins">
										<div class="float-left">
                                            <span><img src="<?php echo $doctor_image; ?>" alt="Profile"></span>
                                            <?php echo $question['name']; ?>
                                        </div>

										<span class="float-right"><?php echo time_elapsed_string($question['created_date']); ?></span>
									</div>
									<h4 style="clear: both" class="pt-3">Q.  #<?php echo $question['questions'];?> </h4>
									<span class="label label-success" style="background: #3dac7b !important;color:white;padding: 5px;border-radius: 6px;display: inline-block;margin-top: 15px;"><?php echo ($question['specialization']); ?></span>
									<div class="d-flex">
										<?php if($this->session->userdata('role')==1){ ?>
										<a href="javascript:void(0);" onclick="show_answer('<?php echo $question['id'];?>')" class="p-3 pl-0"><span style="color:#3dac7b"><i class="fas fa-plus"></i>&nbsp;&nbsp; Add Answer</span></a>
									<?php } ?>
										
									</div>
									<hr />
									<?php $answer_count=$question['answer_count']; 

									?>
									<p><?php echo $answer_count; ?> Answers</p>
									<hr />
									<?php foreach ($answers as $rows) {

										$doctor_image=(!empty($rows['profileimage']))?base_url().$rows['profileimage']:base_url().'assets/img/user.png';
										
									 ?>
									<div>
										<div class="pb-3 mins">
											<span class="float-left">
												<img src="<?php echo $doctor_image; ?>" alt="Profile" class="d-block img-responsive w-100 rounded-circle">
												&nbsp;&nbsp;&nbsp;<span><?php echo "Dr. ".$rows['name']; ?>

											</span>
										</span>
											<span class="float-right"><?php echo time_elapsed_string($rows['created_date']); ?></span>
										</div>
                                        <h5 class=""><?php echo $rows['answers'];?></h5>
                                        <div class="question-images">
                                            <img src="<?php echo base_url('assets/img/stress.png')?>" alt="" style="max-width: 60px" class="rounded">
                                            <img src="<?php echo base_url('assets/img/stress-1.png')?>" alt="" style="max-width: 60px" class="rounded">
                                        </div>
                                        <div class="inline">
                                            <a href="" class="fa fa-thumbs-up mr-3">Useful</a>
                                            <a href="" class="fa fa-thumbs-up">Not Useful</a>
                                        </div>
										
									</div>
									<hr />

								<?php } ?>

								
								</div>
							</div>
							
						</div>
					</div>
   		</div>
   </section>
