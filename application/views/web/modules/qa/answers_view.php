<div class="ques-rply-box">
<?php
if(!empty($answers)){
   foreach ($answers as $rows) { 
 $profile_image=(!empty($rows['profileimage']))?base_url().$rows['profileimage']:base_url().'assets/img/user.png';
   	?>
<!-- <hr> -->
	<div class="ques-re-boc-inner">
	<div class="que-reply">
		<h5 class="table-avatar d-block">
			<a href="<?php echo base_url().'doctor-preview/'.$rows['username'];?>" class="avatar avatar-sm mr-2">
				<img class="avatar-img rounded-circle" src="<?php echo $profile_image;?>" alt="User Image">
				
			</a>
			<a href="<?php echo base_url().'doctor-preview/'.$rows['username'];?>" class="">
				<b><span class="text-secondary"><?php echo ucfirst($rows['name']);?></span></b>
			</a>
            <span class="badge badges-primary"> Consult </span>
		</h5>

		<?php if($this->session->userdata('role')=='1') {
             	if($rows['user_id']==$this->session->userdata('user_id')) { ?>
          <div class="float-right">
			<div class="dropdown dropdown-action">
				<a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
					<i class="material-icons">more_vert</i>
				</a>
				<div class="dropdown-menu dropdown-menu-right">
					<a class="dropdown-item" href="javascript:void(0);" onclick="edit_answers('<?php echo $rows['id'];?>','<?php echo $rows['question_id'];?>')">
						<i class="fas fa-pencil-alt mr-2"></i>Edit
					</a>
					<a class="dropdown-item" href="javascript:void(0);" onclick="delete_answers('<?php echo $rows['id'];?>','<?php echo $rows['question_id'];?>')"><i class="far fa-trash-alt mr-2"></i>Delete
					</a>
					
				</div>
			</div>
		</div>
	<?php } } ?>

		<div class="d-block ml-5">
			<input type="hidden" id="qaanswers_edit<?php echo $rows['id'];?>" value="<?php echo $rows['answers'];?>">
			<h5 class=""><?php echo $rows['answers'];?></h5>
            <div class="question-images">
                <img src="<?php echo base_url('assets/img/stress.png')?>" alt="" style="max-width: 60px" class="rounded">
                <img src="<?php echo base_url('assets/img/stress-1.png')?>" alt="" style="max-width: 60px" class="rounded">
            </div>

		</div>
	</div>
	<span class="text-small d-block ml-5"><?php echo time_elapsed_string($rows['created_date']);?></span>

</div>
<hr>
	<?php } } ?>
</div>

