<!-- Breadcrumb -->
			<div class="breadcrumb-bar">
				<div class="container-fluid">
					<div class="row align-items-center">
						<div class="col-md-12 col-12">
							<nav aria-label="breadcrumb" class="page-breadcrumb">
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="<?php echo base_url();?>">Q & A</a></li>
									<li class="breadcrumb-item active" aria-current="page">Sub Category</li>
								</ol>
							</nav>
							<h2 class="breadcrumb-title">Subcategory</h2>
						</div>
					</div>
				</div>
			</div>
			<!-- /Breadcrumb -->
			<!-- Page Content -->
			<div class="content">
				<div class="container">

					<!-- sub category Widget -->
					<div class="row justify-content-center">
						<div class="col-md-8">
							<div class="card">
								<div class="card-body">
									<h4 class="mb-4">Subcategory of <span class="text-primary"><b> <?php echo $category_name;?></b></span></h4>
								    <div class="list-group">
								    	<?php
								    	if(!empty($subcategories))
								    	{
								    		foreach ($subcategories as $rows) {
								    			echo'<a href="'.base_url().'questions&answers?subcategory='.urlencode($rows['slug']).'" class="list-group-item">'.$rows['subcategory_name'].'</a>';
								    		}
								    	}
								  
									  ?>
									</div>
								</div>
							</div>
						</div>

						

					</div>
					<!-- sub category Widget -->
				</div>
			</div>