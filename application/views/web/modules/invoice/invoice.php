<!-- Breadcrumb -->
			<div class="breadcrumb-bar">
				<div class="container-fluid">
					<div class="row align-items-center">
						<div class="col-md-12 col-12">
							<nav aria-label="breadcrumb" class="page-breadcrumb">
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="<?php echo base_url();?>dashboard">Dashboard</a></li>
									<li class="breadcrumb-item active" aria-current="page">Invoice</li>
								</ol>
							</nav>
							<h2 class="breadcrumb-title">Invoice</h2>
						</div>
					</div>
				</div>
			</div>
			<!-- /Breadcrumb -->
			
			<!-- Page Content -->
			<div class="content">
				<div class="container-fluid">

					<div class="row">
						<div class="col-md-5 col-lg-4 col-xl-3 theiaStickySidebar">
						
							<!-- Profile Sidebar -->
							<?php 
							if($this->session->userdata('role')=='1')
							{
								$this->load->view('web/includes/doctor_sidebar');
							}
							if($this->session->userdata('role')=='2')
							{
								$this->load->view('web/includes/patient_sidebar');
							}
							
                            ?>
							<!-- /Profile Sidebar -->
							
						</div>
						
						<div class="col-md-7 col-lg-8 col-xl-9">
                            <?php if ($invoice_count > 0){ ?>
							<div class="card card-table">
								<div class="card-body">
								
									<!-- Invoice Table -->

									<div class="table-responsive">
										<table class="table table-hover table-center mb-0" id="invoice_table">
											<thead>
												<tr>
													<th>Invoice No </th>
													<th><?php echo ($this->session->userdata('role')=='1')?'Patient':'Doctor';?></th>
													<th>Amount</th>
													<th>Paid On</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												
											</tbody>
										</table>
									</div>
									<!-- /Invoice Table -->

									
								</div>
							</div>
                            <?php }else{ ?>
                                <div style="
    display: flex;
    flex-direction: column;
    width: 100%;
    height: 100%;
    align-items: center;
    justify-content: center;
">
                                    <img src="<?php echo base_url('assets/img/no_invoice.png'); ?>" alt="" class="img-responsive-w-100">
                                    <div class="mt-5">
                                        <h3>No invoices found.</h3>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
					</div>

				</div>

			</div>		
			<!-- /Page Content -->