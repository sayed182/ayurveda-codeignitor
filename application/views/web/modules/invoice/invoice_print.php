<!DOCTYPE html> 
<html lang="en">
  <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
     <title><?php echo !empty(settings("meta_title"))?settings("meta_title"):"Doccure";?></title>
    <meta content="<?php echo !empty(settings("meta_keywords"))?settings("meta_keywords"):"";?>" name="keywords">
    <meta content="<?php echo !empty(settings("meta_description"))?settings("meta_description"):"";?>" name="description">
        <!-- Favicons -->
    <link href="<?php echo !empty(base_url().settings("favicon"))?base_url().settings("favicon"):base_url()."assets/img/favicon.png";?>" rel="icon">
    
     <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
    
    <!-- Fontawesome CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/fontawesome/css/fontawesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/fontawesome/css/all.min.css">
    
    
    <!-- Main CSS -->
    
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/toastr.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="<?php //echo base_url();?>assets/js/html5shiv.min.js"></script>
      <script src="<?php //echo base_url();?>assets/js/respond.min.js"></script>
    <![endif]-->
  
  </head>
			<div class="content">
				<div class="container-fluid">

					<div class="row">
						<div class="col-lg-8 offset-lg-2">
							<div class="invoice-content">
								<div class="invoice-item">
									<div class="row">
										<div class="col-md-6">
											<div class="invoice-logo">
												<img src="<?php echo !empty(base_url().settings("logo_front"))?base_url().settings("logo_front"):base_url()."assets/img/logo.png";?>" alt="logo">
											</div>
										</div>
										<div class="col-md-6">
											<p class="invoice-details">
												<strong>Invoice No:</strong> <?php echo $invoices['invoice_no'];?> <br>
												<strong>Issued:</strong> <?php echo date('d M Y',strtotime($invoices['payment_date']));?>
											</p>
										</div>
									</div>
								</div>
								
								<!-- Invoice Item -->
								<div class="invoice-item">
									<div class="row">
										<div class="col-md-6">
											<div class="invoice-info">
												<strong class="customer-text">Invoice From</strong>
												<p class="invoice-details invoice-details-two">
													Dr. <?php echo ucfirst($invoices['doctor_name']);?> <br>
													<?php echo $invoices['doctoraddress1'].', '.$invoices['doctoraddress2'];?>,<br>
													<?php echo $invoices['doctorcityname'].', '.$invoices['doctorcountryname'];?> <br>
												</p>
											</div>
										</div>
										<div class="col-md-6">
											<div class="invoice-info invoice-info2">
												<strong class="customer-text">Invoice To</strong>
												<p class="invoice-details">
													<?php echo ucfirst($invoices['patient_name']);?> <br>
													<?php echo $invoices['patientaddress1'].', '.$invoices['patientaddress2'];?>,<br>
													<?php echo $invoices['patientcityname'].', '.$invoices['patientcountryname'];?> <br>
												</p>
											</div>
										</div>
									</div>
								</div>
								<!-- /Invoice Item -->
								
								<!-- Invoice Item -->
								<div class="invoice-item">
									<div class="row">
										<div class="col-md-12">
											<div class="invoice-info">
												<strong class="customer-text">Payment Method</strong>
												<p class="invoice-details invoice-details-two">
													<?php echo ucfirst($invoices['payment_type']);?><br>
													
												</p>
											</div>
										</div>
									</div>
								</div>
								<!-- /Invoice Item -->
								<?php


            $user_currency=get_user_currency();
            $user_currency_code=$user_currency['user_currency_code'];
            $user_currency_rate=$user_currency['user_currency_rate'];

            $currency_option = (!empty($user_currency_code))?$user_currency_code:$invoices['currency_code'];
            $rate_symbol = currency_code_sign($currency_option);

                      
            $rate=get_doccure_currency($invoices['per_hour_charge'],$invoices['currency_code'],$user_currency_code);
               
            $amount=$rate_symbol.''.$rate;
              
            $transcation_charge=get_doccure_currency($invoices['transcation_charge'],$invoices['currency_code'],$user_currency_code);

            $tax_amount=get_doccure_currency($invoices['tax_amount'],$invoices['currency_code'],$user_currency_code);

             $total_amount=get_doccure_currency($invoices['total_amount'],$invoices['currency_code'],$user_currency_code);



								?>
								
								<!-- Invoice Item -->
								<div class="invoice-item invoice-table-wrap">
									<div class="row">
										<div class="col-md-12">
											<div class="table-responsive">
												<table class="invoice-table table table-bordered">
													<thead>
														<tr>
															<th>Description</th>
															<th class="text-right">Total</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td>Video Call Booking</td>
															<td class="text-right"><?php echo $amount; ?></td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
										<div class="col-md-6 col-xl-4 ml-auto">
											<div class="table-responsive">
												<table class="invoice-table-two table">
													<tbody>
													<tr>
														<th>Transaction charge:</th>
														<td><span><?php echo $rate_symbol.$transcation_charge; ?></span></td>
													</tr>
													<tr>
														<th>Tax (<?php echo $invoices['tax'] ?>%):</th>
														<td><span><?php echo $rate_symbol.$tax_amount; ?></span></td>
													</tr>
													<tr>
														<th>Total Amount:</th>
														<td><span><?php echo $rate_symbol.number_format($total_amount,2); ?></span></td>
													</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
								<!-- /Invoice Item -->
								
								<!-- Invoice Information -->
							<!-- 	<div class="other-info">
									<h4>Other information</h4>
									<p class="text-muted mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus sed dictum ligula, cursus blandit risus. Maecenas eget metus non tellus dignissim aliquam ut a ex. Maecenas sed vehicula dui, ac suscipit lacus. Sed finibus leo vitae lorem interdum, eu scelerisque tellus fermentum. Curabitur sit amet lacinia lorem. Nullam finibus pellentesque libero.</p>
								</div> -->
								<!-- /Invoice Information -->
								
							</div>
						</div>
					</div>

				</div>

			</div>		
			<!-- /Page Content-->

			 <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
    <script type="text/javascript">
    	window.print();
    </script>