<!-- Breadcrumb -->
<div class="breadcrumb-bar">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-md-12 col-12">
				<nav aria-label="breadcrumb" class="page-breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="<?php echo base_url();?>"><?php echo $language['lg_home'];?></a></li>
						<li class="breadcrumb-item active" aria-current="page"><?php echo $language['lg_dashboard'];?></li>
					</ol>
				</nav>
				<h2 class="breadcrumb-title"><?php echo $language['lg_dashboard'];?></h2>
			</div>
		</div>
	</div>
</div>
<!-- /Breadcrumb -->

<!-- Page Content -->
<div class="content">
	<div class="container">

		<div class="row">
			<div class="col-md-5 col-lg-4 col-xl-3 theiaStickySidebar">
			<!-- Profile Sidebar -->
				<?php 
				$this->load->view('web/includes/patient_sidebar.php');
				$user_detail=user_detail($this->session->userdata('user_id'));
				?>
			<!-- / Profile Sidebar -->
		   </div>

			<div class="col-md-7 col-lg-8 col-xl-9">
					<?php 
					if($user_detail['is_updated']=='0') {
					echo'<div class="alert alert-warning" role="alert">
					<i class="fa fa-exclamation-circle" aria-hidden="true"></i>'.$language['lg_this_is_a_warni'].' <a href="'.base_url().'profile" class="alert-link">'.$language['lg_click_here1'].'</a>. '.$language['lg_give_it_a_click'].'
					</div>';
				    }
				    if($user_detail['is_verified']=='0') {?>
				        <div class="row">
                            <div class="col-md-12">
                                <div class="card verify-notification">

                                    <div class="col-md-2 col-sm-3">
                                                <span class="fa-envelope-check">
                                                    <svg width="74" height="58" viewBox="0 0 74 58" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
<path d="M32.6966 30.5802C32.1127 30.9451 31.4559 31.0911 30.872 31.0911C30.2882 31.0911 29.6313 30.9451 29.0474 30.5802L0 12.8452V36.4189C0 41.4547 4.08708 45.5418 9.12294 45.5418H52.6941C57.7299 45.5418 61.817 41.4547 61.817 36.4189V12.8452L32.6966 30.5802Z"
      fill="#60A2E8"/>
<path d="M52.6946 0H9.12348C4.81746 0 1.16828 3.06531 0.29248 7.15238L30.9455 25.8362L61.5256 7.15238C60.6498 3.06531 57.0007 0 52.6946 0Z"
      fill="#60A2E8"/>
<circle cx="58.9093" cy="42.4806" r="14.0909" fill="#3DAC7B" stroke="#F2F9FF" stroke-width="2"/>
<path d="M55.418 42.2622L58.0362 44.8804L62.618 40.2986" stroke="white" stroke-width="2" stroke-linecap="round"/>
</svg>
                                                </span>
                                    </div>
                                    <div class="col-md-7">
                                        <h4>Verify your Email/Mobile No.</h4>
                                    </div>
                                    <div class="col-md-3">
                                        <button class="btn btn-block verify-notification__btn" data-toggle="modal" data-target="verifyModal">Verify Now</button>
                                    </div>


                                </div>
                            </div>
                        </div>
				    <?php } ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-4 col-sm-12 d-flex justify-content-between">
                                <h4 class="text-uppercase">PROFILE COMPLETED</h4>
                                <h4 style="color: #3c763d;"> 70% </h4>
                            </div>
                            <div class="col-md-8 col-sm-12">
                                <div class="profile_progress">
                                    <span class=""><span class="profile_progress-bar completed"></span><a class="far fa-check-circle"></a></span>
                                    <span><span class="profile_progress-bar completed"></span><a class="far fa-check-circle"></a></span>
                                    <span><span class="profile_progress-bar completed"></span><a class="far fa-check-circle"></a></span>
                                    <span><span class="profile_progress-bar"></span><a class="far fa-check-circle"></a></span>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

				<div class="card">
					<div class="card-body pt-0">

						<!-- Tab Menu -->
						<nav class="user-tabs mb-4">
							<ul class="nav nav-tabs nav-tabs-bottom nav-justified">
								<li class="nav-item">
									<a class="nav-link active" onclick="appoinments_table()" href="#pat_appointments" data-toggle="tab"><?php echo $language['lg_appointments'];?></a>
								</li>
								<li class="nav-item">
									<a class="nav-link" onclick="prescriptions_table()" href="#pres" data-toggle="tab"><span><?php echo $language['lg_prescription'];?></span></a>
								</li>
								<li class="nav-item">
									<a class="nav-link" onclick="medical_records_table()" href="#medical" data-toggle="tab"><span class="med-records"><?php echo $language['lg_medical_records'];?></span></a>
								</li>
								<li class="nav-item">
									<a class="nav-link" onclick="billings_table()"  href="#billing" data-toggle="tab"><span><?php echo $language['lg_billing'];?></span></a>
								</li> 
							</ul>
						</nav>
						<!-- /Tab Menu -->

						<!-- Tab Content -->
						<div class="tab-content pt-0">

							<input type="hidden" id="patient_id" value="<?php echo $this->session->userdata('user_id');?>">
							<!-- Appointment Tab -->
										<div id="pat_appointments" class="tab-pane fade show active">
											<div class="card card-table mb-0">
												<div class="card-body">
													<div class="table-responsive">
														<table id="appoinment_table" class="table table-hover table-center mb-0">
															<thead>
																<tr>
																	<th><?php echo $language['lg_doctor2'];?></th>
																	<th><?php echo $language['lg_appt_date'];?></th>
																	<th><?php echo $language['lg_booking_date'];?></th>
																	<th><?php echo $language['lg_type'];?></th>
																	
																</tr>
															</thead>
															<tbody>
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
										<!-- /Appointment Tab -->
										
										<!-- Prescription Tab -->
										<div class="tab-pane fade" id="pres">
											<div class="card card-table mb-0">
												<div class="card-body">
													<div class="table-responsive">
														<table id="prescription_table" style="width:100%" class="table table-hover table-center mb-0">
															<thead>
																<tr>
																	<th>#</th>
																	<th><?php echo $language['lg_date1'];?></th>
																	<th><?php echo $language['lg_name'];?></th>	
																	<th><?php echo $language['lg_doctor2'];?></th>
																	<th><?php echo $language['lg_view1'];?></th>
																</tr>     
															</thead>
															<tbody>

															</tbody>	
														</table>
													</div>
												</div>
											</div>
										</div>
										<!-- /Prescription Tab -->

										<!-- Medical Records Tab -->
										<div class="tab-pane fade" id="medical">
											<div class="card card-table mb-0">
												<div class="card-body">
													<div class="table-responsive">
														<table id="medical_records_table" class="table table-hover table-center mb-0" style="width: 100%">
															<thead>
																<tr>
																	<th>#</th>
																	<th><?php echo $language['lg_date1'];?> </th>
																	<th><?php echo $language['lg_description'];?></th>
																	<th><?php echo $language['lg_attachment'];?></th>
																	<th><?php echo $language['lg_doctor2'];?></th>
																	
																</tr>     
															</thead>
															<tbody>
															</tbody>  	
														</table>
													</div>
												</div>
											</div>
										</div>
										<!-- /Medical Records Tab -->
										
										<!-- Billing Tab -->
										<div class="tab-pane" id="billing">
											<div class="card card-table mb-0">
												<div class="card-body">
													<div class="table-responsive">
													
														<table id="billing_table" class="table table-hover table-center mb-0" style="width:100%">
															<thead>
																<tr>
																	<th>#</th>
																	<th><?php echo $language['lg_date1'];?></th>
																	<th><?php echo $language['lg_description'];?></th>
																	<th><?php echo $language['lg_doctor2'];?></th>
																	<th><?php echo $language['lg_view1'];?></th>
																</tr>
															</thead>
															<tbody>
																																
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									<!-- Billing Tab -->

						</div>
						<!-- Tab Content -->

					</div>
				</div>
			</div>
		</div>

	</div>

</div>		
<!-- /Page Content -->