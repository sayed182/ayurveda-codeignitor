<!-- Breadcrumb -->
<div class="breadcrumb-bar">
	<div class="container-fluid">
		<div class="row align-items-center">
			<div class="col-md-12 col-12">
				<nav aria-label="breadcrumb" class="page-breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>home/">Home</a></li>
						<li class="breadcrumb-item active" aria-current="page">My Questions</li>
					</ol>
				</nav>
				<h2 class="breadcrumb-title">My Questions</h2>
			</div>
		</div>
	</div>
</div>
<!-- /Breadcrumb -->
	<!-- Page Content -->
			<div class="content">
				<div class="container-fluid">

					<div class="row">
						<div class="col-md-5 col-lg-4 col-xl-3 theiaStickySidebar">
						
							<!-- Profile Sidebar -->
							<?php $this->load->view('web/includes/patient_sidebar');	?>
							<!-- /Profile Sidebar -->
							
						</div>
						
						<div class="col-md-7 col-lg-8 col-xl-9">
							<div class="card card-table mb-0">
								  <div class="card-header">

                                  <h3 class="p-3">Questions & Answers</h3>

									

                                                     <a href="javascript:void(0);" onclick="add_question()"  class="btn btn-primary float-right mt-2">+ Add </a> 
                                </div>
								<div class="card-body">

									

									<div class="table-responsive">
										<table id="my_ques"class="table table-hover table-center dataTable mb-0">
											<thead>
												<tr><th>#</th>
													<th>Date</th>
													<th>Questions</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												<?php if(!empty($my_questions)) {
													$i=1;
													 foreach ($my_questions as $rows) { ?>
													
												<tr>
													<td><?php echo $i++;?></td>
													<td><?php echo date('d M Y',strtotime($rows['created_date']));?></td>
													<td><h4><?php echo $rows['questions'];?></h4></td>
													<td>
														<a href="<?php echo base_url();?>qa/view_answers/<?php echo $rows['id']; ?>"  class="btn btn-sm bg-info-light" ><i class="far fa-eye"></i> View </a>
														<a href="javascript:void(0);" onclick="delete_question('<?php echo $rows['id'];?>')" class="btn btn-sm btn-danger" ><i class="fas fa-trash"></i> Delete </a>
													</td>
												</tr>
												<?php } } else { 
												echo'<tr>
													<td rowspan="4">No questions found</td>
													
												</tr>';
											 } ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							</div>
						</div>
					</div>
				</div>
				</div>
				 <!-- View My question -->
            <div class="modal fade custom-modal" id="view_modal-question" tabindex="-1" role="dialog" aria-hidden="true" >
              <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document" style="width: 90%">
                <div class="modal-content">
                  <div class="modal-header">
                    <h3 class="modal-title">View My Question<span class="view_title"></span></h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  </div>                  
                  <div class="modal-body view_answers"> 
                  		   
                  </div>
                  <div class="clearfix"></div>
                  <div class="modal-footer">                    
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                  </div>
                </div>
              </div>
            </div>
