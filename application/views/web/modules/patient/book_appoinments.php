<!-- Breadcrumb -->
<?php 

$time_zone = $this->session->userdata('time_zone');
  date_default_timezone_set($time_zone);

  

  function get_booked_class($availabe_days,$start_time,$end_time,$day_value,$to_timezone)
  {
   $class = 'available';
   if(!empty($availabe_days)){
     foreach ($availabe_days as $key => $value) {      
      $from_timezone = $value['time_zone'];    
      $from_time = $value['appointment_date'].' '.$value['appointment_time'];
      $to_time = $value['appointment_date'].' '.$value['appointment_end_time'];
      $from_date_time  = converToTz($from_time,$to_timezone,$from_timezone);
      $to_date_time = converToTz($to_time,$to_timezone,$from_timezone);        

      if( date('H:i:s',strtotime($from_time)) == date('H:i:s',strtotime($start_time)) && date('H:i:s',strtotime($to_time))== date('H:i:s',strtotime($end_time)) && date('Y-m-d', strtotime($from_time)) ==date('Y-m-d', strtotime("+$day_value day"))){
        $class = 'notavailable';
      }
    }
  }
  return $class;
}

$time_zone = $this->session->userdata('time_zone');
date_default_timezone_set($time_zone);
$date =  date('Y-m-d');
if($date >= $selected_date){
  $class = 'd-none';
}else{
  $class = '';
}

?>
			<div class="breadcrumb-bar">
				<div class="container-fluid">
					<div class="row align-items-center">
						<div class="col-md-12 col-12">
							<nav aria-label="breadcrumb" class="page-breadcrumb">
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="<?php echo base_url();?>"><?php echo $language['lg_home'];?></a></li>
									<li class="breadcrumb-item active" aria-current="page"><?php echo $language['lg_booking2'];?></li>
								</ol>
							</nav>
							<h2 class="breadcrumb-title"><?php echo $language['lg_booking2'];?></h2>
						</div>
					</div>
				</div>
			</div>
			<!-- /Breadcrumb -->
			
			<!-- Page Content -->
			<div class="content">
				<div class="container">
				
					<div class="row">
						<div class="col-12">
						
							<div class="card">
								<div class="card-body">
									<div class="booking-doc-info">
										<?php
											$profileimage=(!empty($doctors['profileimage']))?base_url().$doctors['profileimage']:base_url().'assets/img/user.png';
										?>
										<a href="<?php echo base_url().'doctor-preview/'.$doctors['username'];?>" class="booking-doc-img">
											<img src="<?php echo $profileimage;?>" alt="User Image">
										</a>
										<div class="booking-info">
											<h4><a href="<?php echo base_url().'doctor-preview/'.$doctors['username'];?>"><?php echo $language['lg_dr'];?> <?php echo ucfirst($doctors['first_name'].' '.$doctors['last_name']);?></a></h4>
											<div class="rating">
												<?php
						                        $rating_value=$doctors['rating_value'];
						                        for( $i=1; $i<=5 ; $i++) {
						                          if($i <= $rating_value){                                        
						                          echo'<i class="fas fa-star filled"></i>';
						                          }else { 
						                          echo'<i class="fas fa-star"></i>';
						                          } 
						                        } 
						                      ?>
												<span class="d-inline-block average-rating">(<?php echo $doctors['rating_count'];?>)</span>
											</div>
											<p class="text-muted mb-0"><i class="fas fa-map-marker-alt"></i> <?php echo $doctors['cityname'].', '. $doctors['countryname'];?></p>
										</div>
									</div>
								</div>
							</div>


							<!-- Schedule Widget -->
							<div class="card booking-schedule schedule-widget bookings-schedule">
							
								<!-- Schedule Header -->
								<div class="schedule-header">
									<div class="row">
                                        <div class="col-md-12">
                                            <?php
                                            $address = !empty($doctors['address1'])?$doctors['address1']:$language['lg_no_address_spec'];
                                            $info = $language['lg_booking_appoinm'].' '.$doctors['first_name'].' '.$doctors['last_name'];
                                            ?>

                                            <input type="hidden" name="doctor_id" id="doctor_id" value="<?php echo $doctors['userid']; ?>">
                                            <input type="hidden" name="doctor_username" id="doctor_username" value="<?php echo $doctors['username']; ?>">
                                            <input id="selected_count"  type="hidden"   />

                                            <input type="hidden" name="price_type" id="price_type" value="<?php echo $doctors['price_type'] ?>">
                                            <input type="hidden" name="hourly_rate" id="hourly_rate" value="<?php echo $doctors['amount'] ?>">

                                            <div class="booking-option px-4 pt-3">
                                                <label class="payment-radio credit-card-option">
                                                    <input type="radio" name="type" value="online" id="online">
                                                    <span class="checkmark"></span>
                                                    <?php echo $language['lg_online'];?>
                                                </label>
                                                <label class="payment-radio credit-card-option">
                                                    <input type="radio" name="type" value="clinic" id="clinic">
                                                    <span class="checkmark"></span>
                                                    <?php echo $language['lg_clinic'];?>
                                                </label>
                                                <label class="payment-radio credit-card-option">
                                                    <input type="radio" name="type" value="both" id="both" checked="checked">
                                                    <span class="checkmark"></span>
                                                    <?php echo $language['lg_both'];?>
                                                </label>
                                            </div>
                                        </div>
										<div class="col-md-12">
                                            <div class="track-dates-container">
                                                <h3 class="text-center" ><?php echo date('F ,Y');?></h3>
                                                <div class="d-flex" id="scheduling-dates">
                                                    <div class="date-block" id="scheduling-dates-prev">
                                                        <span class="fas fa-chevron-left"></span>
                                                    </div>
                                                    <div id="scheduling-dates-wrapper">
                                                        <div class="schedule-dates-track">

                                                        </div>
                                                    </div>
                                                    <div class="date-block" id="scheduling-dates-next">
                                                        <span class="fas fa-chevron-right"></span>
                                                    </div>
                                                </div>
                                            </div>
											
										</div>
									</div>
								</div>
								<!-- /Schedule Header -->
                                <div class="col-md-12" style="color: #3dac7b">
                                    <form class="form-inline">
                                        <div class="form-group mb-2">
                                        <label for="" class="mr-3">Date : </label>
                                        <select name="" id="" class="form-control mr-3" style="color: #3dac7b">
                                            <option value="">12</option>
                                        </select>
                                        <select name="" id="" class="form-control mr-3" style="color: #3dac7b">
                                            <option value="">August</option>
                                        </select>
                                        <label for="" >2021</label>
                                        </div>
                                    </form>
                                </div>
								
								<!-- Schedule Content -->
								<div class="schedule-cont">
									<div class="row">
										<div class="col-md-12">
										
											<!-- Time Slot -->
											<div class="time-slot">
                                                <ul class="available-slots-list">
                                                    <?php for($i = 0; $i<=14; $i++){?>
                                                        <li class="badge badge-light m-1 <?php echo $i==rand(0,13)?'selected':'';?>"><button class="btn">06:00 - 06:10 PM (online)</button></li>
                                                    <?php } ?>

                                                </ul>

											</div>
											<!-- /Time Slot -->
											
										</div>
									</div>
								</div>
								<!-- /Schedule Content -->

								<input type="hidden" name="pre_date" id="pre_date" class="form-control" value="<?php echo date('Y-m-d', strtotime($selected_date."-1 day")); ?>">
                                <input type="hidden" name="next_date" id="next_date" class="form-control" value="<?php echo date('Y-m-d', strtotime($selected_date."+1 day")); ?>">
								
							</div>
							<!-- /Schedule Widget -->

														
							<!-- Submit Section -->
							<div class="submit-section proceed-btn text-right bookingconfirmation">
								<?php echo $language['lg_you_have_booked'];?> <strong class="pr-2">0 <?php echo $language['lg_slot'];?></strong> <a href="javascript:void(0);"  class="btn btn-primary submit-btn"><?php echo $language['lg_proceed_to_pay'];?></a>
							</div>
							<!-- /Submit Section -->
							
						</div>
					</div>
				</div>

			</div>		
			<!-- /Page Content -->
