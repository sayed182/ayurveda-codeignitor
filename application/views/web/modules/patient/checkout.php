<?php 

$hour = 0;  
foreach ($appointment_details as $key => $value) { 
  $from_date_time =  $value->date_value.' '.$value->start_time;
  $to_date_time =  $value->date_value.' '.$value->end_time;
  $from_timezone = $value->time_zone;                         
  $to_timezone = $this->session->userdata('time_zone');
  $from_date_time  = converToTz($from_date_time,$to_timezone,$from_timezone);
  $to_date_time = converToTz($to_date_time,$to_timezone,$from_timezone);
  $from_time  = date('h:i a',strtotime($from_date_time));
  $to_time  = date('h:i a',strtotime($to_date_time));
 $date_time =  '<span>'.date('d-m-Y',strtotime($value->date_value)).' '.date('g:i a',strtotime($from_time)).'-'.date('g:i a',strtotime($to_time)).'</span>';
 $schedule_type=$value->type;
   $hour++; 
 }
?>
<!-- Breadcrumb -->
			<div class="breadcrumb-bar">
				<div class="container-fluid">
					<div class="row align-items-center">
						<div class="col-md-12 col-12">
							<nav aria-label="breadcrumb" class="page-breadcrumb">
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="<?php echo base_url();?>"><?php echo $language['lg_home'];?></a></li>
									<li class="breadcrumb-item active" aria-current="page"><?php echo $language['lg_checkout'];?></li>
								</ol>
							</nav>
							<h2 class="breadcrumb-title"><?php echo $language['lg_checkout'];?></h2>
						</div>
					</div>
				</div>
			</div>
			<!-- /Breadcrumb -->
			
			<!-- Page Content -->
			<div class="content">
				<div class="container">

					<div class="row">
						<div class="col-md-7 col-lg-8">
							<div class="card">
								<div class="card-body">
								
									<!-- Checkout Form -->
																			
										<div class="payment-widget">
											<h4 class="card-title"><?php echo $language['lg_payment_method'];?></h4>
											
											<!-- Credit Card Payment -->
											<div class="payment-list">
												<label class="payment-radio credit-card-option">
													<input type="radio" value="Card Payment" name="payment_methods">
													<span class="checkmark"></span>
													<?php echo $language['lg_credit_card'];?>
												</label>
											</div>
											<!-- /Credit Card Payment -->
											<div class="stripe_payment" style="display: none;">
											  <form action="#" method="post" id="payment-form">
											  <div>
											    <label for="card-element">
											      <?php echo $language['lg_credit_or_debit'];?>
											    </label>
											    <div id="card-element" style="width: 100%">
											      <!-- A Stripe Element will be inserted here. -->
											    </div>

											    <!-- Used to display form errors. -->
											    <div id="card-errors" role="alert"></div>
											  </div>
											<div class="submit-section mt-4 mb-4">
											  <button class="btn btn-primary submit-btn" id="stripe_pay_btn"><?php echo $language['lg_confirm_and_pay1'];?></button>
											    </div>
											</form>
											</div>
											
											<!-- Paypal Payment -->
											<div class="payment-list">
												<label class="payment-radio paypal-option">
													<input type="radio" value="PayPal" name="payment_methods">
													<span class="checkmark"></span>
													<?php echo $language['lg_paypal'];?>
												</label>
											</div>
											<!-- /Paypal Payment -->

											<!-- Paypal Payment -->
											<div class="payment-list">
												<label class="payment-radio paypal-option">
													<input type="radio" value="Razorpay" name="payment_methods">
													<span class="checkmark"></span>
													Razorpay
												</label>
											</div>
											<!-- /Paypal Payment -->

											<?php if($schedule_type=='clinic') { ?>
												  <div class="payment-list">
												    <label class="payment-radio credit-card-option">
												      <input type="radio" value="Pay on Arrive" name="payment_methods" >
												      <span class="checkmark"></span>
												      <?php echo $language['lg_pay_on_arrive'];?>
												    </label>
												  </div>
											<?php } ?>
											
											
											 <div class="terms-accept">
												
											</div> 
											<!-- /Terms Accept -->
											
											<!-- Submit Section -->
											<div class="submit-section mt-4">
												<div class="paypal_payment" style="display: none;">
												<div class="submit-section mt-4">
												  <button type="button"  id="pay_buttons" onclick="appoinment_payment('paypal')" class="btn btn-primary submit-btn"><?php echo $language['lg_confirm_and_pay'];?></button>
												</div>
												</div>
												<div class="razorpay_payment" style="display: none;">
												<div class="submit-section mt-4">
												  <button type="button"  id="razor_pay_btn" onclick="appoinment_payment('razorpay')" class="btn btn-primary submit-btn">Confirm and Pay with Razorpay</button>
												</div>
												</div>
												<div class="clinic_payment" style="display: none;">
												<div class="submit-section mt-4">
												   <button type="button" id="pay_button" onclick="appoinment_payment('stripe')" class="btn btn-primary submit-btn"><?php echo $language['lg_book_appointmen'];?></button>
												</div>
												</div>
											</div>
											<!-- /Submit Section -->
											
										</div>

										<form role="form" method="POST" id="payment_formid" action="<?php echo base_url().'book_appoinments/paypal_pay';?>">
						 <?php					
						 $address = !empty($patients['address1'])?$patients['address1']:$language['lg_no_address_spec']; 
						 $info = $language['lg_booking_appoinm'].' '.$doctors['first_name'].' '.$doctors['last_name']; 
			              ?>
			              <input type="hidden" name="productinfo" id="productinfo"   value="<?php echo $info ?>" />
			              <input type="hidden"  name="name" id="name"  value="<?php echo $patients['first_name'].' '.$patients['last_name']; ?>" /> 
			              <input type="hidden"  name="phone" id="phone"  value="<?php echo $patients['mobileno'] ?>"/>  
			              <input type="hidden"  name="email" id="email"  value="<?php echo $patients['email'] ?>" />
			              <input type="hidden"  name="address1" id="address1" value="<?php echo $address; ?>">
			              <input type="hidden" class="form-control" id="amount" name="amount" value="<?php echo $this->session->userdata('total_amount'); ?>"  readonly/>
			              <input type="hidden" class="form-control" id="currency_code" name="currency_code" value="<?php echo $this->session->userdata('currency_code'); ?>"  readonly/>
			              <input type="hidden" name="access_token" id="access_token" > 
			              <input type="hidden" name="payment_id" id="payment_id" > 
			              <input type="hidden" name="order_id" id="order_id" > 
			              <input type="hidden" name="signature" id="signature" > 
			              


			              <input type="hidden" name="payment_method" id="payment_method" value="Card Payment" >
										</form>
									
									<!-- /Checkout Form -->
									
								</div>
							</div>

							<div class="card">
								<div class="card-body">
                              <center><h5 style="color:grey">Your transactions are 100 % secure</h5></center>
                              <center><img src="<?php echo base_url();?>assets/img/payments.png" width="350"></center>
								</div>
							</div>
							
						</div>
						
						<div class="col-md-5 col-lg-4 theiaStickySidebar">
						
							<!-- Booking Summary -->
							<div class="card booking-card">
								<div class="card-header">
									<h4 class="card-title"><?php echo $language['lg_booking_summary'];?></h4>
								</div>
								<div class="card-body">
									<?php
											$profileimage=(!empty($doctors['profileimage']))?base_url().$doctors['profileimage']:base_url().'assets/img/user.png';
										?>
								
									<!-- Booking Doctor Info -->
									<div class="booking-doc-info">
										<a href="<?php echo base_url().'doctor-preview/'.$doctors['username'];?>" class="booking-doc-img">
											<img src="<?php echo $profileimage;?>" alt="User Image">
										</a>
										<div class="booking-info">
											<h4><a href="<?php echo base_url().'doctor-preview/'.$doctors['username'];?>"><?php echo $language['lg_dr'];?> <?php echo ucfirst($doctors['first_name'].' '.$doctors['last_name']);?></a></h4>
											<div class="rating">
												<?php
						                        $rating_value=$doctors['rating_value'];
						                        for( $i=1; $i<=5 ; $i++) {
						                          if($i <= $rating_value){                                        
						                          echo'<i class="fas fa-star filled"></i>';
						                          }else { 
						                          echo'<i class="fas fa-star"></i>';
						                          } 
						                        } 
						                      ?>
												<span class="d-inline-block average-rating">(<?php echo $doctors['rating_count'];?>)</span>
											</div>
											<div class="clinic-details">
												<p class="doc-location"><i class="fas fa-map-marker-alt"></i> <?php echo $doctors['cityname'].', '. $doctors['countryname'];?></p>
											</div>
										</div>
									</div>
									<!-- Booking Doctor Info -->
									
									<div class="booking-summary">
										<div class="booking-item-wrap">
											<ul class="booking-date">
										<li><?php echo $language['lg_date1'];?> <span> <?php
					                       foreach ($appointment_details as $key => $value) { 
						                      $from_date_time =  $value->date_value.' '.$value->start_time;
						                      $to_date_time =  $value->date_value.' '.$value->end_time;
						                      $from_timezone = $value->time_zone;                         
						                      $to_timezone = $this->session->userdata('time_zone');
						                      $from_date_time  = converToTz($from_date_time,$to_timezone,$from_timezone);
						                      $to_date_time = converToTz($to_date_time,$to_timezone,$from_timezone);
						                      $from_time  = date('h:i a',strtotime($from_date_time));
						                      $to_time  = date('h:i a',strtotime($to_date_time));
						                     echo $date_time = date('d-M-Y',strtotime($value->date_value)).'<br>';
						                      }
	                                       ?></span>
                                        </li><br>
										<li><?php echo $language['lg_date1'];?>Time <span><?php
					                       foreach ($appointment_details as $key => $value) { 
					                      $from_date_time =  $value->date_value.' '.$value->start_time;
					                      $to_date_time =  $value->date_value.' '.$value->end_time;
					                      $from_timezone = $value->time_zone;                         
					                      $to_timezone = $this->session->userdata('time_zone');
					                      $from_date_time  = converToTz($from_date_time,$to_timezone,$from_timezone);
					                      $to_date_time = converToTz($to_date_time,$to_timezone,$from_timezone);
					                      $from_time  = date('h:i a',strtotime($from_date_time));
					                      $to_time  = date('h:i a',strtotime($to_date_time));
					                     echo $date_time =  date('g:i a',strtotime($from_time)).' - '.date('g:i a',strtotime($to_time)).'<br>';
					                      // $hour++; 
					                     } ?></span>
					                    </li>
											</ul><br>
											<?php
										  $tax=!empty(settings("tax"))?settings("tax"):"0";;
										  $amount = $this->session->userdata('amount');
                                          $transcation_charge = $this->session->userdata('transcation_charge');
							              $tax_amount = $this->session->userdata('tax_amount');                
							              $total_amount = $this->session->userdata('total_amount'); 

							              $rate_symbol=$this->session->userdata('currency_symbol');

							                ?>    
											<ul class="booking-fee">
												<li><?php echo $language['lg_call_charge'];?> <span><?php echo $rate_symbol;?><?php echo number_format($amount,2); ?></span></li>
												<li><?php echo $language['lg_transaction_cha'];?> <span><?php echo $rate_symbol;?><?php echo number_format($transcation_charge,2); ?></span></li>
												<li><?php echo $language['lg_tax'];?> (<?php echo $tax ?>%)<span><?php echo $rate_symbol;?><?php echo $tax_amount; ?></span></li>
											</ul>
											<div class="booking-total">
												<ul class="booking-total-list">
													<li>
														<span><?php echo $language['lg_total1'];?></span>
														<span class="total-cost"><?php echo $rate_symbol;?><?php echo number_format($total_amount,2); ?></span>
													</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- /Booking Summary -->
							
						</div>
					</div>

				</div>

			</div>	

			  <button id="my_book_appoinment" style="display: none;"><?php echo $language['lg_purchase'];?></button>	
			<!-- /Page Content -->

			
				<?php
				$stripe_option=!empty(settings("stripe_option"))?settings("stripe_option"):"";
		         if($stripe_option=='1'){
		            $stripe_api_key=!empty(settings("sandbox_api_key"))?settings("sandbox_api_key"):"";
		         }
		         if($stripe_option=='2'){
		            $stripe_api_key=!empty(settings("live_api_key"))?settings("live_api_key"):"";
		         }
		         ?>
			
			<script type="text/javascript">
				var stripe_api_key='<?php echo $stripe_api_key;?>';
			</script>