<!-- Breadcrumb -->
			<div class="breadcrumb-bar">
				<div class="container-fluid">
					<div class="row align-items-center">
						<div class="col-md-12 col-12">
							<nav aria-label="breadcrumb" class="page-breadcrumb">
								<ol class="breadcrumb">
									<li class="breadcrumb-item"><a href="index.html">Home</a></li>
									<li class="breadcrumb-item active" aria-current="page">Accounts</li>
								</ol>
							</nav>
							<h2 class="breadcrumb-title">Accounts</h2>
						</div>
					</div>
				</div>
			</div>
			<!-- /Breadcrumb -->
			
			<!-- Page Content -->
			<div class="content">
				<div class="container-fluid">
					
					<div class="row">
						<div class="col-md-5 col-lg-4 col-xl-3 theiaStickySidebar">
							
							<!-- Profile Sidebar -->
							<?php $this->load->view('web/includes/patient_sidebar');?>
							<!-- /Profile Sidebar -->
							
						</div>
						
						<div class="col-md-7 col-lg-8 col-xl-9">
						<div id="account">
					<div class="row">
						<div class="col-lg-5 d-flex">
							<div class="card flex-fill">
								<div class="card-header">
									<div class="row">
										<div class="col-sm-6">
											<h3 class="card-title">Account</h3>
										</div>
										<div class="col-sm-6">
											<div class="text-right">
												<a title="Edit Profile" class="btn btn-primary btn-sm"  onclick="add_account_details()"><i class="fas fa-pencil"></i> Edit Details</a>
											</div>
										</div>
									</div>
								</div>
								<div class="card-body">
									<div class="profile-view-bottom">
										<div class="row">
											<div class="col-lg-6">
												<div class="info-list">
													<div class="title">Bank Name</div>
													<div class="text" id="bank_name"><?php echo isset($account_details->bank_name)?$account_details->bank_name:'';?></div>
												</div>
											</div>
											<div class="col-lg-6">
												<div class="info-list">
													<div class="title">Branch Name</div>
													<div class="text" id="branch_name"><?php echo isset($account_details->branch_name)?$account_details->branch_name:'';?></div>
												</div>
											</div>
											<div class="col-lg-6">
												<div class="info-list">
													<div class="title">Account Number</div>
													<div class="text" id="account_no"><?php echo isset($account_details->account_no)?$account_details->account_no:'';?></div>
												</div>
											</div>
											<div class="col-lg-6">
												<div class="info-list">
													<div class="title">Account Name</div>
													<div class="text" id="account_name"><?php echo isset($account_details->account_name)?$account_details->account_name:'';?></div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-7 d-flex">
							<div class="card flex-fill">
                                <div class="card-header border-0">
                                    <div class="col-sm-6">
                                        <h3 class="card-title">Payment Methods</h3>
                                    </div>
                                </div>
								<div class="card-body">
                                    <ul class="pay-methods">
                                        <li class="list-item">
                                            <span class="pay-method--icon"><img src="<?php echo base_url('assets/img/card-pay.png')?>" alt="" class="img-responsive"></span><span>Credit / Debit Card</span>
                                        </li>
                                        <li class="list-item">
                                            <span class="pay-method--icon"><img src="<?php echo base_url('assets/img/google-pay.png')?>" alt="" class="img-responsive"></span><span>Google Pay</span>
                                        </li>
                                        <li class="list-item">
                                            <span class="pay-method--icon"><img src="<?php echo base_url('assets/img/amazon-pay.png')?>" alt="" class="img-responsive"></span><span>Amazon Pay</span>
                                        </li>
                                        <li class="list-item">
                                            <span class="pay-method--icon"><img src="<?php echo base_url('assets/img/paypal.png')?>" alt="" class="img-responsive"></span><span>Paypal</span>
                                        </li>
                                    </ul>
								</div>
							</div>
						</div>
					</div>
					
					<div class="user-tabs">
					<ul class="nav nav-tabs nav-tabs-bottom nav-justified flex-wrap">
						<li class="nav-item">
							<a class="nav-link active" onclick="paccount_table()" href="#account_tab" data-toggle="tab">Accounts</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" onclick="doctor_request()" href="#doctor_request_tab" data-toggle="tab"><span>Doctor Request</span></a>
						</li>
					</ul>
				   </div>
				   <div class="tab-content">
					<div id="account_tab" class="tab-pane fade show active">
						<div class="card card-table">  
							<div class="card-body">  
								<div class="table-responsive">
									<table id="accounts_table" class="table table-hover table-center mb-0">
										<thead>
											<tr>
												<th>Date</th>
												<th>Doctor Name</th>
												<th>Amount</th>
												<th>Status</th>
												<th>Actions</th>
											</tr>
										</thead>
										<tbody>
											
										</tbody>
									</table>
								</div>
							</div>
						</div>
			   	    </div>
			   	    <div id="doctor_request_tab" class="tab-pane fade">
					<div class="card card-table">  
						<div class="card-body">  
							<div class="table-responsive">
								<table id="doctor_request" style="width: 100%" class="table table-hover table-center mb-0">
									<thead>
										<tr>
											<th>Date</th>
											<th>Doctor Name</th>
											<th>Amount</th>
											<th>Status</th>
											<th>Actions</th>
										</tr>
									</thead>
									<tbody>
										
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
				
				</div>
						</div>

					</div>
				</div>

			</div>		
			<!-- /Page Content -->
   