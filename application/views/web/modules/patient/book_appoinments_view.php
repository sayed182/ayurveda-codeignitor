                        
<!-- Breadcrumb -->
<?php 
   
  function get_booked_class($availabe_days,$start_time,$end_time,$day_value,$to_timezone)
  {
   $class = 'available';
   if(!empty($availabe_days)){
     foreach ($availabe_days as $key => $value) {      
      $from_timezone = $value['time_zone'];    
      $from_time = $value['appointment_date'].' '.$value['appointment_time'];
      $to_time = $value['appointment_date'].' '.$value['appointment_end_time'];
      $from_date_time  = converToTz($from_time,$to_timezone,$from_timezone);
      $to_date_time = converToTz($to_time,$to_timezone,$from_timezone);        

      if( date('H:i:s',strtotime($from_time)) == date('H:i:s',strtotime($start_time)) && date('H:i:s',strtotime($to_time))== date('H:i:s',strtotime($end_time)) && date('Y-m-d', strtotime($from_time)) ==date('Y-m-d', strtotime("+$day_value day"))){
        $class = 'notavailable';
      }
    }
  }
  return $class;
}

$time_zone = $this->session->userdata('time_zone');
date_default_timezone_set($time_zone);
$date =  date('Y-m-d');
if($date >= $selected_date){
  $class = 'd-none';
}else{
  $class = '';
}

?>
                        <!-- Schedule Header -->
								<div class="schedule-header">
									<div class="row">
										<div class="col-md-12">
										
											<!-- Day Slot -->
											<div class="day-slot">
												<ul>
													<li class="left-arrow <?php echo $class;?>">
														<a href="javascript:void(0);" onclick="getSchedule(1)">
															<i class="fa fa-chevron-left"></i>
														</a>
													</li>
													<?php
													 $num = 1;
                                                     for($i=0;$i<1;$i++) { 
													 ?>
													<li>
														<span><?php echo date('D', strtotime($selected_date."+$i day")); ?></span>
														<span class="slot-date"><?php echo date('d M Y', strtotime($selected_date."+$i day")); ?></span>
													</li>
												    <?php } ?>
													
													<li class="right-arrow">
														<a href="javascript:void(0);" onclick="getSchedule(2)">
															<i class="fa fa-chevron-right"></i>
														</a>
													</li>
												</ul>
											</div>
											<!-- /Day Slot -->
											
										</div>
									</div>
								</div>
								<!-- /Schedule Header -->
								
								<!-- Schedule Content -->
								<div class="schedule-cont">
									<div class="row">
										<div class="col-md-12">
										
											<!-- Time Slot -->
											<div class="time-slot">
												<ul class="clearfix">
													<li>
											    <?php 
											    $count=null;
									            foreach ($schedule as $r) { 
									              $date =  date('Y-m-d');   
									              if($r['day_name'] == date('l', strtotime($selected_date."+0 day"))) {
									              	$count=1;
									                $rep_start1 = $r['from_time'];
									                $rep_end1 = $r['to_time'];
									                $from_timezone = $r['time_zone'];                         
									                $to_timezone = $this->session->userdata('time_zone');
									                $from_time =  $selected_date.' '.$rep_start1;
									                $from_time =  converToTz($from_time,$to_timezone,$from_timezone);
									                if(date('Y-m-d H:i:s') < $from_time){
									                  
									                 $from_time =  $selected_date.' '.$rep_start1;
									                 $to_time =  $selected_date.' '.$rep_end1;
									                 $to_timezone = $this->session->userdata('time_zone');
									                 $from_date_time  = converToTz($from_time,$to_timezone,$from_timezone);
									                 $to_date_time = converToTz($to_time,$to_timezone,$from_timezone);
									                 $from_time  = date('h:i a',strtotime($from_date_time));
									                 $to_time  = date('h:i a',strtotime($to_date_time));              
									                 $class =  get_booked_class($available_data,$rep_start1,$rep_end1,0,$to_timezone);    ?>
													
														 <?php if($class == 'available'){ ?>
										                    <a href="javascript:void(0)" class="timing available_slot <?php echo $r['type'];?>" data-date="<?php echo date('Y-m-d', strtotime($selected_date."+0 day")); ?>" data-type="<?php echo $r['type']; ?>" data-day="<?php echo $r['day_name']; ?>" data-day-id="<?php echo $r['day_id']; ?>"  data-start-time="<?php echo $rep_start1; ?>" data-end-time="<?php echo $rep_end1; ?>" data-timezone="<?php echo $r['time_zone'] ?>" ><?php echo date('h:i a',strtotime($from_time)); // From time  ?> - <?php echo date('h:i a',strtotime($to_time)).' ('.$r['type'].')'; // to time  ?></a>
										                  <?php 
										                }else{                  
										                   echo '<a class="timing" href="javascript:void(0)">'.date('h:i a',strtotime($from_time)).' - '.date('h:i a',strtotime($to_time)).'</a>';
										                 } ?>

														
													
												<?php } }  }


												



												    ?>
												</li>
												
												
													
													
													
													
												</ul>

												<?php 



												if($count != 1){

													echo "<center>No slots available</center>";

												}


												?>
											</div>
											<!-- /Time Slot -->
											
										</div>
									</div>
								</div>
								<!-- /Schedule Content -->

								<input type="hidden" name="pre_date" id="pre_date" class="form-control" value="<?php echo date('Y-m-d', strtotime($selected_date."-1 day")); ?>">
                                <input type="hidden" name="next_date" id="next_date" class="form-control" value="<?php echo date('Y-m-d', strtotime($selected_date."+1 day")); ?>">
