<!DOCTYPE html> 
<html lang="en">
<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
	<title><?php echo !empty(settings("meta_title"))?settings("meta_title"):"Doccure";?></title>
	<meta content="<?php echo !empty(settings("meta_keywords"))?settings("meta_keywords"):"";?>" name="keywords">
	<meta content="<?php echo !empty(settings("meta_description"))?settings("meta_description"):"";?>" name="description">
	<!-- Favicons -->
	<link href="<?php echo !empty(base_url().settings("favicon"))?base_url().settings("favicon"):base_url()."assets/img/favicon.png";?>" rel="icon">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">

	<!-- Fontawesome CSS -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/fontawesome/css/fontawesome.min.css">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/fontawesome/css/all.min.css">

	<!-- Main CSS -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="assets/js/html5shiv.min.js"></script>
<script src="assets/js/respond.min.js"></script>
<![endif]-->

</head>
<body class="call-page video-page">

	<!-- Main Wrapper -->
	<div class="main-wrapper">



		<!-- Page Content -->
		<div class="content">

			<!-- Call Wrapper -->
			<div class="call-wrapper">
				<div class="call-main-row">
					<div class="call-main-wrapper">
						<div class="call-view">
							<div class="call-window">

								<?php
								if($role=='doctor')
								{
									$profileimage=(!empty($appoinments_details['doctor_profileimage']))?base_url().$appoinments_details['doctor_profileimage']:base_url().'assets/img/user.png';
									$name ='Dr. '.ucfirst($appoinments_details['doctor_name']);
								}
								if($role=='patient')
								{
									$profileimage=(!empty($appoinments_details['patient_profileimage']))?base_url().$appoinments_details['patient_profileimage']:base_url().'assets/img/user.png';
									$name =ucfirst($appoinments_details['patient_name']);
								}

								?>

								<!-- Call Header -->
								<div class="fixed-header">
									<div class="navbar">
										<div class="user-details">
											<div class="float-left user-img">
												<a class="avatar avatar-sm mr-2" href="#" title="Charlene Reed">
													<img src="<?php echo $profileimage;?>" alt="User Image" class="rounded-circle">
													<span class="status online"></span>
												</a>
											</div>
											<div class="user-info float-left">
												<a href="#"><span><?php echo $name;?></span></a>

											</div>
										</div>

									</div>
								</div>
								<!-- /Call Header -->

								<!-- Call Contents -->
								<div class="call-contents">
									<div class="call-content-wrap">
										<div class="user-video">
											<div id="subscriber"></div>
										</div>
										<div class="my-video">
											<div id="publisher"></div>
										</div>
									</div>
								</div>
								<!-- Call Contents -->

								<!-- Call Footer -->
								<div class="call-footer">
									<div class="call-icons">

										<ul class="call-items">
											<li class="call-item">
												<a href="javascript:void(0);" id="videocall" title="Enable Video" data-placement="top" data-toggle="tooltip">
													<i class="fas fa-video camera"></i>
												</a>
												
												<a href="javascript:void(0);" id="audiocall" title="Disable Video" data-placement="top" data-toggle="tooltip">
													<i class="fas fa-video camera"></i>
												</a>

											</li>
											<li class="call-item">
												<a class="call-end" id="endcall" href="javascript:void(0);">
													<i class="material-icons">call_end</i>
												</a>
											</li>
										</ul>
									</div>
								</div>
								<!-- /Call Footer -->

							</div>
						</div>

					</div>
				</div>
			</div>
			<!-- /Call Wrapper -->


		</div>		
		<!-- /Page Content -->

		<!-- Footer -->

		<!-- /Footer -->

	</div>
	<!-- /Main Wrapper -->

	<script type="text/javascript">
		var base_url='<?php echo base_url();?>';
		var roles='<?php echo $this->session->userdata('role');?>'
	</script>

	<!-- jQuery -->
	<script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>

	<!-- Bootstrap Core JS -->
	<script src="<?php echo base_url();?>assets/js/popper.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>

	<script src="https://static.opentok.com/v2/js/opentok.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/appoinments.js"></script>

	<script type="text/javascript">
// replace these values with those generated in your TokBox Account

$(document).ready(function () {
	var apiKey = '<?php echo !empty(settings("apiKey"))?settings("apiKey"):"";?>';
	var sessionId = '<?php echo $appoinments_details['tokboxsessionId'];?>';
	var token = '<?php echo $appoinments_details['tokboxtoken'];?>';
	var appoinment_id='<?php echo md5($appoinments_details['id']);?>';




// Handling all of our errors here by alerting them
function handleError(error) {
	if (error) {
		window.close();
	}
}

var videoOn=false;
var session = OT.initSession(apiKey, sessionId);
$('#videocall').hide();


 
 

// (optional) add server code here
//initializeSession();


// Connect to the session
session.connect(token, function(error) {
// If the connection is successful, initialize a publisher and publish to the session
if (error) {
	handleError(error);
} else {


// Subscribe to a newly created stream
var subscriber=session.on('streamCreated', function(event) {
	session.subscribe(event.stream, 'subscriber', {
		insertMode: 'append',
		width: '100%',
		height: '100%'
	}, handleError);
});
subscriber.on("videoDisabled", function(event) {
// You may want to hide the subscriber video element:
domElement = document.getElementById(subscriber.id);
domElement.style["visibility"] = "hidden";

// You may want to add or adjust other UI.
});
// console.log(subscriber);
// Create a publisher
var publisher = OT.initPublisher('publisher', {
	insertMode: 'append',
	width: '100%',
	height: '100%',
	publishAudio:true, 
    publishVideo:false
}, handleError);
$('#endcall').click(function () {  
	var pubOptions = {publishAudio:false, publishVideo:false};
	publisher.destroy();
	publisher = OT.initPublisher("publisher", pubOptions).publishVideo(true);
	videoOn = true;

	session.publish(publisher);
	end_call(appoinment_id);
window.close();

});
// var div_len=0;
// var id='';
// setInterval(function(){ 

// 	div_len=$("#subscriber").children('div').length;
// 	console.log(div_len);

// 	if(div_len == 1){
//       bool=true;  

// 	}

// 	if(div_len>=2){
// 		id=$("#subscriber").find('div').first().attr('id');
// 		$('#'+id).remove();
// 	}

// 	if(bool == true){

// 		if(div_len == 0){
// 			$( "#endcall" ).trigger( "click" );
// 		}
// 	}


// }, 1000);
//audio calling enable
$('#audiocall').click(function () { 

	var pubOptions = {publishAudio:true, publishVideo:false};
	publisher = OT.initPublisher('publisher', pubOptions);

	session.publish(publisher);
	$('#audiocall').hide();
	$('#videocall').show();
});
//end audio calling
//video calling enable
$('#videocall').click(function () {  
	var pubOptions = {publishVideo:true};
	publisher = OT.initPublisher('publisher', pubOptions);
	session.publish(publisher);
	$('#audiocall').show();
	$('#videocall').hide();
});
//end audio calling

session.publish(publisher, handleError);
}

session.on("sessionDisconnected", function(event) {
     alert("The session disconnected. " + event.reason);
 });

});

});



</script>

<style type="text/css">
	#subscriber {
		position: absolute;
		left: 0;
		top: 0;
		width: 100%;
		height: 100%;
		z-index: 10;
	}

	#publisher {
		position: absolute;
		width: 360px;
		height: 240px;
		bottom: 10px;
		left: 10px;
		z-index: 100;
		border: 3px solid white;
		border-radius: 3px;
	}
</style>


</body>
</html>