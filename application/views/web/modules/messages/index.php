<!--Page Content -->
			<div class="content">
				<div class="container-fluid">
					<a class="text-primary d-block mt-3 mb-3 ml-2" href="<?php echo base_url();?>dashboard"><i class="fas fa-arrow-left mr-1"></i>Back</a> 
					<div class="row">
						<div class="col-xl-12">
							<div class="chat-window">
							
								<!-- Chat Left -->
								<div class="chat-cont-left">
									<div class="chat-header">
										<span><?php echo $language['lg_chats'];?></span>
										
									</div>
									<form class="chat-search">
										<div class="input-group">
											<div class="input-group-prepend">
												<i class="fas fa-search"></i>
											</div>
											<input type="text" onkeyup="search_user()" id="search_users" class="form-control" placeholder="<?php echo $language['lg_search3'];?>">
										</div>
									</form>
									<input type="hidden" name="" id="user_selected_id" value="">
									<div class="chat-users-list">
										<div class="chat-scroll chat_users">
											<?php if(!empty($users)){
												foreach ($users as $rows) {

											$profileimage=(!empty($rows['profileimage']))?base_url().$rows['profileimage']:base_url().'assets/img/user.png';
													
											?>		
											<a href="javascript:void(0);" onclick="get_chat_img('<?php echo $rows['userid']; ?>','<?php echo $rows['username']; ?>','<?php echo $rows['first_name'].' '.$rows['last_name']; ?>')" class="media chat-data state<?php echo $rows['userid']; ?>">
														<div class="media-img-wrap">
															<div class="avatar">
																<img src="<?php echo $profileimage; ?>" alt="User Image" class="avatar-img rounded-circle">
															</div>
														</div>
												<div class="media-body">
													<div>
														<div class="user-name"><?php echo ($rows['role']==1)?$language['lg_dr'].' '.$rows['first_name'].' '.$rows['last_name']:$rows['first_name'].' '.$rows['last_name'];?></div>
														<!-- <div class="user-last-chat">Hey, How are you?</div> -->
													</div>
													<!-- <div>
														<div class="last-chat-time block">2 min</div>
														<div class="badge badge-success badge-pill">15</div>
													</div> -->
												</div>
											</a>
											<?php } } else {
												echo $language['lg_no_users_found'];
											} ?>
										</div>
									</div>
								</div>
								<!-- /Chat Left -->
							
								<!-- Chat Right -->
								<div class="chat-cont-right">
									<div class="chat-header">
										<a id="back_user_list" href="javascript:void(0)" class="back-user-list">
											<i class="material-icons">chevron_left</i>
										</a>
										<div class="media">
											<div class="media-img-wrap">
												<div class="avatar chat-img">
													
												</div>
											</div>
											<div class="media-body">
												<div class="user-name user-info pull-left openchat"></div>
												<div class="user-status"></div>
											</div>
										</div>
										<!-- <div class="chat-options chattrash">
											<a href="javascript:void(0)" onclick="delete_conversation();" title="Delete Chat History">
												<i class="far fa-trash-alt"></i>
											</a>
										</div>  -->
										<div class="progress upload-progress d-none">
											<div class="progress-bar progress-bar-success active progress-bar-striped" role="progressbar" aria-valuenow="100" aria-valuemin="100" aria-valuemax="100" style="width: 100%;">
											 <?php echo $language['lg_uploading'];?>  
											</div>
										</div> 
									</div>
									<div id="chat-box" class="chatbox-message chat-body">
										<div class="chat-scroll slimscrollleft">
											<ul class="list-unstyled chats">

											</ul>
										</div>
									</div>
							
									<div class="chat-footer" id="chat" onsubmit="return false">
										<form class="input-group" name="chat_form" id="chat_form" enctype="multipart/form-data" method="post" action="<?php echo base_url(); ?>messages/upload_files">
											<div class="input-group-prepend">
												<a class="link attach-icon btn-file btn" href="javascript:void(0)" onclick="$('#user_file').click();">
													<i class="fa fa-paperclip"></i>
												</a>
											</div>
											<input type="text" name="input_message" id="input_message" placeholder="Type something" class="input-msg-send form-control chat-input" autocomplete="off">
											<input  type="hidden" id="recipients" value="username" >
											<input type="hidden" name="receiver_id" id="receiver_id">
											<input type="hidden" name="to_user_id" id="to_user_id">
											<input type="hidden" name="time" id="time" > 
											<input type="file" name="userfile" id="user_file" class="d-none">
											<div class="input-group-append">
												<a class="link btn msg-send-btn chat-send-btn" href="javascript:void(0)" id="chat-send-btn"><i class="fab fa-telegram-plane"></i></a> 
											</div>
										</form>
									</div>
								</div>
								<!-- /Chat Right -->
								
							</div>
						</div>
					</div>
					<!-- /Row -->

				</div>

			</div>		
			<!-- /Page Content-->
			<?php
			$user_detail=user_detail($this->session->userdata('user_id'));
            $user_profile_image=(!empty($user_detail['profileimage']))?base_url().$user_detail['profileimage']:base_url().'assets/img/user.png';
			?>
			<input type="hidden" name="img" id="img" value="<?php echo $user_profile_image; ?>">



