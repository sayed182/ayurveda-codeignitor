
      <!-- Footer -->
      <footer class="footer">
        
        <!-- Footer Top -->
        <div class="footer-top">
          <div class="container">
            <div class="row">
              <div class="col-lg-3 col-md-6">
              
                <!-- Footer Widget -->
                <div class="footer-widget footer-about">
                  <div style="width: 213px;height: 71px;">
                     <img  src="<?php echo !empty(base_url().settings("logo_footer"))?base_url().settings("logo_footer"):base_url()."assets/img/logo.png";?>" alt="logo"> 
                    
                  </div>
                  <div class="footer-about-content footer-widget footer-menu">
                    <h4 class="footer-title mt-2 mb-3">Reach Us</h4>
                    <ul class="list-unstyled ">
                      <li>
                        <a href="<?php echo base_url(); ?>home/about">About</a>
                      </li>
                      <li>
                        <a href="<?php echo base_url(); ?>blog">Blog</a>
                      </li>
                     
                      <li>
                        <a href="<?php echo base_url(); ?>home/contact">Contact Us</a>
                      </li>
                    </ul>
                     
                    <div class="social-icon">
                      <ul>
                        <?php 
                        if(!empty(settings("facebook")))
                        {
                          echo'<li>
                                <a href="'.base_url().settings("facebook").'" target="_blank"><i class="fab fa-facebook-f"></i> </a>
                              </li>';
                        }
                        
                        if(!empty(settings("twitter")))
                        {
                          echo'<li>
                                <a href="'.base_url().settings("twitter").'" target="_blank"><i class="fab fa-twitter"></i> </a>
                              </li>';
                        }
                              
                        if(!empty(settings("linkedIn")))
                        {
                          echo'<li>
                              <a href="'.base_url().settings("linkedIn").'" target="_blank"><i class="fab fa-linkedin-in"></i></a>
                              </li>';
                        }
                        if(!empty(settings("instagram")))
                        {
                          echo'<li>
                              <a href="'.base_url().settings("instagram").'" target="_blank"><i class="fab fa-instagram"></i></a>
                              </li>';
                        }      
                        if(!empty(settings("google_plus")))
                        {
                          echo'<li>
                               <a href="'.base_url().settings("google_plus").'" target="_blank"><i class="fab fa-google-plus"></i> </a>
                              </li>';
                        }
                        ?>      
                      </ul>
                    </div>
                  </div>
                </div>
                <!-- /Footer Widget -->
                
              </div>
              
              <div class="col-lg-3 col-md-6">
              
                <!-- Footer Widget -->
                <div class="footer-widget footer-menu mt-2">
                  <h2 class="footer-title"><?php echo $language['lg_for_patients'];?></h2>
                  <ul>
                    <li><a href="<?php echo base_url();?>doctors-search"><?php echo $language['lg_search_for_doct'];?></a></li>
                    <li><a href="<?php echo base_url();?>signin"><?php echo $language['lg_login'];?></a></li>
                    <li><a href="<?php echo base_url();?>register"><?php echo $language['lg_register'];?></a></li>
                    <li><a href="<?php echo base_url();?>dashboard"><?php echo $language['lg_patient_dashboa'];?></a></li>
                    <li><a href="#">Booking Appointments</a></li>
                  </ul>
                </div>
                <!-- /Footer Widget -->
                
              </div>
              
              <div class="col-lg-3 col-md-6">
              
                <!-- Footer Widget -->
                <div class="footer-widget footer-menu mt-2">
                  <h2 class="footer-title"><?php echo $language['lg_for_doctors'];?></h2>
                  <ul>
                    <li><a href="<?php echo base_url();?>appoinments"><?php echo $language['lg_appointments'];?></a></li>
                    <li><a href="<?php echo base_url();?>messages"><?php echo $language['lg_chat1'];?></a></li>
                    <li><a href="<?php echo base_url();?>signin"><?php echo $language['lg_login'];?></a></li>
                    <li><a href="<?php echo base_url();?>register"><?php echo $language['lg_register'];?></a></li>
                    <li><a href="<?php echo base_url();?>dashboard"><?php echo $language['lg_doctor_dashboar'];?></a></li>
                  </ul>
                </div>
                <!-- /Footer Widget -->
                
              </div>
              
              <div class="col-lg-3 col-md-6">
              
                <!-- Footer Widget -->
                <div class="footer-widget footer-contact">
                  <!-- <h2 class="footer-title"><?php echo $language['lg_contact_us'];?></h2> -->
                  <h2 class="footer-title"><span>Ayurwayda</span> App Is Coming Soon</h2>

                  <ul class="list-unstyled list-inline-item app-store">
                    <li class="list-inline-item">
                      <a href="#">
                          <img src="<?php echo base_url();?>assets/img/google-play.png" alt="Image">
                      </a>
                    </li>
                    <li class="list-inline-item">
                      <a href="#">
                          <img src="<?php echo base_url();?>assets/img/app-store.png" alt="Image">
                      </a>
                    </li>
                  </ul>
                  <h4 class="footer-title text-right mt-5 mb-3">Follow Us On</h4>
                  <ul class="mb-0 list-unstyled list-inline-item social-icons float-right">
                    <li class="list-inline-item">
                      <a href="#"><i class="fab fa-linkedin-in"></i></a>
                    </li>
                    <li class="list-inline-item">
                      <a href="#"><i class="fab fa-facebook-f"></i></a>
                    </li>
                    <li class="list-inline-item">
                      <a href="#"><i class="fab fa-instagram"></i></a>
                    </li>
                    <li class="list-inline-item">
                      <a href="#"><i class="fab fa-youtube"></i></a>
                    </li>
                    <li class="list-inline-item">
                      <a href="#"><i class="fab fa-twitter"></i></a>
                    </li>
                  </ul>
                 <!--  <div class="footer-contact-info">
                    <div class="footer-address">
                      <span><i class="fas fa-map-marker-alt"></i></span>
                      <p> <?php echo !empty(settings("address"))?settings("address"):"";?> <?php echo !empty(settings("zipcode"))?settings("zipcode"):"";?> </p>
                    </div>
                    <p>
                      <i class="fas fa-phone-alt"></i>
                      <?php echo !empty(settings("contact_no"))?settings("contact_no"):"9876543210";?>
                    </p>
                    <p class="mb-0">
                      <i class="fas fa-envelope"></i>
                      <?php echo !empty(settings("email"))?settings("email"):"";?>
                    </p>
                  </div> -->
                </div>
                <!-- /Footer Widget -->
                
              </div>
              
            </div>
          </div>
        </div>
        <!-- /Footer Top -->
        
        <!-- Footer Bottom -->
                <div class="footer-bottom">
          <div class="container">
          
            <!-- Copyright -->
            <div class="copyright">
              <div class="row">
                <div class="col-md-6 col-lg-6">
                  <div class="copyright-text">
                    <!-- <p class="mb-0">&copy; <?php echo date('Y');?> <?php echo !empty(settings("website_name"))?settings("website_name"):"Doccure";?>. </p> -->
                    <p class="mb-0">© 2020 Ayurwayda. All rights reserved.</p>
                  </div>
                </div>
                <div class="col-md-6 col-lg-6">
                
                  <!-- Copyright Menu -->
                  <div class="copyright-menu">
                    <ul class="policy-menu">
                     <li><a href="<?php echo base_url();?>terms-conditions"><?php echo $language['lg_terms_and_condi'];?></a></li>
                      <li><a href="<?php echo base_url();?>privacy-policy"><?php echo $language['lg_privacy_policy'];?></a></li>
                    </ul>
                  </div>
                  <!-- /Copyright Menu -->
                  
                </div>
              </div>
            </div>
            <!-- /Copyright -->
            
          </div>
        </div>
        <!-- /Footer Bottom -->
        
      </footer>
      <!-- /Footer -->
       
     </div>
     <!-- /Main Wrapper -->



<!--modal Section---->

<?php if($module=='doctor' || $module=='patient'){ if($page=='appoinments' || $page=='doctor_dashboard') { ?>

  <div class="modal fade custom-modal" id="appoinments_details">
      <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title"><?php echo $language['lg_appointment_det'];?></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <ul class="info-details">
              <li>
                <div class="details-header">
                  <div class="row">
                    <div class="col-md-8">
                      <span class="title"><?php echo $language['lg_appointment_dat'];?></span>
                      <span class="text app_date"></span>
                    </div>
                    <div class="col-md-6">
                      <div class="text-right">
                       <!--  <button type="button" class="btn bg-success-light btn-sm" id="topup_status">Completed</button> -->
                      </div>
                    </div>
                  </div>
                </div>
              </li>
             <li>
                <span class="title"><?php echo $language['lg_appoinment_type'];?></span>
                <span class="text type"></span>
              </li>
              <li>
                <span class="title"><?php echo $language['lg_confirm_date'];?></span>
                <span class="text book_date"></span>
              </li>
              <li>
                <span class="title"><?php echo $language['lg_paid_amount'];?></span>
                <span class="text amount"></span>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade custom-modal" id="appoinments_status_modal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="app-modal-title"><?php echo $language['lg_accept'];?></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <input type="hidden" id="appoinments_id">
          <input type="hidden" id="appoinments_status">
          
           <div class="modal-body">
            <p><?php echo $language['lg_are_you_sure_wa1'];?> <span id="app-modal-title"></span> <?php echo $language['lg_this_appoinment'];?></p>
          </div>
          <div class="modal-footer">
            <button type="button" id="change_btn" onclick="change_status()" class="btn btn-primary"><?php echo $language['lg_yes'];?></button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $language['lg_no6'];?></button>
          </div>
        </div>
      </div>
    </div>



  <?php } if($page=='doctor_profile' || $page=='patient_profile') { ?>

    <div class="modal fade custom-modal" id="avatar-modal" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title"><i><?php echo $language['lg_profile_image'];?></i></h4>
      </div>
      <?php $curprofileimage = (!empty($profile['profileimage']))?$profile['profileimage']:''; ?>
      <form class="avatar-form" action="<?php echo base_url('profile/crop_profile_img/'.$curprofileimage)?>" enctype="multipart/form-data" method="post">
        <div class="modal-body">
          <div class="avatar-body">
            <!-- Upload image and data -->
            <div class="avatar-upload">
              <input class="avatar-src" name="avatar_src" type="hidden">
              <input class="avatar-data" name="avatar_data" type="hidden">
              <label for="avatarInput"><?php echo $language['lg_select_image'];?></label>
              <input class="avatar-input" id="avatarInput" name="avatar_file" type="file">
              <span id="image_upload_error" class="error" style="display:none;"> <?php echo $language['lg_please_upload_i'];?> </span>
            </div>
            <!-- Crop and preview -->
            <div class="row">
              <div class="col-md-12">
                <div class="avatar-wrapper"></div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <div class="row avatar-btns">
            <div class="col-md-12">
              <button class="btn btn-success avatar-save" type="submit"><?php echo $language['lg_save'];?></button>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

<?php } if($page == 'my_questions'){?> 




 <div id="question_modal_form" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                     <form action="#" enctype="multipart/form-data" autocomplete="off" id="question_form" method="post"> 
                    <div class="modal-header">
                            <h5 class="modal-title">Add Questions</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                    </div>

                    <div class="modal-body">
                        
                            <input type="hidden" value="" name="id"/> 
                            <input type="hidden" value="" name="method"/>
                           <div class="form-group">
                                <label for="category_name" class="control-label mb-10"> Question <span class="text-danger">*</span></label>
                                <input type="text" parsley-trigger="change" id="questions" name="questions"  class="form-control" >
                            </div>
                            <div class="form-group">
                                <label for="slug" class="control-label mb-10">Question Description </label>
                                <input type="text" parsley-trigger="change" id="questions_description" name="questions_description"  class="form-control" >
                            </div>
                            <div class="form-group">
                                <label for="description" class="control-label mb-10">Specilization </label>
                                <select class="form-control" name="specialization" id="specialization">
                                  
                                       <?php foreach ($specialization as $row) { ?>
                                         <option value="<?php echo $row['id']; ?>"><?php echo $row['specialization']; ?></option>
                                      <?php } ?>                 
                                </select>
                            </div>
                            
                           
                       
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline btn-default btn-sm btn-rounded" data-dismiss="modal">Close</button>
                        <button type="submit" id="btnfamilyssave" class="btn btn-outline btn-success ">Submit</button>
                    </div>
                     </form>
                </div>
            </div>
        </div>





<?php } if($page=='schedule_timings') { ?>

<!-- Add Time Slot Modal -->
  <div class="modal fade custom-modal" id="add_time_slot">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title"><?php echo $language['lg_add_time_slots'];?></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form id="schedule_form" name="schedule_form" method="post" autocomplete="off">
            <div class="hours-info">
              <div class="row form-row hours-cont">
                <div class="col-12 col-md-10">
                  <div class="row form-row">
                    <input type="hidden" name="slot" id="slot" >
                    <input type="hidden" name="day_id" id="day_id" value="1">
                    <input type="hidden" name="day_name" id="day_name" value="Sunday">
                    <input type="hidden" name="id_value" id="id_value" value="">
                    <div class="col-12 col-md-6">
                      <div class="form-group">
                        <label><?php echo $language['lg_start_time'];?></label>
                        <select class="form-control" id="from_time" name="from_time" onchange="get_to_time()">
                         <option value=""><?php echo $language['lg_select_time'];?></option>
                        </select>
                      </div> 
                    </div>
                    <div class="col-12 col-md-6">
                      <div class="form-group">
                        <label><?php echo $language['lg_end_time'];?></label>
                        <select class="form-control" id="to_time" name="to_time">
                          <option value=""><?php echo $language['lg_select_time'];?></option>
                        </select>
                      </div> 
                    </div>
                    <div class="col-12 col-md-6">
                      <div class="form-group">
                        <label class="d-block mb-3"><?php echo $language['lg_booking_system'];?></label>
                        <div class="custom-control custom-radio custom-control-inline">
                          <input type="radio" id="online" value="online" name="type" class="custom-control-input">
                          <label class="custom-control-label" for="online"> <?php echo $language['lg_online'];?></label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                          <input type="radio" id="clinic" value="clinic" name="type" class="custom-control-input">
                          <label class="custom-control-label" for="clinic"><?php echo $language['lg_clinic'];?></label>
                        </div>  
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
            
            <div class="submit-section text-center">
              <button type="submit" id="submit_btn" class="btn btn-primary submit-btn"><?php echo $language['lg_add10'];?></button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- /Add Time Slot Modal -->

   <?php } if($page=='mypatient_preview' || $page=='patient_dashboard') { ?>

    <!-- Delete modal-->

     <div class="modal fade custom-modal" id="delete_modal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title"><?php echo $language['lg_delete'];?></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <input type="hidden" id="delete_id">
           <input type="hidden" id="delete_table">
          <div class="modal-body">
            <p><?php echo $language['lg_are_you_sure_wa'];?> <span id="delete_title"></span> ?</p>
          </div>
          <div class="modal-footer">
            <button type="button" id="delete_btn" onclick="delete_details()" class="btn btn-primary"><?php echo $language['lg_yes'];?></button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $language['lg_no6'];?></button>
          </div>
        </div>
      </div>
    </div>

     <!-- View Prescription -->
            <div class="modal fade custom-modal" id="view_modal" tabindex="-1" role="dialog" aria-hidden="true" >
              <div class="modal-dialog modal-lg" role="document" style="width: 90%">
                <div class="modal-content">
                  <div class="modal-header">
                    <h3 class="modal-title"><?php echo $language['lg_view1'];?> <span class="view_title"></span></h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  </div>                  
                  <div class="modal-body"> 
                    <label><?php echo $language['lg_date1'];?> : <span id="view_date"></span></label><br>
                    <label><?php echo $language['lg_patient_name'];?> : <span id="patient_name"></span></label>

                      <div class="view_details"></div>                    
                  </div>
                  <div class="clearfix"></div>
                  <div class="modal-footer">                    
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo $language['lg_close1'];?></button>
                  </div>
                </div>
              </div>
            </div>

         
     <!-- Add Medical Records Modal -->
    <div class="modal fade custom-modal" id="add_medical_records">
      <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h3 class="modal-title"><?php echo $language['lg_medical_records'];?></h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <form id="medical_records_form"  enctype="multipart/form-data">          
            <div class="modal-body">
              <input type="hidden" name="patient_id" value="<?php echo $patient_id; ?>">
                              
              <div class="form-group">
                <label><?php echo $language['lg_description__op'];?></label>
                <textarea class="form-control" name="description" id="description" rows="5"></textarea>
              </div>
              <div class="form-group">
                <label><?php echo $language['lg_upload_file'];?></label> 
                <input class="form-control" type="file" name="user_file" id="user_files_mr">
              </div>
              
              <div class="submit-section text-center">
                <button type="submit" id="medical_btn" class="btn btn-primary submit-btn"><?php echo $language['lg_submit'];?></button>
                <button type="button" class="btn btn-secondary submit-btn" data-dismiss="modal"><?php echo $language['lg_cancel'];?></button>             
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!-- /Add Medical Records Modal -->
  <?php } if($page=='add_prescription' || $page=='edit_prescription' || $page=='add_billing' || $page=='edit_billing') {  ?>

   <div class="modal fade" id="sign-modal" tabindex="-1" role="dialog" aria-hidden="true">
       <div class="modal-dialog">
    <div class="modal-content" id="signature-pad">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><i class="fa fa-pencil"></i> <?php echo $language['lg_add_signature'];?></h4>
      </div>
      <div class="modal-body">
        <canvas width="570" height="318"></canvas>
        <input type="hidden" id="rowno" name="rowno" value="<?php echo rand();?>">
        <input type="hidden" id="signname" value="">            
        <input type="hidden" id="scount" value="">
      </div>
      <div class="modal-footer clearfix">
        <button type="submit"  id="save2"  class="btn btn-success"  data-action="save" ><i class="fa fa-check"></i> <?php echo $language['lg_save'];?></button>
        <button type="button" data-action="clear" class="btn btn-default" ><i class="fa fa-trash-o"></i> <?php echo $language['lg_clear'];?></button>
        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i><?php echo $language['lg_cancel'];?></button> 
      </div>
    </div>        
  </div>      
</div>  

<?php } } if($module=='post' && $page=='add_post' || $page=='edit_post'){ ?>

<div class="modal fade" id="avatar-image-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" data-backdrop="static" data-keyboard="false" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">   
        <div class="modal-header d-block"> 
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Upload Image</h4>
                  <span id="image_size" > Please Upload a Image of size above 680x454 </span> 
        </div>

        <div class="modal-body">
          <div id="imageimg_loader" class="loader-wrap" style="display: none;">
            <div class="loader">Loading...</div>
          </div>

          <div class="image-editor">
            <input type="file" id="fileopen"  name="file" class="cropit-image-input">
            <span class="error_msg help-block" id="error_msg_model" ></span> 
            <div class="cropit-preview"></div>
            <div class="row resize-bottom">
              <div class="col-md-4">
                <div class="image-size-label">Resize Image</div>
              </div>
              <div class="col-md-4"><input type="range" class="custom cropit-image-zoom-input"></div>
              <div class="col-md-4 text-right"><button class="btn btn-primary export">Done</button></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div> 

<?php } if($page=='accounts'){ ?>

  <div class="modal fade show" id="account_modal" role="dialog">
      <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h3 class="modal-title">Account Details</h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
          </div>
          <div class="modal-body">
            <form id="accounts_form" method="post">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="control-label">Bank Name</label>
                    <input type="text" name="bank_name" class="form-control bank_name" value="">
                    <span class="help-block"></span>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="control-label">Branch Name</label>
                    <input type="text" name="branch_name" class="form-control branch_name" value="">
                    <span class="help-block"></span>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="control-label">Account Number</label>
                    <input type="text" name="account_no" class="form-control account_no" value="">
                    <span class="help-block"></span>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="control-label">Account Name</label>
                    <input type="text" name="account_name" class="form-control acc_name" value="">
                    <span class="help-block"></span>
                  </div>
                </div> 
              </div>
              <div class="modal-footer">
                <button type="submit" id="acc_btn" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    
    <!-- Payment Modal -->
    <div class="modal fade show" id="payment_request_modal" role="dialog">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h3 class="modal-title">Payment Request</h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
          </div>
          <div class="modal-body">
             <form id="payment_request_form" method="post">
              <input type="hidden" name="payment_type" id="payment_type">
            <div class="form-group">
              <label>Request Amount</label>
              <input type="text" onkeyup="amount()" onblur="amount()" name="request_amount" id="request_amount" class="form-control" maxlength="6" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
              <span class="help-block"></span>
            </div>
            <div class="form-group">
              <label>Description (Optional)</label>
              <textarea class="form-control" name="description" id="description"></textarea>
              <span class="help-block"></span>
            </div>
            <div class="modal-footer">
              <button type="submit" id="request_btn" class="btn btn-primary">Request</button>
              <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
            </form>
          </div>
        </div>
      </div>
    </div>

  <?php }  if($this->session->userdata('user_id') !=''){ ?>


  <!-- Video Call Modal -->
    <div class="modal fade call-modal" id="appoinment_user">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-body">
          
            <!-- Incoming Call -->
            <div class="call-box incoming-box">
              <div class="call-wrapper appoinments_users_details">
                
              </div>
            </div>
            <!-- /Incoming Call -->
            
          </div>
        </div>
      </div>
    </div>
    <!-- Video Call Modal -->

    
    <div class="modal fade" id="ratings_review_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle"><?php echo $language['lg_ratings__review'];?></h5>
        <button type="button"  class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <div class="write-review">
              <h4><?php echo $language['lg_write_a_review_'];?> <strong><?php echo $language['lg_dr'];?> <span id="doctor_name"></span></strong></h4>
              
              <!-- Write Review Form -->
              <form method="post" id="rating_reviews_form">
                <div class="form-group">
                  <label><?php echo $language['lg_review2'];?></label>
                  <div class="star-rating">
                    <input id="star-5" type="radio" name="rating" value="5">
                    <label for="star-5" title="5 stars">
                      <i class="active fa fa-star"></i>
                    </label>
                    <input id="star-4" type="radio" name="rating" value="4">
                    <label for="star-4" title="4 stars">
                      <i class="active fa fa-star"></i>
                    </label>
                    <input id="star-3" type="radio" name="rating" value="3">
                    <label for="star-3" title="3 stars">
                      <i class="active fa fa-star"></i>
                    </label>
                    <input id="star-2" type="radio" name="rating" value="2">
                    <label for="star-2" title="2 stars">
                      <i class="active fa fa-star"></i>
                    </label>
                    <input id="star-1" type="radio" name="rating" value="1">
                    <label for="star-1" title="1 star">
                      <i class="active fa fa-star"></i>
                    </label>
                  </div>
                </div>
                <!-- hidden fileds -->
                
                
                <input type="hidden" name="doctor_id" id="doctor_id" >
                <div class="form-group">
                  <label><?php echo $language['lg_title_of_your_r'];?></label>
                  <input class="form-control" name="title" type="text" placeholder="If you could say it in one sentence, what would you say?">
                </div>
                <div class="form-group">
                  <label><?php echo $language['lg_your_review'];?></label>
                  <textarea id="review_desc" name="review" maxlength="100" class="form-control"></textarea>
                  
                  <div class="d-flex justify-content-between mt-3"><small class="text-muted"><span id="chars">100</span> <?php echo $language['lg_characters_rema'];?></small></div>
                </div>
                <hr>
                
                <div class="submit-section">
                  <button id="review_btn" type="submit" class="btn btn-primary submit-btn"><?php echo $language['lg_add_review'];?></button>
                </div>
              </form>
              <!-- /Write Review Form -->
              
            </div>
      </div>
      
    </div>
  </div>
</div>

 <?php } ?>
      <div class="modal fade" id="verifyModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document" style="min-width: 600px; min-height: 500px">
              <div class="modal-content">
                  <button type="button" class="close text-right" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true" class="px-4 py-2 d-block">&times;</span>
                  </button>
                  <div class="modal-body d-flex flex-column justify-content-center align-items-center p-5" style="min-width: 600px; min-height: 500px">
                      <form method="post" action="#" autocomplete="off" id="send_otp" class="d-flex flex-column align-items-center w-75 mx-auto">
                        <span class="fa-envelope-check pb-4">
                                                    <svg width="74" height="58" viewBox="0 0 74 58" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
<path d="M32.6966 30.5802C32.1127 30.9451 31.4559 31.0911 30.872 31.0911C30.2882 31.0911 29.6313 30.9451 29.0474 30.5802L0 12.8452V36.4189C0 41.4547 4.08708 45.5418 9.12294 45.5418H52.6941C57.7299 45.5418 61.817 41.4547 61.817 36.4189V12.8452L32.6966 30.5802Z"
      fill="#60A2E8"/>
<path d="M52.6946 0H9.12348C4.81746 0 1.16828 3.06531 0.29248 7.15238L30.9455 25.8362L61.5256 7.15238C60.6498 3.06531 57.0007 0 52.6946 0Z"
      fill="#60A2E8"/>
<circle cx="58.9093" cy="42.4806" r="14.0909" fill="#3DAC7B" stroke="#F2F9FF" stroke-width="2"/>
<path d="M55.418 42.2622L58.0362 44.8804L62.618 40.2986" stroke="white" stroke-width="2" stroke-linecap="round"/>
</svg>
                                                </span>
                          <div class="form-group w-100">
                              <h5 class="mb-4 text-center text-capitalize">we will send you an OTP on your entered<br>Email/Mobile No.  </h5>
                              <input type="text" name="verify_account_otp" id="verify_account_otp" class="form-control w-100">
                          </div>
                      </form>

                      <button type="button" class="btn btn-secondary mt-3 w-75 mx-auto" id="verifyByOTP">Send OTP</button>
                  </div>

              </div>
          </div>
      </div>
  <!--Answer Modal -->
    <div class="modal fade show" id="answer_modal" role="dialog">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h3 class="modal-title">Add Your Answer Here</h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
          </div>
          <div class="modal-body">
           <form>
        <input type="hidden" id="question_id">
        <div class="form-group">
          <textarea class="form-control" rows="6" id="answer_area"></textarea>
        </div>
        <button onclick="add_answers()" id="submit_answer" class="btn btn-primary mb-2">Submit</button>
      </form>
          </div>
        </div>
      </div>
    </div>

 <audio id="myAudio">
  <source src="<?php echo base_url();?>assets/ring/phone_ring.mp3" type="audio/mp3">
  </audio>

  <?php $this->load->view('web/modules/language_scripts/scripts');?>

     <script type="text/javascript">
           var base_url='<?php echo base_url();?>';
           var modules='<?php echo $module;?>';
           var pages='<?php echo $page;?>';
           var roles='<?php echo $this->session->userdata('role');?>';

       </script>
    
    <!-- jQuery -->
    <?php if($page=='add_post' || $page=='edit_post') { ?>
        <script src="<?php echo base_url();?>assets/js/jquery2.js"></script>
    <?php }  else { ?>
    <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
    <?php } ?>
    <!-- Bootstrap Core JS -->
    <script src="<?php echo base_url();?>assets/js/popper.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>

     <?php if($module=='doctor' || $module=='patient' || $module=='calendar' || $module=='invoice' || $theme=='blog' || $page=='doctors_search'  || $page=='doctors_searchmap' || $page=='patients_search'){ ?>

      
    <!-- Sticky Sidebar JS -->
      <script src="<?php echo base_url();?>assets/plugins/theia-sticky-sidebar/ResizeSensor.js"></script>
      <script src="<?php echo base_url();?>assets/plugins/theia-sticky-sidebar/theia-sticky-sidebar.js"></script>

        <!-- Circle Progress JS -->
   <script src="<?php echo base_url();?>assets/js/circle-progress.min.js"></script>

 <?php } if(($module=='doctor' || $module=='patient') && ($page=='doctor_profile' || $page=='patient_profile') || $page=='doctors_search' || $page=='doctors_searchmap' || $page=='patients_search' || $page=='schedule_timings') { ?>

   <script src="<?php echo base_url();?>assets/plugins/select2/js/select2.min.js"></script>

   <?php } if(($module=='doctor' || $module=='patient' || $module=='post' || $module=='calendar' || $module=='invoice')) {  if($page=='book_appoinments' || $page=='doctor_profile' || $page=='patient_profile' ){ ?> 

    <script src="<?php echo base_url();?>assets/js/bootstrap-datepicker.min.js"></script>
    
   <?php } if($page=='doctor_profile' || $page=='patient_profile' ){ ?>

    <script type="text/javascript" src="<?php echo base_url();?>assets/js/cropper_profile.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/cropper.min.js"></script>
 
   <?php } if($page=='doctor_profile') { ?>

    <script src="<?php echo base_url();?>assets/plugins/dropzone/dropzone.min.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.js"></script>
    <script src="<?php echo base_url();?>assets/js/profile-settings.js"></script>
    <?php } if($page=='calendar'){ ?>

       <script src="<?php echo base_url();?>assets/js/moment.min.js"></script>
       <script src="<?php echo base_url();?>assets/js/bootstrap-datetimepicker.min.js"></script>

        <script src="<?php echo base_url();?>assets/plugins/jquery-ui/jquery-ui.min.js"></script>
        <script src="<?php echo base_url();?>assets/plugins/fullcalendar/fullcalendar.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/calendar.js"></script>

   

   <?php } if($page=='doctor_dashboard' || $page=='mypatient_preview' || $page=='patient_dashboard' || $page=='index' || $page=='pending_post' || $page=='invoice' || $page=='accounts' || $page=='my_questions'){ ?>

    <script src="<?php echo base_url();?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/datatables/datatables.min.js"></script>

   <?php } if($page=='add_prescription' || $page=='edit_prescription' || $page=='add_billing' || $page=='edit_billing'){ ?>

      <script type="text/javascript" src="<?php echo base_url();?>assets/js/signature-pad.js"></script> 

    <?php } } ?>
    
    <!-- Slick JS -->
    <script src="<?php echo base_url();?>assets/js/slick.js"></script>
    
    <!-- Custom JS -->
    <script src="<?php echo base_url();?>assets/js/script.js"></script>

    <?php if($module=='signin'|| ($module=='doctor' || $module=='patient' || $module=='post') && ($page=='doctor_profile' || $page=='patient_profile' || $page=='schedule_timings'|| $page=='add_prescription' || $page=='edit_prescription' || $page=='add_billing' || $page=='edit_billing'|| $page=='change_password' || $page=='add_post' || $page=='edit_post')  || $page =='contact') { ?>

    <script src="<?php echo base_url();?>assets/js/jquery.validate.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/js/jquery.password-validation.js" type="text/javascript"></script>
    <?php }  if($module=='patient' && $page=='checkout') { ?>
    <script src="https://js.stripe.com/v3/"></script>
    <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
    <?php } ?>
    <script src="<?php echo base_url();?>assets/js/toastr.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jstz-1.0.7.min.js"></script>
    <!-- Fancybox JS -->
    <script src="<?php echo base_url(); ?>assets/plugins/fancybox/jquery.fancybox.min.js"></script>


    <script src="<?php echo base_url();?>assets/js/web.js"></script>
    <?php if($module=='messages') { ?>
    <script src="<?php echo base_url();?>assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <?php } if($this->session->userdata('user_id') !=''){ ?>
     <script src="<?php echo base_url();?>assets/js/appoinments.js"></script>
     <script src="<?php echo base_url();?>assets/js/messages.js"></script>
    <?php }  if($theme=='blog') { if($module=='home' && $page=='blog_details') { ?> 
       <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-confirm.min.js"></script>
     <?php } if($module=='post') { if($page=='add_post' || $page=='edit_post') { ?>
      <script src="<?php echo base_url();?>assets/plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.js"></script>
       <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.cropit.js"></script>
       <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/cropper_image.js"></script> 
     <?php } } ?>
       <script src="<?php echo base_url();?>assets/js/blog.js"></script>
     <?php } ?>
    <?php if($this->session->flashdata('error_message')) {  ?>
             <script>
               toastr.error('<?php echo $this->session->flashdata('error_message');?>');
            </script>
        <?php $this->session->unset_userdata('error_message');
        } if($this->session->flashdata('success_message')) {  ?>

            <script>
               toastr.success('<?php echo $this->session->flashdata('success_message');?>');
            </script>
            
      <?php $this->session->unset_userdata('success_message'); } ?>

   

   
  </body>
</html>