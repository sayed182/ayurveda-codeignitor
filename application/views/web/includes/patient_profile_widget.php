


<?php 
$user_detail=user_detail($patient_id);
$profile_image=(!empty($user_detail['profileimage']))?base_url().$user_detail['profileimage']:base_url().'assets/img/user.png';?>
							<div class="card widget-profile pat-widget-profile">
								<div class="card-body">
									<div class="pro-widget-content">
										<div class="profile-info-widget">
											<a href="javascript:void(0);" class="booking-doc-img">
												<img src="<?php echo $profile_image;?>" alt="User Image">
											</a>
											<div class="profile-det-info">
												<h3><?php echo ucfirst($user_detail['first_name'].' '.$user_detail['last_name']);?></h3>
												
												<div class="patient-details">
													<h5><b><?php echo $language['lg_patient_id'];?> :</b> #PT00<?php echo $user_detail['userid'];?></h5>
													<h5 class="mb-0"><i class="fas fa-map-marker-alt"></i> <?php echo $user_detail['cityname'].', '.$user_detail['countryname'];?></h5>
												</div>
											</div>
										</div>
									</div>
									<div class="patient-info">
										<ul>
											<li><?php echo $language['lg_email'];?> <span><?php echo $user_detail['email'];?></span></li>
											<li><?php echo $language['lg_phone'];?> <span><?php echo $user_detail['mobileno'];?></span></li>
											<li><?php echo $language['lg_age'];?> <span><?php echo age_calculate($user_detail['dob']);?>, <?php echo $user_detail['gender'];?></span></li>
											<li><?php echo $language['lg_blood_group'];?> <span><?php echo $user_detail['blood_group'];?></span></li>
										</ul>
									</div>
								</div>
							</div>