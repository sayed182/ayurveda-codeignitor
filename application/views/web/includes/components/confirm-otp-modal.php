<div class="modal-content">
    <button type="button" class="close text-right" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true" class="px-4 py-2 d-block">&times;</span>
    </button>
    <div class="modal-body d-flex flex-column justify-content-center align-items-center p-5" style="min-width: 600px; min-height: 500px">
        <form method="post" action="#" autocomplete="off" id="send_otp" class="d-flex flex-column align-items-center w-75 mx-auto">
                        <span class="fa-envelope-check pb-4">
                                                    <svg width="74" height="58" viewBox="0 0 74 58" fill="none"
                                                         xmlns="http://www.w3.org/2000/svg">
<path d="M32.6966 30.5802C32.1127 30.9451 31.4559 31.0911 30.872 31.0911C30.2882 31.0911 29.6313 30.9451 29.0474 30.5802L0 12.8452V36.4189C0 41.4547 4.08708 45.5418 9.12294 45.5418H52.6941C57.7299 45.5418 61.817 41.4547 61.817 36.4189V12.8452L32.6966 30.5802Z"
      fill="#60A2E8"/>
<path d="M52.6946 0H9.12348C4.81746 0 1.16828 3.06531 0.29248 7.15238L30.9455 25.8362L61.5256 7.15238C60.6498 3.06531 57.0007 0 52.6946 0Z"
      fill="#60A2E8"/>
<circle cx="58.9093" cy="42.4806" r="14.0909" fill="#3DAC7B" stroke="#F2F9FF" stroke-width="2"/>
<path d="M55.418 42.2622L58.0362 44.8804L62.618 40.2986" stroke="white" stroke-width="2" stroke-linecap="round"/>
</svg>
                                                </span>
            <div class="form-group w-100">
                <h5 class="mb-4 text-center text-capitalize">OTP has been sent to your Email </h5>

                <h6 class="mb-4 text-center text-capitalize"><?php echo $user_detail['email']; ?></h6>
                <div class="row">
                    <div class="col-3">
                        <input type="text" name="verify_account_otp[]" id="verify_account_otp_1" class="form-control w-100" required>
                    </div>
                    <div class="col-3">
                        <input type="text" name="verify_account_otp[]" id="verify_account_otp_2" class="form-control w-100" required>
                    </div>
                    <div class="col-3">
                        <input type="text" name="verify_account_otp[]" id="verify_account_otp_3" class="form-control w-100" required>
                    </div>
                    <div class="col-3">
                        <input type="text" name="verify_account_otp[]" id="verify_account_otp_4" class="form-control w-100" required>
                    </div>
                </div>
            </div>
        </form>
        <button type="button" class="btn btn-secondary mt-3 w-75 mx-auto" data-dismiss="modal">Verify OTP</button>
    </div>

</div>