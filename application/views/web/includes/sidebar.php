<body <?php echo ($module == 'messages') ? 'class="chat-page"' : ''; ?><?php echo ($module == 'signin') ? 'class="account-page"' : ''; ?>>

    <!-- Main Wrapper -->
    <div class="main-wrapper">
        <div class="container p-0">
            <!-- Header -->
            <header class="header">

                <nav class="navbar navbar-expand-lg header-nav">
                    <div class="navbar-header">
                        <a id="mobile_btn" href="javascript:void(0);">
                            <span class="bar-icon">
                                <span></span>
                                <span></span>
                                <span></span>
                            </span>
                        </a>
                        <a href="<?php echo base_url(); ?>" class="navbar-brand logo">
                            <?php if ($page == 'contact') { ?>
                                <img src="<?php echo base_url() . "assets/img/logo-white.png"; ?>" alt="logo">
                            <?php } else { ?>
                                <img src="<?php echo !empty(base_url() . settings("logo_front")) ? base_url() . settings("logo_front") : base_url() . "assets/img/logo.png"; ?>" class="" alt="Logo">
                            <?php } ?>


                            <!-- <img src="assets/img/logo.png" class="img-fluid" alt="Logo"> -->
                        </a>
                    </div>
                    <div class="main-menu-wrapper">
                        <div class="menu-header">
                            <a href="<?php echo base_url(); ?>" class="menu-logo">
                                <img src="<?php echo !empty(base_url() . settings("logo_front")) ? base_url() . settings("logo_front") : base_url() . "assets/img/logo.png"; ?>" class="img-fluid" alt="Logo">

                            </a>
                            <a id="menu_close" class="menu-close" href="javascript:void(0);">
                                <i class="fas fa-times"></i>
                            </a>
                        </div>




                        <div class="main-nav">
                            <div></div>
                            <div>
                                <li <?php echo ($theme == 'web' && $module == 'home' && $page == 'index') ? 'class="active"' : ''; ?>>
                                    <a href="<?php echo base_url(); ?>"><?php echo $language['lg_home']; ?></a>
                                </li>
                                <li <?php echo ($theme == 'web' && $module == 'home' && $page == 'about') ? 'class="active"' : ''; ?>>
                                    <a href="<?php echo base_url(); ?>about">About</a>
                                </li>
                                <li <?php echo ($theme == 'blog' && $module == 'home') ? 'class="active"' : ''; ?>>
                                    <a href="<?php echo base_url(); ?>blog"><?php echo $language['lg_blog2']; ?></a>
                                </li>
                                <?php if ($this->session->userdata('user_id')) { ?>
                                    <li <?php echo (($module == 'doctor' || $module == 'patient') && ($page == 'doctor_dashboard' || $page == 'patient_dashboard')) ? 'class="active"' : ''; ?>>
                                        <a href="<?php echo base_url(); ?>dashboard"><?php echo $language['lg_dashboard']; ?></a>
                                    </li>

                                <?php }
                                if ($this->session->userdata('user_id') == '' || is_patient()) { ?>


                                    <li <?php echo ($module == 'home' && ($page == 'doctors_search' || $page == 'doctors_searchmap')) ? 'class="active"' : ''; ?>>
                                        <a href="<?php echo base_url(); ?>doctors-search"><?php echo $language['lg_doctors']; ?></a>
                                    </li>


                                    <?php }
                                if ($this->session->userdata('user_id')) {
                                    if (is_doctor()) { ?>
                                        <li <?php echo ($module == 'home' && $page == 'patients_search') ? 'class="active"' : ''; ?>>
                                            <a href="<?php echo base_url(); ?>patients-search"><?php echo $language['lg_patients1']; ?></a>
                                        </li>
                                <?php }
                                } ?>

                                <li <?php echo ($theme == 'web' && $module == 'qa') ? 'class="active"' : ''; ?>>
                                    <a href="<?php echo base_url(); ?>qa"><?php echo $language['lg_qa']; ?></a>
                                </li>

                                <!--              <li class="dropdown">-->
                                <!--                <a class="dropbtn">Pharmacy &nbsp;&nbsp;<i class="fa fa-chevron-down" aria-hidden="true"></i></a>-->
                                <!--                <div class="dropdown-content">-->
                                <!--                  <a href="-->
                                <?php //echo base_url();
                                ?>
                                <!--home/coming_soon">Pharmacy Login</a>-->
                                <!--                  <a href="-->
                                <?php //echo base_url();
                                ?>
                                <!--home/coming_soon">Pharmacy List</a>-->
                                <!--                 -->
                                <!--                </div>-->
                                <!--              </li>-->

                                <li <?php echo ($theme == 'web' && $module == 'home' && $page == 'contact') ? 'class="active"' : ''; ?>>
                                    <a href="<?php echo base_url(); ?>contact">Contact</a>
                                </li>
                            </div>
                            <div>
                                <?php if ($this->session->userdata('user_id') || $this->session->userdata('admin_id')) { ?>
                                    <li class="login-link">
                                        <a class="dropdown-item" href="<?php echo base_url(); ?>sign-out"><?php echo $language['lg_signout']; ?></a>
                                    </li>
                                <?php } else { ?>
                                    <li class="login-link">
                                        <a href="<?php echo base_url(); ?>signin"><?php echo $language['lg_signin__signup']; ?></a>
                                    </li>
                                <?php } ?>


                                <?php if ($this->session->userdata('user_id') || $this->session->userdata('admin_id')) {

                                    if ($this->session->userdata('user_id')) {
                                        $user_detail = user_detail($this->session->userdata('user_id'));
                                        $user_profile_image = (!empty($user_detail['profileimage'])) ? base_url() . $user_detail['profileimage'] : base_url() . 'assets/img/user.png';
                                ?>
                                        <li class="nav-item dropdown has-arrow logged-item">
                                            <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
                                                <span class="user-img">
                                                    <img class="rounded-circle avatar-view-img" src="<?php echo $user_profile_image; ?>" width="31" alt="<?php echo ucfirst($user_detail['first_name'] . ' ' . $user_detail['last_name']); ?>">
                                                </span>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <div class="user-header">
                                                    <div class="avatar avatar-sm">
                                                        <img src="<?php echo $user_profile_image; ?>" alt="User Image" class="avatar-img avatar-view-img rounded-circle">
                                                    </div>
                                                    <div class="user-text">
                                                        <h6><?php echo ucfirst($user_detail['first_name'] . ' ' . $user_detail['last_name']); ?></h6>
                                                        <p class="text-muted mb-0"><?php echo ($this->session->userdata('role') == '1') ? 'Doctor' : 'Patient'; ?></p>
                                                    </div>
                                                </div>
                                                <a class="dropdown-item" href="<?php echo base_url(); ?>dashboard"><?php echo $language['lg_dashboard']; ?></a>
                                                <a class="dropdown-item" href="<?php echo base_url(); ?>profile"><?php echo $language['lg_profile_setting']; ?></a>
                                                <a class="dropdown-item" href="<?php echo base_url(); ?>sign-out"><?php echo $language['lg_signout']; ?></a>
                                            </div>
                                        </li>
                                    <?php } else {

                                        $admin_detail = admin_detail($this->session->userdata('admin_id'));
                                        $adminprofile_image = (!empty($admin_detail['profileimage'])) ? base_url() . $admin_detail['profileimage'] : base_url() . 'assets/img/user.png';
                                    ?>

                                        <li class="nav-item dropdown has-arrow logged-item">
                                            <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
                                                <span class="user-img">
                                                    <img class="rounded-circle avatar-view-img" src="<?php echo $adminprofile_image; ?>" width="31" alt="<?php echo ucfirst($admin_detail['name']); ?>">
                                                </span>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <div class="user-header">
                                                    <div class="avatar avatar-sm">
                                                        <img src="<?php echo $adminprofile_image; ?>" alt="User Image" class="avatar-img avatar-view-img rounded-circle">
                                                    </div>
                                                    <div class="user-text">
                                                        <h6><?php echo ucfirst($admin_detail['name']); ?></h6>
                                                        <p class="text-muted mb-0">Admin</p>
                                                    </div>
                                                </div>
                                                <a class="dropdown-item" href="<?php echo base_url(); ?>admin/dashboard"><?php echo $language['lg_dashboard']; ?></a>
                                                <a class="dropdown-item" href="<?php echo base_url(); ?>admin/login/logout"><?php echo $language['lg_signout']; ?></a>
                                            </div>
                                        </li>

                                    <?php }
                                } else { ?>
                                    <li class="nav-item">
                                        <a class="nav-link header-login" href="<?php echo base_url(); ?>signin"><?php echo $language['lg_signin__signup']; ?> </a>
                                    </li>
                                <?php } ?>
                            </div>
                            </ul>

                        </div>
                </nav>

            </header>
        </div>
        <!-- /Header -->