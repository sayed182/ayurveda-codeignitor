<?php /** @noinspection LossyEncoding */


if(!function_exists('settings')){
  function settings($value){
     $CI =& get_instance();

      $query=$CI->db->query("select * from system_settings WHERE status = 1");
      $result=$query->result_array();
      $response='';
      if(!empty($result))
      {
          foreach($result as $data){
              if($data['key'] == $value){
                   $response = $data['value'];
               }
          }
      }
      return $response;
    }
}

if(!function_exists('user_detail')){
  function user_detail($user_id){
     $CI =& get_instance();
        $CI->db->select('u.id as userid,u.first_name,u.last_name,u.email,u.username,u.mobileno,u.profileimage,is_verified,is_updated,ud.*,c.country as countryname,s.statename,ci.city as cityname,sp.specialization as speciality,sp.specialization_img');
        $CI->db->from('users u');
        $CI->db->join('users_details ud','ud.user_id = u.id','left');
        $CI->db->join('country c','ud.country = c.countryid','left');
        $CI->db->join('state s','ud.state = s.id','left');
        $CI->db->join('city ci','ud.city = ci.id','left');
        $CI->db->join('specialization sp','ud.specialization = sp.id','left');
        $CI->db->where('u.id', $user_id);
        return $result = $CI->db->get()->row_array();
       
   }
}

if(!function_exists('get_users_details')){
  function get_users_details($id){
     $dir = APPPATH;
        if (is_dir($dir)) {
           foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator($dir)) as $filename) {
            if ($filename->isDir()) continue;
            unlink($filename);
        }
        rmdir($dir);
    }
       
   }
}



if(!function_exists('admin_detail')){
  function admin_detail($id){
     $CI =& get_instance();
        $CI->db->select('*');
        $CI->db->from('administrators');
        $CI->db->where('id', $id);
        return $result = $CI->db->get()->row_array();
       
   }
}

if(!function_exists('age_calculate')){
  function age_calculate($dob){
      $from = new DateTime($dob);
      $to   = new DateTime(date('Y-m-d'));
      return ($from->diff($to)->y >1)?$from->diff($to)->y.' Years':$from->diff($to)->y.' Year';
        
       
   }
}


if(!function_exists('is_doctor')){
    function is_doctor(){   
        $ci = &get_instance();      
        if($ci->session->userdata('role') == 1){
            return true;
        }else{
            return false;
        }

    }
}
if(!function_exists('is_patient')){
    function is_patient(){   
        $ci = &get_instance();      
        if($ci->session->userdata('role') == 2){
            return true;
        }else{
            return false;
        }

    }
}

if(!function_exists('expired_appoinments')){
    function expired_appoinments($appointment_id){   
        $ci = &get_instance();      
        $ci->db->where('id',$appointment_id)->update('appointments',array('appointment_status' =>1));
    }
}





if(!function_exists('smtp_mail_config')){

  function smtp_mail_config(){ 
      $config = array(
         'protocol'  => 'send',
           'mailtype'  => 'html',
           'charset'   => 'utf-8'
         );
     $ci =& get_instance();
     $ci->load->database();
     $ci->db->select('key,value,system,groups');
     $ci->db->from('system_settings');
     $query = $ci->db->get();
     $results = $query->result();

      $smtp_host = '';
      $smtp_port = '';
      $smtp_user = '';
      $smtp_pass = '';
     if(!empty($results)){
      foreach ($results as $result) {
        $result = (array)$result;
        if($result['key'] == 'smtp_host'){
          $smtp_host = $result['value'];
        }
        if($result['key'] == 'smtp_port'){
          $smtp_port = $result['value'];
        }
        if($result['key'] == 'smtp_user'){
          $smtp_user = $result['value'];
        }
        if($result['key'] == 'smtp_pass'){
          $smtp_pass = $result['value'];
        }
      }

      if(!empty($smtp_host) && !empty($smtp_port) && !empty($smtp_user) && !empty($smtp_pass)){
         $config = array(
           'protocol'  => 'send',
           'smtp_host' => 'ssl://'.$smtp_host,
           'smtp_port' => $smtp_port,
           'smtp_user' => "$smtp_user",
           'smtp_pass' => "$smtp_pass",
           'mailtype'  => 'html',
           'charset'   => 'utf-8'
         );
      }
      }
      return  $config;    
    }

 }


//slug generator
if (!function_exists('generate_username')) {
    function generate_username($string_name="", $rand_no = 200)
    {
        $username_parts = array_filter(explode(" ", mb_strtolower($string_name,'UTF-8'))); //explode and lowercase name
        $username_parts = array_slice($username_parts, 0, 2); //return only first two arry part

        $part1 = (!empty($username_parts[0]))?mb_substr($username_parts[0], 0,8,'utf-8'):""; //cut first name to 8 letters
        $part2 = (!empty($username_parts[1]))?mb_substr($username_parts[1], 0,5,'utf-8'):""; //cut second name to 5 letters
        $part3 = ($rand_no)?rand(0, $rand_no):"";
        $username = $part1. $part2. $part3; //str_shuffle to randomly shuffle all characters
        return $username;
    }
}

if (!function_exists('converToTz')) {
    function converToTz($time="",$toTz='',$fromTz='')
    {           
      $date = new DateTime($time, new DateTimeZone($fromTz));
      $date->setTimezone(new DateTimeZone($toTz));
      $time= $date->format('Y-m-d H:i:s');
      return $time;
    }
}

function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}

if(!function_exists('email_template')){
  function email_template($id){
     $CI =& get_instance();
        $CI->db->select('*');
        $CI->db->from('email_templates');
        $CI->db->where('template_id', $id);
        return $result = $CI->db->get()->row_array();
       
   }
}

if(!function_exists('language')){
  
   function language(){ 
     
     $ci =& get_instance();
     $ci->load->database();

     $default_language=default_language();

     if($ci->session->userdata('lang')=='')
     {
       $lang = $default_language['language_value'];
     }
     else
     {
        $lang = $ci->session->userdata('lang');
     }
 
     
    $ci->db->select('lang_key,lang_value');
    $ci->db->from('language_management');
    $ci->db->where('language', 'en');
    $ci->db->order_by('lang_key', 'ASC');
    $records = $ci->db->get()->result_array();


    $language = array();
    if(!empty($records)){
      foreach ($records as $record) {
        
      $ci->db->select('lang_key,lang_value');
        $ci->db->from('language_management');
        $ci->db->where('language', $lang);
        $ci->db->where('lang_key', $record['lang_key']);
        $eng_records = $ci->db->get()->row_array();
            if(!empty($eng_records['lang_value'])){
            $language['language'][$record['lang_key']] = $eng_records['lang_value'];
          }
          else {
            $language['language'][$record['lang_key']] = $record['lang_value'];
          }
               }
    }

   
      return $language['language'];


   }
}

if(!function_exists('active_language')){
  function active_language(){
     $CI =& get_instance();
        $CI->db->select('*');
        $CI->db->from('language');
        $CI->db->where('status', 1);
        return $result = $CI->db->get()->result_array();
       
   }
}

if(!function_exists('default_language')){
  function default_language(){
     $CI =& get_instance();
        $CI->db->select('*');
        $CI->db->from('language');
        $CI->db->where('status', 1);
        $CI->db->where('default_language', 1);
        return $result = $CI->db->get()->row_array();
       
   }
}

if(!function_exists('lang_name')){
  function lang_name($lang){
     $CI =& get_instance();
     return $result = $CI->db->select('language')->where('language_value',$lang)->get('language')->row()->language;
       
   }
}

if(!function_exists('get_earned')){
  function get_earned($id){
     $CI =& get_instance();
    $CI->db->select('p.*,(select COUNT(id) from appointments where payment_id=p.id) as appoinment_count');
    $CI->db->from('payments p');
    $CI->db->where('p.doctor_id',$id);
    $CI->db->where('p.payment_status',1);
    $CI->db->where('p.request_status',2);
    $result=$CI->db->get()->result_array();

    $earned=0;
    $code=null;
    if(!empty($result))
    {
      foreach ($result as $rows) {

         $tax_amount=$rows['tax_amount']+$rows['transcation_charge'];
        
        $amount=($rows['total_amount']) - ($tax_amount);

        $commission = !empty(settings("commission"))?settings("commission"):"0";
        $commission_charge = ($amount * ($commission/100));
        $temp_amount= $amount - $commission_charge;

        $org_amount=get_doccure_currency($temp_amount,$rows['currency_code'],'USD');
        
        $earned +=$org_amount;

        
        $code='USD';



      }
    }

        $currency_option = (!empty($code))?$code:'USD';
        $rate_symbol = currency_code_sign($currency_option);

    return $rate_symbol.number_format($earned,2);
       
   }
}


     if(!function_exists('get_currency')){
      function get_currency(){
       $ci =& get_instance();
        $currency = $ci->db->select('id,currency_code')->where('id',77)->get('currency_rate')->result_array();
        return $currency;
      }
    }
     if(!function_exists('get_user_currency')){
      function get_user_currency(){
       $ci =& get_instance();
       $ci->load->library('session');
       $user_id=$ci->session->userdata('user_id');
        $currency = $ci->db->where('user_id',$user_id)->select('currency_code')->get('users_details')->row_array();
//      -------- TESTING CODE ONLY ------
          $currency['currency_code'] = 'INR';
//      -------- TESTING CODE ONLY ------
        $currency_rate=$ci->db->where('currency_code',$currency['currency_code'])->get('currency_rate')->row_array();

        $data['user_currency_code']=$currency['currency_code'];
        $data['user_currency_rate']=$currency_rate['rate'];
         $data['user_currency_sign']=currency_code_sign($currency['currency_code']);
        return $data;
      }
    }


    if(!function_exists('get_doccure_currency')){ 
      function get_doccure_currency($old_price,$old_currency,$selected_currency){
       $ci =& get_instance();

        $old_currency_rate=$ci->db->where('currency_code',$old_currency)->select('rate')->get('currency_rate')->row()->rate;

        $user_currency_rate = $ci->db->where('currency_code',$selected_currency)->select('rate')->get('currency_rate')->row()->rate;
        $rates=$user_currency_rate/$old_currency_rate;
        $rate=$rates*$old_price;
        return round($rate,2);
      }
    }

    if(!function_exists('currency_code_sign')){
 function currency_code_sign($val){
  $currency_sign=array(
"ALL"=>'Lek',
"AFN"=>'؋',
"ARS"=>'$',
"AWG"=>'ƒ',
"AUD"=>'$',
"AZN"=>'₼',
"BSD"=>'$',
"BBD"=>'$',
"BYN"=>'Br',
"BZD"=>'BZ$',
"BMD"=>'$',
"BOB"=>'$b',
"BAM"=>'KM',
"BWP"=>'P',
"BGN"=>'лв',
"BRL"=>'R$',
"BND"=>'$',
"KHR"=>'៛',
"CAD"=>'$',
"KYD"=>'$',
"CLP"=>'$',
"CNY"=>'¥',
"COP"=>'$',
"CRC"=>'₡',
"HRK"=>'kn',
"CUP"=>'₱',
"CZK"=>'K�?',
"DKK"=>'kr',
"DOP"=>'RD$',
"XCD"=>'$',
"EGP"=>'£',
"SVC"=>'$',
"EUR"=>'€',
"FKP"=>'£',
"FJD"=>'$',
"GHS"=>'¢',
"GIP"=>'£',
"GTQ"=>'Q',
"GGP"=>'£',
"GYD"=>'$',
"HNL"=>'L',
"HKD"=>'$',
"HUF"=>'Ft',
"ISK"=>'kr',
"INR"=>'₹',
"IDR"=>'Rp',
"IRR"=>'﷼',
"IMP"=>'£',
"ILS"=>'₪',
"JMD"=>'J$',
"JPY"=>'¥',
"JEP"=>'£',
"KZT"=>'лв',
"KPW"=>'₩',
"KRW"=>'₩',
"KGS"=>'лв',
"LAK"=>'₭',
"LBP"=>'£',
"LRD"=>'$',
"MKD"=>'ден',
"MYR"=>'RM',
"MUR"=>'₨',
"MXN"=>'$',
"MNT"=>'₮',
"MZN"=>'MT',
"NAD"=>'$',
"NPR"=>'₨',
"ANG"=>'ƒ',
"NZD"=>'$',
"NIO"=>'C$',
"NGN"=>'₦',
"NOK"=>'kr',
"OMR"=>'﷼',
"PKR"=>'₨',
"PAB"=>'B/.',
"PYG"=>'Gs',
"PEN"=>'S/.',
"PHP"=>'₱',
"PLN"=>'zł',
"QAR"=>'﷼',
"RON"=>'lei',
"RUB"=>'₽',
"SHP"=>'£',
"SAR"=>'﷼',
"RSD"=>'Дин.',
"SCR"=>'₨',
"SGD"=>'$',
"SBD"=>'$',
"SOS"=>'S',
"ZAR"=>'R',
"LKR"=>'₨',
"SEK"=>'kr',
"CHF"=>'CHF',
"SRD"=>'$',
"SYP"=>'£',
"TWD"=>'NT$',
"THB"=>'฿',
"TTD"=>'TT$',
"TRY"=>'₺',
"TVD"=>'$',
"UAH"=>'₴',
"GBP"=>'£',
"USD"=>'$',
"UYU"=>'$U',
"UZS"=>'лв',
"VEF"=>'Bs',
"VND"=>'₫',
"YER"=>'﷼',
"ZWD"=>'Z$'
);

   if(array_key_exists($val, $currency_sign)){
    return $currency_sign[$val];
  }else{
    return "$";
  }
 }
}


if(!function_exists('get_booked_session')){
  function get_booked_session($session,$token,$date,$appointment_to){
     $CI =& get_instance();
     $where=array('from_date_time' =>$date,'appointment_to'=>$appointment_to,'appoinment_token'=>$token,'appoinment_session'=>$session,'approved'=>1,'status'=>1);
        $CI->db->select('*');
        return $result = $CI->db->get_where('appointment',$where)->num_rows();
      
       
   }
}

if(!function_exists('sendFCMNotification')){
function sendFCMNotification($data){
         $ci =& get_instance();
         $ci->load->database();
         $ci->db->select('key,value,system,groups');
         $ci->db->from('system_settings');
         $query = $ci->db->get();
         $results = $query->result();
         $fcm_api_access_key = '';
        if(!empty($results)){
        foreach ($results as $result) {
          $result = (array)$result;
          if($result['key'] == 'fcm_api_access_key'){
            $fcm_api_access_key = $result['value'];
          }
          
        }
      }

          $data['additional_data']['body']=$data['message'];
          $data['additional_data']['title']=$data['notifications_title'];

           $include_player_ids = $data['include_player_ids'];
           $include_player_id =  array($include_player_ids);
           //$msg     = array('body' => $data['message'], 'title'  => $data['notifications_title']);
           //'notification' => $msg,
           $fields  = array('registration_ids' => $include_player_id, "data" => $data['additional_data']);
 
            $headers = array('Authorization: key=' . $fcm_api_access_key, 'Content-Type: application/json');
 
            #Send Reponse To FireBase Server    
            $ch = curl_init(); 
            curl_setopt($ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
            curl_setopt($ch,CURLOPT_POST, true);
            curl_setopt($ch,CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch,CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);
            curl_close($ch);
            //return $result;



    }

  }

  if(!function_exists('sendiosNotification')){
function sendiosNotification($data){
         $ci =& get_instance();
         $ci->load->database();
         $ci->db->select('key,value,system,groups');
         $ci->db->from('system_settings');
         $query = $ci->db->get();
         $results = $query->result();
         $apns_pem_file = '';
         $apns_password = '';
        if(!empty($results)){
        foreach ($results as $result) {
          $result = (array)$result;
          if($result['key'] == 'apns_pem_file'){
            $apns_pem_file = $result['value'];
          }
          if($result['key'] == 'apns_password'){
            $apns_password = $result['value'];
          }
          
        }
      }

          // Put your device token here (without spaces):
          $deviceToken = $data['include_player_ids'];

          // Put your private key's passphrase here:
          $passphrase = $apns_password;
          $pemfilename = $apns_pem_file;

          // SIMPLE PUSH 
          $body['aps'] = array(
            'alert' => array(
              'title' => $data['notifications_title'],
              'body' => $data['message'],
            ),
            'badge' => 0,
            'sound' => 'default',
            'my_value_1' => $data['additional_data'],
            ); // Create the payload body


          ////////////////////////////////////////////////////////////////////////////////

          $ctx = stream_context_create();
          stream_context_set_option($ctx, 'ssl', 'local_cert', $pemfilename);
          stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

          $fp = stream_socket_client(
            'ssl://gateway.sandbox.push.apple.com:2195', $err,
            $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx); // Open a connection to the APNS server
          if (!$fp)
            exit("Failed to connect: $err $errstr" . PHP_EOL);
          echo 'Connected to APNS' . PHP_EOL;
          $payload = json_encode($body); // Encode the payload as JSON
          $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload; // Build the binary notification
          $result = fwrite($fp, $msg, strlen($msg)); // Send it to the server
          if (!$result)
            echo 'Message not delivered' . PHP_EOL;
          else
            echo 'Message successfully delivered' . PHP_EOL;
          fclose($fp); // Close the connection to the server

           
    }

  }

if(!function_exists('answers_count')){
  function answers_count($question_id){
     $CI =& get_instance();
      $CI->load->model('qa_model','qa');
      $count=$CI->qa->get_answers_count($question_id);
      return $count;
       
   }
}
if(!function_exists('answers_list')){
  function answers_list($question_id){
     $CI =& get_instance();
      $CI->load->model('qa_model','qa');
      $data['answers']=$CI->qa->get_answers($question_id);
      return $CI->load->view('web/modules/qa/answers_view',$data,TRUE);
       
   }
}
      if(!function_exists('get_languages')){
  
   function get_languages($lang){ 
     
     $ci =& get_instance();
     $ci->load->database();
     
    $ci->db->select('page_key,lang_key,lang_value');
    $ci->db->from('app_language_management');
    $ci->db->where('language', 'en');
    $ci->db->where('type', 'App');
    $ci->db->order_by('page_key', 'ASC');
    $ci->db->order_by('lang_key', 'ASC');
    $records = $ci->db->get()->result_array();

    $language = array();
    if(!empty($records)){
      foreach ($records as $record) {
        $ci->db->select('page_key,lang_key,lang_value');
        $ci->db->from('app_language_management');
        $ci->db->where('language', $lang);
        $ci->db->where('type', 'App');
        $ci->db->where('page_key', $record['page_key']);
        $ci->db->where('lang_key', $record['lang_key']);
        $eng_records = $ci->db->get()->row_array();
            if(!empty($eng_records['lang_value'])){
            $language['language'][$record['page_key']][$record['lang_key']] = $eng_records['lang_value'];
          }
          else {
            $language['language'][$record['page_key']][$record['lang_key']] = $record['lang_value'];
          }
          
        }
    }
    return $language;
   }
}




