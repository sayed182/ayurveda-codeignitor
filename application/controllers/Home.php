<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

public function __construct() {

        parent::__construct();
        $this->data['theme']     = 'web';
        $this->data['module']    = 'home';
        $this->data['page']     = '';
        $this->data['base_url'] = base_url();
        $this->timezone = $this->session->userdata('time_zone');
        if(!empty($this->timezone)){
          date_default_timezone_set($this->timezone);
        }

        $this->data['language'] =language();
        $this->language=language(); 
        $this->data['default_language']=default_language();
        $this->data['active_language'] = active_language();
        $this->load->model('home_model','home');
        $this->load->model('profile_model','profile');
        $this->load->model('my_patients_model','my_patients');

                 
    }

    public function index()
  {
      
      $this->data['page'] = 'index';
      $this->data['doctors']=$this->home->get_doctors();
      $this->data['specialization'] = $this->home->get_specialization();
      $this->data['blogs'] = $this->home->get_blogs();
      $this->data['qa'] = $this->home->get_answers();
      $this->load->vars($this->data);
      $this->load->view($this->data['theme'].'/template');
     
  }

  public function coming_soon(){

      $this->data['page'] = 'coming_soon';
     
      $this->load->vars($this->data);
      $this->load->view($this->data['theme'].'/template');

  }
  public function about(){

      $this->data['page'] = 'about';
     
      $this->load->vars($this->data);
      $this->load->view($this->data['theme'].'/template');

  }
  public function contact(){

      $this->data['page'] = 'contact';
     
      $this->load->vars($this->data);
      $this->load->view($this->data['theme'].'/template');

  }

  public function send_contact_mail(){

    $data['name']=$this->input->post('name');
    $data['mobile']=$this->input->post('mobile');
    $data['email']=$this->input->post('email');
    $data['concern']=$this->input->post('concern');
    $data['explain']=$this->input->post('explain');

     $this->load->library('sendemail');
     $this->sendemail->contact_form_email($data);


     $response['msg']="Mail sent successfully";
     $response['status']=500; 


     echo json_encode($response);



  }


  public function search_keywords()
  {
        $data = array();
        $response=array();
        $sdata=array();
        $sresult=array();
        $result=array();
        $search_keywords = $this->input->post('search_keywords');
        
        if(!empty($search_keywords)){
          $doctor_list= $this->home->search_doctors($search_keywords);     
          $specialization_list= $this->home->search_specialization($search_keywords); 

          foreach ($specialization_list as $srows) {
            $sdata['specialization']=$srows['specialization'];
            $sresult[]=$sdata;
          }
          

          if(!empty($doctor_list)){
            foreach($doctor_list as $rows){
               $data['username']=$rows['username'];
            $data['profileimage']=(!empty($rows['profileimage']))?base_url().$rows['profileimage']:base_url().'assets/img/user.png';
            $data['first_name']=ucfirst($rows['first_name']);
            $data['last_name']=ucfirst($rows['last_name']);
            $data['speciality']=ucfirst($rows['speciality']);
            $result[]=$data;
            }
          }
        }
         $response['specialist']= $sresult;
         $response['doctor']= $result;
        echo json_encode($response);
  }

  public function search_location()
  {
        $data = array();
        $response=array();
        $sdata=array();
        $sresult=array();
        $result=array();
        $search_location = $this->input->post('search_location');
        
        if(!empty($search_location)){
          $location_list= $this->home->search_location($search_location); 

          foreach ($location_list as $rows) {
            $data['location']=$rows['city'];
            $result[]=$data;
          }
     
        }
         $response['location']= $result;
         
        echo json_encode($response);
  }

  public function add_favourities()
  {
        $response=array();
      if(!empty($this->session->userdata('user_id')))
      {
        $doctor_id=$this->input->post('doctor_id');
        $patient_id=$this->session->userdata('user_id');

         if(is_patient()){

            $where=array('doctor_id' =>$doctor_id,'patient_id'=>$patient_id);
            $already_favourities=$this->db->get_where('favourities',$where)->result_array();
            if(count($already_favourities) > 0 )
            {
               $this->db->where('doctor_id',$doctor_id);
               $this->db->where('patient_id',$patient_id);
               $this->db->delete('favourities');

               $response['status']=201;
            }
            else
            {
                $data=array('doctor_id' =>$doctor_id,'patient_id'=>$patient_id);
                $this->db->insert('favourities',$data);
               $response['status']=200;
            }
        }
        else
        {
              $response['msg']=$this->language['lg_invalid_login_p'];
              $response['status']=204;
        }

      }
      else
      {
              $response['msg']=$this->language['lg_please_login_to'];
              $response['status']=204;
      }

      echo json_encode($response);
  }

  public function doctor_preview_new($city='',$spec='',$username)
  {

      $this->data['page'] = 'doctor_preview';
      $this->data['doctors']=$this->home->get_doctor_details(urldecode($username));
      if(!empty($this->data['doctors']))
      {
          $doctor_details=$this->home->get_doctor_details(urldecode($username));

         if($doctor_details['price_type']=='Custom Price'){

            $user_currency=get_user_currency();
            $user_currency_code=$user_currency['user_currency_code'];
            $user_currency_rate=$user_currency['user_currency_rate'];

            $currency_option = (!empty($user_currency_code))?$user_currency_code:$doctor_details['currency_code'];
            $rate_symbol = currency_code_sign($currency_option);

                      if(!empty($this->session->userdata('user_id'))){
                        $rate=get_doccure_currency($doctor_details['amount'],$doctor_details['currency_code'],$user_currency_code);
                      }else{
                           $rate= $doctor_details['amount'];
                        }
            $this->data['amount']=$rate_symbol.''.$rate;

            }else{

              $this->data['amount']="Free";
            }
            $this->data['latitude']=$this->latitude($doctor_details['clinic_address']);
            $this->data['longitude']=$this->longitude($doctor_details['clinic_address']);
      $this->data['clinic_images']= $this->clinic_images($this->data['doctors']['userid']);
      $this->data['education'] = $this->profile->get_education_details($this->data['doctors']['userid']);
      $this->data['experience'] = $this->profile->get_experience_details($this->data['doctors']['userid']);
      $this->data['awards'] = $this->profile->get_awards_details($this->data['doctors']['userid']);
      $this->data['memberships'] = $this->profile->get_memberships_details($this->data['doctors']['userid']);
      $this->data['registrations'] = $this->profile->get_registrations_details($this->data['doctors']['userid']);
      $this->data['business_hours'] = $this->profile->get_business_hours($this->data['doctors']['userid']);
      $this->data['monday_hours'] = $this->profile->get_monday_hours($this->data['doctors']['userid']);
      $this->data['sunday_hours'] = $this->profile->get_sunday_hours($this->data['doctors']['userid']);
      $this->data['tue_hours'] = $this->profile->get_tue_hours($this->data['doctors']['userid']);
      $this->data['wed_hours'] = $this->profile->get_wed_hours($this->data['doctors']['userid']);
      $this->data['thu_hours'] = $this->profile->get_thu_hours($this->data['doctors']['userid']);
      $this->data['fri_hours'] = $this->profile->get_fri_hours($this->data['doctors']['userid']);
      $this->data['sat_hours'] = $this->profile->get_sat_hours($this->data['doctors']['userid']);
      $this->data['reviews'] = $this->home->review_list_view($this->data['doctors']['userid']);
      $this->load->vars($this->data);
      $this->load->view($this->data['theme'].'/template');
      }
      else
      {
        $this->session->set_flashdata('error_message','Oops something went wrong try valid credentials!');
         redirect(base_url().'home');
      }
      
  }

  public function doctor_preview($username)
  {

      $this->data['page'] = 'doctor_preview';
      $this->data['doctors']=$this->home->get_doctor_details(urldecode($username));
      if(!empty($this->data['doctors']))
      {
          $doctor_details=$this->home->get_doctor_details(urldecode($username));

         if($doctor_details['price_type']=='Custom Price'){

            $user_currency=get_user_currency();
            $user_currency_code=$user_currency['user_currency_code'];
            $user_currency_rate=$user_currency['user_currency_rate'];

            $currency_option = (!empty($user_currency_code))?$user_currency_code:$doctor_details['currency_code'];
            $rate_symbol = currency_code_sign($currency_option);

                      if(!empty($this->session->userdata('user_id'))){
                        $rate=get_doccure_currency($doctor_details['amount'],$doctor_details['currency_code'],$user_currency_code);
                      }else{
                           $rate= $doctor_details['amount'];
                        }
            $this->data['amount']=$rate_symbol.''.$rate;

            }else{

              $this->data['amount']="Free";
            }
            $this->data['latitude']=$this->latitude($doctor_details['clinic_address']);
            $this->data['longitude']=$this->longitude($doctor_details['clinic_address']);
      $this->data['clinic_images']= $this->clinic_images($this->data['doctors']['userid']);
      $this->data['education'] = $this->profile->get_education_details($this->data['doctors']['userid']);
      $this->data['experience'] = $this->profile->get_experience_details($this->data['doctors']['userid']);
      $this->data['awards'] = $this->profile->get_awards_details($this->data['doctors']['userid']);
      $this->data['memberships'] = $this->profile->get_memberships_details($this->data['doctors']['userid']);
      $this->data['registrations'] = $this->profile->get_registrations_details($this->data['doctors']['userid']);
      $this->data['business_hours'] = $this->profile->get_business_hours($this->data['doctors']['userid']);
      $this->data['monday_hours'] = $this->profile->get_monday_hours($this->data['doctors']['userid']);
      $this->data['sunday_hours'] = $this->profile->get_sunday_hours($this->data['doctors']['userid']);
      $this->data['tue_hours'] = $this->profile->get_tue_hours($this->data['doctors']['userid']);
      $this->data['wed_hours'] = $this->profile->get_wed_hours($this->data['doctors']['userid']);
      $this->data['thu_hours'] = $this->profile->get_thu_hours($this->data['doctors']['userid']);
      $this->data['fri_hours'] = $this->profile->get_fri_hours($this->data['doctors']['userid']);
      $this->data['sat_hours'] = $this->profile->get_sat_hours($this->data['doctors']['userid']);
      $this->data['reviews'] = $this->home->review_list_view($this->data['doctors']['userid']);
      $this->load->vars($this->data);
      $this->load->view($this->data['theme'].'/template');
      }
      else
      {
        $this->session->set_flashdata('error_message','Oops something went wrong try valid credentials!');
         redirect(base_url().'home');
      }
      
  }

  public function doctors_search()
  {
      $this->data['page'] = 'doctors_search';
       $this->data['keywords'] ='';
       $this->data['city'] ='';
       $this->data['state'] ='';
      if(isset($_GET['keywords']) && !empty($_GET['keywords']))
      {
        $this->data['keywords'] = urldecode($_GET['keywords']);
      }
      if(isset($_GET['location']) && !empty($_GET['location']))
      {
        $location=$this->db->select('id,stateid')->where('city',urldecode($_GET['location']))->get('city')->row_array();
        $this->data['city'] = $location['id'];
        
      }
      $this->load->vars($this->data);
      $this->load->view($this->data['theme'].'/template');
  }

  public function search_doctor()
  {

       $response=array();
       $result=array();
        $page=$this->input->post('page');
        $limit=5;
        $response['count'] =$this->home->search_doctor($page,$limit,1);
        $doctor_list = $this->home->search_doctor($page,$limit,2);

        if (!empty($doctor_list)) {
          foreach ($doctor_list as $rows) {

            $data['id']=$rows['id'];
            $data['username']=$rows['username'];
            $data['profileimage']=(!empty($rows['profileimage']))?base_url().$rows['profileimage']:base_url().'assets/img/user.png';
            $data['first_name']=ucfirst($rows['first_name']);
            $data['last_name']=ucfirst($rows['last_name']);
            $data['specialization_img']=base_url().$rows['specialization_img'];
            $data['speciality']=ucfirst($rows['speciality']);
            $data['cityname']=$rows['cityname'];
            $data['countryname']=$rows['countryname'];
            $data['services']=$rows['services'];
            $data['rating_value']=$rows['rating_value'];
            $data['rating_count']=$rows['rating_count'];
            $data['latitude']=$this->latitude($rows['cityname'].' '.$rows['countryname']);
            $data['longitude']=$this->longitude($rows['cityname'].' '.$rows['countryname']);

            $data['clinic_images']= json_encode($this->clinic_images($rows['user_id']));
            
            if($rows['price_type']=='Custom Price'){

            $user_currency=get_user_currency();
            $user_currency_code=$user_currency['user_currency_code'];
            $user_currency_rate=$user_currency['user_currency_rate'];

            $currency_option = (!empty($user_currency_code))?$user_currency_code:$rows['currency_code'];
            $rate_symbol = currency_code_sign($currency_option);

                      if(!empty($this->session->userdata('user_id'))){
                        $rate=get_doccure_currency($rows['amount'],$rows['currency_code'],$user_currency_code);
                      }else{
                           $rate= $rows['amount'];
                        }
            $data['amount']=$rate_symbol.''.$rate;

            }else{

              $data['amount']="Free";
            }

            
            $result[]=$data;
          }
        }
        $response['current_page_no']= $page;
        $response['total_page']= ceil($response['count']/$limit);
        $response['data']= $result;

     echo json_encode($response);

  }

    public function clinic_images($id){
  
    $this->db->select('clinic_image,user_id');
    $this->db->where('user_id',$id);
    $result=$this->db->get('clinic_images')->result_array();
    return $result;

  }

  public function latitude($address){

      $address = str_replace(" ", "+", $address);

      $url="https://maps.google.com/maps/api/geocode/json?key=AIzaSyA_QD2_rlwEFGhCK0oj2n6cixsvX0D3zgk&address=$address&sensor=false";

      $ch=curl_init();//initiating curl
      curl_setopt($ch,CURLOPT_URL,$url);// CALLING THE URL
      curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
      curl_setopt($ch,CURLOPT_PROXYPORT,3128);
      curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,0);
      curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,0);
      $response=curl_exec($ch);
 
      $response=json_decode($response );
      if (@$response->status == 'OK') {
              $lat=$response->results[0]->geometry->location->lat;
              $lng=$response->results[0]->geometry->location->lng;

              return $lat;
              }
 

}

public function longitude($address){

      $address = str_replace(" ", "+", $address);

      $url="https://maps.google.com/maps/api/geocode/json?key=AIzaSyA_QD2_rlwEFGhCK0oj2n6cixsvX0D3zgk&address=$address&sensor=false";

      $ch=curl_init();//initiating curl
      curl_setopt($ch,CURLOPT_URL,$url);// CALLING THE URL
      curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
      curl_setopt($ch,CURLOPT_PROXYPORT,3128);
      curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,0);
      curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,0);
      $response=curl_exec($ch);
 
      $response=json_decode($response );
      if (@$response->status == 'OK') {
              $lat=$response->results[0]->geometry->location->lat;
              $lng=$response->results[0]->geometry->location->lng;

              return $lng;
              }
 

}

  

  public function patients_search()
  {
      $this->data['page'] = 'patients_search';
      $this->load->vars($this->data);
      $this->load->view($this->data['theme'].'/template');
  }

  public function search_patient()
  {

       $response=array();
       $result=array();
        $page=$this->input->post('page');
        $limit=8;
        $response['count'] =$this->home->search_patient($page,$limit,1);
        $patient_list = $this->home->search_patient($page,$limit,2);

        if (!empty($patient_list)) {
          foreach ($patient_list as $rows) {

            $data['id']=$rows['id'];
            $data['user_id']=$rows['user_id'];
            $data['username']=$rows['username'];
            $data['profileimage']=(!empty($rows['profileimage']))?base_url().$rows['profileimage']:base_url().'assets/img/user.png';
            $data['first_name']=ucfirst($rows['first_name']);
            $data['last_name']=ucfirst($rows['last_name']);
            $data['mobileno']=$rows['mobileno'];
            $data['dob']=$rows['dob'];
            $data['age']=age_calculate($rows['dob']);
            $data['blood_group']=$rows['blood_group'];
            $data['gender']=$rows['gender'];
            $data['cityname']=$rows['cityname'];
            $data['countryname']=$rows['countryname'];
            $result[]=$data;
          }
        }
        $response['current_page_no']= $page;
        $response['total_page']= ceil($response['count']/$limit);
        $response['data']= $result;

     echo json_encode($response);

  }

  public function terms_conditions()
  {
      
      $this->data['page'] = 'terms_conditions';
      $this->load->vars($this->data);
      $this->load->view($this->data['theme'].'/template');
     
  }

  public function privacy_policy()
  {
      
      $this->data['page'] = 'privacy_policy';
      $this->load->vars($this->data);
      $this->load->view($this->data['theme'].'/template');
     
  }

   public function patient_preview($patient_id)
  {
      $this->data['page'] = 'patient_preview';
      $this->data['patient']=$this->my_patients->get_patient_details(base64_decode($patient_id));
      $this->data['patient_id']=base64_decode($patient_id);
      $this->load->vars($this->data);
      $this->load->view($this->data['theme'].'/template');
  }

}
