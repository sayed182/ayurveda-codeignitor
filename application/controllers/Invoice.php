<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Invoice extends CI_Controller
{

    public $data;

    public function __construct()
    {

        parent::__construct();
        if ($this->session->userdata('user_id') == '') {
            if ($this->session->userdata('admin_id')) {
                redirect(base_url() . 'home');
            } else {
                redirect(base_url() . 'signin');
            }
        }

        $this->data['theme'] = 'web';
        $this->data['module'] = 'invoice';
        $this->data['page'] = '';
        $this->data['base_url'] = base_url();
        $this->timezone = $this->session->userdata('time_zone');
        if (!empty($this->timezone)) {
            date_default_timezone_set($this->timezone);
        }
        $this->data['language'] = language();
        $this->language = language();
        $this->data['default_language'] = default_language();
        $this->data['active_language'] = active_language();
        $this->load->model('invoice_model', 'invoice');
    }


    public function index()
    {
        $user_id = $this->session->userdata('user_id');
        $this->data['module'] = 'invoice';
        $this->data['page'] = 'invoice';
        $invoice_count = $this->invoice->count_all($user_id);
        $this->data['invoice_count'] = $invoice_count;
        $this->load->vars($this->data);
        $this->load->view($this->data['theme'] . '/template');
    }

    public function invoice_list()
    {
        $user_id = $this->session->userdata('user_id');
        $list = $this->invoice->get_datatables($user_id);
        $data = array();
        $no = $_POST['start'];
        $a = 1;

        foreach ($list as $invoices) {

            $user_currency = get_user_currency();
            $user_currency_code = $user_currency['user_currency_code'];
            $user_currency_rate = $user_currency['user_currency_rate'];

            $currency_option = (!empty($user_currency_code)) ? $user_currency_code : 'USD';
            $rate_symbol = currency_code_sign($currency_option);

            $org_amount = get_doccure_currency($invoices['total_amount'], $invoices['currency_code'], $user_currency_code);

            $doctor_profileimage = (!empty($invoices['doctor_profileimage'])) ? base_url() . $invoices['doctor_profileimage'] : base_url() . 'assets/img/user.png';
            $patient_profileimage = (!empty($invoices['patient_profileimage'])) ? base_url() . $invoices['patient_profileimage'] : base_url() . 'assets/img/user.png';

            $no++;
            $row = array();
            $row[] = '<a href="' . base_url() . 'invoice-view/' . base64_encode($invoices['id']) . '">' . $invoices['invoice_no'] . '</a>';

            if ($this->session->userdata('role') == '1') {
                $row[] = '<h2 class="table-avatar">
                  <a target="_blank" href="' . base_url() . 'patient-preview/' . base64_encode($invoices['patient_id']) . '" class="avatar avatar-sm mr-2"><img class="avatar-img rounded-circle" src="' . $patient_profileimage . '" alt="User Image"></a>
                  <a target="_blank" href="' . base_url() . 'patient-preview/' . base64_encode($invoices['patient_id']) . '">' . ucfirst($invoices['patient_name']) . ' </a>
                </h2>';
            }
            if ($this->session->userdata('role') == '2') {
                $row[] = '<h2 class="table-avatar">
                  <a target="_blank" href="' . base_url() . 'doctor-preview/' . $invoices['doctor_username'] . '" class="avatar avatar-sm mr-2">
                    <img class="avatar-img rounded-circle" src="' . $doctor_profileimage . '" alt="User Image">
                  </a>
                  <a target="_blank" href="' . base_url() . 'doctor-preview/' . $invoices['doctor_username'] . '">Dr. ' . ucfirst($invoices['doctor_name']) . '</a>
                </h2>';
            }


            $row[] = $rate_symbol . $org_amount;
            $row[] = date('d M Y', strtotime($invoices['payment_date']));
            $row[] = '<div class="table-action">
                  <a href="' . base_url() . 'invoice-view/' . base64_encode($invoices['id']) . '" class="btn btn-sm bg-info-light">
                    <i class="far fa-eye"></i> View
                  </a>
                  <a href="' . base_url() . 'invoice-print/' . base64_encode($invoices['id']) . '" class="btn btn-sm bg-primary-light" target="blank">
                                <i class="fas fa-print"></i> Print
                  </a>
                </div>';

            $data[] = $row;
        }


        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->invoice->count_all($user_id),
            "recordsFiltered" => $this->invoice->count_filtered($user_id),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function view()
    {
        $invoice_id = $this->uri->segment(2);
        $this->data['page'] = 'invoice_view';
        $this->data['invoices'] = $this->invoice->get_invoice_details(base64_decode($invoice_id));
        $this->load->vars($this->data);
        $this->load->view($this->data['theme'] . '/template');
    }

    public function invoice_print()
    {
        $invoice_id = $this->uri->segment(2);
        $data['invoices'] = $this->invoice->get_invoice_details(base64_decode($invoice_id));
        $this->load->view('web/modules/invoice/invoice_print', $data);
    }


}
