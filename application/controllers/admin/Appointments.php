<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Appointments extends CI_Controller {

   public $data;

   public function __construct() {

        parent::__construct();

        if($this->session->userdata('admin_id') ==''){
            redirect(base_url().'admin/login');
        }
        
        $this->data['theme']     = 'admin';
        $this->data['module']    = 'appointments';
        $this->data['page']     = '';
        $this->data['base_url'] = base_url();

         $this->timezone = $this->session->userdata('time_zone');
        if(!empty($this->timezone)){
          date_default_timezone_set($this->timezone);
        }
        
        $this->load->model('appoinments_model','appointments');
        

    }


	public function index()
	{
	    $this->data['page'] = 'index';
      $this->load->vars($this->data);
      $this->load->view($this->data['theme'].'/template');
	   
	}

  public function appoinments_list()
   {
      $list = $this->appointments->get_appoinments_datatables();
      $data = array();
      $no = $_POST['start'];
      $a=1;
       
      foreach ($list as $appoinments) {

        $doctor_profileimage=(!empty($appoinments['doctor_profileimage']))?base_url().$appoinments['doctor_profileimage']:base_url().'assets/img/user.png';
        $patient_profileimage=(!empty($appoinments['patient_profileimage']))?base_url().$appoinments['patient_profileimage']:base_url().'assets/img/user.png';
        $no++;
        $row = array();
        $row[] = $a++;
        $row[] = '<h2 class="table-avatar">
                  <a target="_blank" href="'.base_url().'doctor-preview/'.$appoinments['doctor_username'].'" class="avatar avatar-sm mr-2">
                    <img class="avatar-img rounded-circle" src="'.$doctor_profileimage.'" alt="User Image">
                  </a>
                  <a target="_blank" href="'.base_url().'doctor-preview/'.$appoinments['doctor_username'].'">Dr. '.ucfirst($appoinments['doctor_name']).' <span>'.ucfirst($appoinments['doctor_specialization']).'</span></a>
                </h2>';
        $row[] = '<h2 class="table-avatar">
                  <a target="_blank" href="'.base_url().'patient-preview/'.base64_encode($appoinments['appointment_from']).'" class="avatar avatar-sm mr-2"><img class="avatar-img rounded-circle" src="'.$patient_profileimage.'" alt="User Image"></a>
                  <a target="_blank" href="'.base_url().'patient-preview/'.base64_encode($appoinments['appointment_from']).'">'.ucfirst($appoinments['patient_name']).' </a>
                </h2>';
         $from_date_time = '';
        if(!empty($appoinments['time_zone'])){
          $from_timezone=$appoinments['time_zone'];
          $to_timezone = date_default_timezone_get();
          $from_date_time = $appoinments['from_date_time'];
          $from_date_time = converToTz($from_date_time,$to_timezone,$from_timezone);
          $row[] = date('d M Y',strtotime($from_date_time)).' <span class="d-block text-info">'.date('h:i A',strtotime($from_date_time)).'</span>';
        }else{
          $row[]='-';
       } 
        $row[] = date('d M Y',strtotime($appoinments['created_date']));
        $row[] = ucfirst($appoinments['type']);
        
        $data[] = $row;
      }



      $output = array(
              "draw" => $_POST['draw'],
              "recordsTotal" => $this->appointments->appoinments_count_all(1),
              "recordsFiltered" => $this->appointments->appoinments_count_filtered(1),
              "data" => $data,
          );
      //output to json format
      echo json_encode($output);
  }



  public function upappoinments_list()
   {
      $list = $this->appointments->get_upappoinments_datatables();
      $data = array();
      $no = $_POST['start'];
      $a=1;
       
      foreach ($list as $appoinments) {

        $doctor_profileimage=(!empty($appoinments['doctor_profileimage']))?base_url().$appoinments['doctor_profileimage']:base_url().'assets/img/user.png';
        $patient_profileimage=(!empty($appoinments['patient_profileimage']))?base_url().$appoinments['patient_profileimage']:base_url().'assets/img/user.png';
        $no++;
        $row = array();
        $row[] = $a++;
        $row[] = '<h2 class="table-avatar">
                  <a target="_blank" href="'.base_url().'doctor-preview/'.$appoinments['doctor_username'].'" class="avatar avatar-sm mr-2">
                    <img class="avatar-img rounded-circle" src="'.$doctor_profileimage.'" alt="User Image">
                  </a>
                  <a target="_blank" href="'.base_url().'doctor-preview/'.$appoinments['doctor_username'].'">Dr. '.ucfirst($appoinments['doctor_name']).' <span>'.ucfirst($appoinments['doctor_specialization']).'</span></a>
                </h2>';
        $row[] = '<h2 class="table-avatar">
                  <a target="_blank" href="'.base_url().'patient-preview/'.base64_encode($appoinments['appointment_from']).'" class="avatar avatar-sm mr-2"><img class="avatar-img rounded-circle" src="'.$patient_profileimage.'" alt="User Image"></a>
                  <a target="_blank" href="'.base_url().'patient-preview/'.base64_encode($appoinments['appointment_from']).'">'.ucfirst($appoinments['patient_name']).' </a>
                </h2>';
         $from_date_time = '';
        if(!empty($appoinments['time_zone'])){
          $from_timezone=$appoinments['time_zone'];
          $to_timezone = date_default_timezone_get();
          $from_date_time = $appoinments['from_date_time'];
          $from_date_time = converToTz($from_date_time,$to_timezone,$from_timezone);
          $row[] = date('d M Y',strtotime($from_date_time)).' <span class="d-block text-info">'.date('h:i A',strtotime($from_date_time)).'</span>';
        }else{
          $row[]='-';
       } 
        $row[] = date('d M Y',strtotime($appoinments['created_date']));
        $row[] = ucfirst($appoinments['type']);
        
        $data[] = $row;
      }



      $output = array(
              "draw" => $_POST['draw'],
              "recordsTotal" => $this->appointments->appoinments_count_all(2),
              "recordsFiltered" => $this->appointments->appoinments_count_filtered(2),
              "data" => $data,
          );
      //output to json format
      echo json_encode($output);
  }


  public function missedappoinments_list()
   {
      $list = $this->appointments->get_missedappoinments_datatables();
      $data = array();
      $no = $_POST['start'];
      $a=1;
       
      foreach ($list as $appoinments) {

        $doctor_profileimage=(!empty($appoinments['doctor_profileimage']))?base_url().$appoinments['doctor_profileimage']:base_url().'assets/img/user.png';
        $patient_profileimage=(!empty($appoinments['patient_profileimage']))?base_url().$appoinments['patient_profileimage']:base_url().'assets/img/user.png';
        $no++;
        $row = array();
        $row[] = $a++;
        $row[] = '<h2 class="table-avatar">
                  <a target="_blank" href="'.base_url().'doctor-preview/'.$appoinments['doctor_username'].'" class="avatar avatar-sm mr-2">
                    <img class="avatar-img rounded-circle" src="'.$doctor_profileimage.'" alt="User Image">
                  </a>
                  <a target="_blank" href="'.base_url().'doctor-preview/'.$appoinments['doctor_username'].'">Dr. '.ucfirst($appoinments['doctor_name']).' <span>'.ucfirst($appoinments['doctor_specialization']).'</span></a>
                </h2>';
        $row[] = '<h2 class="table-avatar">
                  <a target="_blank" href="'.base_url().'patient-preview/'.base64_encode($appoinments['appointment_from']).'" class="avatar avatar-sm mr-2"><img class="avatar-img rounded-circle" src="'.$patient_profileimage.'" alt="User Image"></a>
                  <a target="_blank" href="'.base_url().'patient-preview/'.base64_encode($appoinments['appointment_from']).'">'.ucfirst($appoinments['patient_name']).' </a>
                </h2>';
         $from_date_time = '';
        if(!empty($appoinments['time_zone'])){
          $from_timezone=$appoinments['time_zone'];
          $to_timezone = date_default_timezone_get();
          $from_date_time = $appoinments['from_date_time'];
          $from_date_time = converToTz($from_date_time,$to_timezone,$from_timezone);
          $row[] = date('d M Y',strtotime($from_date_time)).' <span class="d-block text-info">'.date('h:i A',strtotime($from_date_time)).'</span>';
        }else{
          $row[]='-';
       } 
        $row[] = date('d M Y',strtotime($appoinments['created_date']));
        $row[] = ucfirst($appoinments['type']);
        
        $data[] = $row;
      }



      $output = array(
              "draw" => $_POST['draw'],
              "recordsTotal" => $this->appointments->appoinments_count_all(3),
              "recordsFiltered" => $this->appointments->appoinments_count_filtered(3),
              "data" => $data,
          );
      //output to json format
      echo json_encode($output);
  }


}
