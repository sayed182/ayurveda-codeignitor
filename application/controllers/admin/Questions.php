<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Questions extends CI_Controller {

   public $data;

   public function __construct() {

        parent::__construct();

        if($this->session->userdata('admin_id') ==''){
            redirect(base_url().'admin/login');
        }
        
        $this->data['theme']     = 'admin';
        $this->data['module']    = 'questions';
        $this->data['page']     = '';
        $this->data['base_url'] = base_url();
         $this->timezone = $this->session->userdata('time_zone');
        if(!empty($this->timezone)){
          date_default_timezone_set($this->timezone);
        }
        $this->load->model('question_model','question');
        

    }

    public function index(){
      
      $this->data['page'] = 'index';
      $this->load->vars($this->data);
      $this->load->view($this->data['theme'].'/template');
    
    }

    public function answers(){

      $this->data['page'] = 'answers';
      $this->load->vars($this->data);
      $this->load->view($this->data['theme'].'/template');


    }

    public function questions_list()
   {

      $list = $this->question->get_questions_datatables();
      $data = array();
      $no = $_POST['start'];
      $a=1;
       
      foreach ($list as $questions) {
       
        $patient_profileimage=(!empty($questions['patient_profileimage']))?base_url().$questions['patient_profileimage']:base_url().'assets/img/user.png';
        $no++;
        $row = array();
        $row[] = $a++;
        
        $row[] = '<h2 class="table-avatar">
                  <a href="javascript:void(0)" class="avatar avatar-sm mr-2"><img class="avatar-img rounded-circle" src="'.$patient_profileimage.'" alt="User Image"></a>
                  <a href="javascript:void(0)">'.ucfirst($questions['patient_name']).' </a>
                </h2>';

        
         $row[] = $questions['specialization'];      
         $row[] = $questions['questions'];              

        $row[] = date('d M Y',strtotime($questions['created_date']));

        if($questions['status']==1){
          $status='<span class="badge badge-success">Approved</span>';
        }else{
          $status='<span class="badge badge-warning">Pending</span>';
        }

        $row[]=$status;
       
        $edit="";
        $delete=""; 


                  if($questions['status']!=1){ 
           $edit=' <a class="btn btn-sm bg-success-light" onclick="approve_questions('.$questions['id'].')">
                                <i class="fe fe-check"></i> Approve
                              </a>';
                }


           $delete=' <a class="btn btn-sm bg-danger-light" onclick="delete_questions('.$questions['id'].')">
                                <i class="fe fe-trash"></i> Delete
                              </a>';

      $row[] = '<div class="actions text-right">'.$edit.' '.$delete.'</div>';

        
        $data[] = $row;
      }



      $output = array(
              "draw" => $_POST['draw'],
              "recordsTotal" => $this->question->questions_count_all(),
              "recordsFiltered" => $this->question->questions_count_filtered(),
              "data" => $data,
          );
      //output to json format
      echo json_encode($output);
  }

  public function questions_delete()
  {
    $id=$this->input->post('questions_id');
    $this->db->where('id',$id);
    $this->db->delete('questions');
  }

  public function questions_approve()
  {
    $id=$this->input->post('questions_id');
    $this->db->where('id',$id);
    $data=array("status"=>1);
    $this->db->update('questions',$data);
  }


  public function answers_list()
   {

      $list = $this->question->get_answers_datatables();
      $data = array();
      $no = $_POST['start'];
      $a=1;
       
      foreach ($list as $questions) {


        $doctor_profileimage=(!empty($reviews['doctor_profileimage']))?base_url().$questions['doctor_profileimage']:base_url().'assets/img/user.png';
        $patient_profileimage=(!empty($questions['patient_profileimage']))?base_url().$questions['patient_profileimage']:base_url().'assets/img/user.png';
        $no++;
        $row = array();
        $row[] = $a++;
        
        $row[] = '<h2 class="table-avatar">
                  <a href="javascript:void(0)" class="avatar avatar-sm mr-2"><img class="avatar-img rounded-circle" src="'.$patient_profileimage.'" alt="User Image"></a>
                  <a href="javascript:void(0)">'.ucfirst($questions['patient_name']).' </a>
                </h2>';
        $row[] = '<h2 class="table-avatar">
                  <a target="_blank" href="'.base_url().'doctor-preview/'.$questions['doctor_username'].'" class="avatar avatar-sm mr-2">
                    <img class="avatar-img rounded-circle" src="'.$doctor_profileimage.'" alt="User Image">
                  </a>
                  <a target="_blank" href="'.base_url().'doctor-preview/'.$questions['doctor_username'].'">Dr. '.ucfirst($questions['doctor_name']).'</a>
                </h2>';
        
         $row[] = $questions['specialization'];      
         $row[] = $questions['questions'];              

        $row[] = $questions['answers']; ;

        if($questions['answers_status']==1){
          $status='<span class="badge badge-success">Approved</span>';
        }else{
          $status='<span class="badge badge-warning">Pending</span>';
        }

        $row[]=$status;
       
        $edit="";
        $delete=""; 


                  if($questions['answers_status']!=1){ 
           $edit=' <a class="btn btn-sm bg-success-light" onclick="approve_answers('.$questions['id'].')">
                                <i class="fe fe-check"></i> Approve
                              </a>';
                }


           $delete=' <a class="btn btn-sm bg-danger-light" onclick="delete_answers('.$questions['id'].')">
                                <i class="fe fe-trash"></i> Delete
                              </a>';

      $row[] = '<div class="actions text-right">'.$edit.' '.$delete.'</div>';

        
        $data[] = $row;
      }



      $output = array(
              "draw" => $_POST['draw'],
              "recordsTotal" => $this->question->answers_count_all(),
              "recordsFiltered" => $this->question->answers_count_filtered(),
              "data" => $data,
          );
      //output to json format
      echo json_encode($output);
  }

  public function answers_delete()
  {
    $id=$this->input->post('answers_id');
    $this->db->where('id',$id);
    $this->db->delete('answers');
  }

  public function answers_approve()
  {
    $id=$this->input->post('answers_id');
    $this->db->where('id',$id);
    $data=array("status"=>1);
    $this->db->update('answers',$data);
  }








}