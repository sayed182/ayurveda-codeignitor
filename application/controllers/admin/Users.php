<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

   public $data;

   public function __construct() {

        parent::__construct();

        if($this->session->userdata('admin_id') ==''){
            redirect(base_url().'admin/login');
        }

        $this->data['theme']     = 'admin';
        $this->data['module']    = 'users';
        $this->data['page']     = '';
        $this->data['base_url'] = base_url();
         $this->timezone = $this->session->userdata('time_zone');
        if(!empty($this->timezone)){
          date_default_timezone_set($this->timezone);
        }
        $this->load->model('users_model','users');
        $this->load->model('signin_model','signin');
         

    }


	public function doctors()
	{
	    $this->data['page'] = 'doctors';
      $this->load->vars($this->data);
      $this->load->view($this->data['theme'].'/template');
	   
	}

  public function doctors_list()
   {
      $list = $this->users->get_doctor_datatables();
      $data = array();
      $no = $_POST['start'];
      $a=1;
       
      foreach ($list as $doctor) {

        $val='';

      if($doctor['status'] == '1')
        {
          $val = 'checked';
        }

        $profileimage=(!empty($doctor['profileimage']))?base_url().$doctor['profileimage']:base_url().'assets/img/user.png';
        
        $no++;
        $row = array();
        $row[] = $a++;
        $row[] ='#D00'.$doctor['id'];
        $row[] = '<h2 class="table-avatar">
                    <a target="_blank" href="'.base_url().'doctor-preview/'.$doctor['username'].'" class="avatar avatar-sm mr-2"><img class="avatar-img rounded-circle" src="'.$profileimage.'" alt="User Image"></a>
                    <a target="_blank" href="'.base_url().'doctor-preview/'.$doctor['username'].'">Dr. '.ucfirst($doctor['first_name'].' '.$doctor['last_name']).'</a>
                  </h2>';
        $row[] = ucfirst($doctor['specialization']);  
        $row[] = $doctor['email'];
        $row[] = $doctor['mobileno'];       
        $row[] = date('d M Y',strtotime($doctor['created_date'])).'<br><small>'.date('h:i A',strtotime($doctor['created_date'])).'</small>';
        $row[] = get_earned($doctor['id']);
        $row[] = '<div class="status-toggle">
                      <input type="checkbox" onchange="change_usersStatus('.$doctor['id'].')" id="status_'.$doctor['id'].'" class="check" '.$val.'>
                      <label for="status_'.$doctor['id'].'" class="checktoggle">checkbox</label>
                    </div>';

        $docs = $this->db->where('user_id',$doctor['id'])->get('document_details')->result_array();
        
        $doc_data = '';
        if(!empty($docs)){
          foreach ($docs as $doc) {
            $doc_data .= '<a href="'.base_url().$doc["doc_file"].'">'.ucwords($doc['doc_name']).'</a><br>';
          }
        }else{
          $doc_data = '<a href="javascript:;">No Documents</a><br>';
        }

        $row[] = $doc_data;
        
        $data[] = $row;
      }



      $output = array(
              "draw" => $_POST['draw'],
              "recordsTotal" => $this->users->doctor_count_all(),
              "recordsFiltered" => $this->users->doctor_count_filtered(),
              "data" => $data,
          );
      //output to json format
      echo json_encode($output);
  }



  public function patients()
  {
      $this->data['page'] = 'patients';
      $this->load->vars($this->data);
      $this->load->view($this->data['theme'].'/template');
     
  }

  public function patients_list()
   {
      $list = $this->users->get_patient_datatables();
      $data = array();
      $no = $_POST['start'];
      $a=1;
       
      foreach ($list as $patient) {

        $val='';

      if($patient['status'] == '1')
        {
          $val = 'checked';
        }

        $profileimage=(!empty($patient['profileimage']))?base_url().$patient['profileimage']:base_url().'assets/img/user.png';
        
        $no++;
        $row = array();
        $row[] = $a++;
        $row[] ='#PT00'.$patient['id'];
        $row[] = '<h2 class="table-avatar">
                    <a target="_blank" href="'.base_url().'patient-preview/'.base64_encode($patient['id']).'" class="avatar avatar-sm mr-2"><img class="avatar-img rounded-circle" src="'.$profileimage.'" alt="User Image"></a>
                    <a target="_blank" href="'.base_url().'patient-preview/'.base64_encode($patient['id']).'">'.ucfirst($patient['first_name'].' '.$patient['last_name']).'</a>
                  </h2>';
        $row[] = (!empty($patient['dob']))?age_calculate($patient['dob']):'';
        $row[] = $patient['blood_group']; 
        $row[] = $patient['email'];
        $row[] = $patient['mobileno'];      
        $row[] = date('d M Y',strtotime($patient['created_date'])).'<br><small>'.date('h:i A',strtotime($patient['created_date'])).'</small>';
        $row[] = '<div class="status-toggle">
                      <input type="checkbox" onchange="change_usersStatus('.$patient['id'].')" id="status_'.$patient['id'].'" class="check" '.$val.'>
                      <label for="status_'.$patient['id'].'" class="checktoggle">checkbox</label>
                    </div>';
        
        $data[] = $row;
      }



      $output = array(
              "draw" => $_POST['draw'],
              "recordsTotal" => $this->users->patient_count_all(),
              "recordsFiltered" => $this->users->patient_count_filtered(),
              "data" => $data,
          );
      //output to json format
      echo json_encode($output);
  }


  public function change_usersStatus()
    {

      $id=$this->input->post('id');
      $status=$this->input->post('status');

       $data = array(
                'status' =>$status,
            );
        $this->users->update(array('id' => $id), $data);
        echo json_encode(array("status" => TRUE));
    }


    public function check_email()
  {
        $email = $this->input->post('email');     
        $result = $this->signin->check_email($email);
         if ($result > 0) {
                   echo 'false';
           } else {
                   echo 'true';
           }
           
  }
public function check_mobileno()
  {
        $mobileno = $this->input->post('mobileno');     
        $result = $this->signin->check_mobileno($mobileno);
         if ($result > 0) {
                   echo 'false';
           } else {
                   echo 'true';
           }
           
  }
public function signup()
  {
    $inputdata=array();
    $response=array();
      
      $inputdata['first_name']=$this->input->post('first_name');
      $inputdata['last_name']=$this->input->post('last_name');
      $inputdata['email']=$this->input->post('email');
      $inputdata['mobileno']=$this->input->post('mobileno');
      $inputdata['country_code']=$this->input->post('country_code');
      $inputdata['username'] = generate_username($inputdata['first_name'].' '.$inputdata['last_name'].' '.$inputdata['mobileno']);
      $inputdata['role']=$this->input->post('role');
      $inputdata['password']=md5($this->input->post('password'));
      $inputdata['confirm_password']=md5($this->input->post('confirm_password'));
      $inputdata['created_date']=date('Y-m-d H:i:s');
      $inputdata['is_verified']=1;
      $already_exits=$this->db->where('email',$inputdata['email'])->get('users')->num_rows();
      $already_exits_mobile_no=$this->db->where('mobileno',$inputdata['mobileno'])->get('users')->num_rows();
  
      if($already_exits >=1)
      {
              $response['msg']='Email already exits';
              $response['status']=500;
      }
      else if($already_exits_mobile_no >=1)
      {
              $response['msg']='Mobile no already exits';
              $response['status']=500;
      }
      else
      {
          $result=$this->signin->signup($inputdata);
          if($result==true)
          {   
               $response['msg']='Registration success'; 
               $response['status']=200;              
          }
         else
          {
               $response['msg']='Registration failed';
              $response['status']=500; 
          } 

      }
      
    echo json_encode($response);
  }


}
