<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Specialization extends CI_Controller {

   public $data;

   public function __construct() {

        parent::__construct();

        if($this->session->userdata('admin_id') ==''){
            redirect(base_url().'admin/login');
        }
        
        $this->data['theme']     = 'admin';
        $this->data['module']    = 'specialization';
        $this->data['page']     = '';
        $this->data['base_url'] = base_url();
         $this->timezone = $this->session->userdata('time_zone');
        if(!empty($this->timezone)){
          date_default_timezone_set($this->timezone);
        }
        $this->load->model('specialization_model','specializations');
        

    }


	public function index()
	{
	    $this->data['page'] = 'index';
      $this->load->vars($this->data);
      $this->load->view($this->data['theme'].'/template');
	   
	}

    public function create_specialization()
    {   
            $id=$this->input->post('id');
            $method=$this->input->post('method');
            $data['specialization']=$this->input->post('specialization');
            $data['specialization_img']=$this->input->post('specialization_imgs');
    
             if($_FILES["specialization_image"]["name"] != '')
             {
                 $config["upload_path"] = './uploads/specialization/';
                 $config["allowed_types"] = '*';
                 $this->load->library('upload', $config);
                 $this->upload->initialize($config);


                        $_FILES["file"]["name"] = 'img_'.time().'.png';
                        $_FILES["file"]["type"] = $_FILES["specialization_image"]["type"];
                        $_FILES["file"]["tmp_name"] = $_FILES["specialization_image"]["tmp_name"];
                        $_FILES["file"]["error"] = $_FILES["specialization_image"]["error"];
                        $_FILES["file"]["size"] = $_FILES["specialization_image"]["size"];
                        if($this->upload->do_upload('file'))
                        {
                           $upload_data = $this->upload->data();
                          
                            $specialization_img='uploads/specialization/'.$upload_data["file_name"];
                            
                            $data['specialization_img']=$specialization_img;
                                                                         
                        }
                }
              
               $data['status'] = 1; 

            if($method=='update')
            {
                  
              $this->db->where('specialization',$data['specialization']);
              $this->db->where('id !=',$id);
              $this->db->where('status',1);
              $query = $this->db->get('specialization');

            
              if ($query->num_rows() > 0)
              {
                $datas['result']='exe';
                $datas['status']='Specialization already exits!';
              }
              else
              {
                    $this->db->where('id',$id);
                    $this->db->update('specialization',$data);
                    $result=($this->db->affected_rows()!= 1)? false:true;

                    if(@$result==true) 
                     {
                        $datas['result']='true';
                        $datas['status']='Specialization update successfully';
                     }  
                     else
                     {
                        $datas['result']='false';
                        $datas['status']='Specialization update failed!';
                     }
              }       

            }
            else
            {

              $this->db->where('specialization',$data['specialization']);
              $this->db->where('status',1);
              $query = $this->db->get('specialization');

            
              if ($query->num_rows() > 0)
              {
                $datas['result']='exe';
                $datas['status']='Specialization already exits!';
              }
              else
              {
                $this->db->insert('specialization',$data);
                $result=($this->db->affected_rows()!= 1)? false:true;

                if(@$result==true) 
                 {
                    $datas['result']='true';
                    $datas['status']='Specialization added successfully';
                 }  
                 else
                 {
                    $datas['result']='false';
                    $datas['status']='Specialization added failed!';
                 }

               }  

            }               
            
            echo json_encode($datas);
    
    }
    
    
    public function specialization_list()
  {
    $list = $this->specializations->get_datatables();
    $data = array();
    $no = $_POST['start'];
    $a=1;
     
    foreach ($list as $specializations) {

      $no++;
      $row = array();
      $row[] = $a++;
      $row[] = '<h2 class="table-avatar">
                    <a href="javascript:void(0);" class="avatar avatar-sm mr-2">
                      <img class="avatar-img" src="'.base_url().$specializations['specialization_img'].'" alt="Speciality">
                    </a>
                    <a href="javascript:void(0);">'.$specializations['specialization'].'</a>
                 </h2>';
               
      $row[] = '<div class="actions">
                  <a class="btn btn-sm bg-success-light" onclick="edit_specialization('.$specializations['id'].')" href="javascript:void(0)">
                    <i class="fe fe-pencil"></i> Edit
                  </a>
                  <a class="btn btn-sm bg-danger-light" href="javascript:void(0)" onclick="delete_specialization('.$specializations['id'].')">
                    <i class="fe fe-trash"></i> Delete
                  </a>
                </div>';
      
      $data[] = $row;
    }



    $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->specializations->count_all(),
            "recordsFiltered" => $this->specializations->count_filtered(),
            "data" => $data,
        );
    //output to json format
    echo json_encode($output);
  }


    public function specialization_edit($id)
    {
        $data = $this->specializations->get_by_id($id);
        
        echo json_encode($data);
    }

   public function specialization_delete($id)
    {
        $data = array(
                'status' =>0,
            );
        $this->specializations->update(array('id' => $id), $data);
        echo json_encode(array("status" => TRUE));
    }


}
