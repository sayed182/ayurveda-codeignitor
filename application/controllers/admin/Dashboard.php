<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

   public $data;

   public function __construct() {

        parent::__construct();

        if($this->session->userdata('admin_id') ==''){
            redirect(base_url().'admin/login');
        }
        
        $this->data['theme']     = 'admin';
        $this->data['module']    = 'dashboard';
        $this->data['page']     = '';
        $this->data['base_url'] = base_url();
         $this->timezone = $this->session->userdata('time_zone');
        if(!empty($this->timezone)){
          date_default_timezone_set($this->timezone);
        }
        $this->load->model('dashboard_model','dashboard');
        

    }


	public function index()
	{
	    $this->data['page'] = 'index';
      $this->data['doctors'] = $this->dashboard->get_doctors();
      $this->data['patients'] = $this->dashboard->get_patients();
      $this->data['doctors_count'] = $this->dashboard->users_count(1);
      $this->data['patients_count'] = $this->dashboard->users_count(2);
      $this->data['appointments_count'] = $this->dashboard->appointments_count();
      $this->data['appointments'] = $this->dashboard->get_appointments();
      $this->data['revenue'] = $this->dashboard->get_revenue();
      $this->load->vars($this->data);
      $this->load->view($this->data['theme'].'/template');
	   
	}


}
