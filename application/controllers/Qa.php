<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Qa extends CI_Controller {

public function __construct() {

        parent::__construct();
        $this->data['theme']     = 'web';
        $this->data['module']    = 'qa';
        $this->data['page']     = '';
        $this->data['base_url'] = base_url();
        $this->timezone = $this->session->userdata('time_zone');
        if(!empty($this->timezone)){
          date_default_timezone_set($this->timezone);
        }


        $this->data['language'] =language();
        $this->language=language(); 
        $this->data['default_language']=default_language();
        $this->data['active_language'] = active_language();
        $this->load->model('qa_model','qa');
        $this->load->model('home_model','home');
        $this->load->model('specialization_model','special');
        

                 
    }

    public function index()
  {
      
      $this->data['page'] = 'index';
      //$this->data['categories']=$this->qa->get_categories();
      $this->data['specialization']=$this->special->get_specilization();
      $this->load->vars($this->data);
      $this->load->view($this->data['theme'].'/template');
     
  }

    public function sub_category()
  {
       $category_slug='';
       if(isset($_GET['category']) && !empty($_GET['category']))
       {
          $category_slug=$_GET['category'];
          $category = $this->qa->get_category_id(urldecode($category_slug));
          $this->data['page'] = 'sub_category';
          $this->data['category_name'] = $category['category_name'];
          $this->data['subcategories']=$this->qa->get_subcategories($category_slug);
          $this->load->vars($this->data);
          $this->load->view($this->data['theme'].'/template');
       }
       else
       {
          redirect(base_url().'q&a');
       }
       
           
  }

   public function questions_answers()
  {

      $this->data['specilization']=$this->uri->segment('3');

     
      $this->data['page'] = 'questions_answers';
      $this->load->vars($this->data);
      $this->load->view($this->data['theme'].'/template');
  }

  public function add_questions()
  {
      $inputdata=array();
      $response=array();
      if($this->session->userdata('user_id')) {
        if($this->session->userdata('role')=='2'){

            $inputdata['user_id']=$this->session->userdata('user_id');
            $inputdata['specilization_id']=$this->input->post('specialization');
            $inputdata['questions']=$this->input->post('questions');
            $inputdata['questions_description']=$this->input->post('questions_description');
            $inputdata['created_date']=date('Y-m-d H:i:s');
            $inputdata['status']=1;
            $result=$this->qa->add_questions($inputdata);
            if($result==true)
            {   
                 $response['msg']='Questions added successfully'; 
                 $response['status']=200;              
            }
           else
            {
                $response['msg']='Questions added failed';
                $response['status']=500; 
            } 
        }
        else
        {
           $response['msg']='Invalid login';
           $response['status']=401; 
        }
      }
      else
      {
        $response['msg']='Please login to continue';
        $response['status']=401; 
      }

      echo json_encode($response);
  } 

  public function add_answers()
  {
      $inputdata=array();
      $response=array();
      if($this->session->userdata('user_id')) {
        if($this->session->userdata('role')=='1'){

            $inputdata['user_id']=$this->session->userdata('user_id');
            $inputdata['question_id']=$this->input->post('question_id');
            $inputdata['answers']=$this->input->post('answers');
            $inputdata['created_date']=date('Y-m-d H:i:s');
            $result=$this->qa->add_answers($inputdata);
            if($result==true)
            {   
                 $response['msg']='Answers added successfully'; 
                 $response['status']=200;              
            }
           else
            {
                $response['msg']='Answers added failed';
                $response['status']=500; 
            } 
        }
        else
        {
           $response['msg']='Invalid login';
           $response['status']=401; 
        }
      }
      else
      {
        $response['msg']='Please login to continue';
        $response['status']=401; 
      }

      echo json_encode($response);
  }


  public function update_answers()
  {
      $inputdata=array();
      $response=array();
      if($this->session->userdata('user_id')) {
        if($this->session->userdata('role')=='1'){

            $answer_id=$this->input->post('answer_id');
            $inputdata['answers']=$this->input->post('answers');
            $inputdata['created_date']=date('Y-m-d H:i:s');
            $result=$this->qa->update_answers($inputdata,$answer_id);
            if($result==true)
            {   
                 $response['msg']='Answers update successfully'; 
                 $response['status']=200;              
            }
           else
            {
                $response['msg']='Answers update failed';
                $response['status']=500; 
            } 
        }
        else
        {
           $response['msg']='Invalid login';
           $response['status']=401; 
        }
      }
      else
      {
        $response['msg']='Please login to continue';
        $response['status']=401; 
      }

      echo json_encode($response);
  }

  public function get_questions()
  {

      
      $page=$this->input->post('page');
      
      $specialization=$this->input->post('specialization');
      $limit=5;
      $response['count'] =$this->qa->get_questions($page,$limit,1,$specialization);
      $data['questions']=$this->qa->get_questions($page,$limit,2,$specialization);
      $response['questions_list']= $this->load->view('web/modules/qa/questions_view',$data,TRUE);
      $response['current_page_no']= $page;
      $response['total_page']= ceil($response['count']/$limit);
      echo json_encode($response);
  }

  public function get_answers()
  {
      $question_id=$this->input->post('question_id');
      $data['answers']=$this->qa->get_answers($question_id);
      $response['answers_list']= $this->load->view('web/modules/qa/answers_view',$data,TRUE);
      echo json_encode($response);
  }


  public function delete_details()
  {
    $id=$this->input->post('id');
    $table_name=$this->input->post('delete_table');

    if($table_name=='questions')
    {
      $this->db->delete('questions',array('id'=>$id));
      $this->db->delete('answers',array('question_id'=>$id));
      echo 1;
    }

    if($table_name=='answers')
    {
      $this->db->delete('answers',array('id'=>$id));
      echo 1;
    }

    
  }

 //Delete question from patients my question sestion
  public function delete_question($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('questions');
        echo json_encode(array("status" => TRUE));
    }

  public function my_questions()
  {
     $user_id=$this->session->userdata('user_id');
        
        if($this->session->userdata('role')=='1'){
           redirect(base_url().'dashboard');
        }
        else
        {
          $this->data['module']    = 'patient';
          $this->data['page'] = 'my_questions';
          $this->data['my_questions'] = $this->qa->get_my_questions($user_id);
          $this->data['specialization']=$this->special->get_specilization();
          $this->load->vars($this->data);
          $this->load->view($this->data['theme'].'/template');
        }
  }

   public function view_answers($id)
  {
    $question_id=$id;
    $this->data['page'] = 'questions_answers';
    $this->data['question']=$this->qa->questions_detail($question_id);
    $this->data['answers']=$this->qa->get_answers($question_id);
    $this->load->vars($this->data);
    $this->load->view($this->data['theme'].'/template');
    //echo $html;
  }

   public function patient_answers()
  
  {
    $question_id=$this->input->post('question_id');
    $question_details=$this->db->where('id',$question_id)->get('questions')->row_array();
    $answers=$this->qa->get_answers($question_id);
    $html='';
    
      
        $html .='<p class="mb-1">Posted date: '.date('d M Y',strtotime($question_details['created_date'])).'</p>
              <h3>'.$question_details['questions'].'</h3> 
              <hr>';
              if(!empty($answers))
            {
            foreach ($answers as $rows) {
              $profile_image=(!empty($rows['profileimage']))?base_url().$rows['profileimage']:base_url().'assets/img/user.png';
              $html .='<div class="que-reply">
              <h5 class="table-avatar d-block">
                <a target="_blank" href="'.base_url().'doctor-preview/'.$rows['username'].'" class="avatar avatar-sm mr-2">
                  <img class="avatar-img rounded-circle" src="'.$profile_image.'" alt="User Image">
                </a>
                <a href="'.base_url().'doctor-preview/'.$rows['username'].'" class="">
                <span class="text-secondary">Dr. '.ucfirst($rows['name']).'</span>
              </a>
              </h5>
              <div class="d-block">
                <p class="text-muted">'.$rows['answers'].' </p>
                <p class="mb-1">'.date('d M Y',strtotime($rows['created_date'])).'</p>
              </div>
            </div>';
         }
    }

    echo $html;
  }






}