<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Accounts extends CI_Controller {

   public $data;

   public function __construct() {

        parent::__construct();
        if($this->session->userdata('user_id') ==''){
          if($this->session->userdata('admin_id'))
            {
              redirect(base_url().'home');
            }
            else
            {
              redirect(base_url().'signin');
            }
        }
        
        $this->data['theme']     = 'web';
        $this->data['module']    = 'invoice';
        $this->data['page']     = '';
        $this->data['base_url'] = base_url();
         $this->timezone = $this->session->userdata('time_zone');
        if(!empty($this->timezone)){
          date_default_timezone_set($this->timezone);
        }
        $this->data['language'] =language();
        $this->language=language(); 
        $this->data['default_language']=default_language();
        $this->data['active_language'] = active_language();
        $this->load->model('accounts_model','accounts');
    }


	public function index()
	{ 
    $user_id=$this->session->userdata('user_id');
        if($this->session->userdata('role')=='1'){
          $this->data['module']    = 'doctor';
          $this->data['page'] = 'accounts';

          $user_currency=get_user_currency();
          $user_currency_code=$user_currency['user_currency_code'];
          $user_currency_rate=$user_currency['user_currency_rate'];

          $currency_option = (!empty($user_currency_code))?$user_currency_code:'USD';
          $rate_symbol = currency_code_sign($currency_option);

          $this->data['currency_symbol']=$rate_symbol;

          $this->data['balance'] = $this->accounts->get_balance($user_id);
          $this->data['requested'] = $this->accounts->get_requested($user_id);
          $this->data['earned'] = $this->accounts->get_earned($user_id);
          $this->data['account_details'] = $this->accounts->get_account_details($user_id);
          $this->load->vars($this->data);
          $this->load->view($this->data['theme'].'/template');
        }
        else
        {
          $this->data['module']    = 'patient';
          $this->data['page'] = 'accounts';

          $user_currency=get_user_currency();
          $user_currency_code=$user_currency['user_currency_code'];
          $user_currency_rate=$user_currency['user_currency_rate'];

          $currency_option = (!empty($user_currency_code))?$user_currency_code:'USD';
          $rate_symbol = currency_code_sign($currency_option);

          $this->data['currency_symbol']=$rate_symbol;

          $this->data['balance'] = $this->accounts->get_patient_balance($user_id);
          $this->data['requested'] = $this->accounts->get_requested($user_id);
          $this->data['earned'] = $this->accounts->get_earned($user_id);
          $this->data['account_details'] = $this->accounts->get_account_details($user_id);
          $this->load->vars($this->data);
          $this->load->view($this->data['theme'].'/template');
        }
	}

    public function doctor_accounts_list()
  { 
    $user_id=$this->session->userdata('user_id');
    $list = $this->accounts->get_datatables($user_id);
    $data = array();
    $no = $_POST['start'];
    $a=1;

    foreach ($list as $account) {

      $patient_profileimage=(!empty($account['patient_profileimage']))?base_url().$account['patient_profileimage']:base_url().'assets/img/user.png';

        $tax_amount=$account['tax_amount']+$account['transcation_charge'];
        
        $amount=($account['total_amount']) - ($tax_amount);
        $patient_currency=$account['currency_code'];
        $commission = !empty(settings("commission"))?settings("commission"):"0";
        $commission_charge = ($amount * ($commission/100));

        if($account['request_status'] == '6'){

        $total_amount = ($amount );

        }else{
        $total_amount = ($amount - $commission_charge);

        }

        

        $user_currency=get_user_currency();
        $user_currency_code=$user_currency['user_currency_code'];
        $user_currency_rate=$user_currency['user_currency_rate'];

        $currency_option = (!empty($user_currency_code))?$user_currency_code:'USD';
        $rate_symbol = currency_code_sign($currency_option);

        $org_amount=get_doccure_currency($total_amount,$patient_currency,$user_currency_code);

        switch ($account['request_status']) {
          case '0':
          $status='<span class="badge badge-primary">New</span>';
          $action='<a href="javascript:void(0)" onclick="send_request(\''.$account['id'].'\',\'1\')" class="btn btn-sm bg-info-light">Send Request</a>';
          break;
          case '1':
          $status='<span class="badge badge-warning">Waiting for Patient approvel</span>';
          $action='';
          break;
          case '2':
          $status='<span class="badge badge-success">Approved</span>';
          $action='';
          break;
          case '3':
          $status='<span class="badge badge-warning">Payment Request Sent</span>';
          $action='';
          break; 
          case '4':
          $status='<span class="badge badge-success">Payment Received</span>';
          $action='';
          break; 
          case '5':
          $status='<span class="badge badge-danger">Cancelled</span>';
          $action='<a href="javascript:void(0)" onclick="send_request(\''.$account['id'].'\',\'1\')" class="btn btn-sm bg-info-light">Send Request</a>';
          break;  

          case '6':
          $status='<span class="badge badge-warning">Waiting for approvel</span>';
          $action='';
          break;

          case '7':
          $status='<span class="badge badge-info">Refund</span>';
          $action='';
          break;
         
          default:
           $status='<span class="badge badge-primary">New</span>';
           $action='<a href="javascript:void(0)" onclick="send_request(\''.$account['id'].'\',\'1\')" class="btn btn-sm bg-info-light">Send Request</a>';
           break;
        }


      $no++;
      $row = array();
      $row[]=date('d M Y',strtotime($account['payment_date']));
      $row[] = '<h2 class="table-avatar">
                  <a target="_blank" href="'.base_url().'patient-preview/'.base64_encode($account['patient_id']).'" class="avatar avatar-sm mr-2"><img class="avatar-img rounded-circle" src="'.$patient_profileimage.'" alt="User Image"></a>
                  <a target="_blank" href="'.base_url().'patient-preview/'.base64_encode($account['patient_id']).'">'.ucfirst($account['patient_name']).' </a>
                </h2>';

          
      

      $row[]=$rate_symbol.number_format($org_amount,2);
      $row[]=$status; 
      $row[]=$action; 
      
      
      $data[] = $row;
    }
   

    $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->accounts->count_all($user_id),
            "recordsFiltered" => $this->accounts->count_filtered($user_id),
            "data" => $data,
        );
    //output to json format
    echo json_encode($output);
  }

   public function patient_refund_request()
  { 
    $user_id=$this->session->userdata('user_id');
    $list = $this->accounts->get_refund_datatables($user_id);
    $data = array();
    $no = $_POST['start'];
    $a=1;

    foreach ($list as $account) {

      $patient_profileimage=(!empty($account['patient_profileimage']))?base_url().$account['patient_profileimage']:base_url().'assets/img/user.png';

       $tax_amount=$account['tax_amount']+$account['transcation_charge'];
        
        $amount=($account['total_amount']) - ($tax_amount);
        $patient_currency=$account['currency_code'];
        $commission = !empty(settings("commission"))?settings("commission"):"0";
        $commission_charge = ($amount * ($commission/100));
        $total_amount = ($amount );

        $user_currency=get_user_currency();
        $user_currency_code=$user_currency['user_currency_code'];
        $user_currency_rate=$user_currency['user_currency_rate'];

        $currency_option = (!empty($user_currency_code))?$user_currency_code:'USD';
        $rate_symbol = currency_code_sign($currency_option);

        $org_amount=get_doccure_currency($total_amount,$patient_currency,$user_currency_code);

       

        switch ($account['request_status']) {
          case '6':
          $status='<span class="badge badge-warning">Waiting for approvel</span>';
          $action='<a href="javascript:void(0)" onclick="send_request(\''.$account['id'].'\',\'7\')" class="btn btn-sm bg-info-light">Approve</a> <a href="javascript:void(0)" onclick="send_request(\''.$account['id'].'\',\'8\')" class="btn btn-sm bg-info-light">Cancel</a>';
          break;
                   
          default:
           $status='<span class="badge badge-primary">New</span>';
           $action='';
           break;
        }


      $no++;
      $row = array();
      $row[]=date('d M Y',strtotime($account['payment_date']));
      $row[] = '<h2 class="table-avatar">
                  <a target="_blank" href="'.base_url().'patient-preview/'.base64_encode($account['patient_id']).'" class="avatar avatar-sm mr-2"><img class="avatar-img rounded-circle" src="'.$patient_profileimage.'" alt="User Image"></a>
                  <a target="_blank" href="'.base_url().'patient-preview/'.base64_encode($account['patient_id']).'">'.ucfirst($account['patient_name']).' </a>
                </h2>';

          
      

      $row[]=$rate_symbol.$org_amount;
      $row[]=$status; 
      $row[]=$action; 
      
      
      $data[] = $row;
    }
   

    $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->accounts->refund_count_all($user_id),
            "recordsFiltered" => $this->accounts->refund_count_filtered($user_id),
            "data" => $data,
        );
    //output to json format
    echo json_encode($output);
  }

  public function send_request()
  {
    $payment_id=$this->input->post('id');
    $status=$this->input->post('status');
    $this->db->where('id',$payment_id);
    $this->db->update('payments',array('request_status' =>$status));
  }

  public function doctor_request()
  { 
    $user_id=$this->session->userdata('user_id');
    $list = $this->accounts->get_doctor_request_datatables($user_id);
    $data = array();
    $no = $_POST['start'];
    $a=1;

    foreach ($list as $account) {

      $doctor_profileimage=(!empty($account['doctor_profileimage']))?base_url().$account['doctor_profileimage']:base_url().'assets/img/user.png';

        $tax_amount=$account['tax_amount']+$account['transcation_charge'];
        
        $amount=($account['total_amount']) - ($tax_amount);
        $patient_currency=$account['currency_code'];
        $commission = !empty(settings("commission"))?settings("commission"):"0";
        $commission_charge = ($amount * ($commission/100));
        $total_amount = ($amount - $commission_charge);

        $user_currency=get_user_currency();
        $user_currency_code=$user_currency['user_currency_code'];
        $user_currency_rate=$user_currency['user_currency_rate'];

        $currency_option = (!empty($user_currency_code))?$user_currency_code:'USD';
        $rate_symbol = currency_code_sign($currency_option);

        $org_amount=get_doccure_currency($total_amount,$patient_currency,$user_currency_code);

        switch ($account['request_status']) {
         
          case '1':
          $status='<span class="badge badge-warning">Waiting for approvel</span>';
          $action='<a href="javascript:void(0)" onclick="send_request(\''.$account['id'].'\',\'2\')" class="btn btn-sm bg-info-light">Approve</a> <a href="javascript:void(0)" onclick="send_request(\''.$account['id'].'\',\'5\')" class="btn btn-sm bg-info-light">Cancel</a>';
          break;
          case '5':
          $status='<span class="badge badge-danger">Cancelled</span>';
          $action='';
          break;  
         
          default:
           $status='';
           $action='';
           break;
        }


      $no++;
      $row = array();
      $row[]=date('d M Y',strtotime($account['payment_date']));
      $row[] = '<h2 class="table-avatar">
                  <a target="_blank" href="'.base_url().'doctor-preview/'.$account['doctor_username'].'" class="avatar avatar-sm mr-2">
                    <img class="avatar-img rounded-circle" src="'.$doctor_profileimage.'" alt="User Image">
                  </a>
                  <a target="_blank" href="'.base_url().'doctor-preview/'.$account['doctor_username'].'">Dr. '.ucfirst($account['doctor_name']).'</a>
                </h2>';

          
      

      $row[]=$rate_symbol.$org_amount;
      $row[]=$status; 
      $row[]=$action; 
      
      
      $data[] = $row;
    }
   

    $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->accounts->doctor_request_count_all($user_id),
            "recordsFiltered" => $this->accounts->doctor_request_filtered($user_id),
            "data" => $data,
        );
    //output to json format
    echo json_encode($output);
  }

  public function patient_accounts_list()
  { 
    $user_id=$this->session->userdata('user_id');
    $list = $this->accounts->get_patient_accounts_datatables($user_id);
    $data = array();
    $no = $_POST['start'];
    $a=1;

    foreach ($list as $account) {

      $doctor_profileimage=(!empty($account['doctor_profileimage']))?base_url().$account['doctor_profileimage']:base_url().'assets/img/user.png';

        $tax_amount=$account['tax_amount']+$account['transcation_charge'];
        
        $amount=($account['total_amount']) - ($tax_amount);
        $patient_currency=$account['currency_code'];
        $commission = !empty(settings("commission"))?settings("commission"):"0";
        $commission_charge = ($amount * ($commission/100));
        $total_amount = ($amount );

        $user_currency=get_user_currency();
        $user_currency_code=$user_currency['user_currency_code'];
        $user_currency_rate=$user_currency['user_currency_rate'];

        $currency_option = (!empty($user_currency_code))?$user_currency_code:'USD';
        $rate_symbol = currency_code_sign($currency_option);

        $org_amount=get_doccure_currency($total_amount,$patient_currency,$user_currency_code);

        
        switch ($account['request_status']) {
         
          case '0':
          $status='<span class="badge badge-primary">New</span>';
          $action='<a href="javascript:void(0)" onclick="send_request(\''.$account['id'].'\',\'6\')" class="btn btn-sm bg-info-light">Send Request</a>';
          break;
          case '1':
          $status='<span class="badge badge-warning">Waiting for approvel</span>';
          $action='';
          break;
          case '2':
          $status='<span class="badge badge-success">Appoinment Completed</span>';
          $action='';
          break;
          case '3':
          $status='<span class="badge badge-warning">Appoinment Completed</span>';
          $action='';
          break; 
          case '4':
          $status='<span class="badge badge-success">Appoinment Completed</span>';
          $action='';
          break; 
          case '5':
          $status='<span class="badge badge-primary">New</span>';
          $action='<a href="javascript:void(0)" onclick="send_request(\''.$account['id'].'\',\'6\')" class="btn btn-sm bg-info-light">Send Request</a>';
          break;  

          case '6':
          $status='<span class="badge badge-warning">Waiting for doctor approvel</span>';
          $action='';
          break;

          case '7':
          $status='<span class="badge badge-success">Refund Approved</span>';
          $action='';
          break;

          case '8':
          $status='<span class="badge badge-danger">Cancelled</span>';
          $action='<a href="javascript:void(0)" onclick="send_request(\''.$account['id'].'\',\'6\')" class="btn btn-sm bg-info-light">Send Request</a>';
          break;

         
          default:
           $status='<span class="badge badge-primary">New</span>';
           $action='<a href="javascript:void(0)" onclick="send_request(\''.$account['id'].'\',\'1\')" class="btn btn-sm bg-info-light">Send Request</a>';
           break;
        }


      $no++;
      $row = array();
      $row[]=date('d M Y',strtotime($account['payment_date']));
      $row[] = '<h2 class="table-avatar">
                  <a target="_blank" href="'.base_url().'doctor-preview/'.$account['doctor_username'].'" class="avatar avatar-sm mr-2">
                    <img class="avatar-img rounded-circle" src="'.$doctor_profileimage.'" alt="User Image">
                  </a>
                  <a target="_blank" href="'.base_url().'doctor-preview/'.$account['doctor_username'].'">Dr. '.ucfirst($account['doctor_name']).'</a>
                </h2>';

          
      

      $row[]=$rate_symbol.$org_amount;
      $row[]=$status; 
      $row[]=$action; 
      
      
      $data[] = $row;
    }
   

    $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->accounts->patient_accounts_count_all($user_id),
            "recordsFiltered" => $this->accounts->patient_accounts_filtered($user_id),
            "data" => $data,
        );
    //output to json format
    echo json_encode($output);
  }


  public function get_account_details()
    {
        $user_id=$this->session->userdata('user_id');
        $data = $this->accounts->get_account_details($user_id);
        echo json_encode($data);
    }


    public function add_account_details()
    {
      $inputdata=array();

      $inputdata['user_id']=$this->session->userdata('user_id');
      $inputdata['bank_name']=$this->input->post('bank_name');
      $inputdata['branch_name']=$this->input->post('branch_name');
      $inputdata['account_no']=$this->input->post('account_no');
      $inputdata['account_name']=$this->input->post('account_name');

      $already_exits=$this->db->where('user_id',$inputdata['user_id'])->get('account_details')->num_rows();
      if($already_exits==0)
      {
        $this->db->insert('account_details',$inputdata);
      }
      else
      {
        $this->db->where('user_id',$inputdata['user_id']);
        $this->db->update('account_details',$inputdata);
      }

       $result=($this->db->affected_rows()!= 1)? false:true;

        if(@$result==true) 
         {
            $datas['result']='true';
            $datas['status']='Account details added successfully';
         }  
         else
         {
            $datas['result']='true';
            $datas['status']='Account details added successfully';
         }

          echo json_encode($datas);

    }

     public function payment_request()
    {
      $inputdata=array();

      $user_currency=get_user_currency();
      $user_currency_code=$user_currency['user_currency_code'];
      $user_currency_rate=$user_currency['user_currency_rate'];


      $inputdata['user_id']=$this->session->userdata('user_id');
      $inputdata['payment_type']=$this->input->post('payment_type');
      $inputdata['request_amount']=$this->input->post('request_amount');
      $inputdata['currency_code']=$user_currency_code;
      $inputdata['description']=$this->input->post('description');
      $inputdata['request_date']=date('Y-m-d H:i:s');
     

      $already_exits=$this->db->where('user_id',$inputdata['user_id'])->get('account_details')->num_rows();
      if($already_exits==0)
      {
            $datas['result']='false';
            $datas['status']='Please enter account details';
            echo json_encode($datas);
            return false;

      }
      
          if($inputdata['payment_type']=='1')
          {
            $balance=$this->accounts->get_balance($inputdata['user_id']);
          }

          if($inputdata['payment_type']=='2')
          {
            $balance=$this->accounts->get_patient_balance($inputdata['user_id']);
          }

          $requested = $this->accounts->get_requested($inputdata['user_id']);
          $earned = $this->accounts->get_earned($inputdata['user_id']);

          $balance=$balance-($earned+$requested);

    if($balance < $inputdata['request_amount'])
      {
        $datas['result']='false';
        $datas['status']='Please enter valid amount';
        echo json_encode($datas);
        return false;
      }

      $this->db->insert('payment_request',$inputdata);
      $result=($this->db->affected_rows()!= 1)? false:true;
        if(@$result==true) 
         {
            $datas['result']='true';
            $datas['status']='Payment request send successfully';
         }  
         else
         {
            $datas['result']='false';
            $datas['status']='Payment request send failed!';
         }

          echo json_encode($datas);

    }

 




}
