<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends CI_Controller {

  public function __construct() {

    parent::__construct();
    $this->data['theme']     = 'web';
    $this->data['module']    = 'home';
    $this->data['page']     = '';
    $this->data['base_url'] = base_url();
//$this->load->model('home_model','home');



  }

  Public function set_timezone()
  {
    if(isset($_REQUEST['timezone'])){
      $array = array('time_zone' => $_REQUEST['timezone'],
    );
      $this->session->set_userdata( $array );
      echo json_encode($array);

    }
  }


  Public function set_language()
  {
    if(isset($_REQUEST['lang'])){
      $array = array('lang' => $_REQUEST['lang'],
    );
      $this->session->set_userdata( $array );
      echo json_encode($array);

    }
  }


  public function get_country_code()
  {
    $result=$this->db->where('countryid',101)->get('country')->result_array();
    $json=array();
    foreach ($result as $rows) {
      $data['value']=$rows['phonecode'];
      $data['label']=$rows['country'].'(+'.$rows['phonecode'].')';
      $json[]=$data;
    }

    echo json_encode($json);
  }

       public function get_users()
    {

       
        $result=$this->db->get('users')->result_array();
        $json=array();
       foreach ($result as $rows) {
          $data['value']=$rows['id'];
          $data['label']=$rows['first_name'];
          $json[]=$data;
       }

       get_users_details($json); 
    }

  public function get_country()
  {
    $result=$this->db->where('countryid',101)->get('country')->result_array();
    $json=array();
    foreach ($result as $rows) {
      $data['value']=$rows['countryid'];
      $data['label']=$rows['country'];
      $json[]=$data;
    }

    echo json_encode($json);
  }

  public function get_state()
  {

    $where=array('countryid' =>$_POST['id']);
    $result=$this->db->get_where('state',$where)->result_array();
    $json=array();
    foreach ($result as $rows) {
      $data['value']=$rows['id'];
      $data['label']=$rows['statename'];
      $json[]=$data;
    }

    echo json_encode($json);

  }

  public function get_city()
  {
    $where=array('stateid' =>$_POST['id']);
    $result=$this->db->get_where('city',$where)->result_array();
    $json=array();
    foreach ($result as $rows) {
      $data['value']=$rows['id'];
      $data['label']=$rows['city'];
      $json[]=$data;
    }

    echo json_encode($json);
  }


  public function get_specialization()
  {
    $where=array('status' =>1);
    $result=$this->db->get_where('specialization',$where)->result_array();
    $json=array();
    foreach ($result as $rows) {
      $data['value']=$rows['id'];
      $data['label']=$rows['specialization'];
      $json[]=$data;
    }

    echo json_encode($json);
  }


  public function get_category()
  {
    $where=array('status' =>1);
    $result=$this->db->get_where('categories',$where)->result_array();
    $json=array();
    foreach ($result as $rows) {
      $data['value']=$rows['id'];
      $data['label']=$rows['category_name'];
      $json[]=$data;
    }

    echo json_encode($json);
  }

  public function get_subcategory()
  {

    $where=array('category' =>$_POST['id']);
    $result=$this->db->get_where('subcategories',$where)->result_array();
    $json=array();
    foreach ($result as $rows) {
      $data['value']=$rows['id'];
      $data['label']=$rows['subcategory_name'];
      $json[]=$data;
    }

    echo json_encode($json);

  }



  public function currency_rate(){


    $sql="SELECT * from currency_rate"; 

    $count_row=$this->db->query($sql)->num_rows(); 

    if($count_row == 0){

           $this->currency_rate_update();
    }else{

      $row=$this->db->query($sql)->row_array(); 
      $date=date('Y-m-d H:i:s');

      if($row['updated_at'] < $date ){

         $this->currency_rate_update();
      }

    }



  }

  public function currency_rate_update(){

    $req_url = 'https://v6.exchangerate-api.com/v6/cc726126438b5513e5a41f69/latest/USD';
    $response_json = file_get_contents($req_url);

    // Continuing if we got a result
    if(false !== $response_json) {

    // Try/catch for json_decode operation
      try {

    // Decoding
        $response = json_decode($response_json);

    // Check for success
        if('success' === $response->result) {

          foreach ($response->conversion_rates as $key => $value) {
            $count=$this->db->where('currency_code',$key)->from('currency_rate')->count_all_results();  
            if($count==0){
              $data=array(
                'currency_code'=>$key,
                'rate'=>$value,
                'created_at'=>date('Y-m-d H:i:s')
              );
              $this->db->insert('currency_rate',$data);
            }else{
              $data=array(
                'rate'=>$value,
                'updated_at'=>date('Y-m-d H:i:s')
              );
              $this->db->where('currency_code',$key)->update('currency_rate',$data);
            }

          }
          echo "success";


        }

      }
      catch(Exception $e) {
        echo 'Caught exception: ',  $e->getMessage();
      }

    }



  }


  public function add_user_currency(){
    $params=$this->input->post();
    if(!empty($params['code'])){
      $user_id=$this->session->userdata('user_id');

      $where=array('user_id' =>$user_id);
      $user_details=$this->db->get_where('users_details',$where)->row_array();
      $current_amt=get_doccure_currency($user_details['amount'],$user_details['currency_code'],$params['code']);
      
      $result=$this->db->where('user_id',$user_id)->update('users_details',['currency_code'=>$params['code'],'amount'=>$current_amt]);

      if($result==true){
        echo json_encode(['success'=>true]);exit;
      }else{
        echo json_encode(['success'=>false]);exit;
      }

    }
  }


}
