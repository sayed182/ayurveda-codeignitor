<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Appoinments extends CI_Controller {

  public function __construct() {

        parent::__construct();

        if($this->session->userdata('user_id') ==''){
          if($this->session->userdata('admin_id'))
            {
              redirect(base_url().'home');
            }
            else
            {
              redirect(base_url().'signin');
            }
        }

        $this->data['theme']     = 'web';
        $this->data['page']     = '';
        $this->data['base_url'] = base_url();
        $this->timezone = $this->session->userdata('time_zone');
        if(!empty($this->timezone)){
          date_default_timezone_set($this->timezone);
        }
        $this->data['language'] =language();
        $this->language=language(); 
        $this->data['default_language']=default_language();
        $this->data['active_language'] = active_language();
        $this->load->model('appoinments_model','appoinment');
        
        
         
    }

    public function index()
    {
        $user_id = $this->session->userdata('user_id');
        $count = $this->appoinment->count_all($user_id);
        $this->data['appoinment_count'] = $count;
        if($this->session->userdata('role')=='1'){
          $this->data['module']    = 'doctor';
          $this->data['page'] = 'appoinments';
          $this->load->vars($this->data);
          $this->load->view($this->data['theme'].'/template');
        }
        else
        {
          $this->data['module']    = 'patient';
          $this->data['page'] = 'appoinments';
          $this->load->vars($this->data);
          $this->load->view($this->data['theme'].'/template');
        }
       
    }

    public function doctor_appoinments_list()
    {
        $response=array();
        $result=array();
        $page=$this->input->post('page');
        $limit=8;
        $user_id=$this->session->userdata('user_id');
        $response['count'] =$this->appoinment->doctor_appoinments_list($page,$limit,1,$user_id);
        $data['appoinments_list'] = $this->appoinment->doctor_appoinments_list($page,$limit,2,$user_id);
        $data['app_type']='doctor';
        $data['language']=$this->language;
        $result= $this->load->view('web/modules/appoinments/appoinments_view',$data,TRUE);
        $response['current_page_no']= $page;
        $response['total_page']= ceil($response['count']/$limit);
        $response['data']= $result;

     echo json_encode($response);

    }

    public function patient_appoinments_list()
    {
        $response=array();
        $result=array();
        $page=$this->input->post('page');
        $limit=8;
        $user_id=$this->session->userdata('user_id');
        $response['count'] =$this->appoinment->patient_appoinments_list($page,$limit,1,$user_id);
        $data['appoinments_list'] = $this->appoinment->patient_appoinments_list($page,$limit,2,$user_id);
        $data['app_type']='patient';
        $data['language']=$this->language;
        $result= $this->load->view('web/modules/appoinments/appoinments_view',$data,TRUE);
        $response['current_page_no']= $page;
        $response['total_page']= ceil($response['count']/$limit);
        $response['data']= $result;

     echo json_encode($response);

    }

    
    public function outgoingvideocall($appoinment_id)
    {
       if($this->session->userdata('role')=='1'){

             $data['appoinments_details']=$this->appoinment->get_appoinment_call_details($appoinment_id);
             $data['role']='patient';
             $this->call_details($data['appoinments_details'],'patient','Video');
             $this->load->view('web/modules/call/videocall',$data);
        }
        else
        {
           
            $data['appoinments_details']=$this->appoinment->get_appoinment_call_details($appoinment_id);
            $data['role']='doctor';
            $this->call_details($data['appoinments_details'],'doctor','Video');
            $this->load->view('web/modules/call/videocall',$data);
        }
    }


    public function outgoingcall($appoinment_id)
    {
       if($this->session->userdata('role')=='1'){

             $data['appoinments_details']=$this->appoinment->get_appoinment_call_details($appoinment_id);
             $data['role']='patient';
             $this->call_details($data['appoinments_details'],'patient','Audio');
             $this->load->view('web/modules/call/audiocall',$data);
        }
        else
        {
           
            $data['appoinments_details']=$this->appoinment->get_appoinment_call_details($appoinment_id);
            $data['role']='doctor';
            $this->call_details($data['appoinments_details'],'doctor','Audio');
            $this->load->view('web/modules/call/audiocall',$data);
        }
    }

    public function incomingvideocall($appoinment_id)
    {
       if($this->session->userdata('role')=='1'){
             $data['appoinments_details']=$this->appoinment->get_appoinment_call_details($appoinment_id);
             $data['role']='patient';
             $this->remove_call_details($data['appoinments_details']['id']);
             $this->call_accept($data['appoinments_details']['id']);
             $this->load->view('web/modules/call/videocall',$data);
        }
        else
        {
           
            $data['appoinments_details']=$this->appoinment->get_appoinment_call_details($appoinment_id);
            $data['role']='doctor';
            $this->remove_call_details($data['appoinments_details']['id']);
            $this->call_accept($data['appoinments_details']['id']);
            $this->load->view('web/modules/call/videocall',$data);
        }
    }

    public function incomingcall($appoinment_id)
    {
       if($this->session->userdata('role')=='1'){
             $data['appoinments_details']=$this->appoinment->get_appoinment_call_details($appoinment_id);
             $data['role']='patient';
             $this->remove_call_details($data['appoinments_details']['id']);
             $this->call_accept($data['appoinments_details']['id']);
             $this->load->view('web/modules/call/audiocall',$data);
        }
        else
        {
           
            $data['appoinments_details']=$this->appoinment->get_appoinment_call_details($appoinment_id);
            $data['role']='doctor';
            $this->remove_call_details($data['appoinments_details']['id']);
            $this->call_accept($data['appoinments_details']['id']);
            $this->load->view('web/modules/call/audiocall',$data);
        }
    }

    
  

    private function call_details($appoinments_details,$to,$call_type)
    {
        if($to=='doctor')
        {
          $call_from=$this->session->userdata('user_id');
          $call_to=$appoinments_details['appointment_to'];
        }
        if($to=='patient')
        {
          $call_from=$this->session->userdata('user_id');
          $call_to=$appoinments_details['appointment_from'];
        }

        $data['appointments_id']=$appoinments_details['id'];
        $data['call_from']=$call_from;
        $data['call_to']=$call_to;
        $data['call_type']=$call_type;
        $this->db->insert('call_details',$data);
        
    }

    private function remove_call_details($appointments_id)
    {
      $this->db->where('appointments_id',$appointments_id);
      $this->db->delete('call_details');
    }

     private function call_accept($appointments_id)
    {
      $this->db->where('id',$appointments_id);
      $this->db->update('appointments',array('call_status' =>1));
    }

    public function get_call()
    {
        $response['status']=500;
        
      $user_id=$this->session->userdata('user_id');
      $result=$this->appoinment->get_call($user_id);
      if(!empty($result))
      {
        $response['status']=200;
        $response['name']=$result['name'];
        $response['profileimage']=(!empty($result['profileimage']))?base_url().$result['profileimage']:base_url().'assets/img/user.png';
        $response['role']=($result['role']=='1')?'Dr.':'';
        $response['appointment_id']=md5($result['appointments_id']);
        $response['call_type']=$result['call_type'];
      }
      echo json_encode($response);
    }

    public function end_call()
    {
      $appointment_id=$this->input->post('appointment_id');
      $this->db->where('md5(appointments_id)',$appointment_id);
      $this->db->delete('call_details');

      echo $doctor_id=$this->db->select('appointment_to')->where('md5(id)',$appointment_id)->get('appointments')->row()->appointment_to;

    }

    public function update_expire_status()
    {
      $appointment_id=$this->input->post('appoinment_id');
      $this->db->where('id',$appointment_id)->update('appointments',array('appointment_status' =>1));
    }

    public function change_status()
    {
      $appoinments_id=$this->input->post('appoinments_id');
      $appoinments_status=$this->input->post('appoinments_status');
      $this->db->where('id',$appoinments_id)->update('appointments',array('approved' =>$appoinments_status));

      if($appoinments_status == 0){
        //Cancel the appointment 
        $this->send_appoinment_cancelmail($appoinments_id);
        //$this->send_appoinment_cancelsms($appoinments_id);

      }elseif($appoinments_status == 1){
        //accept the appointment
        $this->send_appoinment_acceptmail($appoinments_id);
        //$this->send_appoinment_acceptsms($appoinments_id);

      }
    }

    public function send_appoinment_cancelmail($appointment_id)
  {
      $inputdata=$this->book->get_appoinments_details($appointment_id);
      $this->load->library('sendemail');
      $this->sendemail->send_appoinment_cancelmail($inputdata);
  }

  public function send_appoinment_acceptmail($appointment_id)
  {
      $inputdata=$this->book->get_appoinments_details($appointment_id);
      $this->load->library('sendemail');
      $this->sendemail->send_appoinment_acceptmail($inputdata);
  }

  public function send_appoinment_cancelsms($appointment_id)
  {

    $inputdata=$this->book->get_appoinments_details($appointment_id);

    $AccountSid = settings("tiwilio_apiKey");
    $AuthToken = settings("tiwilio_apiSecret");
    $from = settings("tiwilio_from_no");
    $twilio = new Client($AccountSid, $AuthToken);

    $msg = 'Your appointment has been cancelled by the doctor '.$inputdata["doctor_name"];

    $mobileno="+".$inputdata['patient_mobile'];

            try {
                $message = $twilio->messages
                  ->create($mobileno, // to
                           ["body" => $msg, "from" => $from]
                  );
                $response = array('status' => true);
                $status=0;
            } catch (Exception $error) {
                //echo $error;
               $status=500;
            }


  }

  public function send_appoinment_acceptsms($appointment_id)
  {

    $inputdata=$this->book->get_appoinments_details($appointment_id);

    $AccountSid = settings("tiwilio_apiKey");
    $AuthToken = settings("tiwilio_apiSecret");
    $from = settings("tiwilio_from_no");
    $twilio = new Client($AccountSid, $AuthToken);

    $msg = 'Your appointment has been accepted by the doctor '.$inputdata["doctor_name"];

    $mobileno="+".$inputdata['patient_mobile'];

            try {
                $message = $twilio->messages
                  ->create($mobileno, // to
                           ["body" => $msg, "from" => $from]
                  );
                $response = array('status' => true);
                $status=0;
            } catch (Exception $error) {
                //echo $error;
               $status=500;
            }


  }

    public function get_doctor_details()
    {
       $doctor_id=$this->input->post('doctor_id');
       $appointment_id=$this->input->post('appointment_id');
       $user_detail=user_detail($doctor_id);
       $response['status']=$this->db->select('call_status,review_status')->where('md5(id)',$appointment_id)->get('appointments')->row_array();
       $response['name']=ucfirst($user_detail['first_name'].' '.$user_detail['last_name']);

       echo json_encode($response);
    }

    public function add_reviews()
  {

       $this->db->where('md5(id)',$this->input->post('appointment_id'));
       $this->db->update('appointments',array('review_status' =>1));


      $review_data['rating']=$this->input->post('rating');
      $review_data['doctor_id']=$this->input->post('doctor_id');
      $review_data['title']=$this->input->post('title');
      $review_data['review']=$this->input->post('review');
      $review_data['user_id']=$this->session->userdata('user_id');
      $review_data['created_date'] = date('Y-m-d H:i:s');
      $review_data['time_zone'] = date_default_timezone_get();

      $this->db->insert('rating_reviews',$review_data);
      echo 'success';


      
  }

  }