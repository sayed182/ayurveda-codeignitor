<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Doctor extends CI_Controller {

public function __construct() {

        parent::__construct();
        $this->data['theme']     = 'web';
        $this->data['module']    = 'home';
        $this->data['page']     = '';
        $this->data['base_url'] = base_url();
        $this->timezone = $this->session->userdata('time_zone');
        if(!empty($this->timezone)){
          date_default_timezone_set($this->timezone);
        }

        $this->data['language'] =language();
        $this->language=language(); 
        $this->data['default_language']=default_language();
        $this->data['active_language'] = active_language();
        $this->load->model('home_model','home');
        $this->load->model('profile_model','profile');
        $this->load->model('my_patients_model','my_patients');

                 
    }

    public function view($username,$city,$spec)
  {


     $this->data['page'] = 'doctor_preview';
      $this->data['doctors']=$this->home->get_doctor_details(urldecode($username));
      if(!empty($this->data['doctors']))
      {
          $doctor_details=$this->home->get_doctor_details(urldecode($username));

         if($doctor_details['price_type']=='Custom Price'){

            $user_currency=get_user_currency();
            $user_currency_code=$user_currency['user_currency_code'];
            $user_currency_rate=$user_currency['user_currency_rate'];

            $currency_option = (!empty($user_currency_code))?$user_currency_code:$doctor_details['currency_code'];
            $rate_symbol = currency_code_sign($currency_option);

                      if(!empty($this->session->userdata('user_id'))){
                        $rate=get_doccure_currency($doctor_details['amount'],$doctor_details['currency_code'],$user_currency_code);
                      }else{
                           $rate= $doctor_details['amount'];
                        }
            $this->data['amount']=$rate_symbol.''.$rate;

            }else{

              $this->data['amount']="Free";
            }
            $this->data['latitude']=$this->latitude($doctor_details['clinic_address']);
            $this->data['longitude']=$this->longitude($doctor_details['clinic_address']);
      $this->data['clinic_images']= $this->clinic_images($this->data['doctors']['userid']);
      $this->data['education'] = $this->profile->get_education_details($this->data['doctors']['userid']);
      $this->data['experience'] = $this->profile->get_experience_details($this->data['doctors']['userid']);
      $this->data['awards'] = $this->profile->get_awards_details($this->data['doctors']['userid']);
      $this->data['memberships'] = $this->profile->get_memberships_details($this->data['doctors']['userid']);
      $this->data['registrations'] = $this->profile->get_registrations_details($this->data['doctors']['userid']);
      $this->data['business_hours'] = $this->profile->get_business_hours($this->data['doctors']['userid']);
      $this->data['monday_hours'] = $this->profile->get_monday_hours($this->data['doctors']['userid']);
      $this->data['sunday_hours'] = $this->profile->get_sunday_hours($this->data['doctors']['userid']);
      $this->data['tue_hours'] = $this->profile->get_tue_hours($this->data['doctors']['userid']);
      $this->data['wed_hours'] = $this->profile->get_wed_hours($this->data['doctors']['userid']);
      $this->data['thu_hours'] = $this->profile->get_thu_hours($this->data['doctors']['userid']);
      $this->data['fri_hours'] = $this->profile->get_fri_hours($this->data['doctors']['userid']);
      $this->data['sat_hours'] = $this->profile->get_sat_hours($this->data['doctors']['userid']);
      $this->data['reviews'] = $this->home->review_list_view($this->data['doctors']['userid']);
      $this->load->vars($this->data);
      $this->load->view($this->data['theme'].'/template');
      }
      else
      {
        $this->session->set_flashdata('error_message','Oops something went wrong try valid credentials!');
         redirect(base_url().'home');
      }

  }




  public function latitude($address){

      $address = str_replace(" ", "+", $address);

      $url="https://maps.google.com/maps/api/geocode/json?key=AIzaSyA_QD2_rlwEFGhCK0oj2n6cixsvX0D3zgk&address=$address&sensor=false";

      $ch=curl_init();//initiating curl
      curl_setopt($ch,CURLOPT_URL,$url);// CALLING THE URL
      curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
      curl_setopt($ch,CURLOPT_PROXYPORT,3128);
      curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,0);
      curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,0);
      $response=curl_exec($ch);
 
      $response=json_decode($response );
      if (@$response->status == 'OK') {
              $lat=$response->results[0]->geometry->location->lat;
              $lng=$response->results[0]->geometry->location->lng;

              return $lat;
              }
 

}

public function longitude($address){

      $address = str_replace(" ", "+", $address);

      $url="https://maps.google.com/maps/api/geocode/json?key=AIzaSyA_QD2_rlwEFGhCK0oj2n6cixsvX0D3zgk&address=$address&sensor=false";

      $ch=curl_init();//initiating curl
      curl_setopt($ch,CURLOPT_URL,$url);// CALLING THE URL
      curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
      curl_setopt($ch,CURLOPT_PROXYPORT,3128);
      curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,0);
      curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,0);
      $response=curl_exec($ch);
 
      $response=json_decode($response );
      if (@$response->status == 'OK') {
              $lat=$response->results[0]->geometry->location->lat;
              $lng=$response->results[0]->geometry->location->lng;

              return $lng;
              }
 

}

public function clinic_images($id){
  
    $this->db->select('clinic_image,user_id');
    $this->db->where('user_id',$id);
    $result=$this->db->get('clinic_images')->result_array();
    return $result;

  }



}