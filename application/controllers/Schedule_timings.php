<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Schedule_timings extends CI_Controller {

  public function __construct() {

        parent::__construct();

        if($this->session->userdata('user_id') ==''){
          if($this->session->userdata('admin_id'))
            {
              redirect(base_url().'home');
            }
            else
            {
              redirect(base_url().'signin');
            }
        }

        $this->data['theme']     = 'web';
        $this->data['module']    = 'doctor';
        $this->data['page']     = '';
        $this->data['base_url'] = base_url();
        $this->timezone = $this->session->userdata('time_zone');
        if(!empty($this->timezone)){
          date_default_timezone_set($this->timezone);
        }
        
        $this->data['language'] =language();
        $this->language=language(); 
        $this->data['default_language']=default_language();
        $this->data['active_language'] = active_language();
         
    }

    public function index()
    {
        
        if($this->session->userdata('role')=='1'){
          $this->data['page'] = 'schedule_timings';
          $where = array('user_id'=>$this->session->userdata('user_id'));
          $this->data['slots']=$this->db->get_where('schedule_timings',$where)->row_array();
          $this->load->vars($this->data);
          $this->load->view($this->data['theme'].'/template');
        }
        else
        {
          redirect(base_url().'dashboard');
        }
       
    }

    function schedule_list()
    {

       $id = $this->session->userdata('user_id');
       $day_id = $this->input->post('day_id');
       $day_name = $this->input->post('day_name');
       $this->db->where('user_id',$id);
       $this->db->where('day_id',$day_id); 
       $this->db->group_by('time_start');
       $result = $this->db->get('schedule_timings')->result_array();
       $data['available_time'] = $result;
       $data['language']=$this->language;
       echo  $this->load->view('web/modules/doctor/schedule_timings_view',$data,TRUE);
     
    }

    Public function get_available_time_slots(){

  if(!empty($_POST['slot'])){

    $user_id = $this->session->userdata('user_id');

    $slot =$_POST['slot'];  
   
   $start=strtotime("00:00");
   $end=strtotime("24:00");
   $user_id = $this->session->userdata('user_id');
   if($slot >= 5){
    for ($i=$start;$i<=$end;$i = $i + $slot*60){
     $res['label']= date('g:i A',$i);
     $res['value']= date('H:i:s',$i);


     /* Disabling already added timeslots */

    $res['added'] = false;
    $res['from_time'] = 0;
    $res['to_time'] = 0;


    
     $where = array('from_time'=>$res['value'],'day_id'=>$_POST['day_id'],'user_id'=>$user_id);
     $result =$this->db->get_where('schedule_timings',$where)->row_array();


     if($result['slot'] != $slot){
   
      $wh = array('slot'=>$result['slot'],'user_id'=>$user_id);
      $this->db->delete('schedule_timings',$wh);
     }

     if($result['from_time'] == $res['value']){ // already added or not       
      $res['added'] = true;
      $res['from_time'] = $result['from_time'];
      $res['to_time'] = $result['to_time'];
     }
      

     $where = array('to_time'=>$res['value'],'day_id'=>$_POST['day_id'],'user_id'=>$user_id);
     $result =$this->db->get_where('schedule_timings',$where)->row_array();

       if($result['to_time'] == $res['value']){ // already added or not       
      $res['added'] = true;
      $res['from_time'] = $result['from_time'];
      $res['to_time'] = $result['to_time'];
      }


      if(!empty($_POST['from_time']) ){
          if($_POST['from_time'] == $res['value'] || strtotime($_POST['from_time']) > strtotime($res['value'])){
            $res['added'] = true;
            $res['from_time'] = 0;
            $res['to_time'] = 0;
          }
      }


     $datas[]=$res;
   }   
 }else{
   for ($i=$start;$i<=$end;$i = $i + 60*60){
     $res['label']= date('g:i A',$i);
     $res['value']= date('H:i:s',$i);


     /* Disabling already added timeslots */

    $res['added'] = false;
    $res['from_time'] = 0;
    $res['to_time'] = 0;
     
     $where = array('from_time'=>$res['value'],'day_id'=>$_POST['day_id'],'user_id'=>$user_id);
     $result =$this->db->get_where('schedule_timings',$where)->row_array();

     if($result['from_time'] == $res['value']){ // already added or not       
      $res['added'] = true;
      $res['from_time'] = $result['from_time'];
      $res['to_time'] = $result['to_time'];
     }
      

     $where = array('to_time'=>$res['value'],'day_id'=>$_POST['day_id'],'user_id'=>$user_id);
     $result =$this->db->get_where('schedule_timings',$where)->row_array();

       if($result['to_time'] == $res['value']){ // already added or not       
      $res['added'] = true;
      $res['from_time'] = $result['from_time'];
      $res['to_time'] = $result['to_time'];
      }

      if(!empty($_POST['from_time']) ){
          if($_POST['from_time'] == $res['value'] || strtotime($_POST['from_time']) > strtotime($res['value']) ){
            $res['added'] = true;
            $res['from_time'] = 0;
            $res['to_time'] = 0;
          }
      }

     $datas[]=$res;
   }       
 }
 echo json_encode($datas);   
}
}

public function add_schedule()
{
  $inputdata=array();
  $response=array();
  $id=$this->session->userdata('user_id');



$inputdata['slot']=$this->input->post('slot');
$inputdata['day_id']=$this->input->post('day_id');
switch ($inputdata['day_id']) {
  case '1':
    $inputdata['day_name']='Sunday';
    break;
    case '2':
    $inputdata['day_name']='Monday';
    break;
  case '3':
    $inputdata['day_name']='Tuesday';
    break;
    case '4':
    $inputdata['day_name']='Wednesday';
    break;
  case '5':
    $inputdata['day_name']='Thursday';
    break;
    case '6':
    $inputdata['day_name']='Friday';
    break;
  case '7':
    $inputdata['day_name']='Saturday';
    break;
   
  default:
    $inputdata['day_name']='';
    break;
}
$inputdata['time_start']=date('H:i:s',strtotime($this->input->post('from_time')));
$inputdata['time_end']=date('H:i:s',strtotime($this->input->post('to_time')));
$inputdata['type']=$this->input->post('type');
$inputdata['user_id'] =$id;
$inputdata['time_zone'] =$this->session->userdata('time_zone');

 
 $start=strtotime($this->input->post('from_time'));
 $end=strtotime($this->input->post('to_time'));

 if($inputdata['slot'] >= 5){
    for ($i=$start;$i<=$end;$i = $i + $inputdata['slot']*60){
     $datas[]=date('H:i:s',$i);   
   }   
 }else{
   for ($i=$start;$i<=$end;$i = $i + 60*60){
     $datas[]=date('H:i:s',$i);   
   }       
 }

for ($i=0; $i <count($datas) ; $i++) { 
  $j = $i+1;
  if(!empty($datas[$j])){
    $inputdata['from_time'] = $datas[$i];
    $inputdata['to_time'] = $datas[$j];  

    $count = $this->db->get_where('schedule_timings',array('user_id'=>$inputdata['user_id'],'from_time'=>$inputdata['from_time'],'to_time'=>$inputdata['to_time'],'day_id'=>$inputdata['day_id']))->num_rows();
    if($count == 0){
      $this->db->insert('schedule_timings',$inputdata);
    }   
    
  }
  
}
$result=($this->db->affected_rows()!= 1)?false:true;
         if($result==true)
        {
             $response['msg']=$this->language['lg_schedule_timing1']; 
             $response['status']=200;              
        }
       else
        {
            $response['msg']=$this->language['lg_schedule_timing2'];
            $response['status']=500; 
        } 
   
     echo json_encode($response);

}

public function delete_schedule_time($value='')
{
  $sts = 0;
  $id = $this->input->post('delete_value');


  $where = array('id'=>$id);
  $data = $this->db->get_where('schedule_timings',$where)->row_array();

  $this->db->where('day_id',$data['day_id']);
  $this->db->where('time_start',$data['time_start']);
  $this->db->where('user_id',$data['user_id']);  
  if($this->db->delete('schedule_timings')){
    $sts = 1;
  }
  echo $sts;
}


}

