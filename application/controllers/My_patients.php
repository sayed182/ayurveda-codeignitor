<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class My_patients extends CI_Controller {

  public function __construct() {

        parent::__construct();

        if($this->session->userdata('user_id') =='' && $this->session->userdata('admin_id')==''){
         
              redirect(base_url().'signin');
           
        }
        $this->data['module']    = 'doctor';
        $this->data['theme']     = 'web';
        $this->data['page']     = '';
        $this->data['base_url'] = base_url();

        $this->timezone = $this->session->userdata('time_zone');
        if(!empty($this->timezone)){
          date_default_timezone_set($this->timezone);
        }

        $this->data['language'] =language();
        $this->language=language(); 
        $this->data['default_language']=default_language();
        $this->data['active_language'] = active_language();
        
        $this->load->model('my_patients_model','my_patients');
         
         
    }

    public function index()
    {
        
        if($this->session->userdata('role')=='1'){
          $this->data['page'] = 'my_patients';
          $this->load->vars($this->data);
          $this->load->view($this->data['theme'].'/template');
        }
        else
        {
         redirect(base_url().'dashboard');
        }
       
    }

   public function patient_list()
  {

       $response=array();
       $result=array();
        $page=$this->input->post('page');
        $limit=8;
        $user_id=$this->session->userdata('user_id');
        $response['count'] =$this->my_patients->patient_list($page,$limit,1,$user_id);
        $patient_list = $this->my_patients->patient_list($page,$limit,2,$user_id);

        if (!empty($patient_list)) {
          foreach ($patient_list as $rows) {

            $data['id']=$rows['id'];
            $data['user_id']=$rows['user_id'];
            $data['userid']=base64_encode($rows['user_id']);
            $data['username']=$rows['username'];
            $data['profileimage']=(!empty($rows['profileimage']))?base_url().$rows['profileimage']:base_url().'assets/img/user.png';
            $data['first_name']=ucfirst($rows['first_name']);
            $data['last_name']=ucfirst($rows['last_name']);
            $data['mobileno']=$rows['mobileno'];
            $data['dob']=$rows['dob'];
            $data['age']=age_calculate($rows['dob']);
            $data['blood_group']=$rows['blood_group'];
            $data['gender']=$rows['gender'];
            $data['cityname']=$rows['cityname'];
            $data['countryname']=$rows['countryname'];
            $result[]=$data;
          }
        }
        $response['current_page_no']= $page;
        $response['total_page']= ceil($response['count']/$limit);
        $response['data']= $result;

     echo json_encode($response);

  }

  public function mypatient_preview($patient_id)
  {
      $this->data['page'] = 'mypatient_preview';
      $this->data['patient']=$this->my_patients->get_patient_details(base64_decode($patient_id));
      $this->data['patient_id']=base64_decode($patient_id);
      $this->load->vars($this->data);
      $this->load->view($this->data['theme'].'/template');
  }

  public function appoinments_list()
   {
      $list = $this->my_patients->get_appoinments_datatables();
      $data = array();
      $no = $_POST['start'];
      $a=1;
       
      foreach ($list as $appoinments) {

        

        $profile_image=(!empty($appoinments['profileimage']))?base_url().$appoinments['profileimage']:base_url().'assets/img/user.png';
        $no++;
        $row = array();
        $row[] = '<h2 class="table-avatar">
                  <a href="'.base_url().'doctor-preview/'.$appoinments['username'].'" class="avatar avatar-sm mr-2">
                    <img class="avatar-img rounded-circle" src="'.$profile_image.'" alt="User Image">
                  </a>
                  <a href="'.base_url().'doctor-preview/'.$appoinments['username'].'">'.$this->language['lg_dr'].' '.ucfirst($appoinments['first_name'].' '.$appoinments['last_name']).' <span>'.ucfirst($appoinments['specialization']).'</span></a>
                </h2>
                  ';
       
         $from_date_time = '';
        if(!empty($appoinments['time_zone'])){
          $from_timezone=$appoinments['time_zone'];
          $to_timezone = date_default_timezone_get();
          $from_date_time = $appoinments['from_date_time'];
          $from_date_time = converToTz($from_date_time,$to_timezone,$from_timezone);
          $row[] = date('d M Y',strtotime($from_date_time)).' <span class="d-block text-info">'.date('h:i A',strtotime($from_date_time)).'</span>';
        }else{
          $row[]='-';
       } 
        $row[] = date('d M Y',strtotime($appoinments['created_date']));
        $row[] = ucfirst($appoinments['type']);
               
        $data[] = $row;
      }



      $output = array(
              "draw" => $_POST['draw'],
              "recordsTotal" => $this->my_patients->appoinments_count_all(),
              "recordsFiltered" => $this->my_patients->appoinments_count_filtered(),
              "data" => $data,
          );
      //output to json format
      echo json_encode($output);
  }

  public function add_prescription($patient_id)
  {
      $this->data['page'] = 'add_prescription';
      $this->data['patient_id']=base64_decode($patient_id);
      $this->load->vars($this->data);
      $this->load->view($this->data['theme'].'/template');
  }

   public function edit_prescription()
  {
     $prescription_id=$this->uri->segment(2);
     $patient_id=$this->uri->segment(3);
      $this->data['page'] = 'edit_prescription';
      $this->data['patient_id']=base64_decode($patient_id);
      $this->data['prescription']=$this->my_patients->get_prescription(base64_decode($prescription_id));
      $this->load->vars($this->data);
      $this->load->view($this->data['theme'].'/template');
  }

  public function print_prescription()
  {
     $prescription_id=$this->uri->segment(2);
     $data['prescription']=$this->my_patients->get_prescription_details(base64_decode($prescription_id));
     $this->load->view('web/modules/print/print_prescription',$data);
  }

  

   public function save_prescription()
  {
          
            $data = array(
            'doctor_id' => $this->session->userdata('user_id'),
            'patient_id' => $this->input->post('patient_id'),
            'signature_id' => $this->input->post('signature_id'),
            'created_at' => date('Y-m-d H:i:s')
          );
    $this->db->insert('prescription', $data);
    $prescription_id = $this->db->insert_id();         
    
    $drug_name=$this->input->post('drug_name');
    $qty=$this->input->post('qty');
    $type=$this->input->post('type');
    $days=$this->input->post('days');
    $time=$this->input->post('time');

    for ($i=0; $i <count($drug_name) ; $i++) {      
      $time = '';
      $j = $i+1;
      if(!empty($_POST['time'.$j])){
        $time = implode(',',$_POST['time'.$j]);
      }

      $datas = array(
        'prescription_id' => $prescription_id,
        'drug_name' => $drug_name[$i],
        'qty' => $qty[$i],
        'type'=>$type[$i],
        'days'=>$days[$i],
        'time'=>$time,
        'created_at'  => date('Y-m-d H:i:s')
      );
      $this->db->insert('prescription_item_details', $datas);  
    }

    $result= ($this->db->affected_rows()!= 1)? false:true;
    if($result==true)
      {   
          
           $response['msg']=$this->language['lg_prescription_ad1']; 
           $response['status']=200; 
           $response['patient_id']=base64_encode($this->input->post('patient_id')); 
             
      }
     else
      {
          $response['msg']=$this->language['lg_prescription_ad'];
          $response['status']=500; 
      } 



            echo json_encode($response);

      }


   public function update_prescription()
  {
          
    $prescription_id = $this->input->post('prescription_id'); 

    $data = array(
            'signature_id' => $this->input->post('signature_id'),
            );
     $this->db->where('id',$prescription_id);
     $this->db->update('prescription', $data);  

    $where = array('prescription_id' => $prescription_id);
    $this->db->delete('prescription_item_details',$where);      
    
    $drug_name=$this->input->post('drug_name');
    $qty=$this->input->post('qty');
    $type=$this->input->post('type');
    $days=$this->input->post('days');
    $time=$this->input->post('time');

    for ($i=0; $i <count($drug_name) ; $i++) {      
      $time = '';
      $j = $i+1;
      if(!empty($_POST['time'.$j])){
        $time = implode(',',$_POST['time'.$j]);
      }

      $datas = array(
        'prescription_id' => $prescription_id,
        'drug_name' => $drug_name[$i],
        'qty' => $qty[$i],
        'type'=>$type[$i],
        'days'=>$days[$i],
        'time'=>$time,
        'created_at'  => date('Y-m-d H:i:s')
      );
      $this->db->insert('prescription_item_details', $datas);  
    }

    $result= ($this->db->affected_rows()!= 1)? false:true;
    if($result==true)
      {   
          
           $response['msg']=$this->language['lg_prescription_up1'];  
           $response['status']=200; 
           $response['patient_id']=base64_encode($this->input->post('patient_id')); 
             
      }
     else
      {
          $response['msg']=$this->language['lg_prescription_up'];
          $response['status']=500; 
      } 



            echo json_encode($response);

  }

  Public function insert_signature(){

    $img = $_POST['image'];
    $img = str_replace('data:image/png;base64,', '', $img);
    $img = str_replace(' ', '+', $img);
    $data = base64_decode($img);
    $file = './uploads/signature-image/' . uniqid() . '.png';
    $success = file_put_contents($file, $data);
    $image=str_replace('./','',$file);
    $check=$this->get_single_signs();
    if($check==0){
      $data=array('img'=>$image,'rowno'=>$_POST['rowno']);
      $this->db->insert('signature', $data);
      $id= $this->db->insert_id();
    }
    else
    {
      $data=array('img'=>$image);
      $this->db->where('rowno',$_POST['rowno'])->update('signature', $data);
      $id = $this->db->where('rowno',$_POST['rowno'])->get('signature')->row()->id;
    }
    echo '<div id="edit">
          <img src="'.base_url().$image.'" style="width:200px;height:auto">
        <div class="edit" align="center">
          <i class="fa fa-edit" onclick="show_modal()" title="Click here to edit the signature"></i>
        </div>
        <input type="hidden" name="signature_id" value="'.$id.'" id="signature_id">
      </div>';
  }


  public function get_single_signs()
  {
    $datas=array('rowno'=>$_POST['rowno']);
    return $this->db->get_where('signature',$datas)->num_rows();
  }


   public function prescriptions_list()
   {
      $list = $this->my_patients->get_prescription_datatables();
      $data = array();
      $no = $_POST['start'];
      $a=1;
      $b=1; 
      foreach ($list as $prescriptions) {

        $profile_image=(!empty($prescriptions['profileimage']))?base_url().$prescriptions['profileimage']:base_url().'assets/img/user.png';
        $no++;
        $row = array();
        $row[] =$a++;
        $row[] = date('d M Y',strtotime($prescriptions['created_at']));
        $row[] ='Prescription '.$b++;
        $row[] = '<h2 class="table-avatar">
                  <a href="'.base_url().'doctor-preview/'.$prescriptions['username'].'" class="avatar avatar-sm mr-2">
                    <img class="avatar-img rounded-circle" src="'.$profile_image.'" alt="User Image">
                  </a>
                  <a href="'.base_url().'doctor-preview/'.$prescriptions['username'].'">Dr. '.ucfirst($prescriptions['first_name'].' '.$prescriptions['last_name']).' <span>'.ucfirst($prescriptions['specialization']).'</span></a>
                </h2>
                  ';
       
                $html='<div class="table-action">
                  <a target="_blank" href="'.base_url().'print-prescription/'.base64_encode($prescriptions['id']).'" class="btn btn-sm bg-primary-light">
                    <i class="fas fa-print"></i> '.$this->language['lg_print'].'
                  </a>
                  <a href="javascript:void(0);" onclick="view_prescription('.$prescriptions['id'].')" class="btn btn-sm bg-info-light">
                    <i class="far fa-eye"></i> '.$this->language['lg_view1'].'
                  </a>';
                  if(is_doctor()){
                  $html .='<a href="'.base_url().'edit-prescription/'.base64_encode($prescriptions['id']).'/'.base64_encode($prescriptions['patient_id']).'" class="btn btn-sm bg-success-light">
                    <i class="fas fa-edit"></i> '.$this->language['lg_edit2'].'
                  </a>
                  <a href="javascript:void(0);" onclick="delete_prescription('.$prescriptions['id'].')" class="btn btn-sm bg-danger-light">
                    <i class="far fa-trash-alt"></i> '.$this->language['lg_delete'].'
                  </a>';
                }
                $html .='</div>';

        $row[] = $html;
        
        $data[] = $row;
      }



      $output = array(
              "draw" => $_POST['draw'],
              "recordsTotal" => $this->my_patients->prescription_count_all(),
              "recordsFiltered" => $this->my_patients->prescription_count_filtered(),
              "data" => $data,
          );
      //output to json format
      echo json_encode($output);
  }

  public function get_prescription_details()
  {
      
            $prescription_id=$this->input->post('pre_id');
            $result=$this->my_patients->get_prescription_details($prescription_id);
          echo json_encode($result);
  }


  Public function upload_medical_records(){
    $data = array();
    //ob_flush();
    $doctor_id = $this->session->userdata('user_id');
    $patient_id=$this->input->post('patient_id');
    $description=$this->input->post('description');
       
    if(!empty($_FILES['user_file']['name'])){

      $path = "uploads/medical_records/".$patient_id;
      $upload_path = $path.'/'.date('d-m-Y');
      if(!is_dir($path)){
        mkdir($path);         
      }
      if(!is_dir($path.'/'.date('d-m-Y'))){
        mkdir($path.'/'.date('d-m-Y')); 
      }
      $path = $path.'/'.date('d-m-Y');
      
      $target_file =$path . basename($_FILES["user_file"]["name"]);
      $file_type = pathinfo($target_file,PATHINFO_EXTENSION);

      if($file_type != "jpg" && $file_type != "png" && $file_type != "jpeg" && $file_type != "gif" ){
        $type = 'others';
      }else{
        $type = 'image';
      }
      $config['upload_path']   = './'.$path;
      $config['allowed_types'] = '*';   
      $config['file_name'] = date('d-m-Y_h_i_A').'_prescription';   
      $this->load->library('upload',$config);
      if($this->upload->do_upload('user_file')){  
        $file_name=$this->upload->data('file_name');
        $data +=array('file_name' => $upload_path.'/'.$file_name);
      }else{
        $response['status']=500;
        $response['msg'] = $this->upload->display_errors();
        echo json_encode($result);
        exit;
      }
    }

    $data +=array(
      'date' =>  date('Y-m-d H:i:s'),
      'description' => $description,     
      'doctor_id' => $doctor_id,
      'patient_id' => $patient_id
    );

    
       $this->db->insert('medical_records',$data);
    
     $result= ($this->db->affected_rows()!= 1)? false:true;
     if($result==true)
      {   
          
           $response['msg']=$this->language['lg_medical_records2']; 
           $response['status']=200; 
          
      }
     else
      {
          $response['msg']=$this->language['lg_medical_records1'];
          $response['status']=500; 
      } 
    
     echo json_encode($response);
  }


     public function medical_records_list()
   {
      $list = $this->my_patients->get_medical_record_datatables();
      $data = array();
      $no = $_POST['start'];
      $a=1;
      $b=1; 
      foreach ($list as $medical_records) {

        $profile_image=(!empty($medical_records['profileimage']))?base_url().$medical_records['profileimage']:base_url().'assets/img/user.png';
        $no++;
        $row = array();
        $row[] =$a++;
        $row[] = date('d M Y',strtotime($medical_records['date']));
        $row[] =$medical_records['description'];
        $row[]='<a href="'.base_url().$medical_records['file_name'].'" target="_blank" title="Download attachment" class="btn btn-primary btn-sm">Download <i class="fa fa-download"></i></a>';
        $row[] = '<h2 class="table-avatar">
                  <a href="'.base_url().'doctor-preview/'.$medical_records['username'].'" class="avatar avatar-sm mr-2">
                    <img class="avatar-img rounded-circle" src="'.$profile_image.'" alt="User Image">
                  </a>
                  <a href="'.base_url().'doctor-preview/'.$medical_records['username'].'">'.$this->language['lg_dr'].' '.ucfirst($medical_records['first_name'].' '.$medical_records['last_name']).' <span>'.ucfirst($medical_records['specialization']).'</span></a>
                </h2>
                  ';
       if(is_doctor()){
        $row[] ='<div class="table-action">
                  <a href="javascript:void(0);" onclick="delete_medical_records('.$medical_records['id'].')" class="btn btn-sm bg-danger-light">
                    <i class="far fa-trash-alt"></i> '.$this->language['lg_delete'].'
                  </a>
                </div>';
       }
        
        
        $data[] = $row;
      }



      $output = array(
              "draw" => $_POST['draw'],
              "recordsTotal" => $this->my_patients->medical_record_count_all(),
              "recordsFiltered" => $this->my_patients->medical_record_count_filtered(),
              "data" => $data,
          );
      //output to json format
      echo json_encode($output);
  }

  
   public function add_billing($patient_id)
  {
      $this->data['page'] = 'add_billing';
      $this->data['patient_id']=base64_decode($patient_id);
      $this->load->vars($this->data);
      $this->load->view($this->data['theme'].'/template');
  }

  public function edit_billing()
  {
     $billing_id=$this->uri->segment(2);
     $patient_id=$this->uri->segment(3);
      $this->data['page'] = 'edit_billing';
      $this->data['patient_id']=base64_decode($patient_id);
      $this->data['billing']=$this->my_patients->get_billing(base64_decode($billing_id));
      $this->load->vars($this->data);
      $this->load->view($this->data['theme'].'/template');
  }

  public function print_billing()
  {
     $billing_id=$this->uri->segment(2);
     $data['billing']=$this->my_patients->get_billing_details(base64_decode($billing_id));
     $this->load->view('web/modules/print/print_billing',$data);
  }

   public function save_billing()
  {
          
            $data = array(
            'doctor_id' => $this->session->userdata('user_id'),
            'patient_id' => $this->input->post('patient_id'),
            'signature_id' => $this->input->post('signature_id'),
            'created_at' => date('Y-m-d H:i:s')
          );
    $this->db->insert('billing', $data);
    $billing_id = $this->db->insert_id();         
    
    $name=$this->input->post('name');
    $amount=$this->input->post('amount');
    
    for ($i=0; $i <count($name) ; $i++) {      
     
      $datas = array(
        'billing_id' => $billing_id,
        'name' => $name[$i],
        'amount' => $amount[$i],
        'created_at'  => date('Y-m-d H:i:s')
      );
      $this->db->insert('billing_item_details', $datas);  
    }

    $result= ($this->db->affected_rows()!= 1)? false:true;
    if($result==true)
      {   
          
            $response['msg']=$this->language['lg_billing_added_s']; 
           $response['status']=200; 
           $response['patient_id']=base64_encode($this->input->post('patient_id')); 
             
      }
     else
      {
          $response['msg']=$this->language['lg_billing_added_f'];
          $response['status']=500; 
      } 



            echo json_encode($response);

      }


      public function update_billing()
  {
          
    $billing_id = $this->input->post('billing_id');
    $data = array(
            'signature_id' => $this->input->post('signature_id'),
            );
     $this->db->where('id',$billing_id);
     $this->db->update('billing', $data);
    $where = array('billing_id' => $billing_id);
    $this->db->delete('billing_item_details',$where);      
    
   $name=$this->input->post('name');
    $amount=$this->input->post('amount');
    
    for ($i=0; $i <count($name) ; $i++) {      
     
      $datas = array(
        'billing_id' => $billing_id,
        'name' => $name[$i],
        'amount' => $amount[$i],
        'created_at'  => date('Y-m-d H:i:s')
      );
      $this->db->insert('billing_item_details', $datas);  
    }

    $result= ($this->db->affected_rows()!= 1)? false:true;
    if($result==true)
      {   
          
           $response['msg']=$this->language['lg_billing_update_']; 
           $response['status']=200; 
           $response['patient_id']=base64_encode($this->input->post('patient_id')); 
             
      }
     else
      {
           $response['msg']=$this->language['lg_billing_added_f'];
          $response['status']=500; 
      } 



            echo json_encode($response);

  }

     public function billing_list()
   {
      $list = $this->my_patients->get_billing_datatables();
      $data = array();
      $no = $_POST['start'];
      $a=1;
      $b=1; 
      foreach ($list as $billings) {

        $profile_image=(!empty($billings['profileimage']))?base_url().$billings['profileimage']:base_url().'assets/img/user.png';
        $no++;
        $row = array();
        $row[] =$a++;
        $row[] = date('d M Y',strtotime($billings['created_at']));
        $row[] ='Billno '.$b++;
        $row[] = '<h2 class="table-avatar">
                  <a href="'.base_url().'doctor-preview/'.$billings['username'].'" class="avatar avatar-sm mr-2">
                    <img class="avatar-img rounded-circle" src="'.$profile_image.'" alt="User Image">
                  </a>
                  <a href="'.base_url().'doctor-preview/'.$billings['username'].'">'.$this->language['lg_dr'].' '.ucfirst($billings['first_name'].' '.$billings['last_name']).' <span>'.ucfirst($billings['specialization']).'</span></a>
                </h2>';
      
        $html='<div class="table-action">
                  <a target="_blank" href="'.base_url().'print-billing/'.base64_encode($billings['id']).'" class="btn btn-sm bg-primary-light">
                    <i class="fas fa-print"></i> '.$this->language['lg_print'].'
                  </a>
                  <a href="javascript:void(0);" onclick="view_billing('.$billings['id'].')" class="btn btn-sm bg-info-light">
                    <i class="far fa-eye"></i> '.$this->language['lg_view1'].'
                  </a>';
                  if(is_doctor()){
                  $html .='<a href="'.base_url().'edit-billing/'.base64_encode($billings['id']).'/'.base64_encode($billings['patient_id']).'" class="btn btn-sm bg-success-light">
                    <i class="fas fa-edit"></i> '.$this->language['lg_edit2'].'
                  </a>
                  <a href="javascript:void(0);" onclick="delete_billing('.$billings['id'].')" class="btn btn-sm bg-danger-light">
                    <i class="far fa-trash-alt"></i> '.$this->language['lg_delete'].'
                  </a>';
                }
          $html .='</div>';

        $row[] =$html;
        
        
        $data[] = $row;
      }



      $output = array(
              "draw" => $_POST['draw'],
              "recordsTotal" => $this->my_patients->billing_count_all(),
              "recordsFiltered" => $this->my_patients->billing_count_filtered(),
              "data" => $data,
          );
      //output to json format
      echo json_encode($output);
  }

  

  public function get_billing_details()
  {
    $billing_id=$this->input->post('id');
    $result=$this->my_patients->get_billing_details($billing_id);
    echo json_encode($result);
  }

  public function delete_details()
  {
    $id=$this->input->post('id');
    $table_name=$this->input->post('delete_table');

    if($table_name=='prescription')
    {
      $this->db->delete('prescription',array('id'=>$id));
      $this->db->delete('prescription_item_details',array('prescription_id'=>$id));
      echo 1;
    }

    if($table_name=='billing')
    {
      $this->db->delete('billing',array('id'=>$id));
      $this->db->delete('billing_item_details',array('billing_id'=>$id));
      echo 1;
    }

    if($table_name=='medical_records')
    {
      $this->db->delete('medical_records',array('id'=>$id));
      echo 1;
    }

  }




}