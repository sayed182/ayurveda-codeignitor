<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

  public function __construct() {

        parent::__construct();

        if($this->session->userdata('user_id') ==''){
          if($this->session->userdata('admin_id'))
            {
              redirect(base_url().'home');
            }
            else
            {
              redirect(base_url().'signin');
            }
        }

        $this->data['theme']     = 'web';
        $this->data['page']     = '';
        $this->data['base_url'] = base_url();

        $this->timezone = $this->session->userdata('time_zone');
        if(!empty($this->timezone)){
          date_default_timezone_set($this->timezone);
        }

        $this->data['language'] =language();
        $this->language=language(); 
        $this->data['default_language']=default_language();
        $this->data['active_language'] = active_language();

        $this->load->model('appoinments_model','appoinment');
        $this->load->model('home_model','home');
        $this->load->model('users_model','user');
         
    }

    public function index()
    {

        $user_id=$this->session->userdata('user_id');
        if($this->session->userdata('role')=='1'){
          $this->data['module']    = 'doctor';
          $this->data['page'] = 'doctor_dashboard';
          $this->data['total_patient'] =  $this->appoinment->get_total_patient($user_id);  
          $this->data['today_patient'] =  $this->appoinment->get_today_patient($user_id);  
          $this->data['recent'] =  $this->appoinment->get_recent_booking($user_id);
          $this->load->vars($this->data);
          $this->load->view($this->data['theme'].'/template');
        }
        else
        {
          $this->data['module']    = 'patient';
          $this->data['page'] = 'patient_dashboard';
          $this->data['patient_id']=$user_id;
          $this->load->vars($this->data);
          $this->load->view($this->data['theme'].'/template');
        }
       
    }

    public function appoinments_list()
   {
    $user_id=$this->session->userdata('user_id');
    $list = $this->appoinment->get_datatables($user_id);
    $data = array();
    $no = $_POST['start'];
    $a=1;
     
    foreach ($list as $appoinments) {

      if($appoinments['payment_method']=='Pay on Arrive') {
                $hourly_rate='Pay on Arrive';
              }
              else {
                $hourly_rate=!empty($appoinments['per_hour_charge'])?$appoinments['per_hour_charge']:'Free';
              }

              $current_timezone = $appoinments['time_zone'];               
              $old_timezone = $this->session->userdata('time_zone');    

            $appointment_date=date('d M Y',strtotime(converToTz($appoinments['appointment_date'],$old_timezone,$current_timezone)));
            $appointment_time=date('h:i A',strtotime(converToTz($appoinments['from_date_time'],$old_timezone,$current_timezone)));
            $appointment_end_time=date('h:i A',strtotime(converToTz($appoinments['to_date_time'],$old_timezone,$current_timezone)));
            $created_date=date('d M Y',strtotime(converToTz($appoinments['created_date'],$old_timezone,$current_timezone)));
            $hourly_rate=$hourly_rate;
            $type=$appoinments['type'];

      $profile_image=(!empty($appoinments['profileimage']))?base_url().$appoinments['profileimage']:base_url().'assets/img/user.png';
      $no++;
      $row = array();
      $row[] = '<h2 class="table-avatar">
                  <a href="'.base_url().'mypatient-preview/'.base64_encode($appoinments['appointment_from']).'" class="avatar avatar-sm mr-2"><img class="avatar-img rounded-circle" src="'.$profile_image.'" alt="User Image"></a>
                  <a href="'.base_url().'mypatient-preview/'.base64_encode($appoinments['appointment_from']).'">'.ucfirst($appoinments['first_name'].' '.$appoinments['last_name']).' <span>#PT00'.$appoinments['appointment_from'].'</span></a>
                </h2>';
     
       $from_date_time = '';
      if(!empty($appoinments['time_zone'])){
        $from_timezone=$appoinments['time_zone'];
        $to_timezone = date_default_timezone_get();
        $from_date_time = $appoinments['from_date_time'];
        $from_date_time = converToTz($from_date_time,$to_timezone,$from_timezone);
        $row[] = date('d M Y',strtotime($from_date_time)).' <span class="d-block text-info">'.date('h:i A',strtotime($from_date_time)).'</span>';
      }else{
        $row[]='-';
     } 

      $row[] = ucfirst($appoinments['type']);
      $row[] = '<div class="table-action">
                  <a href="javascript:void(0);" onclick="show_appoinments_modal(\''.$appointment_date.'\',\''.$appointment_time.' - '.$appointment_end_time.'\',\''.$created_date.'\',\''.$hourly_rate.'\',\''.$type.'\')" class="btn btn-sm bg-info-light">
                    <i class="far fa-eye"></i> View
                  </a>
                </div>';
      
      $data[] = $row;
    }



    $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->appoinment->count_all($user_id),
            "recordsFiltered" => $this->appoinment->count_filtered($user_id),
            "data" => $data,
        );
    //output to json format
    echo json_encode($output);
  }

  public function favourites()
  {
     $user_id=$this->session->userdata('user_id');
        
        if($this->session->userdata('role')=='1'){
           redirect(base_url().'dashboard');
        }
        else
        {
          $this->data['module']    = 'patient';
          $this->data['page'] = 'favourites';
          $this->data['favourites'] = $this->appoinment->get_favourites($user_id);
          $this->load->vars($this->data);
          $this->load->view($this->data['theme'].'/template');
        }
  }


  public function reviews()
  {
     $user_id=$this->session->userdata('user_id');
        
        if($this->session->userdata('role')=='1'){
           
            $this->data['module']    = 'doctor';
            $this->data['page'] = 'reviews';
            $this->data['reviews'] = $this->home->review_list_view($user_id);
            $this->load->vars($this->data);
            $this->load->view($this->data['theme'].'/template');
        }
        else
        {
          redirect(base_url().'dashboard');
        }
  }

  public function send_verification_email()
  {
         $user_detail=user_detail($this->session->userdata('user_id'));
         $this->load->library('sendemail');
         $this->sendemail->send_email_verification($user_detail);

  }

  

 public function maps() {
  $user_id = $this->session->userdata('user_id');

  
      $this->data['module'] = 'patient';
      $this->data['page'] = 'maps';
      $this->load->vars($this->data);
      $this->load->view($this->data['theme'] . '/template');
  
    }

    public function get_direction($id = NULL) {
  $id = isset($id) ? base64_decode($id) : '';
  $user_id = $this->session->userdata('user_id');

  
      if ($id > 0) {
    $doctor_details =user_detail($id);
    $this->data['to_address'] = $doctor_details['clinic_address'];
    
      }
      $patient_details = user_detail($this->session->userdata('user_id'));
      //print_r($patient_details);
      //exit;

      $this->data['from_address'] = $patient_details['address1'] . "," . $patient_details['cityname'] . ', ' . $patient_details['statename'] . ', ' . $patient_details['postal_code'] . ' ' . $patient_details['countryname'];
      $this->data['module'] = 'patient';
      $this->data['page'] = 'maps';
      $this->load->vars($this->data);
      $this->load->view($this->data['theme'] . '/template');
  
    }

    public function generate_otp_for_dashboard_modal()
    {
        $user_detail=user_detail($this->session->userdata('user_id'));
        $this->load->vars(['user_detail' => $user_detail]);
        $view = $this->load->view('web/includes/components/confirm-otp-modal');
        return $view;
    }

}
