<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . '../vendor/autoload.php');
use Twilio\Rest\Client;


class Signin extends CI_Controller {


  public function __construct() {

    parent::__construct();
    


    $this->data['theme']     = 'web';
    $this->data['module']    = 'signin';
    $this->data['page']     = '';
    $this->data['base_url'] = base_url();
    $this->timezone = $this->session->userdata('time_zone');
    if(!empty($this->timezone)){
      date_default_timezone_set($this->timezone);
    }

    $this->data['language'] =language();
    $this->language=language(); 
    $this->data['default_language']=default_language();
    $this->data['active_language'] = active_language();
    $this->load->model('signin_model','signin');
    $this->load->helper('cookie');


    
    
  }

  public function index()
  {


   if($this->session->userdata('user_id') || $this->session->userdata('admin_id')){
    
    redirect(base_url().'home');
    
  }

  $cookie_user=$this->input->cookie('user_id',TRUE);

  if($cookie_user){

     $result = $this->signin->cookie_login($cookie_user);
  if(!empty($result['status']==1))
  {
    $session_data=array('user_id' =>$result['id'],'role'=>$result['role']);
    $this->session->set_userdata($session_data);
    $this->session->unset_userdata('admin_id');

    redirect('dashboard');

  
  }else{

    $this->data['page'] = 'index';
    $this->load->vars($this->data);
    $this->load->view($this->data['theme'].'/template');
  }



  }else{

  $this->data['page'] = 'index';
  $this->load->vars($this->data);
  $this->load->view($this->data['theme'].'/template');
 }
  
}





public function register()
{

  if($this->session->userdata('user_id') || $this->session->userdata('admin_id')){
    
    redirect(base_url().'home');
    
  }
  
  $this->data['page'] = 'register';
  $this->load->vars($this->data);
  $this->load->view($this->data['theme'].'/template');
  
}



public function check_email()
{
  $email = $this->input->post('email');     
  $result = $this->signin->check_email($email);
  
  if ($result > 0) {
   echo 'false';
 } else {
   echo 'true';
 }
 
}

public function check_resetemail()
{
  $email = $this->input->post('resetemail');     
  $result = $this->signin->check_email($email);
  if ($result > 0) {
   echo 'true';
 } else {
   echo 'false';
 }
 
}

public function check_mobileno()
{
  $mobileno = $this->input->post('mobileno');     
  $result = $this->signin->check_mobileno($mobileno);
  if ($result > 0) {
   echo 'false';
 } else {
   echo 'true';
 }
 
}

public function sendotp()
{
 
  $mobile_number ='';
  $mobile_number = $this->input->post('mobileno');
  $country_code= $this->input->post('country_code');
  $otpcount=$this->input->post('otpcount');
  $inputdata=array();
  $inputdata['mobileno']=$mobile_number;
  $already_exits_mobile_no=$this->db->where('mobileno',$inputdata['mobileno'])->get('users')->num_rows();
  if($already_exits_mobile_no >=1)
  {
    $response['msg']='Your mobileno already exits';
    $response['status']=500;
    echo json_encode($response);
  }
  else
  {
    $otp_checking=$this->db->where('mobileno ',$inputdata['mobileno'])->get('otp_history')->num_rows();
    $AccountSid = settings("tiwilio_apiKey");
    $AuthToken = settings("tiwilio_apiSecret");
    $from = settings("tiwilio_from_no");
    $twilio = new Client($AccountSid, $AuthToken);
    if($otp_checking >=1 && $otpcount==2)
    {
      $this->db->select('otpno,mobileno');
      $this->db->from('otp_history');
      $this->db->where('mobileno ',$inputdata['mobileno']);
      $otpdata=$this->db->get()->row_array();
      
      
      $otp=$otpdata['otpno'];
      $msg = 'Hello, Welcome to '.settings("website_name").'. Your one time password (OTP):'.$otp;

      $mobileno="+".$country_code.$mobile_number;

      try {
        $message = $twilio->messages
                ->create($mobileno, // to
                 ["body" => $msg, "from" => $from]
               );
                $response = array('status' => true);
                $status=0;
              } catch (Exception $error) {
              //echo $error;
               $status=500;
             }

             if($status==0)
             {
              $response['msg']='OTP send successfully'; 
              $response['status']=200;
            }
            else
            {
             $response['msg']='OTP send failed'; 
             $response['status']=500; 
           }
           
           echo json_encode($response);
         }
         else{
          // Generate random verification code
          $otp = rand(10000, 99999);
          
          $inputdata['otpno']=$otp;
          $inputdata['status']=0;
          $inputdata['created_date']=date('Y-m-d H:i:s');
          $this->signin->saveotp($inputdata);
          
          $mobileno="+".$country_code.$mobile_number;
          /*Mobile number validation otp send starts*/
          
          $msg = 'Hello Welcome to '.settings("website_name").'.  Your one time password (OTP):'.$otp;
          
          
          try {
            $message = $twilio->messages
          ->create($mobileno, // to
           ["body" => $msg, "from" => $from]
         );
          $response = array('status' => true);
          $status=0;
        } catch (Exception $error) {
        //echo $error;
          $status=500;
        }
        
              //print($message->sid);exit;
        if($status==0)
        {
          $response['msg']='OTP send successfully'; 
          $response['status']=200;
        }
        else
        {
         $response['msg']='OTP send failed'; 
         $response['status']=500; 
       }
       
       echo json_encode($response);
       
     }
   }
 }

 public function sendotp_login()
{
 
  $mobile_number ='';
  $mobile_number = $this->input->post('mobileno');
  $country_code= $this->input->post('country_code');
  $otpcount=$this->input->post('otpcount');
  $inputdata=array();
  $inputdata['mobileno']=$mobile_number;
  $already_exits_mobile_no=$this->db->where('mobileno',$inputdata['mobileno'])->get('users')->num_rows();
  if($already_exits_mobile_no ==1)
  { 
    $otp_checking=$this->db->where('mobileno ',$inputdata['mobileno'])->get('otp_history')->num_rows();
    $AccountSid = settings("tiwilio_apiKey");
    $AuthToken = settings("tiwilio_apiSecret");
    $from = settings("tiwilio_from_no");
    $twilio = new Client($AccountSid, $AuthToken);
  
          // Generate random verification code
          $otp = rand(10000, 99999);
          
        

          $user=$this->db->where('mobileno',$inputdata['mobileno'])->get('users')->row_array();

          $this->db->where('id',$user['id']);
          $this->db->update('users',array('login_otp'=>$otp));
          
          $mobileno="+".$country_code.$mobile_number;
          /*Mobile number validation otp send starts*/
          
          $msg = 'Hello Welcome to '.settings("website_name").'.  Your one time password (OTP):'.$otp;
          
          
          try {
            $message = $twilio->messages
          ->create($mobileno, // to
           ["body" => $msg, "from" => $from]
         );
          $response = array('status' => true);
          $status=0;
        } catch (Exception $error) {
        //echo $error;
          $status=500;
        }
        
              //print($message->sid);exit;
        if($status==0)
        {
          $response['msg']='OTP send successfully'; 
          $response['status']=200;
        }
        else
        {
         $response['msg']='OTP send failed'; 
         $response['status']=500; 
       }
       
       echo json_encode($response);
       
     
   
  }
  else
  {

     $response['msg']='Your mobileno not registered';
     $response['status']=500;
     echo json_encode($response);
   
   }
 }

 public function signup()
 {

  $inputdata=array();
  $response=array();
  
  $otpno=$this->input->post('otpno');
  $inputdata['first_name']=$this->input->post('first_name');
  $inputdata['last_name']=$this->input->post('last_name');
  $inputdata['email']=$this->input->post('email');
  $inputdata['mobileno']=$this->input->post('mobileno');
  $inputdata['country_code']=$this->input->post('country_code');
  $inputdata['username'] = generate_username($inputdata['first_name'].' '.$inputdata['last_name'].' '.$inputdata['mobileno']);
  $inputdata['role']=$this->input->post('role');
  $inputdata['password']=md5($this->input->post('password'));
  $inputdata['confirm_password']=md5($this->input->post('confirm_password'));
  $inputdata['created_date']=date('Y-m-d H:i:s');
  $already_exits=$this->db->where('email',$inputdata['email'])->get('users')->num_rows();
  $already_exits_mobile_no=$this->db->where('mobileno',$inputdata['mobileno'])->get('users')->num_rows();
  if(settings('tiwilio_option')=='1') {
   $otp_checking =$this->db->select('otpno,mobileno')->from('otp_history')->where('otpno', $otpno)->where('mobileno',$this->input->post('mobileno'))->get()->num_rows();
   if($otp_checking ==0)
   {
    $response['msg']='Your OTP is invalid or please enter OTP';
    $response['status']=500;
    echo json_encode($response);
    return false;
  }
}
if($already_exits >=1)
{
  $response['msg']=$this->language['lg_your_email_addr1'];
  $response['status']=500;
}
else if($already_exits_mobile_no >=1)
{
  $response['msg']=$this->language['lg_your_mobileno_a'];
  $response['status']=500;
}
else
{
  $result=$this->signin->signup($inputdata);
  if($result==true)
  {
    $user_id = $this->db->insert_id();
    $total = count($_FILES['doc_files']['name']);
    $doc_file = array();

    if(!empty($_FILES['doc_files']['name']))
    {     
      $total = count($_FILES['doc_files']['name']);

      if (!is_dir('uploads/documents')) {
        mkdir('./uploads/documents', 0777, TRUE);
      }

      for( $i=0 ; $i < $total ; $i++ ) {

        if($_FILES["doc_files"]["name"][$i] != '')
        {
            $config["upload_path"] = './uploads/documents/';
            $config["allowed_types"] = '*';
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            $_FILES["files"]["name"] = $_FILES["doc_files"]["name"][$i];
            $_FILES["files"]["type"] = $_FILES["doc_files"]["type"][$i];
            $_FILES["files"]["tmp_name"] = $_FILES["doc_files"]["tmp_name"][$i];
            $_FILES["files"]["error"] = $_FILES["doc_files"]["error"][$i];
            $_FILES["files"]["size"] = $_FILES["doc_files"]["size"][$i];
            if($this->upload->do_upload('files'))
            {
              $upload_data = $this->upload->data();
              $doc_files='uploads/documents/'.$upload_data["file_name"];
              $doc_file[$i]=$doc_files;
            }
        }
      }

    }

    if(!empty($this->input->post('doc_name')))
    {        
        $doc_name=array_filter($this->input->post('doc_name'));
        
          for ($l=0; $l <count($doc_name) ; $l++) { 
           $docdata = array('user_id' => $user_id,
                    'doc_name' => $doc_name[$l],
                            'doc_file' => $doc_file[$l]);
           $this->db->insert('document_details', $docdata); 
        }
    }

   $inputdata['id']=$this->db->insert_id();
   $this->load->library('sendemail');
   $this->sendemail->send_email_verification($inputdata);
   $response['msg']=$this->language['lg_registration_su']; 
   $this->session->set_flashdata('success_message',$this->language['lg_registration_su']);
   $response['status']=200;              
 }
 else
 {
   $response['msg']=$this->language['lg_registration_fa'];
   $response['status']=500; 
 } 

}

echo json_encode($response);
}

public function is_valid_login()
{
  $response=array();
  
 $otp_form=$this->input->post('otpno');
 $remember=$this->input->post('remember');

  
  if(!empty($otp_form)){
    $email = $this->input->post('mobileno');
    $password = $this->input->post('otpno');
    

  }else{
  $email = $this->input->post('email');
  $password = $this->input->post('password');
  }



  $result = $this->signin->is_valid_login($email,$password,$otp_form);
  if(!empty($result['status']) && $result['status'] == 1)
  {
    $session_data=array('user_id' =>$result['id'],'role'=>$result['role']);
    $this->session->set_userdata($session_data);
    $this->session->unset_userdata('admin_id');

    if($remember==1){
            
                //Set cookie
                $cookie = array(
                    'name'   => 'user_id',
                    'value'  => $result['id'],
                    'domain' => '.ayurwayda.dreamguystech.com',
                    'expire' => '1209600',
                    'path'   => '/',
                    'secure' => TRUE
                );
                $this->input->set_cookie($cookie);            
            }
    
    
      $this->db->where('id',$result['id']);
      $this->db->update('users',array('login_otp'=>''));
    

    $response['msg']='';
    $response['status']=200;
  }
  else if(!empty($result['status']) && $result['status'] == 2)
  {
   $response['status']=500;
   $response['msg']=$this->language['lg_your_account_ha1'];
 }
 else
 {
   $response['status']=500;
   $response['msg']=$this->language['lg_wrong_login_cre'];
 }
 
 

 echo json_encode($response); 
}

public function sign_out()
{
  
 $this->session->sess_destroy();
 
 $this->session->set_flashdata('success_message',$this->language['lg_logged_out_succ']);
 redirect(base_url());
}

public function activate($id)
{
 
  $user_details=$this->db->where('md5(id)',$id)->get('users')->row_array();
  if($user_details['is_verified']=='1')
  {
   $this->session->set_flashdata('error_message',$this->language['lg_your_account_al']);
   redirect(base_url());
 }
 
 $inputdata['is_verified']=1;
 $result=$this->signin->update($inputdata,$id);
 if($result==true)
 {   
  $this->session->set_flashdata('success_message',$this->language['lg_your_account_ha']);
  redirect(base_url());
  
}
else
{
 $this->session->set_flashdata('error_message',$this->language['lg_your_account_ve']);
 redirect(base_url()); 
} 

}

public function update_password()
{
  $inputdata=array();
  $response=array();
  $id=$this->input->post('id');
  $user_details=$this->db->where('md5(id)',$id)->get('users')->row_array();
  $inputdata['password']=md5($this->input->post('password'));
  $inputdata['confirm_password']=md5($this->input->post('confirm_password'));
  $result=$this->signin->update($inputdata,$id);
  if($result==true)
  {
   $response['msg']=$this->language['lg_password_change1']; 
   $response['status']=200;              
 }
 else
 {
  $response['msg']=$this->language['lg_password_change'];
  $response['status']=500; 
} 

echo json_encode($response);
}

public function reset($id)
{
  

  $user_details=$this->db->where('forget',$id)->get('users')->row_array();
  if(!empty($user_details))
  {
    $currenttime=date('Y-m-d H:i:s');

    if ($user_details['expired_reset'] >= $currenttime){

      $inputdata['forget']='';
      $this->signin->update($inputdata,md5($user_details['id']));

      $this->data['id']=md5($user_details['id']);

      $this->data['page'] = 'change_password';
      $this->load->vars($this->data);
      $this->load->view($this->data['theme'].'/template');
    }
    else
    {
     $this->session->set_flashdata('error_message',$this->language['lg_your_reset_link']);
     redirect(base_url().'home');
   }
 }
 else
 {
  $this->session->set_flashdata('error_message',$this->language['lg_your_reset_link']);
  redirect(base_url().'home');
}



}

public function reset_password()
{
  $inputdata=array();
  $response=array();
  $inputdata['email']=$this->input->post('resetemail');
  $query=$this->db->where('email',$inputdata['email'])->get('users');
  $user_details=$query->row_array();
  $already_exits=$query->num_rows();
  
  if($already_exits >=1)
  {
    $inputdata['expired_reset']=date('Y-m-d H:i:s', strtotime("+3 hours"));
    $inputdata['forget']=urlencode($this->encryptor('encrypt',$user_details['email'].time()));
    $this->signin->update($inputdata,md5($user_details['id']));
    $user_details['url']=$inputdata['forget'];
    $this->load->library('sendemail');
    $this->sendemail->send_resetpassword_email($user_details);
    $response['msg']=$this->language['lg_your_reset_pass'];
    $response['status']=200;
  }
  
  else
  {
    
   $response['msg']=$this->language['lg_your_email_addr'];
   $response['status']=500; 
   

 }
 

 echo json_encode($response);
} 

function encryptor($action, $string) 
{

  $output = false;
  $encrypt_method = "AES-256-CBC";
  $secret_key = 'bookotv';
  $secret_iv = 'bookotv123';
  $key = hash('sha256', $secret_key);
  $iv = substr(hash('sha256', $secret_iv), 0, 16);
  if( $action == 'encrypt' ) {

    $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);

    $output = base64_encode($output);

  }
  else if( $action == 'decrypt' ){

        //decrypt the given text/string/number

    $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);

  }



  return $output;

}

public function forgot_password()
{
  $this->data['page'] = 'forgot_password';
  $this->load->vars($this->data);
  $this->load->view($this->data['theme'].'/template');
}  


}