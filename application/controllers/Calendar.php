<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Calendar extends CI_Controller {

  public function __construct() {

        parent::__construct();

        if($this->session->userdata('user_id') ==''){
          if($this->session->userdata('admin_id'))
            {
              redirect(base_url().'home');
            }
            else
            {
              redirect(base_url().'signin');
            }
        }

        $this->data['theme']     = 'web';
        $this->data['page']     = '';
        $this->data['base_url'] = base_url();
        $this->timezone = $this->session->userdata('time_zone');
        if(!empty($this->timezone)){
          date_default_timezone_set($this->timezone);
        } 
        $this->data['language'] =language();
        $this->language=language(); 
        $this->data['default_language']=default_language();
        $this->data['active_language'] = active_language();
        $this->load->model('calendar_model','calendar');
        
         
    }


    public function index()
    {

          $this->data['module']    = 'calendar';
          $this->data['page'] = 'calendar';
          $this->load->vars($this->data);
          $this->load->view($this->data['theme'].'/template');
       
       
    }

    public function calendar_view()
  {


     $id = $this->session->userdata('user_id');
     $role = $this->session->userdata('role');


     $result = $this->calendar->calendar_view($id,$role);

     foreach($result as $record){

      $from_date_time =  $record['appointment_date'].' '.$record['appointment_time'];
      $to_date_time =  $record['appointment_date'].' '.$record['appointment_end_time'];
      $from_timezone =$record['time_zone'];

      $to_timezone = $this->session->userdata('time_zone');



      $from_date_time  = converToTz($from_date_time,$to_timezone,$from_timezone);
      $to_date_time  = converToTz($to_date_time,$to_timezone,$from_timezone);

      $from_time  = date('h:i a',strtotime($from_date_time));
      $to_time  = date('h:i a',strtotime($to_date_time));



      $start_time = date('g:i a',strtotime($from_time));
      $end_time = date('g:i a',strtotime($to_time));
      
      $title = $record['first_name'].' '.$record['last_name'];

      
         // setting color here
     if($record['approved'] == 0 && date('Y-m-d') > $record['appointment_date']) {
            $color = '#d9534f'; // Cancelled
          }else{
            $color = '#5bc0de'; // Pending
          }
          if($record['approved'] == 1) {
          $color = '#5cb85c';  // Approved
        }

        if($record['approved'] == 2) {
          $color = '#d9534f'; // Cancelled
        }


        $event_array[] = array(
         'id' => $record['id'],
         'user_id' => $record['appointment_to'],
         'title' => $title,
         'start' =>  $from_date_time,
         'end' => $to_date_time,
         'color' => $color,
         'timezone' =>$from_timezone
       );
      }


if(!empty($event_array)){
       // $this->session->set_userdata(array('search_id'=>''));
    echo json_encode($event_array);
}

}

}