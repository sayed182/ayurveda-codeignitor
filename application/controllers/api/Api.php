<?php
error_reporting(0);
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . '../vendor/stripe/stripe-php/init.php');
require_once(APPPATH . '../vendor/autoload.php');

use Twilio\Rest\Client;
use OpenTok\OpenTok;
use OpenTok\MediaMode;
use OpenTok\ArchiveMode;
use OpenTok\Session;
use OpenTok\Role;

require APPPATH . '/libraries/REST_Controller.php';

class Api extends REST_Controller {

public function __construct() {

        parent::__construct();

        $this->load->model('api_model','api');


           $header =  $this->input->request_headers(); // Get Header Data
            $token = (!empty($header['token']))?$header['token']:'';
          if(empty($token)){
            $token = (!empty($header['Token']))?$header['Token']:'';
          }

          $time_zone = (!empty($header['timezone']))?$header['timezone']:'';
          if(empty($time_zone)){
            $time_zone = (!empty($header['Timezone']))?$header['Timezone']:'';
          }

          $lang = (!empty($header['language']))?$header['language']:'';
          if(empty($lang)){
            $lang = (!empty($header['Language']))?$header['Language']:'en';
          }
            $language = get_languages($lang);
            $language = (!empty($language['language']['api']))?$language['language']['api']:'';
            $this->language_content = $language;

         
          date_default_timezone_set($time_zone);

          $this->tokbox_apiKey=!empty(settings("apiKey"))?settings("apiKey"):"";
          $this->tokbox_apiSecret=!empty(settings("apiSecret"))?settings("apiSecret"):"";

          $this->default_token = md5('Dreams99');
          $this->api_token = $token;
          $this->time_zone = $time_zone;
          $this->user_details = $this->api->get_user_id_using_token($token);/*patient or doctor*/
          $this->user_id=$this->user_details['id'];
          $this->role=$this->user_details['role'];
        
    }


    public function language_list_get()
       {
          if($this->user_id !=0  || ($this->default_token ==$this->api_token)) {

                $response=array();
                $result=array();
                
                 $response['language_list']=$this->api->language_list();
                
                 $response_code = '200';
                 $response_message = "";
                 $response_data=$response;
                 $result = $this->data_format($response_code,$response_message,$response_data);
                 $this->response($result, REST_Controller::HTTP_OK);

              }
            else
            {
              $this->token_error();
            }
       }


       public function language_keywords_post()
       {
          if($this->user_id !=0  || ($this->default_token ==$this->api_token)) {
                $user_data = array();
                $user_data = $this->post();

                $response=array();
                $result=array();

                 if(!empty($user_data['language']))
                 {
                
                     $response['language_keywords']=$this->api->language_keywords($user_data['language']);

                     if(empty($response['language_keywords']))
                      {
                           $response_code = '201';
                           $response_message = "No Results found";
                      }
                      else
                      {
                           $response_code = '200';
                           $response_message = "";
                      }
                    
                     

                 }
                 else
                 {

                     $response_code='500';
                     $response_message='Inputs field missing';
                 }

                 $response_data=$response;
                 $result = $this->data_format($response_code,$response_message,$response_data);
                 $this->response($result, REST_Controller::HTTP_OK);

              }
            else
            {
              $this->token_error();
            }
       }


     public function home_get()
    {
       if($this->user_id !=0  || ($this->default_token ==$this->api_token)) {
        
           $doctor_list = $this->api->doctor_list();
           $specialization_list =$this->api->specialization_list();

            $response=array();
            $result=array();
            $sresult=array();

            $response['doctor_list']=$result;
            $response['specialization_list']=$sresult;
          
          if (!empty($doctor_list)) {
            foreach ($doctor_list as $rows) {

              $data['id']=$rows['user_id'];
              $data['username']=$rows['username'];
              $data['profileimage']=(!empty($rows['profileimage']))?$rows['profileimage']:'assets/img/user.png';
              $data['first_name']=ucfirst($rows['first_name']);
              $data['last_name']=ucfirst($rows['last_name']);
              $data['specialization_img']=$rows['specialization_img'];
              $data['speciality']=ucfirst($rows['speciality']);
              $data['cityname']=$rows['cityname'];
              $data['countryname']=$rows['countryname'];
              $data['services']=$rows['services'];
              $data['rating_value']=$rows['rating_value'];
              $data['rating_count']=$rows['rating_count'];
              $data['is_favourite']=$this->api->is_favourite($rows['user_id'],$this->user_id);
              $data['currency']='$';
              $data['price_type']=($rows['price_type']=='Custom Price')?'Paid':'Free';
              $data['slot_type']='per slot';
              $data['amount']=($rows['price_type']=='Custom Price')?$rows['amount']:'0';
              $result[]=$data;
            }

            $response['doctor_list']=$result;
        }
       
       if (!empty($specialization_list)) {
            foreach ($specialization_list as $srows) {
              $sdata['id']=$srows['id'];
              $sdata['specialization_img']=base_url().$srows['specialization_img'];
              $sdata['specialization']=ucfirst($srows['specialization']);
              $sresult[]=$sdata;
            }

            $response['specialization_list']=$sresult;
        }

        if(empty($response['specialization_list']) && empty($response['doctor_list']))
        {
             $response_code = '201';
               $response_message = "No Results found";
        }
        else
        {
             $response_code = '200';
               $response_message = "";
        }

        $response_data=$response;
                     
        $result = $this->data_format($response_code,$response_message,$response_data);

        $this->response($result, REST_Controller::HTTP_OK);

       }
       else
        {
          $this->token_error();
        }



    }

     public function signin_post()
    {
         if($this->user_id !=0  || ($this->default_token ==$this->api_token)) {
              $data=array();
              $user_data = array();
              $response=array();
              $user_data = $this->post();
              $device_id='';
              $device_type='';
      
            if(!empty($user_data['email']) && !empty($user_data['password']))
            { 
              if(!empty($user_data['device_id']))
              {
                $device_id=$user_data['device_id'];
              }

              if(!empty($user_data['device_type']))
              {
                $device_type=$user_data['device_type'];
              }
                  $user_result = $this->api->is_valid_login($user_data['email'],$user_data['password'],$device_id,$device_type);
                  if(!empty($user_result['status']==1))
                  {
                      $response_code='200';
                      $response_message='';
                      $response['user_details']=$user_result;
                  }
                  else if(!empty($user_result['status']==2))
                  {

                      $response_code='500';
                      $response_message='Your account has been inactive.';
                  }
                  else
                  {
                      $response_code='500';
                      $response_message='Wrong login credentials.';
                  }
            }
            else
            {
                   $response_code='500';
                   $response_message='Inputs field missing';
            }

             
             
             $result = $this->data_format($response_code,$response_message,$response);
             $this->response($result, REST_Controller::HTTP_OK);
         
         }
         else
          {
            $this->token_error();
          }
     }


     public function signup_post()
    {
         if($this->user_id !=0  || ($this->default_token ==$this->api_token)) {
              $data=array();
              $user_data = array();
              $response=array();
              $user_data = $this->post();

        
      if(!empty($user_data['first_name']) && !empty($user_data['last_name']) && !empty($user_data['email']) && !empty($user_data['mobileno']) && !empty($user_data['role']) && !empty($user_data['password']) && !empty($user_data['confirm_password']))
      {   

        $inputdata['first_name']=$user_data['first_name'];
        $inputdata['last_name']=$user_data['last_name'];
        $inputdata['email']=$user_data['email'];
        $inputdata['mobileno']=$user_data['mobileno'];
        $inputdata['username'] = generate_username($inputdata['first_name'].' '.$inputdata['last_name'].' '.$inputdata['mobileno']);
        $inputdata['role']=$user_data['role'];
        $inputdata['password']=md5($user_data['password']);
        $inputdata['confirm_password']=md5($user_data['confirm_password']);
        $inputdata['created_date']=date('Y-m-d H:i:s');


          $already_exits=$this->db->where('email',$inputdata['email'])->get('users')->num_rows();
          $already_exits_mobile_no=$this->db->where('mobileno',$inputdata['mobileno'])->get('users')->num_rows();
          if($already_exits >=1)
          {
                  $response_code='500';
                  $response_message='Your email address already exits';
          }
          else if($already_exits_mobile_no >=1)
          {
                  $response_code='500';
                  $response_message='Your mobileno already exits';
          }
          else
          {
              $results=$this->api->signup($inputdata);
              if($results==true)
              {   
                   $inputdata['id']=$this->db->insert_id();
                   $this->load->library('sendemail');
                   $this->sendemail->send_email_verification($inputdata);
                   $response_code='200';
                   $response_message='Registration success';
              }
             else
              {

                   $response_code='500';
                   $response_message='Registration failed';
                  
              } 

          }
      
            
        }
        else
        {
               $response_code='500';
               $response_message='Inputs field missing';
        }

             
           $result = $this->data_array_format($response_code,$response_message,$response);
           $this->response($result, REST_Controller::HTTP_OK);
         
         }
         else
          {
            $this->token_array_error();
          }
     }

     public function doctor_list_post()
     {
            if($this->user_id !=0  || ($this->default_token ==$this->api_token)) {

                $response=array();
                $docresult=array();
                $user_data = array();
                $user_data = $this->post();

                $page = $user_data['page'];
                $limit = $user_data['limit'];

               
            
               $doctor_list_count = $this->api->doctor_lists($page,$limit,1);
               $doctor_list = $this->api->doctor_lists($page,$limit,2); 
  
                              
              if (!empty($doctor_list)) {
                foreach ($doctor_list as $rows) {
                  $data['id']=$rows['user_id'];
                  $data['username']=$rows['username'];
                  $data['profileimage']=(!empty($rows['profileimage']))?$rows['profileimage']:'assets/img/user.png';
                  $data['first_name']=ucfirst($rows['first_name']);
                  $data['last_name']=ucfirst($rows['last_name']);
                  $data['specialization_img']=$rows['specialization_img'];
                  $data['speciality']=ucfirst($rows['speciality']);
                  $data['cityname']=$rows['cityname'];
                  $data['countryname']=$rows['countryname'];
                  $data['services']=$rows['services'];
                  $data['rating_value']=$rows['rating_value'];
                  $data['rating_count']=$rows['rating_count'];
                  $data['currency']='$';
                  $data['is_favourite']=$this->api->is_favourite($rows['user_id'],$this->user_id);
                  $data['price_type']=($rows['price_type']=='Custom Price')?'Paid':'Free';
                  $data['slot_type']='per slot';
                  $data['amount']=($rows['price_type']=='Custom Price')?$rows['amount']:'0';
                  $docresult[]=$data;
                }
            }

                $pages = !empty($page)?$page:1;
                $doctor_list_count = ceil($doctor_list_count/$limit);
                $next_page    = $pages + 1;
                $next_page    = ($next_page <=$doctor_list_count)?$next_page:-1;

                $response['doctor_list']=$docresult;
                $response['next_page']=$next_page;
                $response['current_page']=$page;
            
           
          
            if(empty($response['doctor_list']))
            {
                 $response_code = '201';
                 $response_message = "No Results found";
            }
            else
            {
                 $response_code = '200';
                 $response_message = "";
            }

            $response_data=$response;
                         
            $result = $this->data_format($response_code,$response_message,$response_data);

            $this->response($result, REST_Controller::HTTP_OK);

           }
           else
            {
              $this->token_error();
            }
     }

     public function doctor_search_post()
     {
            if($this->user_id !=0  || ($this->default_token ==$this->api_token)) {

                $response=array();
                $docresult=array();
                $user_data = array();
                $user_data = $this->post();
                $response_data=array();

               if(!empty($user_data['page']) && !empty($user_data['limit'])){ 
                      $page = $user_data['page'];
                      $limit = $user_data['limit'];

                      
                  
                     $doctor_list_count = $this->api->doctor_search($user_data,$page,$limit,1);
                     $doctor_list = $this->api->doctor_search($user_data,$page,$limit,2); 
        
                                    
                    if (!empty($doctor_list)) {
                      foreach ($doctor_list as $rows) {
                        $data['id']=$rows['user_id'];
                        $data['username']=$rows['username'];
                        $data['profileimage']=(!empty($rows['profileimage']))?$rows['profileimage']:'assets/img/user.png';
                        $data['first_name']=ucfirst($rows['first_name']);
                        $data['last_name']=ucfirst($rows['last_name']);
                        $data['specialization_img']=$rows['specialization_img'];
                        $data['speciality']=ucfirst($rows['speciality']);
                        $data['cityname']=$rows['cityname'];
                        $data['countryname']=$rows['countryname'];
                        $data['services']=$rows['services'];
                        $data['rating_value']=$rows['rating_value'];
                        $data['rating_count']=$rows['rating_count'];
                        $data['currency']='$';
                        $data['is_favourite']=$this->api->is_favourite($rows['user_id'],$this->user_id);
                        $data['price_type']=($rows['price_type']=='Custom Price')?'Paid':'Free';
                        $data['slot_type']='per slot';
                        $data['amount']=($rows['price_type']=='Custom Price')?$rows['amount']:'0';
                        $docresult[]=$data;
                      }
                  }

                      $pages = !empty($page)?$page:1;
                      $doctor_list_count = ceil($doctor_list_count/$limit);
                      $next_page    = $pages + 1;
                      $next_page    = ($next_page <=$doctor_list_count)?$next_page:-1;

                      $response['doctor_list']=$docresult;
                      $response['next_page']=$next_page;
                      $response['current_page']=$page;
                                  
                
                  if(empty($response['doctor_list']))
                  {
                       $response_code = '201';
                       $response_message = "No Results found";
                  }
                  else
                  {
                       $response_code = '200';
                       $response_message = "";
                  }

                  $response_data=$response;
                               
                  

                }
                else
                {
                  $response_code='500';
                  $response_message='Inputs field missing';
                }

                $result = $this->data_format($response_code,$response_message,$response_data);



                  $this->response($result, REST_Controller::HTTP_OK);

           }
           else
            {
              $this->token_error();
            }
     }

     public function specialization_list_post()
     {
            if($this->user_id !=0  || ($this->default_token ==$this->api_token)) {

                $response=array();
                $result=array();
                $user_data = array();
                $user_data = $this->post();

                $page = $user_data['page'];
                $limit = $user_data['limit'];

                $response['specialization_list']=$result;
            
               $specialization_list_count = $this->api->specialization_lists($page,$limit,1);
               $specialization_list = $this->api->specialization_lists($page,$limit,2); 
  
                              
              if (!empty($specialization_list)) {
                foreach ($specialization_list as $rows) {
                  $data['id']=$rows['id'];
                  $data['specialization_img']=$rows['specialization_img'];
                  $data['specialization']=ucfirst($rows['specialization']);
                  $result[]=$data;
                }

                $pages = !empty($page)?$page:1;
                $specialization_list_count = ceil($specialization_list_count/$limit);
                $next_page    = $pages + 1;
                $next_page    = ($next_page <=$specialization_list_count)?$next_page:-1;

                $response['specialization_list']=$result;
                $response['next_page']=$next_page;
                $response['current_page']=$page;
            }
           
          
            if(empty($response['specialization_list']))
            {
                 $response_code = '201';
                 $response_message = "No Results found";
            }
            else
            {
                 $response_code = '200';
                   $response_message = "";
            }

            $response_data=$response;
                         
            $result = $this->data_format($response_code,$response_message,$response_data);

            $this->response($result, REST_Controller::HTTP_OK);

           }
           else
            {
              $this->token_error();
            }
     }

      public function doctor_preview_post()
      {
            if($this->user_id !=0  || ($this->default_token ==$this->api_token)) {

                $response=array();
                $result=array();
                $user_data = array();
                $user_data = $this->post();
                $res=array();
                $doctor_details=$this->api->get_doctor_details($user_data['doctor_id']);
                foreach ($doctor_details as $key => $value) {
                  $res[$key]=$value;
                  $res['currency']='$';
                  $res['currency_code']='USD';
                  $res['is_favourite']=$this->api->is_favourite($user_data['doctor_id'],$this->user_id);
                }
                $response['doctor_details'] =$res;
                $response['education'] = $this->api->get_education_details($user_data['doctor_id']);
                $response['experience'] = $this->api->get_experience_details($user_data['doctor_id']);
                $response['awards'] = $this->api->get_awards_details($user_data['doctor_id']);
                $response['memberships'] = $this->api->get_memberships_details($user_data['doctor_id']);
                $response['registrations'] = $this->api->get_registrations_details($user_data['doctor_id']);
                $response['business_hours'] = $this->api->get_business_hours($user_data['doctor_id']);
                $response['reviews'] = $this->api->review_list_view($user_data['doctor_id']);

                 $response_code = '200';
                 $response_message = "";
                 $response_data=$response;
                 $result = $this->data_format($response_code,$response_message,$response_data);
                 $this->response($result, REST_Controller::HTTP_OK);

              }
            else
            {
              $this->token_error();
            }

       }

       public function patient_profile_get()
       {
          if($this->user_id !=0  || ($this->default_token ==$this->api_token)) {

                $response=array();
                $result=array();
                $user_data = array();
                $user_data = $this->post();

                 $response['patient_details']=$this->api->get_patient_details($this->user_id);
                
                 $response_code = '200';
                 $response_message = "";
                 $response_data=$response;
                 $result = $this->data_format($response_code,$response_message,$response_data);
                 $this->response($result, REST_Controller::HTTP_OK);

              }
            else
            {
              $this->token_error();
            }
       }



      public function update_patient_profile_post()
     {
        if($this->user_id !=0  || ($this->default_token ==$this->api_token)) {
              $data=array();
              $user_data = array();
              $response=array();
              $user_data = $this->post();



        
          if(!empty($user_data['first_name']) && !empty($user_data['last_name']) && !empty($user_data['gender']) && !empty($user_data['dob']) && !empty($user_data['blood_group']) && !empty($user_data['address1']) && !empty($user_data['address2']) && !empty($user_data['country'])&& !empty($user_data['state'])&& !empty($user_data['city'])&& !empty($user_data['postal_code']))
          {   



                if($_FILES["profile_image"]["name"] != '')
               {
                   $config["upload_path"] = './uploads/profileimage/temp/';
                   $config["allowed_types"] = '*';
                   $this->load->library('upload', $config);
                   $this->upload->initialize($config);

                          $_FILES["file"]["name"] = 'img_'.time().'.png';
                          $_FILES["file"]["type"] = $_FILES["profile_image"]["type"];
                          $_FILES["file"]["tmp_name"] = $_FILES["profile_image"]["tmp_name"];
                          $_FILES["file"]["error"] = $_FILES["profile_image"]["error"];
                          $_FILES["file"]["size"] = $_FILES["profile_image"]["size"];
                        if($this->upload->do_upload('file'))
                        {
                           $upload_data = $this->upload->data();
                          
                            $profile_img='uploads/profileimage/temp/'.$upload_data["file_name"];
                            $inputdata['profileimage']= $this->image_resize(200, 200, $profile_img, $upload_data["file_name"]);
                            
                            
                                                                         
                        }
                  }

                $inputdata['first_name']=$user_data['first_name'];
                $inputdata['last_name']=$user_data['last_name'];
                $inputdata['is_updated']=1;

                $userdata['user_id']=$this->user_id;
                $userdata['gender']=$user_data['gender'];
                $userdata['dob']=date('Y-m-d',strtotime(str_replace('/', '-', $user_data['dob'])));
                $userdata['blood_group']=$user_data['blood_group'];
                $userdata['address1']=$user_data['address1'];
                $userdata['address2']=$user_data['address2'];
                $userdata['country']=$user_data['country'];
                $userdata['state']=$user_data['state'];
                $userdata['city']=$user_data['city'];
                $userdata['postal_code']=$user_data['postal_code'];
                $userdata['update_at']=date('Y-m-d H:i:s');

                $results=$this->api->update_patient_profile($inputdata,$userdata,$this->user_id);

            if($results==true)
            {
                 $response_code='200';
                 $response_message='Profile successfully updated';            
            }
           else
            {
                $response_code='500';
                $response_message='Profile update failed';  
            } 
      
            
          }
          else
          {
                 $response_code='500';
                 $response_message='Inputs field missing';
          }

          $response['patient_details']=$this->api->get_patient_details($this->user_id);

             
           $result = $this->data_format($response_code,$response_message,$response);
           $this->response($result, REST_Controller::HTTP_OK);
         
         }
         else
          {
            $this->token_error();
          }
     }

     public function image_resize($width=0,$height=0,$image_url,$filename)

    {

      $source_path = $image_url;

      list($source_width, $source_height, $source_type) = getimagesize($source_path);

      switch ($source_type) {

        case IMAGETYPE_GIF:

          $source_gdim = imagecreatefromgif($source_path);

          break;

        case IMAGETYPE_JPEG:

          $source_gdim = imagecreatefromjpeg($source_path);

          break;

        case IMAGETYPE_PNG:

          $source_gdim = imagecreatefrompng($source_path);

          break;

      }

      $source_aspect_ratio = $source_width / $source_height;



       $desired_aspect_ratio = $width / $height;



      if ($source_aspect_ratio > $desired_aspect_ratio) {

        /*

         * Triggered when source image is wider

         */



        $temp_height = $height;

        $temp_width = ( int ) ($height * $source_aspect_ratio);

      } else {

        /*

         * Triggered otherwise (i.e. source image is similar or taller)

         */

        $temp_width = $width;

        $temp_height = ( int ) ($width / $source_aspect_ratio);

      }



      /*

       * Resize the image into a temporary GD image

       */

      $temp_gdim = imagecreatetruecolor($temp_width, $temp_height);

      imagecopyresampled(

        $temp_gdim,

        $source_gdim,

        0, 0,

        0, 0,

        $temp_width, $temp_height,

        $source_width, $source_height

      );
      $x0 = ($temp_width - $width) / 2;

      $y0 = ($temp_height - $height) / 2;

      $desired_gdim = imagecreatetruecolor($width, $height);

      imagecopy(

        $desired_gdim,

        $temp_gdim,

        0, 0,

        $x0, $y0,

        $width, $height

      );
      $image_url =  "uploads/profileimage/".$width."_".$height."_".$filename."";

      if($source_type ==IMAGETYPE_PNG){
        imagepng($desired_gdim,$image_url);
      }
      if ($source_type ==IMAGETYPE_JPEG) {
        imagejpeg($desired_gdim,$image_url);
      }
      return $image_url;
    }

     public function check_currentpassword($user_id,$current_password)
  {
        $result = $this->api->check_currentpassword($current_password,$user_id);
         if ($result > 0) {
                   return '1';
           } else {
                   return '2';
           }
           
         
  }

     public function change_password_post()
    {

       if($this->user_id !=0  || ($this->default_token ==$this->api_token)) {
              $data=array();
              $user_data = array();
              $response=array();
              $user_data = $this->post();

        
          if(!empty($user_data['current_password']) && !empty($user_data['password']) && !empty($user_data['confirm_password']))
          {   

              $valid=$this->check_currentpassword($this->user_id,$user_data['current_password']);
              if($valid=='1')
              {
                  if($user_data['password']==$user_data['confirm_password'])
                  {
                        $inputdata['password']=md5($user_data['password']);
                        $inputdata['confirm_password']=md5($user_data['confirm_password']);
                        $result=$this->api->update($inputdata,$this->user_id);
                            if($result==true)
                            {
                                 $response_code='200';
                                 $response_message='Password successfully updated';                   
                            }
                           else
                            {
                                $response_code='500';
                                $response_message='Password changed failed';
                            } 
                  }
                  else
                  {
                      $response_code='500';
                      $response_message='Your password does not match';
                  }
              }
              else
              {
                  $response_code='500';
                  $response_message='Your current password is invalid';  
              }
    
          }
          else
          {
                 $response_code='500';
                 $response_message='Inputs field missing';
          }

             
           $result = $this->data_array_format($response_code,$response_message,$response);
           $this->response($result, REST_Controller::HTTP_OK);
         
         }
         else
          {
            $this->token_array_error();
          }
            
    }

    public function forgot_password_post()
    {
     if($this->user_id !=0  || ($this->default_token ==$this->api_token)) {
              $data=array();
              $user_data = array();
              $response=array();
              $user_data = $this->post();

        if(!empty($user_data['email'])){

      $query=$this->db->where('email',$user_data['email'])->get('users');
      $user_details=$query->row_array();
      $already_exits=$query->num_rows();
     
            if($already_exits >=1)
            {
                    $inputdata['email']=$user_data['email'];
                    $inputdata['expired_reset']=date('Y-m-d H:i:s', strtotime("+3 hours"));
                    $inputdata['forget']=urlencode($this->encryptor('encrypt',$user_details['email'].time()));
                    $this->api->update($inputdata,$user_details['id']);
                    $user_details['url']=$inputdata['forget'];
                    $this->load->library('sendemail');
                    $this->sendemail->send_resetpassword_email($user_details);
                    $response_code='200';
                    $response_message='Your reset password email sent successfully';
            }
            
            else
            {
                    $response_code='200';
                    $response_message='Your email address not registered';
                              

            }
      

         }
        else
        {
               $response_code='500';
               $response_message='Inputs field missing';
        }

           $result = $this->data_array_format($response_code,$response_message,$response);
           $this->response($result, REST_Controller::HTTP_OK);
         
     }
     else
      {
        $this->token_array_error();
      }
    } 


    public function master_post()
    {
      if($this->user_id !=0  || ($this->default_token ==$this->api_token)) {
              $data=array();
              $user_data = array();
              $response=array();
              $user_data = $this->post();

              if($user_data['type']==1)
              {
                 $country=$this->db->get('country')->result_array();
                  $country_data=array();
                 foreach ($country as $rows) {
                    $cdata['value']=$rows['countryid'];
                    $cdata['label']=$rows['country'];
                    $country_data[]=$cdata;
                 }

                 $response['list']=$country_data;
               }

               if($user_data['type']==2)
              {
                 $swhere=array('countryid' =>$user_data['id']);
                 $state=$this->db->get_where('state',$swhere)->result_array();
                 $state_data=array();
                 foreach ($state as $srows) {
                    $sdata['value']=$srows['id'];
                    $sdata['label']=$srows['statename'];
                    $state_data[]=$sdata;
                 }

                 $response['list']=$state_data;
               }

               if($user_data['type']==3)
              {
                    $cwhere=array('stateid' =>$user_data['id']);
                   $city=$this->db->get_where('city',$cwhere)->result_array();
                   $city_data=array();
                   foreach ($city as $cirows) {
                      $cidata['value']=$cirows['id'];
                      $cidata['label']=$cirows['city'];
                      $city_data[]=$cidata;
                   }

                  $response['list']=$city_data;
               }


                if($user_data['type']==4)
              {
                    $spwhere=array('status' =>1);
                   $specialization=$this->db->get_where('specialization',$spwhere)->result_array();
                   $specialization_data=array();
                   foreach ($specialization as $sprows) {
                      $spdata['value']=$sprows['id'];
                      $spdata['label']=$sprows['specialization'];
                      $specialization_data[]=$spdata;
                   }

                  $response['list']=$specialization_data;
               }

              

               $response_code='200';
               $response_message='';    

               $result = $this->data_format($response_code,$response_message,$response);
               $this->response($result, REST_Controller::HTTP_OK);

         }
       else
        {
          $this->token_error();
        }
                 
    }




Public function available_time_slots_post(){

  if($this->user_id !=0  || ($this->default_token ==$this->api_token)) {
    $data=array();
    $user_data = array();
    $response_data=array();
    $user_data = $this->post();
    $token='0';
    if(!empty($user_data['slot']) && !empty($user_data['day_id']))
    {  

      if(!empty($user_data['start_times']) && !empty($user_data['end_time']))
      {
        $start=strtotime($user_data['start_times']);
        $end=strtotime($user_data['end_time']);
        $slots=$user_data['slot'];
         if($slots >= 5){
            for ($i=$start;$i<=$end;$i = $i + $slots*60){
             $tdatas[]=date('H:i:s',$i);   
           }   
         }else{
           for ($i=$start;$i<=$end;$i = $i + 60*60){
             $tdatas[]=date('H:i:s',$i);   
           }       
         }

         $token= count($tdatas)-1;
      }

       $slot =$user_data['slot'];  
      @$day_id =$user_data['day_id'];  
      $start=strtotime('00:00');
      $end=strtotime('24:00');
      $user_id = $this->user_id;
      if($slot >= 5){
        for ($i=$start;$i<=$end;$i = $i + $slot*60){
          $res['label']= date('g:i A',$i);
          $res['value']= date('H:i:s',$i);
          $res['added'] = 'false';
          
          $where = array('start_time'=>$res['value'],'user_id'=>$user_id,'day_id' =>$day_id);
          $result =$this->db->get_where('schedule_timing',$where)->row_array();


          if($result['slot'] != $slot){

            $wh = array('slot'=>$result['slot'],'user_id'=>$user_id);
            $this->db->delete('schedule_timing',$wh);
          }


          $where = array('end_time'=>$res['value'],'user_id'=>$user_id ,'day_id' =>$day_id);
          $result =$this->db->get_where('schedule_timing',$where)->row_array();


          if(!empty($user_data['start_time']) ){
            if($user_data['start_time'] == $res['value'] || strtotime($user_data['start_time']) > strtotime($res['value'])){
              $res['added'] = 'true';
            }
          }

          if(!empty($user_data['end_time']) ){
            $end_times=strtotime($user_data['end_time']);
            $end_time=($end_times - (($slot*60)));
            $end_time=date('H:i:s',$end_time);

            if($end_time == $res['value'] || strtotime($end_time) > strtotime($res['value'])){
              $res['added'] = 'true';
            }
          }


          $datas[]=$res;
        }   
      }else{

        for ($i=$start;$i<=$end;$i = $i + 60*60){
          $res['label']= date('g:i A',$i);
          $res['value']= date('H:i:s',$i);
          $res['added'] = 'false';
          
          $where = array('start_time'=>$res['value'],'day_id'=>$user_data['day_id'],'user_id'=>$user_id);
          $result =$this->db->get_where('schedule_timing',$where)->row_array();

          $where = array('end_time'=>$res['value'],'day_id'=>$user_data['day_id'],'user_id'=>$user_id);
          $result =$this->db->get_where('schedule_timing',$where)->row_array();


          if(!empty($user_data['start_time']) ){
            if($user_data['start_time'] == $res['value'] || strtotime($user_data['start_time']) > strtotime($res['value']) ){
              $res['added'] = 'true';
            }
          }

          if(!empty($user_data['end_time']) ){
            $end_times=strtotime($user_data['end_time']);
            $end_time=($end_times - ((60*60)));
            $end_time=date('H:i:s',$end_time);

            if($end_time == $res['value'] || strtotime($end_time) > strtotime($res['value'])){
              $res['added'] = 'true';
            }
          }

          $datas[]=$res;
        }       
      }
      $resdatas=array();
      foreach ($datas as $arows) {
        if($arows['added']=='false')
        {
          $respo['label']= $arows['label'];
          $respo['value']= $arows['value'];

          $resdatas[]=$respo;
        }
      }

      $response_schedule['schedule_list']=$resdatas;
      $response_schedule['token']=strval($token);

       $response_code = '200';
       $response_message = "";
       $response_data=$response_schedule;  

      
    }
    else
    {
      $response_code='500';
      $response_message='Inputs field missing';
    }


    $result = $this->data_format($response_code,$response_message,$response_data);
    $this->response($result, REST_Controller::HTTP_OK);

  }
  else
  {
    $this->token_error();
  }
}


public function add_schedule_post()
{
   if($this->user_id !=0  || ($this->default_token ==$this->api_token)) {

     $data=array();
     $user_data = array();
     $response=array();
     $user_data = $this->post();
     $inputdata=array();
     $results=false;
 

if(!empty($user_data['booking_details']) && !empty($user_data['day_id']) && !empty($this->time_zone)){

    $day_id=json_decode($user_data['day_id'],true);
    $booking_details=json_decode($user_data['booking_details'],true);
    
    $inputdata['user_id'] =$this->user_id;
    $inputdata['time_zone'] =$this->time_zone;
    $day_name='';
  for ($j=0; $j < count($day_id) ; $j++) {

    $this->db->where('user_id',$this->user_id)->where('day_id',$day_id[$j]);
    $this->db->delete('schedule_timing');


    switch ($day_id[$j]) {
       case '1':
        $day_name='Sunday';
         break;
       case '2':
        $day_name='Monday';
         break;
         case '3':
        $day_name='Tuesday';
         break;
         case '4':
        $day_name='Wednesday';
         break;
         case '5':
        $day_name='Thursday';
         break;
         case '6':
        $day_name='Friday';
         break;
         case '7':
        $day_name='Saturday';
         break;
       default:
        $day_name='';
         break;
     } 
   
   $inputdata['day_id']=$day_id[$j];
   $inputdata['day_name']=$day_name;

    
 
  for ($i=0; $i < count($booking_details) ; $i++) { 

   $inputdata['start_time'] = date('H:i:s',strtotime($booking_details[$i]['start_time_label'])); 
   $inputdata['end_time'] = date('H:i:s',strtotime($booking_details[$i]['end_time_label'])); 
   $inputdata['sessions']=$booking_details[$i]['session_id']; 
   $inputdata['token']=$booking_details[$i]['token'];  
   $inputdata['slot']=$booking_details[$i]['slot'];

  
   
      $count = $this->db->get_where('schedule_timing',array('user_id'=>$inputdata['user_id'],'start_time'=>$inputdata['start_time'],'end_time'=>$inputdata['end_time'],'sessions'=>$inputdata['sessions'],'token'=>$inputdata['token'],'day_id'=>$inputdata['day_id']))->num_rows();
      if($count == 0){
        $this->db->insert('schedule_timing',$inputdata);
        $results=($this->db->affected_rows()!= 1)?false:true;
      }   
   }

 }

     
     if($results==true)
    {
         $response_code='200';
         $response_message='Schedule timings added successfully';           
    }
    else
    {
        $response_code='500';
        $response_message='Schedule timings added failed';  
    } 

    

 }
  else
  {     
    if(empty($this->time_zone) && !empty($user_data['day_id']))
    {
        $day_id=json_decode($user_data['day_id'],true);
        for ($j=0; $j < count($day_id) ; $j++) {
            $this->db->where('user_id',$this->user_id)->where('day_id',$day_id[$j]);
            $this->db->delete('schedule_timing');
        }

         $response_code='200';
         $response_message='Schedule timings updated successfully';     
    }
    else
    {
          $response_code='500';
          $response_message='Inputs field missing';
    }
         
  }

     $result = $this->data_array_format($response_code,$response_message,$response);
     $this->response($result, REST_Controller::HTTP_OK);
              
  }
 else
  {
    $this->token_array_error();
  }

}



public function get_schedule_post()
{

 if($this->user_id !=0  || ($this->default_token ==$this->api_token)) {

         $data=array();
         $user_data = array();
         $response=array();
         $user_data = $this->post();
         $inputdata=array();

      if(!empty($user_data['day_id'])){

              $where = array('day_id'=>$user_data['day_id'],'user_id'=>$this->user_id);
              $data = $this->db->get_where('schedule_timing',$where)->result_array();
              $slot='';
              if(!empty($data))
              {
                foreach ($data as $rows) {
                  $res['id']=$rows['id'];
                  $res['user_id']=$rows['user_id'];
                  $res['session_id']=$rows['sessions'];
                  $res['token']=$rows['token'];
                  $res['start_time_label']=date('h:i A',strtotime($rows['start_time']));
                  $res['start_time_value']=$rows['start_time'];
                  $res['end_time_label']=date('h:i A',strtotime($rows['end_time']));
                  $res['end_time_value']=$rows['end_time'];
                  $res['day_id']=$rows['day_id'];
                  $res['day_name']=$rows['day_name'];
                  $res['time_zone']=$rows['time_zone'];
                  $res['slot']=$rows['slot'];
                  $slot=$rows['slot'];

                  $resdata[]=$res;
                }
                $response['schedule_list']=$resdata;
                $response_code = '200';
                 $response_message = "";
              }
              else
              {
                 $response['schedule_list']=array();
                 $response_code = '201';
                 $response_message = "No schedule found";
              }

              $response['already_day']=array();
              $already_day_id=$this->db->select('day_id')->where('user_id',$this->user_id)->group_by('day_id')->get('schedule_timing')->result_array();
               $response['already_day']='[]';
               if($already_day_id){
                $already_day=$this->multi_to_single($already_day_id);
                $response['already_day']='['.implode(',', $already_day).']';
               }
              $response['slot']=$slot;
              
        }
        else
        {
               $response_code='500';
               $response_message='Inputs field missing';
        }

     $result = $this->data_format($response_code,$response_message,$response);
     $this->response($result, REST_Controller::HTTP_OK);
              
  }
 else
  {
    $this->token_error();
  }
  

}


public function get_token_post()
{

       if($this->user_id !=0  || ($this->default_token ==$this->api_token)) {
              $data=array();
              $user_data = array();
              $response=array();
              $user_data = $this->post();
      if(!empty($user_data['schedule_date']) && !empty($user_data['doctor_id']) && !empty($this->time_zone))
      {      

            $schedule_date = $user_data['schedule_date'];
            $doctor_id = $user_data['doctor_id'];
            $day=date('D',strtotime(str_replace('/', '-', $schedule_date)));
            $day_id=0;
           switch ($day) {
              case 'Sun':
               $day_id=1;
                break;
              case 'Mon':
               $day_id=2;
                break;  
               case 'Tue':
               $day_id=3;
                break;
                 case 'Wed':
               $day_id=4;
                break;
                 case 'Thu':
               $day_id=5;
                break;
                 case 'Fri':
               $day_id=6;
                break;
                 case 'Sat':
               $day_id=7;
                break;
              default:
                $day_id=0;
                break;
            }

           $schedule_date=date('Y-m-d',strtotime(str_replace('/', '-', $schedule_date)));

            $schedule =  $this->api->get_schedule_timings($doctor_id,$day_id);
            $token_response=array();
           if(!empty($schedule)){ 
            $i=1; 
            foreach ($schedule as $rows) { 

                 $time_zone = $rows['time_zone'];                         
                 $current_timezone = $this->time_zone;

                
                 $start=strtotime($rows['start_time']);
                 $end=strtotime($rows['end_time']);
                 $datas=array();
                 if($rows['slot'] >= 5){
                    for ($j=$start;$j<= $end;$j = $j + $rows['slot']*60){
                     $datas[]=date('H:i:s',$j);   
                   }   
                 }else{
                   for ($j=$start;$j<= $end;$j = $j + 60*60){
                     $datas[]=date('H:i:s',$j);   
                   }       
                 }
                  $token_details=array();
                 for ($k=0; $k < $rows['token'] ; $k++) {
                  $l = $k+1;
                        $start_time =  converToTz($schedule_date.' '.$datas[$k],$current_timezone,$time_zone);
                       
                        if(date('Y-m-d H:i:s') < $start_time){
                            $token=$k+1;
                            $booked_session=get_booked_session($i,$token,$start_time,$doctor_id);
                            $tok_details['token_schedule_date']=date('Y-m-d',strtotime(str_replace('/', '-', $schedule_date)));
                            $tok_details['token_time_zone']=$rows['time_zone'];
                            $tok_details['token_start_time']=date('h:i A',strtotime($datas[$k]));
                            $tok_details['token_end_time']=date('h:i A',strtotime($datas[$l]));
                            $tok_details['token_session']=strval($i);
                            $tok_details['token_no']=strval($token);
                            $tok_details['is_selected']="0";
                            
                            
                            if($booked_session >= 1){
                              $tok_details['token_type']="1";
                            }
                            else{         
                         
                              $tok_details['token_type']="0";        
                            }

                            $token_details[]=$tok_details; 
                       }
                       

                     }

                    
                    $tokenresponse['session']=strval($i);
                    $tokenresponse['session_start_time']=date('h:i A',strtotime(converToTz($rows['start_time'],$current_timezone,$time_zone)));
                    $tokenresponse['session_end_time']=date('h:i A',strtotime(converToTz($rows['end_time'],$current_timezone,$time_zone)));
                    $tokenresponse['session_slot']=$rows['slot'];
                    $tokenresponse['token_details']=$token_details;
                  

                    $response[]=$tokenresponse;
                    $response_code = '200';
                    $response_message = "";

               $i++; }
            }
            else
            {
               $response_code = '201';
               $response_message = "No Token found";
            }

        }
        else
        {
               $response_code='500';
               $response_message='Inputs field missing';
        }


        $response_data=$response;
                     
        $result = $this->data_array_format($response_code,$response_message,$response_data);

        $this->response($result, REST_Controller::HTTP_OK);

      }
     else
      {
        $this->token_array_error();
      }


 }

 public function appoinments_calculation_post()
{
    if($this->user_id !=0  || ($this->default_token ==$this->api_token)) {
        $data=array();
        $user_data = array();
        $response=array();
        $user_data = $this->post();

        if(!empty($user_data['hourly_rate']))
        {
              $tax = !empty(settings("tax"))?settings("tax"):"0";
              $hourly_rate = $user_data['hourly_rate'];
                            
              $amount = $hourly_rate;
              $transcation_charge = ($amount * (2/100));
              $total_amount = $amount + $transcation_charge;
              $total_amount = $total_amount;
              $transcation_charge = $transcation_charge;


              $tax_amount = ($total_amount * $tax/100);
              $total_amount = $total_amount + $tax_amount;
                
               
              $response = array(
                'transcation_charge' => strval($transcation_charge),
                'tax_amount' => strval($tax_amount),
                'total_amount' => strval($total_amount),
                'hourly_rate'=>strval($hourly_rate),
                'currency_code'=>'USD',
                'currency'=>'$'            
              ); 

              $response_code='200';  
              $response_message=''; 
              
             
        }
        else
        {
               $response_code='500';
               $response_message='Inputs field missing';
        }

        $response_data=$response;
                     
        $result = $this->data_format($response_code,$response_message,$response_data);

        $this->response($result, REST_Controller::HTTP_OK);

    }
    else
    {
      $this->token_error();
    }          
}

public function checkout_post()
{
    if($this->user_id !=0  || ($this->default_token ==$this->api_token)) {
        $data=array();
        $user_data = array();
        $response=array();
        $user_data = $this->post();

        if(!empty($user_data['payment_type']))
        {
            if($user_data['payment_type']=='Free')
            {
                $details=$this->book_free_appoinment($user_data);
                $details=json_decode($details);
                $response_code=$details->code;
                $response_message=$details->message;
                
            }
            else
            {
              if($user_data['payment_method']=='1'){

                $details=$this->stripe_pay($user_data);
                $details=json_decode($details);
                $response_code=$details->code;
                $response_message=$details->message;
              }
              else
              {
                $details=$this->clinic_pay($user_data);
                $details=json_decode($details);
                $response_code=$details->code;
                $response_message=$details->message;
              }
            }
        }
        else
        {
               $response_code='500';
               $response_message='Inputs field missing';
        }

        $response_data=$response;
                     
        $result = $this->data_array_format($response_code,$response_message,$response_data);

        $this->response($result, REST_Controller::HTTP_OK);

    }
    else
    {
      $this->token_array_error();
    }          
}



  public function book_free_appoinment($user_data)
  {


     $data=array();
     $response=array();

    if(!empty($user_data['doctor_id']) && !empty($user_data['appoinment_date']) && !empty($user_data['appoinment_start_time']) && !empty($user_data['appoinment_end_time']) && !empty($user_data['appoinment_token']) && !empty($user_data['appoinment_session']) && !empty($user_data['appoinment_timezone']) )
    {

         $paymentdata = array(
           'user_id' => $this->user_id,
           'doctor_id' => $user_data['doctor_id'],
           'payment_status' => 0,
           'payment_date' => date('Y-m-d H:i:s'),
           'currency_code' => 'USD'
         );
         $this->db->insert('payment',$paymentdata);
         $payment_id = $this->db->insert_id();

         
          $opentok = new OpenTok($this->tokbox_apiKey,$this->tokbox_apiSecret);
            // An automatically archived session:
          $sessionOptions = array(
                  // 'archiveMode' => ArchiveMode::ALWAYS,
            'mediaMode' => MediaMode::ROUTED
          );
          $new_session = $opentok->createSession($sessionOptions);
                 // Store this sessionId in the database for later use
          $tokboxsessionId= $new_session->getSessionId();

          $tokboxtoken=$opentok->generateToken($tokboxsessionId);

             
               $appointmentdata['payment_id'] =  $payment_id;   
               $appointmentdata['appointment_from'] = $this->user_id;
               $appointmentdata['appointment_to'] = $user_data['doctor_id'];
               $appointmentdata['from_date_time'] = $user_data['appoinment_date'].' '.date('H:i:s',strtotime($user_data['appoinment_start_time']));
               $appointmentdata['to_date_time'] = $user_data['appoinment_date'].' '.date('H:i:s',strtotime($user_data['appoinment_end_time']));
               $appointmentdata['appointment_date'] = $user_data['appoinment_date'];
               $appointmentdata['appointment_time'] = date('H:i:s',strtotime($user_data['appoinment_start_time']));
               $appointmentdata['appointment_end_time'] = date('H:i:s',strtotime($user_data['appoinment_end_time']));
               $appointmentdata['appoinment_token'] = $user_data['appoinment_token'];
               $appointmentdata['appoinment_session'] = $user_data['appoinment_session'];
               $appointmentdata['payment_method'] = '1';
               $appointmentdata['tokboxsessionId'] = $tokboxsessionId;
               $appointmentdata['tokboxtoken'] = $tokboxtoken;
               $appointmentdata['paid'] = 1;
               $appointmentdata['approved'] = 1;
               $appointmentdata['time_zone'] = $user_data['appoinment_timezone'];
               $appointmentdata['created_date'] = date('Y-m-d H:i:s');
               $this->db->insert('appointment',$appointmentdata);
               $appointment_id = $this->db->insert_id();
               $appoinments_details = $this->api->get_appoinment_call_details($appointment_id);
              if($this->role==1)
              {
                $notifydata['include_player_ids'] = $appoinments_details['patient_device_id'];
                $device_type = $appoinments_details['patient_device_type'];
                $nresponse['from_name']=$appoinments_details['doctor_name'];
              }
              if($this->role==2)
              {
                $notifydata['include_player_ids'] = $appoinments_details['doctor_device_id'];
                $device_type = $appoinments_details['doctor_device_type'];
                $nresponse['from_name']=$appoinments_details['patient_name'];
              }

              $notifydata['message']=$nresponse['from_name'].' has has booked appointment on '.date('d M Y',strtotime($user_data['appoinment_date']));
              $notifydata['notifications_title']='';
              $nresponse['type']='Booking';
              $notifydata['additional_data'] = $nresponse;
              if($device_type=='Android')
              {
                sendFCMNotification($notifydata);
              }
              if($device_type=='IOS')
              {
                sendiosNotification($notifydata);
              }
              
               
               $response['code']='200';
               $response['message']='Appoinments added successfully';

    }
    else
    {
           $response['code']='500';
           $response['message']='Inputs field missing';
    }

    return json_encode($response);



         
  }

  public function stripe_pay($user_data)
  {

      $data=array();
     $response=array();

    if(!empty($user_data['doctor_id']) && !empty($user_data['payment_method']) && !empty($user_data['amount']) && !empty($user_data['access_token']) && !empty($user_data['hourly_rate'])&&  !empty($user_data['appoinment_date']) && !empty($user_data['appoinment_start_time']) && !empty($user_data['appoinment_end_time']) && !empty($user_data['appoinment_token']) && !empty($user_data['appoinment_session']) && !empty($user_data['appoinment_timezone']) )
    {

        $doctor_id = $user_data['doctor_id'];
   
         $amount = $user_data['amount'];
         $token=$user_data['access_token'];

         $stripe_option=!empty(settings("stripe_option"))?settings("stripe_option"):"";
         if($stripe_option=='1'){
            $stripe_secert_key=!empty(settings("sandbox_rest_key"))?settings("sandbox_rest_key"):"";
         }
         if($stripe_option=='2'){
            $stripe_secert_key=!empty(settings("live_rest_key"))?settings("live_rest_key"):"";
         }
        
         

         try {

          \Stripe\Stripe::setApiKey($stripe_secert_key);
          $striperesponse = \Stripe\Charge::create([
              'amount' => ($amount * 100),
              'currency' => 'USD',
              'description' => 'Exam charge',
              'source' => $token,
          ]);

          $txnid = "-";
          $status='failure';

          if(!empty($striperesponse)){
           $transaction_status = $striperesponse; 
 
            if(!empty($striperesponse->id)){
              $txnid = $striperesponse->id;
              $status='success';
            }else{
              $txnid = '-';
              $status='failure';
              $message='Transaction failure!.Please try again';
            }
          }


          } catch (Exception $e) {
                    
                 $status='failure';
                $body = $e->getJsonBody();
                $err  = $body['error'];
                $message  ='Transaction failure!.Please try again';
                 
                    
          }
          
        
            if($status == 'success'){

               $opentok = new OpenTok($this->tokbox_apiKey,$this->tokbox_apiSecret);
            // An automatically archived session:
              $sessionOptions = array(
                      // 'archiveMode' => ArchiveMode::ALWAYS,
                'mediaMode' => MediaMode::ROUTED
              );
              $new_session = $opentok->createSession($sessionOptions);
                     // Store this sessionId in the database for later use
              $tokboxsessionId= $new_session->getSessionId();

              $tokboxtoken=$opentok->generateToken($tokboxsessionId);

              /* Get Invoice id */

              $invoice = $this->db->order_by('id','desc')->limit(1)->get('payment')->row_array();
              if(empty($invoice)){
                $invoice_id = 1;   
              }else{
                $invoice_id = $invoice['id'];    
              }
              $invoice_id++;
              $invoice_no = 'I0000'.$invoice_id;

           // Store the Payment details

              $payments_data = array(
               'user_id' => $this->user_id,
               'doctor_id' => $doctor_id,
               'invoice_no' => $invoice_no,
               'per_hour_charge' => $user_data['hourly_rate'],
               'total_amount' => $amount,
               'currency_code' => 'USD',
               'txn_id' => $txnid,
               'order_id' => 'OD'.time().rand(),
               'transaction_status' => $transaction_status,  
               'payment_type' =>'Stripe',
               'tax'=>!empty(settings("tax"))?settings("tax"):"0",
               'tax_amount' => !empty($user_data['tax_amount'])?$user_data['tax_amount']:"0",
               'transcation_charge' => !empty($user_data['transcation_charge'])?$user_data['transcation_charge']:"0",
               'payment_status' => 1,
               'payment_date' => date('Y-m-d H:i:s'),
               );
              $this->db->insert('payment',$payments_data);
              $payment_id = $this->db->insert_id();
              

              
               $appointmentdata['payment_id'] =  $payment_id;   
               $appointmentdata['appointment_from'] = $this->user_id;
               $appointmentdata['appointment_to'] = $user_data['doctor_id'];
               $appointmentdata['from_date_time'] = $user_data['appoinment_date'].' '.date('H:i:s',strtotime($user_data['appoinment_start_time']));
               $appointmentdata['to_date_time'] = $user_data['appoinment_date'].' '.date('H:i:s',strtotime($user_data['appoinment_end_time']));
               $appointmentdata['appointment_date'] = $user_data['appoinment_date'];
               $appointmentdata['appointment_time'] = date('H:i:s',strtotime($user_data['appoinment_start_time']));
               $appointmentdata['appointment_end_time'] = date('H:i:s',strtotime($user_data['appoinment_end_time']));
               $appointmentdata['appoinment_token'] = $user_data['appoinment_token'];
               $appointmentdata['appoinment_session'] = $user_data['appoinment_session'];
               $appointmentdata['payment_method'] = $user_data['payment_method'];
               $appointmentdata['tokboxsessionId'] = $tokboxsessionId;
               $appointmentdata['tokboxtoken'] = $tokboxtoken;
               $appointmentdata['paid'] = 1;
               $appointmentdata['approved'] = 1;
               $appointmentdata['time_zone'] = $user_data['appoinment_timezone'];
               $appointmentdata['created_date'] = date('Y-m-d H:i:s');
               $this->db->insert('appointment',$appointmentdata);
               $appointment_id = $this->db->insert_id();
               $appoinments_details = $this->api->get_appoinment_call_details($appointment_id);
              if($this->role==1)
              {
                $notifydata['include_player_ids'] = $appoinments_details['patient_device_id'];
                $device_type = $appoinments_details['patient_device_type'];
                $nresponse['from_name']=$appoinments_details['doctor_name'];
              }
              if($this->role==2)
              {
                $notifydata['include_player_ids'] = $appoinments_details['doctor_device_id'];
                $device_type = $appoinments_details['doctor_device_type'];
                $nresponse['from_name']=$appoinments_details['patient_name'];
              }

              $notifydata['message']=$nresponse['from_name'].' has has booked appointment on '.date('d M Y',strtotime($user_data['appoinment_date']));
              $notifydata['notifications_title']='';
              $nresponse['type']='Booking';
              $notifydata['additional_data'] = $nresponse;
              if($device_type=='Android')
              {
                sendFCMNotification($notifydata);
              }
              if($device_type=='IOS')
              {
                sendiosNotification($notifydata);
              }
               
               $response['code']='200';
               $response['message']='Transaction success';

           }else{

               $response['code']='500';
               $response['message']=$message;
               
          }

      }
      else
      {
             $response['code']='500';
             $response['message']='Inputs field missing';
      }


      

       return json_encode($response);

  }


  public function clinic_pay($user_data)
  {

      $data=array();
     $response=array();

    if(!empty($user_data['doctor_id']) && !empty($user_data['payment_method']) && !empty($user_data['amount'])  && !empty($user_data['hourly_rate'])&&  !empty($user_data['appoinment_date']) && !empty($user_data['appoinment_start_time']) && !empty($user_data['appoinment_end_time']) && !empty($user_data['appoinment_token']) && !empty($user_data['appoinment_session']) && !empty($user_data['appoinment_timezone']) )
    {

        $doctor_id = $user_data['doctor_id'];
   
         $amount = $user_data['amount'];
        
           

           $paymentdata = array(
           'user_id' => $this->user_id,
           'doctor_id' => $user_data['doctor_id'],
           'payment_status' => 0,
           'payment_date' => date('Y-m-d H:i:s'),
           'currency_code' => 'USD'
         );
         $this->db->insert('payment',$paymentdata);
             $payment_id = $this->db->insert_id();  

              
               $appointmentdata['payment_id'] =  $payment_id;   
               $appointmentdata['appointment_from'] = $this->user_id;
               $appointmentdata['appointment_to'] = $user_data['doctor_id'];
               $appointmentdata['from_date_time'] = $user_data['appoinment_date'].' '.date('H:i:s',strtotime($user_data['appoinment_start_time']));
               $appointmentdata['to_date_time'] = $user_data['appoinment_date'].' '.date('H:i:s',strtotime($user_data['appoinment_end_time']));
               $appointmentdata['appointment_date'] = $user_data['appoinment_date'];
               $appointmentdata['appointment_time'] = date('H:i:s',strtotime($user_data['appoinment_start_time']));
               $appointmentdata['appointment_end_time'] = date('H:i:s',strtotime($user_data['appoinment_end_time']));
               $appointmentdata['appoinment_token'] = $user_data['appoinment_token'];
               $appointmentdata['appoinment_session'] = $user_data['appoinment_session'];
               $appointmentdata['payment_method'] = $user_data['payment_method'];
               $appointmentdata['paid'] = 1;
               $appointmentdata['approved'] = 1;
               $appointmentdata['time_zone'] = $user_data['appoinment_timezone'];
               $appointmentdata['created_date'] = date('Y-m-d H:i:s');
               $this->db->insert('appointment',$appointmentdata);
               $appointment_id = $this->db->insert_id();
               $appoinments_details = $this->api->get_appoinment_call_details($appointment_id);
              if($this->role==1)
              {
                $notifydata['include_player_ids'] = $appoinments_details['patient_device_id'];
                $device_type = $appoinments_details['patient_device_type'];
                $nresponse['from_name']=$appoinments_details['doctor_name'];
              }
              if($this->role==2)
              {
                $notifydata['include_player_ids'] = $appoinments_details['doctor_device_id'];
                $device_type = $appoinments_details['doctor_device_type'];
                $nresponse['from_name']=$appoinments_details['patient_name'];
              }

              $notifydata['message']=$nresponse['from_name'].' has has booked appointment on '.date('d M Y',strtotime($user_data['appoinment_date']));
              $notifydata['notifications_title']='';
              $nresponse['type']='Booking';
              $notifydata['additional_data'] = $nresponse;
              if($device_type=='Android')
              {
                sendFCMNotification($notifydata);
              }
              if($device_type=='IOS')
              {
                sendiosNotification($notifydata);
              }
               
               $response['code']='200';
               $response['message']='Transaction success';

           

      }
      else
      {
             $response['code']='500';
             $response['message']='Inputs field missing';
      }


      

       return json_encode($response);

  }


  public function appointments_list_post()
     {
            if($this->user_id !=0  || ($this->default_token ==$this->api_token)) {

                $response=array();
                $docresult=array();
                $user_data = array();
                $user_data = $this->post();
                $appresult= array();
                $page = $user_data['page'];
                $limit = $user_data['limit'];

               
            
               $appointments_list_count = $this->api->appointments_lists($page,$limit,1,$user_data,$this->user_id,$this->role);
               $appointments_list = $this->api->appointments_lists($page,$limit,2,$user_data,$this->user_id,$this->role); 
               
                              
              if (!empty($appointments_list)) {
                foreach ($appointments_list as $rows) {

                

              $current_timezone = $rows['time_zone'];               
              $old_timezone = $this->time_zone; 
                 

            $appointment_date=date('d M Y',strtotime(converToTz($rows['appointment_date'],$old_timezone,$current_timezone)));
            $appointment_time=converToTz($rows['from_date_time'],$old_timezone,$current_timezone);
            $appointment_end_time=converToTz($rows['to_date_time'],$old_timezone,$current_timezone);
            $created_date=date('d M Y',strtotime(converToTz($rows['created_date'],$old_timezone,$current_timezone)));
            
                  $data['id']=$rows['id'];
                  $data['profileimage']=(!empty($rows['profileimage']))?$rows['profileimage']:'assets/img/user.png';
                  $data['first_name']=ucfirst($rows['first_name']);
                  $data['last_name']=ucfirst($rows['last_name']);
                  $data['patient_id']='#PT00'.$rows['appointment_from'];
                  $data['patient_user_id']=$rows['appointment_from'];
                  $data['doctor_user_id']=$rows['appointment_to'];
                  $data['date']=$appointment_date;
                  $data['start_time']=date('h:i A',strtotime($appointment_time));
                  $data['end_time']=date('h:i A',strtotime($appointment_end_time));
                  $data['appoinment_start_time']=$appointment_time;
                  $data['appoinment_end_time']=$appointment_end_time;
                  $data['created_date']=$created_date;
                  $data['type']=ucfirst($rows['type']);
                  $data['payment_method']=$rows['payment_method'];
                  $data['amount']=!empty($rows['per_hour_charge'])?$rows['per_hour_charge']:'0';
                  $data['approved']=$rows['approved'];
                  $appresult[]=$data;
                }
            }

                $pages = !empty($page)?$page:1;
                $appointments_list_count = ceil($appointments_list_count/$limit);
                $next_page    = $pages + 1;
                $next_page    = ($next_page <=$appointments_list_count)?$next_page:-1;

                $response['appointments_list']=$appresult;
                $response['next_page']=$next_page;
                $response['current_page']=$page;
            
           
          
            if(empty($response['appointments_list']))
            {
                 $response_code = '201';
                 $response_message = "No Results found";
            }
            else
            {
                 $response_code = '200';
                 $response_message = "";
            }

            $response_data=$response;
                         
            $result = $this->data_format($response_code,$response_message,$response_data);

            $this->response($result, REST_Controller::HTTP_OK);

           }
           else
            {
              $this->token_error();
            }
     }


     public function appointments_history_post()
     {
            if($this->user_id !=0  || ($this->default_token ==$this->api_token)) {

                $response=array();
                $docresult=array();
                $user_data = array();
                $user_data = $this->post();
                $appresult= array();
                $page = $user_data['page'];
                $limit = $user_data['limit'];

               if($this->role==1 && empty($user_data['patient_id']))
               {
                  $response_code='500';
                  $response_message='Inputs field missing';
               }
               else
               {

                  if($this->role==1)
                  {
                    $patient_id=$user_data['patient_id'];
                  }
                  if($this->role==2)
                  {
                    $patient_id=$this->user_id;
                  }

                     $appointments_list_count = $this->api->appointments_history($page,$limit,1,$patient_id,$this->user_id,$this->role,$user_data);
                     $appointments_list = $this->api->appointments_history($page,$limit,2,$patient_id,$this->user_id,$this->role,$user_data); 
        
                                    
                    if (!empty($appointments_list)) {
                      foreach ($appointments_list as $rows) {

                      

                    $current_timezone = $rows['time_zone'];               
                    $old_timezone = $this->time_zone; 
                       

                  $appointment_date=date('d M Y',strtotime(converToTz($rows['appointment_date'],$old_timezone,$current_timezone)));
                  $appointment_time=converToTz($rows['from_date_time'],$old_timezone,$current_timezone);
                  $appointment_end_time=converToTz($rows['to_date_time'],$old_timezone,$current_timezone);
                  $created_date=date('d M Y',strtotime(converToTz($rows['created_date'],$old_timezone,$current_timezone)));
                  
                        $data['id']=$rows['id'];
                        $data['profileimage']=(!empty($rows['profileimage']))?$rows['profileimage']:'assets/img/user.png';
                        $data['first_name']=ucfirst($rows['first_name']);
                        $data['last_name']=ucfirst($rows['last_name']);
                        $data['patient_id']=($rows['role']=='2')?'#PT00'.$rows['appointment_from']:'';
                        $data['date']=$appointment_date;
                        $data['start_time']=date('h:i A',strtotime($appointment_time));
                        $data['end_time']=date('h:i A',strtotime($appointment_end_time));
                        $data['appoinment_start_time']=$appointment_time;
                        $data['appoinment_end_time']=$appointment_end_time;
                        $data['created_date']=$created_date;
                        $data['type']=ucfirst($rows['type']);
                        $data['payment_method']=$rows['payment_method'];
                        $data['amount']=!empty($rows['per_hour_charge'])?$rows['per_hour_charge']:'0';
                        $data['approved']=$rows['approved'];
                        $appresult[]=$data;
                      }
                  }

                      $pages = !empty($page)?$page:1;
                      $appointments_list_count = ceil($appointments_list_count/$limit);
                      $next_page    = $pages + 1;
                      $next_page    = ($next_page <=$appointments_list_count)?$next_page:-1;

                      $response['appointments_list']=$appresult;
                      $response['next_page']=$next_page;
                      $response['current_page']=$page;
                  
                 
                
                  if(empty($response['appointments_list']))
                  {
                       $response_code = '201';
                       $response_message = "No Results found";
                  }
                  else
                  {
                       $response_code = '200';
                       $response_message = "";
                  }
              }

                         

            $response_data=$response;
                         
            $result = $this->data_format($response_code,$response_message,$response_data);

            $this->response($result, REST_Controller::HTTP_OK);

           }
           else
            {
              $this->token_error();
            }
     }


     public function prescription_list_post()
     {
            if($this->user_id !=0  || ($this->default_token ==$this->api_token)) {

                $response=array();
                $docresult=array();
                $user_data = array();
                $user_data = $this->post();
                $preresult= array();
                $page = $user_data['page'];
                $limit = $user_data['limit'];

               if($this->role==1 && empty($user_data['patient_id']))
               {
                  $response_code='500';
                  $response_message='Inputs field missing';
               }
               else
               {

                  if($this->role==1)
                  {
                    $patient_id=$user_data['patient_id'];
                  }
                  if($this->role==2)
                  {
                    $patient_id=$this->user_id;
                  }

                     $prescription_list_count = $this->api->prescription_list($page,$limit,1,$patient_id,$this->user_id,$this->role);
                     $prescription_list = $this->api->prescription_list($page,$limit,2,$patient_id,$this->user_id,$this->role); 
        
                                    
                    if (!empty($prescription_list)) {
                      foreach ($prescription_list as $rows) {
                 
                  
                        $data['id']=$rows['id'];
                        $data['patient_image']=(!empty($rows['patient_image']))?$rows['patient_image']:'assets/img/user.png';
                        $data['doctor_image']=(!empty($rows['doctor_image']))?$rows['doctor_image']:'assets/img/user.png';
                        $data['patient_name']=ucfirst($rows['patient_name']);
                        $data['doctor_name']=ucfirst($rows['doctor_name']);
                        $data['specialization']=ucfirst($rows['specialization']);
                        $data['signature_image']=$rows['signature_image'];
                        $data['created_date']=date('d M Y',strtotime($rows['created_at']));
                        $data['prescription_details']=$this->api->prescription_details($rows['id']);
                        $preresult[]=$data;
                      }
                  }

                      $pages = !empty($page)?$page:1;
                      $prescription_list_count = ceil($prescription_list_count/$limit);
                      $next_page    = $pages + 1;
                      $next_page    = ($next_page <=$prescription_list_count)?$next_page:-1;

                      $response['prescription_list']=$preresult;
                      $response['next_page']=$next_page;
                      $response['current_page']=$page;
                  
                 
                
                  if(empty($response['prescription_list']))
                  {
                       $response_code = '201';
                       $response_message = "No Results found";
                  }
                  else
                  {
                       $response_code = '200';
                       $response_message = "";
                  }
              }

                         

            $response_data=$response;
                         
            $result = $this->data_format($response_code,$response_message,$response_data);

            $this->response($result, REST_Controller::HTTP_OK);

           }
           else
            {
              $this->token_error();
            }
     }


      public function medical_records_list_post()
     {
            if($this->user_id !=0  || ($this->default_token ==$this->api_token)) {

                $response=array();
                $docresult=array();
                $user_data = array();
                $user_data = $this->post();
                $mrresult= array();
                $page = $user_data['page'];
                $limit = $user_data['limit'];

               if($this->role==1 && empty($user_data['patient_id']))
               {
                  $response_code='500';
                  $response_message='Inputs field missing';
               }
               else
               {

                  if($this->role==1)
                  {
                    $patient_id=$user_data['patient_id'];
                  }
                  if($this->role==2)
                  {
                    $patient_id=$this->user_id;
                  }

                     $medical_records_list_count = $this->api->medical_records_list($page,$limit,1,$patient_id,$this->user_id,$this->role);
                     $medical_records_list = $this->api->medical_records_list($page,$limit,2,$patient_id,$this->user_id,$this->role); 
        
                                    
                    if (!empty($medical_records_list)) {
                      foreach ($medical_records_list as $rows) {
                 
                  
                        $data['id']=$rows['id'];
                        $data['patient_image']=(!empty($rows['patient_image']))?$rows['patient_image']:'assets/img/user.png';
                        $data['doctor_image']=(!empty($rows['doctor_image']))?$rows['doctor_image']:'assets/img/user.png';
                        $data['patient_name']=ucfirst($rows['patient_name']);
                        $data['doctor_name']=ucfirst($rows['doctor_name']);
                        $data['specialization']=ucfirst($rows['specialization']);
                        $data['filename']=$rows['file_name'];
                        $data['description']=$rows['description'];
                        $data['created_date']=date('d M Y',strtotime($rows['date']));
                        
                        $mrresult[]=$data;
                      }
                  }

                      $pages = !empty($page)?$page:1;
                      $medical_records_list_count = ceil($medical_records_list_count/$limit);
                      $next_page    = $pages + 1;
                      $next_page    = ($next_page <=$medical_records_list_count)?$next_page:-1;

                      $response['medical_records_list']=$mrresult;
                      $response['next_page']=$next_page;
                      $response['current_page']=$page;
                  
                 
                
                  if(empty($response['medical_records_list']))
                  {
                       $response_code = '201';
                       $response_message = "No Results found";
                  }
                  else
                  {
                       $response_code = '200';
                       $response_message = "";
                  }
              }

                         

            $response_data=$response;
                         
            $result = $this->data_format($response_code,$response_message,$response_data);

            $this->response($result, REST_Controller::HTTP_OK);

           }
           else
            {
              $this->token_error();
            }
     }

     public function billing_list_post()
     {
            if($this->user_id !=0  || ($this->default_token ==$this->api_token)) {

                $response=array();
                $docresult=array();
                $user_data = array();
                $user_data = $this->post();
                $preresult= array();
                $page = $user_data['page'];
                $limit = $user_data['limit'];

               if($this->role==1 && empty($user_data['patient_id']))
               {
                  $response_code='500';
                  $response_message='Inputs field missing';
               }
               else
               {

                  if($this->role==1)
                  {
                    $patient_id=$user_data['patient_id'];
                  }
                  if($this->role==2)
                  {
                    $patient_id=$this->user_id;
                  }

                     $billing_list_count = $this->api->billing_list($page,$limit,1,$patient_id,$this->user_id,$this->role);
                     $billing_list = $this->api->billing_list($page,$limit,2,$patient_id,$this->user_id,$this->role); 
        
                                    
                    if (!empty($billing_list)) {
                      foreach ($billing_list as $rows) {
                 
                  
                        $data['id']=$rows['id'];
                        $data['patient_image']=(!empty($rows['patient_image']))?$rows['patient_image']:'assets/img/user.png';
                        $data['doctor_image']=(!empty($rows['doctor_image']))?$rows['doctor_image']:'assets/img/user.png';
                        $data['patient_name']=ucfirst($rows['patient_name']);
                        $data['doctor_name']=ucfirst($rows['doctor_name']);
                        $data['specialization']=ucfirst($rows['specialization']);
                        $data['signature_image']=$rows['signature_image'];
                        $data['created_date']=date('d M Y',strtotime($rows['created_at']));
                        $data['billing_details']=$this->api->billing_details($rows['id']);
                        $preresult[]=$data;
                      }
                  }

                      $pages = !empty($page)?$page:1;
                      $billing_list_count = ceil($billing_list_count/$limit);
                      $next_page    = $pages + 1;
                      $next_page    = ($next_page <=$billing_list_count)?$next_page:-1;

                      $response['billing_list']=$preresult;
                      $response['next_page']=$next_page;
                      $response['current_page']=$page;
                  
                 
                
                  if(empty($response['billing_list']))
                  {
                       $response_code = '201';
                       $response_message = "No Results found";
                  }
                  else
                  {
                       $response_code = '200';
                       $response_message = "";
                  }
              }

                         

            $response_data=$response;
                         
            $result = $this->data_format($response_code,$response_message,$response_data);

            $this->response($result, REST_Controller::HTTP_OK);

           }
           else
            {
              $this->token_error();
            }
     }

     public function make_outgoing_call_post()
     {
            if($this->user_id !=0  || ($this->default_token ==$this->api_token)) {
              $data=array();
              $user_data = array();
              $response=array();
              $user_data = $this->post();
                   
            if(!empty($user_data['appoinment_id']) && !empty($user_data['call_type']) )
            { 

                  $appoinments_details = $this->api->get_appoinment_call_details($user_data['appoinment_id']);

                  if($this->role==1)
                  {
                    $response['from_user_id']=$appoinments_details['appointment_to'];
                    $response['from_name']=$appoinments_details['doctor_name'];
                    $response['to']=$appoinments_details['appointment_from'];
                    $notifydata['include_player_ids'] = $appoinments_details['patient_device_id'];
                    $device_type = $appoinments_details['patient_device_type'];
                    $notifydata['message']='Incoming call from '.$appoinments_details['doctor_name'];
                  }
                  if($this->role==2)
                  {
                    $response['from_user_id']=$appoinments_details['appointment_from'];
                    $response['from_name']=$appoinments_details['patient_name'];
                    $response['to']=$appoinments_details['appointment_to'];
                    $notifydata['include_player_ids'] = $appoinments_details['doctor_device_id'];
                    $device_type = $appoinments_details['doctor_device_type'];
                    $notifydata['message']='Incoming call from '.$appoinments_details['patient_name'];
                  }
                  $response['invite_id'] = $user_data['appoinment_id'];
                  $response['type'] = $user_data['call_type'];
                  $response['sessionId'] = $appoinments_details['tokboxsessionId'];
                  $response['token'] = $appoinments_details['tokboxtoken'];
                  $response['tokbox_apiKey'] =$this->tokbox_apiKey;
                  $response['tokbox_apiSecret'] =$this->tokbox_apiSecret;
                  $notifydata['notifications_title']='Incoming call';
                  $notifydata['additional_data'] = $response;


                  if($device_type=='Android')
                  {
                    sendFCMNotification($notifydata);
                  }
                  if($device_type=='IOS')
                  {
                    sendiosNotification($notifydata);
                  }
                  $this->call_details($response['invite_id'],$response['from_user_id'],$response['to'],$user_data['call_type']);

                  $response_data=$response;
                  $response_code='200';
                  $response_message='';
            }
            else
            {
                   $response_code='500';
                   $response_message='Inputs field missing';
            }

           
             
             $result = $this->data_format($response_code,$response_message,$response);
             $this->response($result, REST_Controller::HTTP_OK);
         
         }
         else
          {
            $this->token_error();
          }
 }

 public function make_incoming_call_post()
{
    if($this->user_id !=0  || ($this->default_token ==$this->api_token)) {
              $data=array();
              $user_data = array();
              $response=array();
              $user_data = $this->post();
      
            if(!empty($user_data['appoinment_id']) && !empty($user_data['call_type']))
            { 


                  $appoinments_details = $this->api->get_appoinment_call_details($user_data['appoinment_id']);
                  if($this->role==1)
                  {
                    $response['from_user_id']=$appoinments_details['appointment_to'];
                    $response['from_name']=$appoinments_details['doctor_name'];
                    $response['to']=$appoinments_details['appointment_from'];
                    $notifydata['include_player_ids'] = $appoinments_details['patient_device_id'];
                    $notifydata['message']='Incoming call from '.$appoinments_details['doctor_name'];
                  }
                  if($this->role==2)
                  {
                    $response['from_user_id']=$appoinments_details['appointment_from'];
                    $response['from_name']=$appoinments_details['patient_name'];
                    $response['to']=$appoinments_details['appointment_to'];
                    $notifydata['include_player_ids'] = $appoinments_details['doctor_device_id'];
                    $notifydata['message']='Incoming call from '.$appoinments_details['patient_name'];
                  }
                  $response['invite_id'] = $user_data['appoinment_id'];
                  $response['type'] = $user_data['call_type'];
                  $response['sessionId'] = $appoinments_details['tokboxsessionId'];
                  $response['token'] = $appoinments_details['tokboxtoken'];
                  $response['tokbox_apiKey'] =$this->tokbox_apiKey;
                  $response['tokbox_apiSecret'] =$this->tokbox_apiSecret;
                  $notifydata['notifications_title']='Incoming call';
                  $notifydata['additional_data'] = $response;
                  
                  $this->remove_call_details($response['invite_id']);
                  $this->call_accept($response['invite_id']);

                  $response_data=$response;
                  $response_code='200';
                  $response_message='';
            }
            else
            {
                   $response_code='500';
                   $response_message='Inputs field missing';
            }

           
             
             $result = $this->data_format($response_code,$response_message,$response);
             $this->response($result, REST_Controller::HTTP_OK);
         
         }
         else
          {
            $this->token_error();
          }
}

 public function end_call_post()
{
    if($this->user_id !=0  || ($this->default_token ==$this->api_token)) {
              $data=array();
              $user_data = array();
              $response=array();
              $user_data = $this->post();
      
            if(!empty($user_data['appoinment_id']) )
            { 
                  $appoinments_details = $this->api->get_appoinment_call_details($user_data['appoinment_id']);

                  if($this->role==1)
                  {
                    $notifydata['include_player_ids'] = $appoinments_details['patient_device_id'];
                    $response['from_name']=$appoinments_details['doctor_name'];
                    $device_type = $appoinments_details['patient_device_type'];
                  }
                  if($this->role==2)
                  {
                    $notifydata['include_player_ids'] = $appoinments_details['doctor_device_id'];
                    $response['from_name']=$appoinments_details['patient_name'];
                    $device_type = $appoinments_details['doctor_device_type'];
                  }
                  $response['type']='Decline';
                  $notifydata['message']=$response['from_name'].' has declined your call';
                  $notifydata['notifications_title']='';
                  $notifydata['additional_data'] = $response;


                  if($device_type=='Android')
                  {
                    sendFCMNotification($notifydata);
                  }
                  if($device_type=='IOS')
                  {
                    sendiosNotification($notifydata);
                  }

                  $this->remove_call_details($response['appointments_id']);
                  
                  $response_data=$response;
                  $response_code='200';
                  $response_message='';
            }
            else
            {
                   $response_code='500';
                   $response_message='Inputs field missing';
            }

           
             
             $result = $this->data_format($response_code,$response_message,$response);
             $this->response($result, REST_Controller::HTTP_OK);
         
         }
         else
          {
            $this->token_error();
          }
}

  private function call_details($appointments_id,$from,$to,$call_type)
    {
        
        $data['appointments_id']=$appointments_id;
        $data['call_from']=$from;
        $data['call_to']=$to;
        $data['call_type']=$call_type;
        $this->db->insert('call_details',$data);
        
    }

    private function remove_call_details($appointments_id)
    {
      $this->db->where('appointments_id',$appointments_id);
      $this->db->delete('call_details');
    }

     private function call_accept($appointments_id)
    {
      $this->db->where('id',$appointments_id);
      $this->db->update('appointment',array('call_status' =>1));
    }

private function multi_to_single($array) { 

      foreach ($array as $value) {
       $days_id[]=$value['day_id'];
      }
       return $days_id; 
  } 



function encryptor($action, $string) 
{

  $output = false;
  $encrypt_method = "AES-256-CBC";
  $secret_key = 'bookotv';
  $secret_iv = 'bookotv123';
  $key = hash('sha256', $secret_key);
  $iv = substr(hash('sha256', $secret_iv), 0, 16);
  if( $action == 'encrypt' ) {

    $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);

    $output = base64_encode($output);

  }
  else if( $action == 'decrypt' ){

//decrypt the given text/string/number

    $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);

  }



  return $output;

}


 public function my_patients_post()
  {
            if($this->user_id !=0  || ($this->default_token ==$this->api_token)) {

                $response=array();
                $patresult=array();
                $user_data = array();
                $user_data = $this->post();

                $page = $user_data['page'];
                $limit = $user_data['limit'];

               
            
               $patient_list_count = $this->api->patients_lists($page,$limit,1,$this->user_id);
               $patient_list = $this->api->patients_lists($page,$limit,2,$this->user_id); 
  
                              
              if (!empty($patient_list)) {
                foreach ($patient_list as $rows) {
                  $data['id']=$rows['id'];
                  $data['patient_id']=$rows['user_id'];
                  $data['username']=$rows['username'];
                  $data['profileimage']=(!empty($rows['profileimage']))?base_url().$rows['profileimage']:base_url().'assets/img/user.png';
                  $data['first_name']=ucfirst($rows['first_name']);
                  $data['last_name']=ucfirst($rows['last_name']);
                  $data['mobileno']=$rows['mobileno'];
                  $data['dob']=$rows['dob'];
                  $data['age']=age_calculate($rows['dob']);
                  $data['blood_group']=$rows['blood_group'];
                  $data['gender']=$rows['gender'];
                  $data['cityname']=$rows['cityname'];
                  $data['countryname']=$rows['countryname'];
                  $data['patient_list_count']=strval($patient_list_count);
                  $patresult[]=$data;
                }
            }

                $pages = !empty($page)?$page:1;
                $patient_list_count = ceil($patient_list_count/$limit);
                $next_page    = $pages + 1;
                $next_page    = ($next_page <=$patient_list_count)?$next_page:-1;

                $response['patient_list']=$patresult;
                $response['patient_count']=strval($patient_list_count);
                $response['next_page']=$next_page;
                $response['current_page']=$page;
            
           
          
            if(empty($response['patient_list']))
            {
                 $response_code = '201';
                 $response_message = "No Results found";
            }
            else
            {
                 $response_code = '200';
                 $response_message = "";
            }

            $response_data=$response;
                         
            $result = $this->data_format($response_code,$response_message,$response_data);

            $this->response($result, REST_Controller::HTTP_OK);

           }
           else
            {
              $this->token_error();
            }
     }

     public function my_doctors_post()
     {
           if($this->user_id !=0  || ($this->default_token ==$this->api_token)) {

                $response=array();
                $docresult=array();
                $user_data = array();
                $user_data = $this->post();

                $page = $user_data['page'];
                $limit = $user_data['limit'];

               
            
               $doctor_list_count = $this->api->my_doctor_lists($page,$limit,1,$this->user_id);
               $doctor_list = $this->api->my_doctor_lists($page,$limit,2,$this->user_id); 
  
                              
              if (!empty($doctor_list)) {
                foreach ($doctor_list as $rows) {
                  $data['id']=$rows['user_id'];
                  $data['username']=$rows['username'];
                  $data['profileimage']=(!empty($rows['profileimage']))?$rows['profileimage']:'assets/img/user.png';
                  $data['first_name']=ucfirst($rows['first_name']);
                  $data['last_name']=ucfirst($rows['last_name']);
                  $data['specialization_img']=$rows['specialization_img'];
                  $data['speciality']=ucfirst($rows['speciality']);
                  $data['cityname']=$rows['cityname'];
                  $data['countryname']=$rows['countryname'];
                  $data['services']=$rows['services'];
                  $data['rating_value']=$rows['rating_value'];
                  $data['rating_count']=$rows['rating_count'];
                  $data['currency']='$';
                  $data['is_favourite']=$this->api->is_favourite($rows['user_id'],$this->user_id);
                  $data['price_type']=($rows['price_type']=='Custom Price')?'Paid':'Free';
                  $data['slot_type']='per slot';
                  $data['amount']=($rows['price_type']=='Custom Price')?$rows['amount']:'0';
                  $docresult[]=$data;
                }
            }

                $pages = !empty($page)?$page:1;
                $doctor_list_count = ceil($doctor_list_count/$limit);
                $next_page    = $pages + 1;
                $next_page    = ($next_page <=$doctor_list_count)?$next_page:-1;

                $response['doctor_list']=$docresult;
                $response['next_page']=$next_page;
                $response['current_page']=$page;
            
           
          
            if(empty($response['doctor_list']))
            {
                 $response_code = '201';
                 $response_message = "No Results found";
            }
            else
            {
                 $response_code = '200';
                 $response_message = "";
            }

            $response_data=$response;
                         
            $result = $this->data_format($response_code,$response_message,$response_data);

            $this->response($result, REST_Controller::HTTP_OK);

           }
           else
            {
              $this->token_error();
            }
     }

     public function add_favourities_post()
    {
         if($this->user_id !=0  || ($this->default_token ==$this->api_token)) {
              $data=array();
              $user_data = array();
              $response=array();
              $user_data = $this->post();

        
      if(!empty($user_data['doctor_id']))
      {   

        if($this->role==2)
        {
            $inputdata['doctor_id']=$user_data['doctor_id'];
            $inputdata['patient_id']=$this->user_id;

            $where=array('doctor_id' =>$inputdata['doctor_id'],'patient_id'=>$inputdata['patient_id']);
            $already_favourities=$this->db->get_where('favourities',$where)->result_array();
            if(count($already_favourities) > 0 )
            {
               $this->db->where('doctor_id',$inputdata['doctor_id']);
               $this->db->where('patient_id',$inputdata['patient_id']);
               $this->db->delete('favourities');

               $response_code='200';
               $response_message='Favorites Removed Successfully';
            }
            else
            {
                $this->db->insert('favourities',$inputdata);
                $response_code='200';
               $response_message='Favorites Added Successfully';
            }
        }
        else
        {
              $response_code='500';
              $response_message='Invalid login';
        }

         
            
        }
        else
        {
               $response_code='500';
               $response_message='Inputs field missing';
        }

             
           $result = $this->data_array_format($response_code,$response_message,$response);
           $this->response($result, REST_Controller::HTTP_OK);
         
         }
         else
          {
            $this->token_array_error();
          }
}

 public function change_appoinments_status_post()
{
         if($this->user_id !=0  || ($this->default_token ==$this->api_token)) {
              $data=array();
              $user_data = array();
              $response=array();
              $user_data = $this->post();

        
      if(!empty($user_data['appoinments_id']) && !empty($user_data['appoinments_status']) )
      {   
        $appoinments_status=$user_data['appoinments_status'];
        if($user_data['appoinments_status']==2)
        {
          $appoinments_status=0;
        }

           $this->db->where('id',$user_data['appoinments_id'])->update('appointment',array('approved' =>$user_data['appoinments_status']));

             $appoinments_details = $this->api->get_appoinment_call_details($user_data['appoinments_id']);
              if($this->role==1)
              {
                $notifydata['include_player_ids'] = $appoinments_details['patient_device_id'];
                $device_type = $appoinments_details['patient_device_type'];
                $nresponse['from_name']=$appoinments_details['doctor_name'];
              }
              if($this->role==2)
              {
                $notifydata['include_player_ids'] = $appoinments_details['doctor_device_id'];
                $device_type = $appoinments_details['doctor_device_type'];
                $nresponse['from_name']=$appoinments_details['patient_name'];
              }



           if($user_data['appoinments_status']=='1')
           {
                 $response_code='200';
                 $response_message='Appoinments approved Successfully';
                 $notifydata['message']=$nresponse['from_name'].' has accepted the appointment';
           }else{
                 $response_code='200';
                 $response_message='Appoinments cancelled Successfully';
                 $notifydata['message']=$nresponse['from_name'].' has cancelled the appointment';
           }


              $notifydata['notifications_title']='';
              $notifydata['additional_data'] = $nresponse;
              if($device_type=='Android')
              {
                sendFCMNotification($notifydata);
              }
              if($device_type=='IOS')
              {
                sendiosNotification($notifydata);
              }

         
            
        }
        else
        {
               $response_code='500';
               $response_message='Inputs field missing';
        }

             
           $result = $this->data_array_format($response_code,$response_message,$response);
           $this->response($result, REST_Controller::HTTP_OK);
         
         }
         else
          {
            $this->token_array_error();
          }
}

public function add_reviews_post()
    {
         if($this->user_id !=0  || ($this->default_token ==$this->api_token)) {
              $data=array();
              $user_data = array();
              $response=array();
              $user_data = $this->post();

      if(!empty($user_data['doctor_id']) && !empty($user_data['rating']) && !empty($user_data['title'])&& !empty($user_data['review']))
      {   

              $inputdata['doctor_id']=$user_data['doctor_id'];
              $inputdata['user_id']=$this->user_id;
              $inputdata['rating']=$user_data['rating'];
              $inputdata['title']=$user_data['title'];
              $inputdata['review']=$user_data['review'];
              $inputdata['created_date'] = date('Y-m-d H:i:s');
              $inputdata['time_zone'] = $this->time_zone;

             $this->db->insert('rating_reviews',$inputdata);

                $response_code='200';
                $response_message='Reviews Added Successfully';
           
        
            
        }
        else
        {
               $response_code='500';
               $response_message='Inputs field missing';
        }

             
           $result = $this->data_array_format($response_code,$response_message,$response);
           $this->response($result, REST_Controller::HTTP_OK);
         
         }
         else
          {
            $this->token_array_error();
          }
}

 public function reviews_list_post()
{
            if($this->user_id !=0  || ($this->default_token ==$this->api_token)) {

                $response=array();
                $user_data = array();
                $user_data = $this->post();

                $page = $user_data['page'];
                $limit = $user_data['limit'];

               
            
               $reviews_list_count = $this->api->review_list($page,$limit,1,$this->user_id);
               $reviews_list = $this->api->review_list($page,$limit,2,$this->user_id); 
                             

                $pages = !empty($page)?$page:1;
                $reviews_list_count = ceil($reviews_list_count/$limit);
                $next_page    = $pages + 1;
                $next_page    = ($next_page <=$reviews_list_count)?$next_page:-1;

                $response['reviews_list']=$reviews_list;
                $response['next_page']=$next_page;
                $response['current_page']=$page;
            
                          
            if(empty($reviews_list))
            {
                 $response_code = '201';
                 $response_message = "No Results found";
            }
            else
            {
                 $response_code = '200';
                 $response_message = "";
            }

            $response_data=$response;
                         
            $result = $this->data_format($response_code,$response_message,$response_data);

            $this->response($result, REST_Controller::HTTP_OK);

           }
           else
            {
              $this->token_error();
            }
     }

     public function dashboard_count_post()
     {
          if($this->user_id !=0  || ($this->default_token ==$this->api_token)) {

                $response=array();
                $user_data = array();
                $user_data = $this->post();
                
               if(empty($user_data['patient_id']))
               {
                 $patient_id='0';
               }
               else
               {
                 $patient_id=$user_data['patient_id'];
               }

                  if($this->role==1)
                  {
                    $patient_id=$user_data['patient_id'];
                    
                  }
                  if($this->role==2)
                  {
                    $patient_id=$this->user_id;
                    
                  }

                  $user_id=$this->user_id;

                    

                      $response['doctors']['my_patients_count']=strval($this->api->patients_lists(1,1,1,$this->user_id));
                      $response['doctors']['appoinments_count']=strval($this->api->appointments_count('',$user_id,$this->role));
                      $response['doctors']['today_appoinments_count']=strval($this->api->appointments_count('1',$user_id,$this->role));
                      $response['doctors']['upcoming_appoinments_count']=strval($this->api->appointments_count('2',$user_id,$this->role));

                      $response['doctors']['doctor_details']=$this->api->doctor_details($user_id);

                       $response['patients']['appoinments_count']=strval($this->api->appointments_count('',$user_id,$this->role));
                      $response['patients']['today_appoinments_count']=strval($this->api->appointments_count('1',$user_id,$this->role));
                      $response['patients']['upcoming_appoinments_count']=strval($this->api->appointments_count('2',$user_id,$this->role));
                      $response['patients']['prescriptions_count']=strval($this->api->prescription_list(1,1,1,$patient_id,$this->user_id,$this->role));
                      $response['patients']['medical_count']=strval($this->api->medical_records_list(1,1,1,$patient_id,$this->user_id,$this->role));
                      $response['patients']['billing_count']=strval($this->api->billing_list(1,1,1,$patient_id,$this->user_id,$this->role));
                      $response['patients']['patient_details']=$this->api->patient_details($patient_id);
                      
                        $response_code='200';
                        $response_message='';               
                 
              

                         

            $response_data=$response;
                         
            $result = $this->data_format($response_code,$response_message,$response_data);

            $this->response($result, REST_Controller::HTTP_OK);

           }
           else
            {
              $this->token_error();
            }
     }


     public function favourities_list_post()
     {
            if($this->user_id !=0  || ($this->default_token ==$this->api_token)) {

                $response=array();
                $favresult=array();
                $user_data = array();
                $user_data = $this->post();

                $page = $user_data['page'];
                $limit = $user_data['limit'];

               
            
               $favourities_list_count = $this->api->get_favourites($page,$limit,1,$this->user_id);
               $favourities_list = $this->api->get_favourites($page,$limit,2,$this->user_id); 
  
                              
              if (!empty($favourities_list)) {
                foreach ($favourities_list as $rows) {
                  $data['id']=$rows['user_id'];
                  $data['username']=$rows['username'];
                  $data['profileimage']=(!empty($rows['profileimage']))?$rows['profileimage']:'assets/img/user.png';
                  $data['first_name']=ucfirst($rows['first_name']);
                  $data['last_name']=ucfirst($rows['last_name']);
                  $data['specialization_img']=$rows['specialization_img'];
                  $data['speciality']=ucfirst($rows['speciality']);
                  $data['cityname']=$rows['cityname'];
                  $data['countryname']=$rows['countryname'];
                  $data['services']=$rows['services'];
                  $data['rating_value']=$rows['rating_value'];
                  $data['rating_count']=$rows['rating_count'];
                  $data['currency']='$';
                  $data['is_favourite']=$this->api->is_favourite($rows['user_id'],$this->user_id);
                  $data['price_type']=($rows['price_type']=='Custom Price')?'Paid':'Free';
                  $data['slot_type']='per slot';
                  $data['amount']=($rows['price_type']=='Custom Price')?$rows['amount']:'0';
                  $favresult[]=$data;
                }
            }

                $pages = !empty($page)?$page:1;
                $favourities_list_count = ceil($favourities_list_count/$limit);
                $next_page    = $pages + 1;
                $next_page    = ($next_page <=$favourities_list_count)?$next_page:-1;

                $response['doctor_list']=$favresult;
                $response['next_page']=$next_page;
                $response['current_page']=$page;
            
           
          
            if(empty($response['doctor_list']))
            {
                 $response_code = '201';
                 $response_message = "No Results found";
            }
            else
            {
                 $response_code = '200';
                 $response_message = "";
            }

            $response_data=$response;
                         
            $result = $this->data_format($response_code,$response_message,$response_data);

            $this->response($result, REST_Controller::HTTP_OK);

           }
           else
            {
              $this->token_error();
            }
}

public function chat_users_get()
{
  if($this->user_id !=0  || ($this->default_token ==$this->api_token)) {
               
                $response=array();
                $result=array();

                  
                     if($this->role=='1')
                     {
                       $users_list=$this->api->get_patients($this->user_id);

                     }
                       
                     if($this->role=='2')
                     {
                       $users_list=$this->api->get_doctors($this->user_id);  
                     }

                     

                     foreach ($users_list as $rows) {
                       $row['userid']=$rows['userid'];
                       $row['role']=$rows['role'];
                       $row['first_name']=$rows['first_name'];
                       $row['last_name']=$rows['last_name'];
                       $row['username']=$rows['username'];
                       $row['profileimage']=$rows['profileimage'];
                       $row['chatdate']=($rows['chatdate']==null)?'':$rows['chatdate'];
                       $row['lastchat']=($rows['lastchat']==null)?'Welcome to doccure':$rows['lastchat'];
                       $response[]=$row;
                     }

                     $response_data['chat_list']=$response;



                     if(empty($users_list))
                      {
                           $response_code = '201';
                           $response_message = "No Results found";
                      }
                      else
                      {
                           $response_code = '200';
                           $response_message = "";
                      }
                    
                                     

                
                 $result = $this->data_format($response_code,$response_message,$response_data);
                 $this->response($result, REST_Controller::HTTP_OK);

              }
            else
            {
              $this->token_error();
            }
} 


     public function conversation_post()
    {
         if($this->user_id !=0  || ($this->default_token ==$this->api_token)) {
              $data=array();
              $user_data = array();
              $response=array();
              $user_data = $this->post();
              
      
            if(!empty($user_data['user_id']))
            { 
              
                  $user_id = $this->user_id;
                  $selected_user = $user_data['user_id']; /* Selected user  id */
                  $time_zone = $this->time_zone;


              $latest_chat= $this->api->get_latest_chat($selected_user,$user_id); 
              if(!empty($latest_chat)){
                foreach($latest_chat as $key => $currentuser) { 

                  $from_timezone = $currentuser['time_zone'];
                  $date_time = $currentuser['chatdate'];
                  $date_time  = converToTz($date_time,$time_zone,$from_timezone);

                  $msgdata['chat_time'] = date('Y-m-d H:i:s',strtotime($date_time));
                  $type = $currentuser['type'];        
                  $attachment_file = ($currentuser['file_path'])?($currentuser['file_path'].'/'.$currentuser['file_name']):'';        
                  $message = $currentuser['msg'];      

                 
                  $msgdata['msg_type'] = ($currentuser['sender_id'] != $user_id) ? 'received' : 'sent';
                  $msgdata['chat_from']= $currentuser['sender_id'];  
                  $msgdata['chat_to']= $currentuser['receiver_id'];          
                  $msgdata['from_user_name']= $currentuser['senderName'];  
                  $msgdata['to_user_name']= $currentuser['receiverName'];  
                  $msgdata['profile_from_image']= (!empty($currentuser['senderImage']))?base_url().$currentuser['senderImage']:base_url().'assets/img/user.png';
                  $msgdata['profile_to_image']= (!empty($currentuser['receiverImage']))?base_url().$currentuser['receiverImage']:base_url().'assets/img/user.png';

                    $msgdata['content']= ($message)?$message:'';
                    $datas[]=$msgdata;    
                 
                      }
                      $response_data['chat_details']=$datas;
                     $response=$response_data; 
                     $response_code='200';
                     $response_message='';

                  }
                  else
                  {
                         $response_code='201';
                         $response_message='No message found';
                  }

           }
            
            else
            {
                   $response_code='500';
                   $response_message='Inputs field missing';
            }

             
             
             $result = $this->data_format($response_code,$response_message,$response);
             $this->response($result, REST_Controller::HTTP_OK);
         
         }
         else
          {
            $this->token_error();
          }
}

   public function send_message_post()
  { 
      if($this->user_id !=0  || ($this->default_token ==$this->api_token)) {
              $data=array();
              $user_data = array();
              $response=array();
              $user_data = $this->post();
              
      
            if(!empty($user_data['user_id']) && !empty($user_data['message']))
            {

                $data['recieved_id'] =$user_data['user_id'];
                $data['sent_id'] = $this->user_id;
                $data['time_zone'] = $this->time_zone;
                $data['chatdate'] = date('Y-m-d H:i:s');
                $data['msg'] = $user_data['message'];
                
                $result = $this->db->insert('chat',$data);
                $chat_id = $this->db->insert_id();
                $users = array($data['recieved_id'],$data['sent_id']);
                for ($i=0; $i <2 ; $i++) { 
                  $datas = array('chat_id' =>$chat_id ,'can_view'=>$users[$i]);
                  $this->db->insert('chat_deleted_details',$datas);
                }




                  $user_id = $this->user_id;
                  $selected_user = $user_data['user_id']; /* Selected user  id */
                  $time_zone = $this->time_zone;

              $latest_chat= $this->api->get_latest_chat($selected_user,$user_id); 
              if(!empty($latest_chat)){
                foreach($latest_chat as $key => $currentuser) { 

                  $from_timezone = $currentuser['time_zone'];
                  $date_time = $currentuser['chatdate'];
                  $date_time  = converToTz($date_time,$time_zone,$from_timezone);

                  $msgdata['chat_time'] = date('Y-m-d H:i:s',strtotime($date_time));
                  $type = $currentuser['type'];        
                  $attachment_file = ($currentuser['file_path'])?($currentuser['file_path'].'/'.$currentuser['file_name']):'';        
                  $message = $currentuser['msg'];      

                 
                  $msgdata['msg_type'] = ($currentuser['sender_id'] != $user_id) ? 'received' : 'sent';
                  $msgdata['chat_from']= $currentuser['sender_id'];  
                  $msgdata['chat_to']= $currentuser['receiver_id'];          
                  $msgdata['from_user_name']= $currentuser['senderName'];  
                  $msgdata['to_user_name']= $currentuser['receiverName'];  
                  $msgdata['profile_from_image']= (!empty($currentuser['senderImage']))?base_url().$currentuser['senderImage']:base_url().'assets/img/user.png';
                  $msgdata['profile_to_image']= (!empty($currentuser['receiverImage']))?base_url().$currentuser['receiverImage']:base_url().'assets/img/user.png';

                    $msgdata['content']= ($message)?$message:'';
                    $msgdata['type']= 'Message';
                   
              }

                  $notifydata['include_player_ids'] = $currentuser['receiver_device_id'];
                  $device_type = $currentuser['receiver_device_type'];
                  $notifydata['message']=$message;
                  $notifydata['notifications_title']=$msgdata['from_user_name'];
                  $notifydata['additional_data'] = $msgdata;


                  if($device_type=='Android')
                  {
                    sendFCMNotification($notifydata);
                  }
                  if($device_type=='IOS')
                  {
                    sendiosNotification($notifydata);
                  }


              $response=$msgdata;
              $response_code='200';
              $response_message='';

            }


          }
           
          else
          {
                 $response_code='500';
                 $response_message='Inputs field missing';
          }

             
             
             $result = $this->data_format($response_code,$response_message,$response);
             $this->response($result, REST_Controller::HTTP_OK);
         
         }
         else
          {
            $this->token_error();
          }

  }

  public function logout_get()
  {
          if($this->user_id !=0  || ($this->default_token ==$this->api_token)) {

                $response=array();
                $result=array();
                
                 $this->api->logout($this->user_id);
                
                 $response_code = '200';
                 $response_message = "Logout successfully";
                 $response_data=$response;
                 $result = $this->data_format($response_code,$response_message,$response_data);
                 $this->response($result, REST_Controller::HTTP_OK);

              }
            else
            {
              $this->token_error();
            }
  }


public function data_format($response_code,$response_message,$data)
{
  $final_result = array();
  $response = array();
  $response['response_code']    = $response_code;
  $response['response_message'] = $response_message;
  $response['base_url'] = base_url();
  $final_result['response'] = $response;
  if(!empty($data))
  {
    $final_result['data'] = $data;
  }
  else
  {
    $final_result['data'] = new stdClass();
  }
  

  return $final_result;
}


public function data_array_format($response_code,$response_message,$data)
{
  $final_result = array();
  $response = array();
  $response['response_code']    = $response_code;
  $response['response_message'] = $response_message;
  $response['base_url'] = base_url();
  $final_result['response'] = $response;
  if(!empty($data))
  {
    $final_result['data'] = $data;
  }
  else
  {
    $final_result['data'] = array();
  }
  

  return $final_result;
}

public function token_error(){
  $response_code = '498';
  $response_message = "Invalid token or token missing";
  $data=new stdClass();
  $result = $this->data_format($response_code,$response_message,$data);

  $this->response($result, REST_Controller::HTTP_OK);
}

public function token_array_error(){
  $response_code = '498';
  $response_message = "Invalid token or token missing";
  $data=array();
  $result = $this->data_format($response_code,$response_message,$data);

  $this->response($result, REST_Controller::HTTP_OK);
}

}