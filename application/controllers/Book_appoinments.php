<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . '../vendor/stripe/stripe-php/init.php');
require_once(APPPATH . '../vendor/autoload.php');

use Twilio\Rest\Client;
use OpenTok\OpenTok;
use OpenTok\MediaMode;
use OpenTok\ArchiveMode;
use OpenTok\Session;
use OpenTok\Role;
use Razorpay\Api\Api;
class Book_appoinments extends CI_Controller {

  public function __construct() {

        parent::__construct();

        if($this->session->userdata('user_id') ==''){
          if($this->session->userdata('admin_id'))
            {
              redirect(base_url().'home');
            }
            else
            {
              redirect(base_url().'signin');
            }
        }

        $this->data['theme']     = 'web';
        $this->data['module']    = 'patient';
        $this->data['page']     = '';
        $this->data['base_url'] = base_url();
        
        $this->timezone = $this->session->userdata('time_zone');
        if(!empty($this->timezone)){
          date_default_timezone_set($this->timezone);
        }

        $this->data['language'] =language();
        $this->language=language(); 
        $this->data['default_language']=default_language();
        $this->data['active_language'] = active_language();

        $this->load->model('home_model','home');
        $this->load->model('book_appoinments_model','book');

        $this->tokbox_apiKey=!empty(settings("apiKey"))?settings("apiKey"):"";
        $this->tokbox_apiSecret=!empty(settings("apiSecret"))?settings("apiSecret"):"";
         
    }

    public function book($username)
    {
       
        if($this->session->userdata('role')=='2'){
          $this->data['page'] = 'book_appoinments';
          $this->data['doctors']=$this->home->get_doctor_details($username);
          $this->data['schedule']=$this->book->get_schedule_timings($this->data['doctors']['userid']);
          $this->session->set_userdata('doctor_id',$this->data['doctors']['userid']);
          $this->data['available_data'] = $this->book->get_available_appoinments($this->data['doctors']['userid'],'');
          $this->data['selected_date'] = date('Y-m-d');
          $this->load->vars($this->data);
          $this->load->view($this->data['theme'].'/template');
        }
        else
        {
          redirect(base_url().'dashboard');
        }
       
    }

    public function get_schedule_from_date()
    {

        $selected_date = $this->input->post('selected_date');
        $doctor_id = $this->input->post('doctor_id');
        $data['schedule'] =  $this->book->get_schedule_timings($doctor_id);
        $data['available_data'] = $this->book->get_available_appoinments($doctor_id,$selected_date);
        $data['selected_date'] = $selected_date;
        echo $this->load->view('web/modules/patient/book_appoinments_view',$data,TRUE);
    }

    public function set_booked_session()
    {
          $response=array();

         if($_POST['price_type'] == 'Free' || trim($_POST['hourly_rate']) == '' ){
            $appointment_details =  json_decode($this->input->post('appointment_details'));
            $this->session->set_userdata('appointment_details',$appointment_details);
            $this->book_free_appoinment();
            $response['status']=202;
            echo json_encode($response);
          }
          else
          {
           
              $tax = !empty(settings("tax"))?settings("tax"):"0";
              $appointment_details =  json_decode($this->input->post('appointment_details'));
              $hourly_rate = $this->input->post('hourly_rate');
              $selected_count = $this->input->post('selected_count');
              $doctor_username= $this->input->post('doctor_username');

              $doctor_details=$this->home->get_doctor_details(urldecode($doctor_username));

              $user_currency=get_user_currency();
              $user_currency_code=$user_currency['user_currency_code'];
              $user_currency_rate=$user_currency['user_currency_rate'];

              $currency_option = (!empty($user_currency_code))?$user_currency_code:$doctor_details['currency_code'];

              $rate_symbol = currency_code_sign($currency_option);

                      if(!empty($this->session->userdata('user_id'))){
                        $rate=get_doccure_currency($doctor_details['amount'],$doctor_details['currency_code'],$user_currency_code);
                      }else{
                           $rate= $doctor_details['amount'];
                        }
              $this->data['amount']=$rate_symbol.''.$rate;
              
              
              $amount = $rate * $selected_count;
              $transcation_charge = ($amount * (2/100));
              $total_amount = $amount + $transcation_charge;

              $tax_amount = (round($total_amount) * $tax/100);
             
              $total_amount = $total_amount + $tax_amount;


              $total_amount = $total_amount;
              $transcation_charge = $transcation_charge;
              $tax_amount = $tax_amount;  
              
               
              $new_data = array(
                'amount' => $amount,
                'transcation_charge' => $transcation_charge,
                'tax_amount' => $tax_amount,
                'total_amount' => $total_amount,
                'hourly_rate'=>$rate,
                'currency_code'=>$currency_option,
                'currency_symbol' =>$rate_symbol,           
              );    
              $this->session->set_userdata($new_data);
              $this->session->set_userdata('appointment_details',$appointment_details);
              $response['status']=200;
            echo json_encode($response);

          }

  }

  public function checkout()
  {
        if($this->session->userdata('role')=='2'){
          $doctor_id = $this->session->userdata('doctor_id');
          $this->data['page'] = 'checkout';
          $this->data['doctor_id'] =$doctor_id;
          $this->data['doctors']=$this->book->get_user_details($doctor_id);
          $this->data['patients']=$this->book->get_user_details($this->session->userdata('user_id'));
          $this->data['appointment_details'] =$this->session->userdata('appointment_details');
          $this->load->vars($this->data);
          $this->load->view($this->data['theme'].'/template');
        }
        else
        {
          redirect(base_url().'dashboard');
        }
  }

  public function book_free_appoinment()
  {

         $paymentdata = array(
           'user_id' => $this->session->userdata('user_id'),
           'doctor_id' => $this->session->userdata('doctor_id'),
           'payment_status' => 0,
           'payment_date' => date('Y-m-d H:i:s'),
           'currency_code' => 'USD'
         );
         $this->db->insert('payments',$paymentdata);
         $payment_id = $this->db->insert_id();

                      // Sending notification to mentor
         $doctor_id = $this->session->userdata('doctor_id');
         $appointment_details =$this->session->userdata('appointment_details');

          $opentok = new OpenTok($this->tokbox_apiKey,$this->tokbox_apiSecret);
            // An automatically archived session:
          $sessionOptions = array(
                  // 'archiveMode' => ArchiveMode::ALWAYS,
            'mediaMode' => MediaMode::ROUTED
          );
          $new_session = $opentok->createSession($sessionOptions);
                 // Store this sessionId in the database for later use
          $tokboxsessionId= $new_session->getSessionId();

          $tokboxtoken=$opentok->generateToken($tokboxsessionId);

         foreach ($appointment_details as $key => $value) {
           $appointmentdata['payment_id'] =  $payment_id;
           $appointmentdata['appointment_from'] = $this->session->userdata('user_id');
           $appointmentdata['appointment_to'] = $this->session->userdata('doctor_id');
           $appointmentdata['from_date_time'] = $value->date_value.' '.$value->start_time;
           $appointmentdata['to_date_time'] = $value->date_value.' '.$value->end_time;
           $appointmentdata['type'] = $value->type;
           $appointmentdata['appointment_date'] = $value->date_value;
           $appointmentdata['appointment_time'] = $value->start_time;
           $appointmentdata['appointment_end_time'] = $value->end_time;
           $appointmentdata['paid'] = 0;
           $appointmentdata['approved'] = 1;
           $appointmentdata['tokboxsessionId'] = $tokboxsessionId;
           $appointmentdata['tokboxtoken'] = $tokboxtoken;
           $appointmentdata['created_date'] = date('Y-m-d H:i:s');
           $appointmentdata['time_zone'] = $value->time_zone;
           $this->db->insert('appointments',$appointmentdata);
            $appointment_id = $this->db->insert_id();
           $this->send_appoinment_mail($appointment_id);
           if(settings('tiwilio_option')=='1') {
             $this->send_appoinment_sms($appointment_id);
           }
          }
          
         
  }

  public function stripe_pay()
  {


        $doctor_id = $this->session->userdata('doctor_id');
   
         $name = $this->input->post('firstname');
         $amount = round($this->input->post('amount'));
         $currency_code=$this->input->post('currency_code');
         $productinfo = $this->input->post('productinfo');
         $email = $this->input->post('email');
         $token=$this->input->post('access_token');

         $stripe_option=!empty(settings("stripe_option"))?settings("stripe_option"):"";
         if($stripe_option=='1'){
            $stripe_secert_key=!empty(settings("sandbox_rest_key"))?settings("sandbox_rest_key"):"";
         }
         if($stripe_option=='2'){
            $stripe_secert_key=!empty(settings("live_rest_key"))?settings("live_rest_key"):"";
         }
        
         \Stripe\Stripe::setApiKey($stripe_secert_key);


          $response = \Stripe\Charge::create([
              'amount' => ($amount * 100),
              'currency' => $currency_code,
              'description' => 'Exam charge',
              'source' => $token,
          ]);

          $txnid = "-";
          $status='failure';

          if(!empty($response)){
           $transaction_status = $response; 
 
            if(!empty($response->id)){
              $txnid = $response->id;
              $status='success';
            }else{
              $txnid = '-';
              $status='failure';
            }
          }
      
           
            if($status == 'success'){

               $opentok = new OpenTok($this->tokbox_apiKey,$this->tokbox_apiSecret);
            // An automatically archived session:
              $sessionOptions = array(
                      // 'archiveMode' => ArchiveMode::ALWAYS,
                'mediaMode' => MediaMode::ROUTED
              );
              $new_session = $opentok->createSession($sessionOptions);
                     // Store this sessionId in the database for later use
              $tokboxsessionId= $new_session->getSessionId();

              $tokboxtoken=$opentok->generateToken($tokboxsessionId);

              /* Get Invoice id */

              $invoice = $this->db->order_by('id','desc')->limit(1)->get('payments')->row_array();
              if(empty($invoice)){
                $invoice_id = 1;   
              }else{
                $invoice_id = $invoice['id'];    
              }
              $invoice_id++;
              $invoice_no = 'I0000'.$invoice_id;

           // Store the Payment details

              $payments_data = array(
               'user_id' => $this->session->userdata('user_id'),
               'doctor_id' => $doctor_id,
               'invoice_no' => $invoice_no,
               'per_hour_charge' => $this->session->userdata('hourly_rate'),
               'total_amount' => $amount,
               'currency_code' => $currency_code,
               'txn_id' => $txnid,
               'order_id' => 'OD'.time().rand(),
               'transaction_status' => $transaction_status,  
               'payment_type' =>'Stripe',
               'tax'=>!empty(settings("tax"))?settings("tax"):"0",
               'tax_amount' => $this->session->userdata('tax_amount'),
               'transcation_charge' => $this->session->userdata('transcation_charge'),
               'payment_status' => 1,
               'payment_date' => date('Y-m-d H:i:s'),
               );
              $this->db->insert('payments',$payments_data);
              $payment_id = $this->db->insert_id();

              $appointment_details =$this->session->userdata('appointment_details');

              foreach ($appointment_details as $key => $value) {
               $appointmentdata['payment_id'] =  $payment_id;   
               $appointmentdata['appointment_from'] = $this->session->userdata('user_id');
               $appointmentdata['appointment_to'] = $this->session->userdata('doctor_id');
               $appointmentdata['from_date_time'] = $value->date_value.' '.$value->start_time;
               $appointmentdata['to_date_time'] = $value->date_value.' '.$value->end_time;
               $appointmentdata['type'] = $value->type;
               $appointmentdata['appointment_date'] = $value->date_value;
               $appointmentdata['appointment_time'] = $value->start_time;
               $appointmentdata['appointment_end_time'] = $value->end_time;
               $appointmentdata['payment_method'] = $this->input->post('payment_method');
               $appointmentdata['tokboxsessionId'] = $tokboxsessionId;
               $appointmentdata['tokboxtoken'] = $tokboxtoken;
               $appointmentdata['paid'] = 1;
               $appointmentdata['approved'] = 1;
               $appointmentdata['time_zone'] = $value->time_zone;
               $appointmentdata['created_date'] = date('Y-m-d H:i:s');
               $this->db->insert('appointments',$appointmentdata);
               $appointment_id = $this->db->insert_id();
              // $this->send_appoinment_mail($appointment_id);
                //if(settings('tiwilio_option')=='1') {
                 $this->send_appoinment_sms($appointment_id);
               //}
             }
             
             $results = array('status'=>200);
             echo json_encode($results);

           }else{

              $this->data['title'] = $this->language['lg_transaction_fai'];
              $results = array('status'=>500);
              echo json_encode($results);
          }

  }

  public function create_razorpay_orders()
  {
    
    $amount=$this->input->post('amount');
    $razorpay_option=!empty(settings("razorpay_option"))?settings("razorpay_option"):"";
     if($razorpay_option=='1'){
        $api_key=!empty(settings("sandbox_key_id"))?settings("sandbox_key_id"):"";
        $api_secret=!empty(settings("sandbox_key_secret"))?settings("sandbox_key_secret"):"";
     }
     if($razorpay_option=='2'){
        $api_key=!empty(settings("live_key_id"))?settings("live_key_id"):"";
        $api_secret=!empty(settings("live_key_secret"))?settings("live_key_secret"):"";
     }
    $api = new Api($api_key, $api_secret);
    $order  = $api->order->create(array('receipt' => time(), 'amount' => ($amount * 100), 'currency' => 'INR')); 

    $doctor_id = $this->session->userdata('doctor_id');
    $patient_id=$this->session->userdata('user_id');

    $user_detail=user_detail($this->session->userdata('user_id'));
 
     
     $response['order_id']=$order['id'];
     $response['key_id']=$api_key;
     $response['amount']=($amount * 100);
     $response['currency']='INR';
     $response['sitename']=!empty(settings("meta_title"))?settings("meta_title"):"Doccure";
     $response['siteimage']=!empty(base_url().settings("logo_front"))?base_url().settings("logo_front"):base_url()."assets/img/logo.png";
     $response['patientname']=ucfirst($user_detail['first_name'].' '.$user_detail['last_name']);
     $response['email']=$user_detail['email'];
     $response['mobileno']=$user_detail['mobileno'];

     echo json_encode($response);

  }

  public function razorpay_appoinments()
  {
       $doctor_id = $this->session->userdata('doctor_id');
   
         $name = $this->input->post('firstname');
         $amount = $this->input->post('amount');
         $productinfo = $this->input->post('productinfo');
         $email = $this->input->post('email');
         $payment_id=$this->input->post('payment_id');
         $order_id=$this->input->post('order_id');
         $signature=$this->input->post('signature');

         $razorpay_option=!empty(settings("razorpay_option"))?settings("razorpay_option"):"";
         if($razorpay_option=='1'){
            $api_key=!empty(settings("sandbox_key_id"))?settings("sandbox_key_id"):"";
            $api_secret=!empty(settings("sandbox_key_secret"))?settings("sandbox_key_secret"):"";
         }
         if($razorpay_option=='2'){
            $api_key=!empty(settings("live_key_id"))?settings("live_key_id"):"";
            $api_secret=!empty(settings("live_key_secret"))?settings("live_key_secret"):"";
         }

         $api = new Api($api_key, $api_secret);

         $attributes  = array('razorpay_signature'  => $signature,  'razorpay_payment_id'  => $payment_id ,  'razorpay_order_id' => $order_id);
         $order  = $api->utility->verifyPaymentSignature($attributes);
         $response['payment_id']=$payment_id;
         $response['order_id']=$order_id;
         $response['signature']=$signature;
 
           $transaction_status = json_encode($response); 
            
              $txnid = $payment_id;
              $status='success';
            
               $opentok = new OpenTok($this->tokbox_apiKey,$this->tokbox_apiSecret);
            // An automatically archived session:
              $sessionOptions = array(
                      // 'archiveMode' => ArchiveMode::ALWAYS,
                'mediaMode' => MediaMode::ROUTED
              );
              $new_session = $opentok->createSession($sessionOptions);
                     // Store this sessionId in the database for later use
              $tokboxsessionId= $new_session->getSessionId();

              $tokboxtoken=$opentok->generateToken($tokboxsessionId);

              /* Get Invoice id */

              $invoice = $this->db->order_by('id','desc')->limit(1)->get('payments')->row_array();
              if(empty($invoice)){
                $invoice_id = 1;   
              }else{
                $invoice_id = $invoice['id'];    
              }
              $invoice_id++;
              $invoice_no = 'I0000'.$invoice_id;

           // Store the Payment details

              $payments_data = array(
               'user_id' => $this->session->userdata('user_id'),
               'doctor_id' => $doctor_id,
               'invoice_no' => $invoice_no,
               'per_hour_charge' => $this->session->userdata('hourly_rate'),
               'total_amount' => $amount,
               'currency_code' => 'INR',
               'txn_id' => $txnid,
               'order_id' => 'OD'.time().rand(),
               'transaction_status' => $transaction_status,  
               'payment_type' =>'Razorpay',
               'tax'=>!empty(settings("tax"))?settings("tax"):"0",
               'tax_amount' => $this->session->userdata('tax_amount'),
               'transcation_charge' => $this->session->userdata('transcation_charge'),
               'payment_status' => 1,
               'payment_date' => date('Y-m-d H:i:s'),
               );
              $this->db->insert('payments',$payments_data);
              $payment_id = $this->db->insert_id();

              $appointment_details =$this->session->userdata('appointment_details');

              foreach ($appointment_details as $key => $value) {
               $appointmentdata['payment_id'] =  $payment_id;   
               $appointmentdata['appointment_from'] = $this->session->userdata('user_id');
               $appointmentdata['appointment_to'] = $this->session->userdata('doctor_id');
               $appointmentdata['from_date_time'] = $value->date_value.' '.$value->start_time;
               $appointmentdata['to_date_time'] = $value->date_value.' '.$value->end_time;
               $appointmentdata['type'] = $value->type;
               $appointmentdata['appointment_date'] = $value->date_value;
               $appointmentdata['appointment_time'] = $value->start_time;
               $appointmentdata['appointment_end_time'] = $value->end_time;
               $appointmentdata['payment_method'] = $this->input->post('payment_method');
               $appointmentdata['tokboxsessionId'] = $tokboxsessionId;
               $appointmentdata['tokboxtoken'] = $tokboxtoken;
               $appointmentdata['paid'] = 1;
               $appointmentdata['approved'] = 1;
               $appointmentdata['time_zone'] = $value->time_zone;
               $appointmentdata['created_date'] = date('Y-m-d H:i:s');
               $this->db->insert('appointments',$appointmentdata);
               $appointment_id = $this->db->insert_id();
               //$this->send_appoinment_mail($appointment_id);
             }
             
             $results = array('status'=>200);
             echo json_encode($results);

           
  }

  public function paypal_pay()
  {

      $this->load->library('paypal_lib');
    // Set variables for paypal form
      $returnURL = base_url().'book_appoinments/success';
      $cancelURL = base_url().'book_appoinments/failure';
      $notifyURL = base_url().'book_appoinments/ipn';
      $paypal_email='';
      $paypal_option=!empty(settings("paypal_option"))?settings("paypal_option"):"";
       if($paypal_option=='1'){
          $paypal_email=!empty(settings("sandbox_email"))?settings("sandbox_email"):"";
       }
       if($paypal_option=='2'){
          $paypal_email=!empty(settings("live_email"))?settings("live_email"):"";
       }


      
     $name = $this->input->post('name');
     $amount = $this->input->post('amount');
     $currency_code = $this->input->post('currency_code'); 
     $productinfo = $this->input->post('productinfo');
     $doctor_id = $this->session->userdata('doctor_id');
     $patient_id=$this->session->userdata('user_id');
      
      // Add fields to paypal form
      $this->paypal_lib->add_field('return', $returnURL);
      $this->paypal_lib->add_field('cancel_return', $cancelURL);
      $this->paypal_lib->add_field('notify_url', $notifyURL);
      $this->paypal_lib->add_field('item_name', $productinfo);
      $this->paypal_lib->add_field('custom', $patient_id);
      $this->paypal_lib->add_field('item_number',  $doctor_id);
      $this->paypal_lib->add_field('amount',  $amount);
      $this->paypal_lib->add_field('currency_code', $currency_code);  
      $this->paypal_lib->add_field('business', $paypal_email); 
      $this->paypal_lib->paypal_auto_form();
     
  }

  public function success()
  {
      if(isset($_POST["txn_id"]) && !empty($_POST["txn_id"]))
      {
        $paypalInfo =  $this->input->post();
        $txnid= $paypalInfo['txn_id']; 
        $doctor_id=$paypalInfo['item_number']; 
        $patient_id=$paypalInfo['custom'];
        $amount=$paypalInfo['payment_gross'];
        
      }
      else
      {
        $paypalInfo =  $this->input->get();
        $txnid= $paypalInfo['tx']; 
        $doctor_id=$paypalInfo['item_number'];
        $patient_id=$paypalInfo['custom'];
        $amount=$paypalInfo['payment_gross'];
      }

         $transaction_status=json_encode($paypalInfo);

         $status='success';

         if($status == 'success'){

               $opentok = new OpenTok($this->tokbox_apiKey,$this->tokbox_apiSecret);
            // An automatically archived session:
              $sessionOptions = array(
                      // 'archiveMode' => ArchiveMode::ALWAYS,
                'mediaMode' => MediaMode::ROUTED
              );
              $new_session = $opentok->createSession($sessionOptions);
                     // Store this sessionId in the database for later use
              $tokboxsessionId= $new_session->getSessionId();

              $tokboxtoken=$opentok->generateToken($tokboxsessionId);

              /* Get Invoice id */

              $invoice = $this->db->order_by('id','desc')->limit(1)->get('payments')->row_array();
              if(empty($invoice)){
                $invoice_id = 1;   
              }else{
                $invoice_id = $invoice['id'];    
              }
              $invoice_id++;
              $invoice_no = 'I0000'.$invoice_id;

           // Store the Payment details

              $payments_data = array(
               'user_id' => $this->session->userdata('user_id'),
               'doctor_id' => $doctor_id,
               'invoice_no' => $invoice_no,
               'per_hour_charge' => $this->session->userdata('hourly_rate'),
               'total_amount' => $amount,
               'currency_code' => $currency_code,
               'txn_id' => $txnid,
               'order_id' => 'OD'.time().rand(),
               'transaction_status' => $transaction_status,  
               'payment_type' =>'Paypal',
               'tax'=>!empty(settings("tax"))?settings("tax"):"0",
               'tax_amount' => $this->session->userdata('tax_amount'),
               'transcation_charge' => $this->session->userdata('transcation_charge'),
               'payment_status' => 1,
               'payment_date' => date('Y-m-d H:i:s'),
               );
              $this->db->insert('payments',$payments_data);
              $payment_id = $this->db->insert_id();

              $appointment_details =$this->session->userdata('appointment_details');

              foreach ($appointment_details as $key => $value) {
               $appointmentdata['payment_id'] =  $payment_id;   
               $appointmentdata['appointment_from'] = $this->session->userdata('user_id');
               $appointmentdata['appointment_to'] = $this->session->userdata('doctor_id');
               $appointmentdata['from_date_time'] = $value->date_value.' '.$value->start_time;
               $appointmentdata['to_date_time'] = $value->date_value.' '.$value->end_time;
               $appointmentdata['type'] = $value->type;
               $appointmentdata['appointment_date'] = $value->date_value;
               $appointmentdata['appointment_time'] = $value->start_time;
               $appointmentdata['appointment_end_time'] = $value->end_time;
               $appointmentdata['payment_method'] = 'Card Payment';
               $appointmentdata['tokboxsessionId'] = $tokboxsessionId;
               $appointmentdata['tokboxtoken'] = $tokboxtoken;
               $appointmentdata['paid'] = 1;
               $appointmentdata['approved'] = 1;
               $appointmentdata['time_zone'] = $value->time_zone;
               $appointmentdata['created_date'] = date('Y-m-d H:i:s');
               $this->db->insert('appointments',$appointmentdata);
               $appointment_id = $this->db->insert_id();
               $this->send_appoinment_mail($appointment_id);
                if(settings('tiwilio_option')=='1') {
                   $this->send_appoinment_sms($appointment_id);
                 }
             }
               


               $this->session->set_flashdata('success_message',$this->language['lg_transaction_suc']);
               redirect(base_url().'dashboard');

           }else{

               $this->session->set_flashdata('error_message',$this->language['lg_transaction_fai']);
               redirect(base_url().'dashboard');
          }


  }

  public function failure(){

         $this->session->set_flashdata('error_message',$this->language['lg_transaction_fai']);
        redirect(base_url().'dashboard');

    }

  public function clinic_appoinments()
  {


    $paymentdata = array(
           'user_id' => $this->session->userdata('user_id'),
           'doctor_id' => $this->session->userdata('doctor_id'),
           'payment_status' => 0,
           'payment_date' => date('Y-m-d H:i:s'),
           'currency_code' => $this->session->userdata('currency_code'),
         );
         $this->db->insert('payments',$paymentdata);
         $payment_id = $this->db->insert_id();

                      // Sending notification to mentor
         $doctor_id = $this->session->userdata('doctor_id');
         $appointment_details =$this->session->userdata('appointment_details');

         foreach ($appointment_details as $key => $value) {
           $appointmentdata['payment_id'] =  $payment_id;
           $appointmentdata['appointment_from'] = $this->session->userdata('user_id');
           $appointmentdata['appointment_to'] = $this->session->userdata('doctor_id');
           $appointmentdata['from_date_time'] = $value->date_value.' '.$value->start_time;
           $appointmentdata['to_date_time'] = $value->date_value.' '.$value->end_time;
           $appointmentdata['type'] = 'clinic';
           $appointmentdata['appointment_date'] = $value->date_value;
           $appointmentdata['appointment_time'] = $value->start_time;
           $appointmentdata['appointment_end_time'] = $value->end_time;
           $appointmentdata['payment_method'] = $this->input->post('payment_method');
           $appointmentdata['paid'] = 0;
           $appointmentdata['approved'] = 1;
           $appointmentdata['created_date'] = date('Y-m-d H:i:s');
           $appointmentdata['time_zone'] = $value->time_zone;
           $this->db->insert('appointments',$appointmentdata);
           $appointment_id = $this->db->insert_id();
           $this->send_appoinment_mail($appointment_id);
           if(settings('tiwilio_option')=='1') {
             $this->send_appoinment_sms($appointment_id);
           }
          }
           

          $results = array('status'=>200);
        echo json_encode($results);
  }

  public function send_appoinment_mail($appointment_id)
  {
      $inputdata=$this->book->get_appoinments_details($appointment_id);
      $this->load->library('sendemail');
      $this->sendemail->send_appoinment_email($inputdata);
  }

  public function send_appoinment_sms($appointment_id)
  {

    $inputdata=$this->book->get_appoinments_details($appointment_id);

    $AccountSid = settings("tiwilio_apiKey");
    $AuthToken = settings("tiwilio_apiSecret");
    $from = settings("tiwilio_from_no");
    $twilio = new Client($AccountSid, $AuthToken);

    $msg = 'You have new appointment booked by the patient '.$inputdata["patient_name"];

    $mobileno="+".$inputdata['doctor_mobile'];

            try {
                $message = $twilio->messages
                  ->create($mobileno, // to
                           ["body" => $msg, "from" => $from]
                  );
                $response = array('status' => true);
                $status=0;
            } catch (Exception $error) {
                //echo $error;
               $status=500;
            }


  }



}