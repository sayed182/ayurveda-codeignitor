<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Question_model extends CI_Model {

// admin
  var $questions = 'questions q'; 
  var $doctor ='users d';
  var $doctor_details ='users_details dd';
  var $patient ='users p';
  var $patient_details ='users_details pd';
  var $specialization ='specialization s';
  var $answers='answers a';

  var $questions_column_search = array('d.first_name','d.last_name','d.profileimage','p.first_name','p.last_name','p.profileimage','q.questions','s.specialization','q.created_date'); 
  var $questions_order = array('q.status' => 'ASC'); // default order 

  var $answers_column_search = array('p.first_name','p.last_name','p.profileimage','q.questions','s.specialization','q.created_date'); 
  var $answers_order = array('a.status' => 'ASC'); // default order 

  public function __construct()
  {
    parent::__construct();
    $this->load->database();
  }



  


  //admin----------------------------

  private function _get_questions__datatables_query()
  {
  
        $this->db->select('q.*,CONCAT(p.first_name," ", p.last_name) as patient_name,p.profileimage as patient_profileimage,s.specialization as specialization');
        $this->db->from($this->questions);
        $this->db->join($this->specialization, 'q.specilization_id = s.id', 'left'); 
        $this->db->join($this->patient, 'p.id = q.user_id', 'left'); 
                       
   
    $i = 0;
  
    foreach ($this->questions_column_search as $item) // loop column 
    {
      if($_POST['search']['value']) // if datatable send POST for search
      {
        
        if($i===0) // first loop
        {
          $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
          $this->db->like($item, $_POST['search']['value']);
        }
        else
        {
          $this->db->or_like($item, $_POST['search']['value']);
        }

         if(count($this->questions_column_search) - 1 == $i) //last loop
          $this->db->group_end(); //close bracket
      }
      $i++;
    }
    
    if(isset($_POST['order'])) // here order processing
    {
            $this->db->order_by('id', $_POST['order']['0']['dir']);

       //$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } 
    else if(isset($this->questions_order))
    {
      $order = $this->questions_order;
      $this->db->order_by(key($order), $order[key($order)]);
    }
  }

  public function get_questions_datatables()
  {
    $this->_get_questions__datatables_query();
    if($_POST['length'] != -1)
    $this->db->limit($_POST['length'], $_POST['start']);
    $query = $this->db->get();
    return $query->result_array();
  }

  public function questions_count_filtered()
  {
    $this->_get_questions__datatables_query();
    $query = $this->db->get();
    return $query->num_rows();
  }

  public function questions_count_all()
  {
    $this->db->from($this->questions);
    return $this->db->count_all_results();
  }


  private function _get_answers__datatables_query()
  {
  
        $this->db->select('a.*,CONCAT(d.first_name," ", d.last_name) as doctor_name,d.username as doctor_username,d.profileimage as doctor_profileimage,CONCAT(p.first_name," ", p.last_name) as patient_name,p.profileimage as patient_profileimage,s.specialization as specialization,a.answers as answers,a.status as answers_status,q.questions as questions');
        $this->db->from($this->answers);
        $this->db->join($this->questions,'q.id =a.question_id','left');
        $this->db->join($this->specialization, 'q.specilization_id = s.id', 'left'); 
        $this->db->join($this->patient, 'p.id = q.user_id', 'left'); 
        $this->db->join($this->doctor, 'd.id = a.user_id', 'left'); 
                       
   
    $i = 0;
  
    foreach ($this->answers_column_search as $item) // loop column 
    {
      if($_POST['search']['value']) // if datatable send POST for search
      {
        
        if($i===0) // first loop
        {
          $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
          $this->db->like($item, $_POST['search']['value']);
        }
        else
        {
          $this->db->or_like($item, $_POST['search']['value']);
        }

         if(count($this->answers_column_search) - 1 == $i) //last loop
          $this->db->group_end(); //close bracket
      }
      $i++;
    }
    
    if(isset($_POST['order'])) // here order processing
    {
            $this->db->order_by('id', $_POST['order']['0']['dir']);

       //$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } 
    else if(isset($this->answers_order))
    {
      $order = $this->answers_order;
      $this->db->order_by(key($order), $order[key($order)]);
    }
  }


  public function get_answers_datatables()
  {
    $this->_get_answers__datatables_query();
    if($_POST['length'] != -1)
    $this->db->limit($_POST['length'], $_POST['start']);
    $query = $this->db->get();
    return $query->result_array();
  }

  public function answers_count_filtered()
  {
    $this->_get_answers__datatables_query();
    $query = $this->db->get();
    return $query->num_rows();
  }

  public function answers_count_all()
  {
    $this->db->from($this->answers);
    return $this->db->count_all_results();
  }


  

}