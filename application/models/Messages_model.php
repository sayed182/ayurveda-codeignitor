<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Messages_model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}

	public function get_patients($user_id,$keywords='')
	{
		$this->db->select('u.id as userid,u.role,u.first_name,u.last_name,u.username,u.profileimage,(select chatdate from chat where sent_id = a.appointment_to ORDER BY chatdate DESC LIMIT 1) as chatdate');
        $this->db->from('appointments a');
        $this->db->join('users u', 'u.id = a.appointment_from', 'left');
        $this->db->where('a.appointment_to',$user_id);
        if(!empty($keywords))
        {
          $this->db->group_start();
          $this->db->like('u.first_name',$keywords,'after');
          $this->db->or_like('u.last_name',$keywords,'after');
          $this->db->group_end();
        }
        $this->db->group_by('a.appointment_from');
        $this->db->order_by('chatdate','DESC');
        return $this->db->get()->result_array();
        
	}

	public function get_doctors($user_id,$keywords='')
	{
		$this->db->select('u.id as userid,u.role,u.first_name,u.last_name,u.username,u.profileimage,(select chatdate from chat where recieved_id = a.appointment_from ORDER BY chatdate DESC LIMIT 1) as chatdate');
        $this->db->from('appointments a');
        $this->db->join('users u', 'u.id = a.appointment_to', 'left');
        $this->db->where('a.appointment_from',$user_id);
         if(!empty($keywords))
        {
          $this->db->group_start();
          $this->db->like('u.first_name',$keywords,'after');
          $this->db->or_like('u.last_name',$keywords,'after');
          $this->db->group_end();
        }
        $this->db->group_by('a.appointment_to');
         $this->db->order_by('chatdate','DESC');
        return $this->db->get()->result_array();
        
	}

	public function get_latest_chat($selected_user,$user_id)
{


    $per_page = 5;
    $total =  $this->get_total_chat_count($selected_user,$user_id);
    if($total>5){
        $total = $total-5;
    }else{
        $total = 0;
    }

    $this->update_counts($selected_user,$user_id);

    $query = $this->db->query("SELECT DISTINCT CONCAT(sender.first_name,' ',sender.last_name) as senderName, sender.profileimage as senderImage, sender.id as sender_id,msg.msg, msg.chatdate,msg.id,msg.type,msg.file_name,msg.file_path,msg.time_zone,msg.id
        from chat msg
        LEFT  join users sender on msg.sent_id = sender.id
        left join chat_deleted_details cd on cd.chat_id  = msg.id
        where cd.can_view = $user_id AND ((msg.recieved_id = $selected_user AND msg.sent_id = $user_id) or  (msg.recieved_id = $user_id AND msg.sent_id =  $selected_user))   ORDER BY msg.id ASC LIMIT $total,$per_page ");
    $result = $query->result_array();
    return $result;

}
public function get_old_chat($selected_user,$user_id,$total)
{
    $per_page = 5;

    $query = $this->db->query("SELECT DISTINCT CONCAT(sender.first_name,' ',sender.last_name) as senderName, sender.profileimage as senderImage, sender.id as sender_id,msg.msg, msg.chatdate,msg.id,msg.type,msg.file_name,msg.file_path,msg.time_zone,msg.id
        from chat msg
         LEFT  join users sender on msg.sent_id = sender.id
        left join chat_deleted_details cd on cd.chat_id  = msg.id
        where cd.can_view = $user_id AND ((msg.recieved_id = $selected_user AND msg.sent_id = $user_id) or  (msg.recieved_id = $user_id AND msg.sent_id =  $selected_user))   ORDER BY msg.id ASC LIMIT $total,$per_page  ");
    $result = $query->result_array();
    return $result;

}
Public function get_last_message_id($selected_user,$user_id)
{



    $sql = "SELECT msg.id  from chat msg
    left join chat_deleted_details cd on cd.chat_id  = msg.id
    where  cd.can_view = $user_id AND ((msg.recieved_id = $selected_user AND msg.sent_id = $user_id) or  (msg.recieved_id = $user_id AND msg.sent_id =  $selected_user))   ORDER BY msg.id DESC ";

    $data =   $this->db->query($sql)->row_array();


    if(!empty($data['id'])){
        return $data['id'];
    }else{
        return 0;
    }


}
Public function get_total_chat_count($selected_user,$user_id)
{

    $sql = "SELECT msg.id  from chat msg
    left join chat_deleted_details cd on cd.chat_id  = msg.id
    where  cd.can_view = $user_id AND ((msg.recieved_id = $selected_user AND msg.sent_id = $user_id) or  (msg.recieved_id = $user_id AND msg.sent_id =  $selected_user))   ORDER BY msg.id DESC ";

    return  $this->db->query($sql)->num_rows();


}

Public function update_counts($selected_user,$user_id)
{

    $query = $this->db->query("SELECT msg.id
        from chat msg
        LEFT  join users sender on msg.sent_id = sender.id
        where msg.delete_sts = 0 AND  msg.read_status = 0 AND (msg.recieved_id = $user_id AND msg.sent_id =  $selected_user) ");
    $result = $query->result_array();

    if(!empty($result)){
        foreach ($result as $d) {
            $this->db->update('chat',array('read_status'=>1),array('id'=>$d['id']));
        }

    }else{
        return true;
    }

}


}