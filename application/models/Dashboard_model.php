<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Dashboard_model extends CI_Model
{
	var $appoinments = 'appointments a'; 
  var $doctor ='users d';
  var $doctor_details ='users_details dd';
  var $patient ='users p';
  var $patient_details ='users_details pd';
  var $specialization ='specialization s';
	public function __construct()
	{
		parent::__construct();
	}

	public function get_doctors()
	{
		$this->db->select('u.*,s.specialization,(select COUNT(rating) from rating_reviews where doctor_id=u.id) as rating_count,(select ROUND(AVG(rating)) from rating_reviews where doctor_id=u.id) as rating_value');
        $this->db->from('users u'); 
        $this->db->join('users_details ud','ud.user_id = u.id','left'); 
        $this->db->join('specialization s','ud.specialization = s.id','left');
        $this->db->where('u.status','1');
        $this->db->where('u.role','1');
        $this->db->order_by('u.id','DESC');
        $this->db->limit('10');
        return $this->db->get()->result_array();
	}

	public function get_patients()
	{
		$this->db->select('u.*,ud.dob,ud.blood_group');
        $this->db->from('users u'); 
        $this->db->join('users_details ud','ud.user_id = u.id','left'); 
        $this->db->where('u.status','1');
        $this->db->where('u.role','2');
        $this->db->order_by('u.id','DESC');
        $this->db->limit('10');
        return $this->db->get()->result_array();
	}

	public function users_count($role)
	{
		$this->db->where('role',$role);
		$this->db->where('status',1);
		return $this->db->get('users')->num_rows();
	}

	Public function appointments_count()
	{
		$this->db->where('status',1);
		return $this->db->get('appointments')->num_rows();
	}

	Public function get_appointments()
	{
		$this->db->select('a.*, CONCAT(d.first_name," ", d.last_name) as doctor_name,d.username as doctor_username,d.profileimage as doctor_profileimage, CONCAT(p.first_name," ", p.last_name) as patient_name,p.profileimage as patient_profileimage,s.specialization as doctor_specialization');
        $this->db->from($this->appoinments);
        $this->db->join($this->doctor, 'd.id = a.appointment_to', 'left'); 
        $this->db->join($this->doctor_details,'dd.user_id = d.id','left'); 
        $this->db->join($this->patient, 'p.id = a.appointment_from', 'left'); 
        $this->db->join($this->patient_details,'pd.user_id = p.id','left'); 
        $this->db->join($this->specialization,'dd.specialization = s.id','left');
        $this->db->order_by('a.from_date_time','ASC');
        $this->db->group_by('a.id');
        $this->db->limit('10');
        return $this->db->get()->result_array();
	}

    Public function get_revenue()
    {
           $this->db->select('p.*,(select COUNT(id) from appointments where payment_id=p.id) as appoinment_count');
            $this->db->from('payments p');
            $this->db->where('p.payment_status',1);
            $this->db->where('p.request_status !=',7);
            $result=$this->db->get()->result_array();

            $revenue=0;
               if(!empty($result))
            {
              foreach ($result as $rows) {

                    $tax_amount=$rows['tax_amount']+$rows['transcation_charge'];
        
                    $amount=($rows['total_amount']) - ($tax_amount);

                    $commission = !empty(settings("commission"))?settings("commission"):"0";
                    $commission_charge = ($amount * ($commission/100));
                    $balance_temp= $commission_charge;

                    
                    $user_currency_code='USD';
                    

                    $currency_option = (!empty($user_currency_code))?$user_currency_code:'USD';
                    $rate_symbol = currency_code_sign($currency_option);

                    $org_amount=get_doccure_currency($balance_temp,$rows['currency_code'],$user_currency_code);
                    
                    $revenue +=$org_amount;
                
              }
            }

            return $revenue;
    }

  

  
}
?>
