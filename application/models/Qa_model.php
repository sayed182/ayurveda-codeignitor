<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Qa_model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}

 

  public function add_questions($inputdata)
  {
       $this->db->insert('questions',$inputdata);
       return ($this->db->affected_rows()!= 1)? false:true;
  }

   public function update_answers($inputdata,$answer_id)
  {
        $this->db->where('id',$answer_id);
       $this->db->update('answers',$inputdata);
       return ($this->db->affected_rows()!= 1)? false:true;
  }

   public function add_answers($inputdata)
  {
       $this->db->insert('answers',$inputdata);
       return ($this->db->affected_rows()!= 1)? false:true;
  } 

  public function get_questions($page,$limit,$type,$specialization)
  {
        $this->db->select('q.*,CONCAT(u.first_name," ", u.last_name) as name,u.username,u.profileimage,sp.specialization,(select answers from answers where question_id=q.id order by answers.id desc limit 1) as answer,(select count(answers) from answers where question_id=q.id) as answer_count,(select user_id from answers where question_id=q.id order by answers.id desc limit 1) as answer_user_id,(select created_date from answers where question_id=q.id order by answers.id desc limit 1) as answer_date,(select CONCAT(first_name," ", last_name) as doc_name from users where id=answer_user_id) as doctor_name,(select profileimage from users where id=answer_user_id) as doctor_image');
        $this->db->from('questions q'); 
        $this->db->join('users u','q.user_id = u.id','left'); 
        $this->db->join('specialization sp','sp.id = q.specilization_id','left');
        $this->db->where('q.status','1'); 

        if($specialization !=''){
        $this->db->where('q.specilization_id',$specialization);
        }

          if(!empty($_POST['keywords'])) {  
          $this->db->group_start();
          $this->db->like('q.questions',$_POST['keywords']);
          $this->db->group_end();
        }

         if(!empty($_POST['specialization'])) {   
         $this->db->where('q.specilization_id',$_POST['specialization']);
        }


        
         $this->db->order_by('q.id','DESC');
        if($type == 1){
         return $this->db->count_all_results(); 
        }else{

          $page = !empty($page)?$page:'';
          if($page>=1){
          $page = $page - 1 ;
          }
          $page =  intval(($page * $limit));  
          $this->db->limit($limit,$page);
          return $this->db->get()->result_array();
        }
  }

    public function questions_detail($id)
  {
        $this->db->select('q.*,CONCAT(u.first_name," ", u.last_name) as name,u.username,u.profileimage,sp.specialization,(select answers from answers where question_id=q.id order by answers.id desc limit 1) as answer,(select count(answers) from answers where question_id=q.id) as answer_count,(select user_id from answers where question_id=q.id order by answers.id desc limit 1) as answer_user_id,(select created_date from answers where question_id=q.id order by answers.id desc limit 1) as answer_date,(select CONCAT(first_name," ", last_name) as doc_name from users where id=answer_user_id) as doctor_name,(select profileimage from users where id=answer_user_id) as doctor_image');
        $this->db->from('questions q'); 
        $this->db->join('users u','q.user_id = u.id','left'); 
        $this->db->join('specialization sp','sp.id = q.specilization_id','left');
        $this->db->where('q.status','1'); 
        $this->db->where('q.id',$id); 
        return $this->db->get()->row_array();
  }
  

   public function get_answers($question_id)
  {
        $this->db->select('a.*,CONCAT(u.first_name," ", u.last_name) as name,u.username,u.profileimage');
        $this->db->from('answers a'); 
        $this->db->join('users u','a.user_id = u.id','left');
        $this->db->where('a.status','1'); 
        $this->db->where('a.question_id',$question_id); 
        $this->db->order_by('a.id','DESC');
        return $this->db->get()->result_array();
        
  }

  public function get_answers_count($question_id)
  {
        $this->db->select('a.*,CONCAT(u.first_name," ", u.last_name) as name,u.username,u.profileimage');
        $this->db->from('answers a'); 
        $this->db->join('users u','a.user_id = u.id','left');
        $this->db->where('a.status','1'); 
        $this->db->where('a.question_id',$question_id); 
        $this->db->order_by('a.id','DESC');
        return $this->db->get()->num_rows();
        
  }

  public function get_my_questions($user_id)
  {
        $this->db->select('*');
        $this->db->from('questions');
        $this->db->where('user_id',$user_id);
        $this->db->order_by('created_date', 'DESC');
        return $result = $this->db->get()->result_array();
  }

}