<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice_model extends CI_Model {

    var $table = 'payments p';
    var $doctor ='users d';
    var $doctor_details ='users_details dd';
    var $patient ='users pi';
    var $patient_details ='users_details pd';
    var $users ='users u';
  var $column_search = array('p.invoice_no','u.first_name','u.last_name','u.profileimage','p.total_amount','p.payment_date'); //set column field database for datatable searchable 
  var $order = array('p.id' => 'ASC'); // default order

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	private function _get_datatables_query($user_id)
  {
  
    $this->db->select('p.*, CONCAT(d.first_name," ", d.last_name) as doctor_name,d.username as doctor_username,d.profileimage as doctor_profileimage,d.id as doctor_id,CONCAT(pi.first_name," ", pi.last_name) as patient_name,pi.profileimage as patient_profileimage,pi.id as patient_id');
    $this->db->from($this->table);
    $this->db->join($this->doctor, 'd.id = p.doctor_id', 'left'); 
    $this->db->join($this->doctor_details,'dd.user_id = d.id','left'); 
    $this->db->join($this->patient, 'pi.id = p.user_id', 'left'); 
    $this->db->join($this->patient_details,'pd.user_id = pi.id','left');
    if($this->session->userdata('role')=='1')
    {
      $this->db->where('p.doctor_id',$user_id);
    } 
    if($this->session->userdata('role')=='2')
    {
      $this->db->where('p.user_id',$user_id);
    }
    $this->db->where('p.payment_status',1);

        $i = 0;
  
    foreach ($this->column_search as $item) // loop column 
    {
      if($_POST['search']['value']) // if datatable send POST for search
      {
        
        if($i===0) // first loop
        {
          $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
          $this->db->like($item, $_POST['search']['value']);
        }
        else
        {
          $this->db->or_like($item, $_POST['search']['value']);
        }

         if(count($this->column_search) - 1 == $i) //last loop
          $this->db->group_end(); //close bracket
      }
      $i++;
    }
    
    if(isset($_POST['order'])) // here order processing
    {
            $this->db->order_by('id', $_POST['order']['0']['dir']);

       //$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } 
    else if(isset($this->order))
    {
      $order = $this->order;
      $this->db->order_by(key($order), $order[key($order)]);
    }
  }

  public function get_datatables($user_id)
  {
    $this->_get_datatables_query($user_id);
    if($_POST['length'] != -1)
    $this->db->limit($_POST['length'], $_POST['start']);
    $query = $this->db->get();
    return $query->result_array();
  }

  public function count_filtered($user_id)
  {
    $this->_get_datatables_query($user_id);
    $query = $this->db->get();
    return $query->num_rows();
  }

  public function count_all($user_id)
  {
    $this->db->where('p.payment_status',1);
    if($this->session->userdata('role')=='1')
    {
      $this->db->where('p.doctor_id',$user_id);
    } 
    if($this->session->userdata('role')=='2')
    {
      $this->db->where('p.user_id',$user_id);
    }
    $this->db->from($this->table);
    return $this->db->count_all_results();
  }


	public function get_invoice_details($invoice_id)
  {
    $this->db->select('p.*, CONCAT(d.first_name," ", d.last_name) as doctor_name,d.username as doctor_username,d.profileimage as doctor_profileimage, CONCAT(pi.first_name," ", pi.last_name) as patient_name,pi.profileimage as patient_profileimage,dc.country as doctorcountryname,ds.statename as doctorstatename,dci.city as doctorcityname,pc.country as patientcountryname,ps.statename as patientstatename,pci.city as patientcityname,dd.address1 as doctoraddress1,dd.address2 as doctoraddress2,pd.address1 as patientaddress1,pd.address2 as patientaddress2');
        $this->db->from($this->table);
        $this->db->join($this->doctor, 'd.id = p.doctor_id', 'left'); 
        $this->db->join($this->doctor_details,'dd.user_id = d.id','left'); 
        $this->db->join($this->patient, 'pi.id = p.user_id', 'left'); 
        $this->db->join($this->patient_details,'pd.user_id = pi.id','left'); 
        $this->db->join('country dc','dd.country = dc.countryid','left');
        $this->db->join('state ds','dd.state = ds.id','left');
        $this->db->join('city dci','dd.city = dci.id','left');
        $this->db->join('country pc','pd.country = pc.countryid','left');
        $this->db->join('state ps','pd.state = ps.id','left');
        $this->db->join('city pci','pd.city = pci.id','left');
        $this->db->where('p.id',$invoice_id);;
        return $this->db->get()->row_array();
  }



	


}
