<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class My_patients_model extends CI_Model
{

  var $appoinments = 'appointments a';
  var $prescription = 'prescription p';
  var $medical_records ='medical_records m';
  var $billing ='billing b';

  var $users ='users u';
  var $users_details ='users_details ud';
  var $specialization ='specialization s';

  var $appoinments_column_search = array('u.first_name','u.last_name','u.profileimage','a.appointment_date','a.created_date'); 
  var $appoinments_order = array('a.id' => 'ASC'); // default order 

  var $prescription_column_search = array('u.first_name','u.last_name','u.profileimage','p.created_at'); 
  var $prescription_order = array('p.id' => 'ASC'); // default order 

  var $medical_records_column_search = array('u.first_name','u.last_name','u.profileimage','m.date'); 
  var $medical_records_order = array('m.id' => 'ASC'); // default order 

  var $billing_column_search = array('u.first_name','u.last_name','u.profileimage','b.date'); 
  var $billing_records_order = array('b.id' => 'ASC'); // default order 
  


  public function __construct()
  {
    parent::__construct();
  }

  public function patient_list($page,$limit,$type,$user_id)
    {
        $this->db->select('u.first_name,u.last_name,u.email,u.username,u.mobileno,u.profileimage,ud.*,c.country as countryname,s.statename,ci.city as cityname');
        $this->db->from('appointments a');
        $this->db->join('users u','a.appointment_from = u.id','left');
        $this->db->join('users_details ud','ud.user_id = u.id','left');
        $this->db->join('country c','ud.country = c.countryid','left');
        $this->db->join('state s','ud.state = s.id','left');
        $this->db->join('city ci','ud.city = ci.id','left');
        $this->db->where('u.role','2');
        $this->db->where('a.appointment_to',$user_id);
        $this->db->group_by('a.appointment_from');

          if($type == 1){
         return $this->db->count_all_results(); 
        }else{

          $page = !empty($page)?$page:'';
          if($page>=1){
          $page = $page - 1 ;
          }
          $page =  ($page * $limit);  
          $this->db->limit($limit,$page);
          return $this->db->get()->result_array();
        }

  }

  public function get_patient_details($userid)
  {
       $this->db->select('u.id as userid,u.first_name,u.last_name,u.email,u.username,u.mobileno,u.profileimage,ud.*,c.country as countryname,s.statename,ci.city as cityname');
        $this->db->from('users u');
        $this->db->join('users_details ud','ud.user_id = u.id','left');
        $this->db->join('country c','ud.country = c.countryid','left');
        $this->db->join('state s','ud.state = s.id','left');
        $this->db->join('city ci','ud.city = ci.id','left');
        $this->db->where('u.id',$userid);
        return $result = $this->db->get()->row_array();
        
  }

  private function _get_appoinments__datatables_query()
  {
  
        $this->db->select('a.*, u.first_name,u.last_name,u.username,u.profileimage,s.specialization');
        $this->db->from($this->appoinments);
        $this->db->join($this->users, 'u.id = a.appointment_to', 'left'); 
        $this->db->join($this->users_details,'ud.user_id = u.id','left'); 
        $this->db->join($this->specialization,'ud.specialization = s.id','left');
        $this->db->where('a.appointment_from',$_POST['patient_id']);
        if(is_doctor()){
          $this->db->where('a.appointment_to',$this->session->userdata('user_id'));
        }
   
    $i = 0;
  
    foreach ($this->appoinments_column_search as $item) // loop column 
    {
      if($_POST['search']['value']) // if datatable send POST for search
      {
        
        if($i===0) // first loop
        {
          $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
          $this->db->like($item, $_POST['search']['value']);
        }
        else
        {
          $this->db->or_like($item, $_POST['search']['value']);
        }

         if(count($this->appoinments_column_search) - 1 == $i) //last loop
          $this->db->group_end(); //close bracket
      }
      $i++;
    }
    
    if(isset($_POST['order'])) // here order processing
    {
            $this->db->order_by('id', $_POST['order']['0']['dir']);

       //$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } 
    else if(isset($this->appoinments_order))
    {
      $order = $this->appoinments_order;
      $this->db->order_by(key($order), $order[key($order)]);
    }
  }

  public function get_appoinments_datatables()
  {
    $this->_get_appoinments__datatables_query();
    if($_POST['length'] != -1)
    $this->db->limit($_POST['length'], $_POST['start']);
    $query = $this->db->get();
    return $query->result_array();
  }

  public function appoinments_count_filtered()
  {
    $this->_get_appoinments__datatables_query();
    $query = $this->db->get();
    return $query->num_rows();
  }

  public function appoinments_count_all()
  {
    $this->db->from($this->appoinments);
    if(is_doctor()){
          $this->db->where('a.appointment_to',$this->session->userdata('user_id'));
        }
    return $this->db->count_all_results();
  }


  /* prescription*/

  private function _get_prescription__datatables_query()
  {
  
        $this->db->select('p.*, u.first_name,u.last_name,u.username,u.profileimage,s.specialization');
        $this->db->from($this->prescription);
        $this->db->join($this->users, 'u.id = p.doctor_id', 'left'); 
        $this->db->join($this->users_details,'ud.user_id = u.id','left'); 
        $this->db->join($this->specialization,'ud.specialization = s.id','left');
        $this->db->where('p.patient_id',$_POST['patient_id']);
        $this->db->where('p.status',1);
        if(is_doctor()){
          $this->db->where('p.doctor_id',$this->session->userdata('user_id'));
        }
   
    $i = 0;
  
    foreach ($this->prescription_column_search as $item) // loop column 
    {
      if($_POST['search']['value']) // if datatable send POST for search
      {
        
        if($i===0) // first loop
        {
          $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
          $this->db->like($item, $_POST['search']['value']);
        }
        else
        {
          $this->db->or_like($item, $_POST['search']['value']);
        }

         if(count($this->prescription_column_search) - 1 == $i) //last loop
          $this->db->group_end(); //close bracket
      }
      $i++;
    }
    
    if(isset($_POST['order'])) // here order processing
    {
            $this->db->order_by('id', $_POST['order']['0']['dir']);

       //$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } 
    else if(isset($this->prescription_order))
    {
      $order = $this->prescription_order;
      $this->db->order_by(key($order), $order[key($order)]);
    }
  }

  public function get_prescription_datatables()
  {
    $this->_get_prescription__datatables_query();
    if($_POST['length'] != -1)
    $this->db->limit($_POST['length'], $_POST['start']);
    $query = $this->db->get();
    return $query->result_array();
  }

  public function prescription_count_filtered()
  {
    $this->_get_prescription__datatables_query();
    $query = $this->db->get();
    return $query->num_rows();
  }

  public function prescription_count_all()
  {
    $this->db->from($this->prescription);
    $this->db->where('p.status',1);
    if(is_doctor()){
          $this->db->where('p.doctor_id',$this->session->userdata('user_id'));
        }
    return $this->db->count_all_results();
  }

  public function get_prescription($prescription_id){
      $this->db->select('pd.*,p.signature_id,s.img,s.rowno');
      $this->db->from('prescription_item_details pd');
      $this->db->join('prescription p','pd.prescription_id=p.id','left');
      $this->db->join('signature s','p.signature_id=s.id','left');
      $this->db->where('p.id',$prescription_id);
      return $this->db->get()->result_array();
  }

  public function get_prescription_details($prescription_id){
      $this->db->select('CONCAT(u.first_name," ", u.last_name) as doctor_name,CONCAT(u1.first_name," ", u1.last_name) as patient_name,pd.*,p.signature_id,s.img,s.rowno,DATE_FORMAT(pd.created_at, "%d-%m-%Y") as prescription_date');
      $this->db->from('prescription_item_details pd');
      $this->db->join('prescription p','pd.prescription_id=p.id','left');
      $this->db->join('signature s','p.signature_id=s.id','left');
      $this->db->join('users u', 'u.id = p.doctor_id', 'left');
      $this->db->join('users u1', 'u1.id = p.patient_id', 'left');
      $this->db->where('p.id',$prescription_id);
      return $this->db->get()->result_array();
  }


   /* medical_record*/

  private function _get_medical_record__datatables_query()
  {
  
        $this->db->select('m.*, u.first_name,u.last_name,u.username,u.profileimage,s.specialization');
        $this->db->from($this->medical_records);
        $this->db->join($this->users, 'u.id = m.doctor_id', 'left'); 
        $this->db->join($this->users_details,'ud.user_id = u.id','left'); 
        $this->db->join($this->specialization,'ud.specialization = s.id','left');
        $this->db->where('m.patient_id',$_POST['patient_id']);
        $this->db->where('m.status',1);
        if(is_doctor()){
          $this->db->where('m.doctor_id',$this->session->userdata('user_id'));
        }
   
    $i = 0;
  
    foreach ($this->medical_records_column_search as $item) // loop column 
    {
      if($_POST['search']['value']) // if datatable send POST for search
      {
        
        if($i===0) // first loop
        {
          $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
          $this->db->like($item, $_POST['search']['value']);
        }
        else
        {
          $this->db->or_like($item, $_POST['search']['value']);
        }

         if(count($this->medical_records_column_search) - 1 == $i) //last loop
          $this->db->group_end(); //close bracket
      }
      $i++;
    }
    
    if(isset($_POST['order'])) // here order processing
    {
            $this->db->order_by('id', $_POST['order']['0']['dir']);

       //$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } 
    else if(isset($this->medical_records_order))
    {
      $order = $this->medical_records_order;
      $this->db->order_by(key($order), $order[key($order)]);
    }
  }

  public function get_medical_record_datatables()
  {
    $this->_get_medical_record__datatables_query();
    if($_POST['length'] != -1)
    $this->db->limit($_POST['length'], $_POST['start']);
    $query = $this->db->get();
    return $query->result_array();
  }

  public function medical_record_count_filtered()
  {
    $this->_get_medical_record__datatables_query();
    $query = $this->db->get();
    return $query->num_rows();
  }

  public function medical_record_count_all()
  {
    $this->db->from($this->medical_records);
    $this->db->where('m.status',1);
    if(is_doctor()){
          $this->db->where('m.doctor_id',$this->session->userdata('user_id'));
        }
    return $this->db->count_all_results();
  }


/* billing*/

  private function _get_billing__datatables_query()
  {
  
        $this->db->select('b.*, u.first_name,u.last_name,u.username,u.profileimage,s.specialization');
        $this->db->from($this->billing);
        $this->db->join($this->users, 'u.id = b.doctor_id', 'left'); 
        $this->db->join($this->users_details,'ud.user_id = u.id','left'); 
        $this->db->join($this->specialization,'ud.specialization = s.id','left');
        $this->db->where('b.patient_id',$_POST['patient_id']);
        $this->db->where('b.status',1);
        if(is_doctor()){
          $this->db->where('b.doctor_id',$this->session->userdata('user_id'));
        }
   
    $i = 0;
  
    foreach ($this->billing_column_search as $item) // loop column 
    {
      if($_POST['search']['value']) // if datatable send POST for search
      {
        
        if($i===0) // first loop
        {
          $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
          $this->db->like($item, $_POST['search']['value']);
        }
        else
        {
          $this->db->or_like($item, $_POST['search']['value']);
        }

         if(count($this->billing_column_search) - 1 == $i) //last loop
          $this->db->group_end(); //close bracket
      }
      $i++;
    }
    
    if(isset($_POST['order'])) // here order processing
    {
            $this->db->order_by('id', $_POST['order']['0']['dir']);

       //$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } 
    else if(isset($this->billing_order))
    {
      $order = $this->billing_order;
      $this->db->order_by(key($order), $order[key($order)]);
    }
  }

  public function get_billing_datatables()
  {
    $this->_get_billing__datatables_query();
    if($_POST['length'] != -1)
    $this->db->limit($_POST['length'], $_POST['start']);
    $query = $this->db->get();
    return $query->result_array();
  }

  public function billing_count_filtered()
  {
    $this->_get_billing__datatables_query();
    $query = $this->db->get();
    return $query->num_rows();
  }

  public function billing_count_all()
  {
    $this->db->from($this->billing);
    $this->db->where('b.status',1);
    if(is_doctor()){
          $this->db->where('b.doctor_id',$this->session->userdata('user_id'));
        }
    return $this->db->count_all_results();
  }

  public function get_billing($billing_id){
      $this->db->select('bd.*,b.signature_id,s.img,s.rowno');
      $this->db->from('billing_item_details bd');
      $this->db->join('billing b','bd.billing_id=b.id','left');
      $this->db->join('signature s','b.signature_id=s.id','left');
      $this->db->where('b.id',$billing_id);
      return $this->db->get()->result_array();
  }

  public function get_billing_details($billing_id){
      $this->db->select('CONCAT(u.first_name," ", u.last_name) as doctor_name,CONCAT(u1.first_name," ", u1.last_name) as patient_name,bd.*,b.signature_id,s.img,s.rowno,DATE_FORMAT(bd.created_at, "%d-%m-%Y") as billing_date');
      $this->db->from('billing_item_details bd');
      $this->db->join('billing b','bd.billing_id=b.id','left');
      $this->db->join('signature s','b.signature_id=s.id','left');
      $this->db->join('users u', 'u.id = b.doctor_id', 'left');
      $this->db->join('users u1', 'u1.id = b.patient_id', 'left');
      $this->db->where('b.id',$billing_id);
      return $this->db->get()->result_array();
  }

  




}