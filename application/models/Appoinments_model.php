<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Appoinments_model extends CI_Model {

  var $table = 'appointments a';
  var $users ='users u';
  var $column_search = array('u.first_name','u.last_name','u.profileimage','a.appointment_date','a.from_date_time'); //set column field database for datatable searchable 
  var $order = array('a.id' => 'ASC'); // default order

// admin
  var $appoinments = 'appointments a'; 
  var $doctor ='users d';
  var $doctor_details ='users_details dd';
  var $patient ='users p';
  var $patient_details ='users_details pd';
  var $specialization ='specialization s';

  var $appoinments_column_search = array('d.first_name','d.last_name','d.profileimage','p.first_name','p.last_name','p.profileimage','a.appointment_date','a.created_date'); 
  var $appoinments_order = array('a.id' => 'ASC'); // default order 

  public function __construct()
  {
    parent::__construct();
    $this->load->database();
  }

  private function _get_datatables_query($user_id)
  {
  
        $current_date = date('Y-m-d');
        $from_date_time = date('Y-m-d H:i:s');
        $this->db->select('a.*,u.first_name,u.last_name,u.username,u.profileimage,p.per_hour_charge');
        $this->db->from($this->table);
        $this->db->join($this->users, 'u.id = a.appointment_from', 'left'); 
        $this->db->join('payments p','p.id = a.payment_id','left');   
        $this->db->where('a.appointment_to',$user_id);

       if($_POST['type'] == 1){
        $this->db->where('a.appointment_date',$current_date);
       }

       if($_POST['type'] == 2){
          $this->db->where('a.from_date_time > ',$from_date_time);
       }

   
    $i = 0;
  
    foreach ($this->column_search as $item) // loop column 
    {
      if($_POST['search']['value']) // if datatable send POST for search
      {
        
        if($i===0) // first loop
        {
          $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
          $this->db->like($item, $_POST['search']['value']);
        }
        else
        {
          $this->db->or_like($item, $_POST['search']['value']);
        }

         if(count($this->column_search) - 1 == $i) //last loop
          $this->db->group_end(); //close bracket
      }
      $i++;
    }
    
    if(isset($_POST['order'])) // here order processing
    {
            $this->db->order_by('id', $_POST['order']['0']['dir']);

       //$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } 
    else if(isset($this->order))
    {
      $order = $this->order;
      $this->db->order_by(key($order), $order[key($order)]);
    }
  }

  public function get_datatables($user_id)
  {
    $this->_get_datatables_query($user_id);
    if($_POST['length'] != -1)
    $this->db->limit($_POST['length'], $_POST['start']);
    $query = $this->db->get();
    return $query->result_array();
  }

  public function count_filtered($user_id)
  {
    $this->_get_datatables_query($user_id);
    $query = $this->db->get();
    return $query->num_rows();
  }

  public function count_all($user_id)
  {
    $this->db->where('a.appointment_to',$user_id);
    $this->db->from($this->table);
    return $this->db->count_all_results();
  }


  public function doctor_appoinments_list($page,$limit,$type,$user_id)
  {
         $this->db->select('a.*,u.id as userid,u.first_name,u.last_name,u.username,u.profileimage,u.email,u.mobileno,c.country as countryname,s.statename,ci.city as cityname,p.per_hour_charge');
        $this->db->from('appointments a');
        $this->db->join('users u', 'u.id = a.appointment_from', 'left');
        $this->db->join('users_details ud', 'u.id = ud.user_id', 'left');
        $this->db->join('payments p','p.id = a.payment_id','left');   
        $this->db->join('country c','ud.country = c.countryid','left');
        $this->db->join('state s','ud.state = s.id','left');
        $this->db->join('city ci','ud.city = ci.id','left'); 
        $this->db->where('a.appointment_to',$user_id);
        $this->db->where('a.appointment_status',0);
        $this->db->order_by('a.from_date_time','ASC');
       $this->db->group_by('a.id');
        if($type == 1){
         return $this->db->count_all_results(); 
        }else{

          $page = !empty($page)?$page:'';
          if($page>=1){
          $page = $page - 1 ;
          }
          $page =  ($page * $limit);  
          $this->db->limit($limit,$page);
          return $this->db->get()->result_array();
        }
  }

  public function patient_appoinments_list($page,$limit,$type,$user_id)
  {
         $this->db->select('a.*,u.id as userid,u.first_name,u.last_name,u.username,u.profileimage,u.email,u.mobileno,c.country as countryname,s.statename,ci.city as cityname,p.per_hour_charge');
        $this->db->from('appointments a');
        $this->db->join('users u', 'u.id = a.appointment_to', 'left');
        $this->db->join('users_details ud', 'u.id = ud.user_id', 'left');
        $this->db->join('payments p','p.id = a.payment_id','left');   
        $this->db->join('country c','ud.country = c.countryid','left');
        $this->db->join('state s','ud.state = s.id','left');
        $this->db->join('city ci','ud.city = ci.id','left'); 
        $this->db->where('a.appointment_from',$user_id);
        $this->db->where('a.appointment_status',0);
        $this->db->order_by('a.from_date_time','ASC');
       $this->db->group_by('a.id');
        if($type == 1){
         return $this->db->count_all_results(); 
        }else{

          $page = !empty($page)?$page:'';
          if($page>=1){
          $page = $page - 1 ;
          }
          $page =  ($page * $limit);  
          $this->db->limit($limit,$page);
          return $this->db->get()->result_array();
        }
  }

  Public function get_total_patient($user_id){
    $where =array('appointment_to' => $user_id);
    return $this->db->group_by('appointment_from')->get_where('appointments',$where)->num_rows();
  }  
  Public function get_today_patient($user_id){
    $where =array('appointment_to' => $user_id,'appointment_date'=>date('Y-m-d'));
    return $this->db->group_by('appointment_from')->get_where('appointments',$where)->num_rows();
  }
    Public function get_recent_booking($user_id){
    $where =array('doctor_id' => $user_id,'payment_date >='=>date('Y-m-d'));
    return $this->db->get_where('payments',$where)->num_rows();
  }


  //admin----------------------------

  private function _get_appoinments__datatables_query($type)
  {
  
        $this->db->select('a.*, CONCAT(d.first_name," ", d.last_name) as doctor_name,d.username as doctor_username,d.profileimage as doctor_profileimage, CONCAT(p.first_name," ", p.last_name) as patient_name,p.profileimage as patient_profileimage,s.specialization as doctor_specialization');
        $this->db->from($this->appoinments);
        $this->db->join($this->doctor, 'd.id = a.appointment_to', 'left'); 
        $this->db->join($this->doctor_details,'dd.user_id = d.id','left'); 
        $this->db->join($this->patient, 'p.id = a.appointment_from', 'left'); 
        $this->db->join($this->patient_details,'pd.user_id = p.id','left'); 
        $this->db->join($this->specialization,'dd.specialization = s.id','left');

        if($type == 1){
          //Get completed appointmets
         $this->db->where('a.appointment_status',1);
         $this->db->where('a.call_status',1);


        }elseif($type == 2){
          //Upcoming appointments
          $from_date_time = date('Y-m-d H:i:s');
         $this->db->where('a.from_date_time > ',$from_date_time); 
         $this->db->where('a.appointment_status',0);
         $this->db->where('a.call_status',0);


        }elseif($type == 3){
          //missed apppointments
         $this->db->where('a.appointment_status',1);
         $this->db->where('a.call_status',0);

        }

        $this->db->order_by('a.from_date_time','ASC');
        $this->db->group_by('a.id');
        
   
    $i = 0;
  
    foreach ($this->appoinments_column_search as $item) // loop column 
    {
      if($_POST['search']['value']) // if datatable send POST for search
      {
        
        if($i===0) // first loop
        {
          $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
          $this->db->like($item, $_POST['search']['value']);
        }
        else
        {
          $this->db->or_like($item, $_POST['search']['value']);
        }

         if(count($this->appoinments_column_search) - 1 == $i) //last loop
          $this->db->group_end(); //close bracket
      }
      $i++;
    }
    
    if(isset($_POST['order'])) // here order processing
    {
            $this->db->order_by('id', $_POST['order']['0']['dir']);

       //$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } 
    else if(isset($this->appoinments_order))
    {
      $order = $this->appoinments_order;
      $this->db->order_by(key($order), $order[key($order)]);
    }
  }

  public function get_appoinments_datatables()
  {
    $this->_get_appoinments__datatables_query(1);
    if($_POST['length'] != -1)
    $this->db->limit($_POST['length'], $_POST['start']);

    $query = $this->db->get();

    return $query->result_array();
  }

  public function get_upappoinments_datatables()
  {
    $this->_get_appoinments__datatables_query(2);
    if($_POST['length'] != -1)
    $this->db->limit($_POST['length'], $_POST['start']);
    $query = $this->db->get();
    return $query->result_array();
  }

  public function get_missedappoinments_datatables()
  {
    $this->_get_appoinments__datatables_query(3);
    if($_POST['length'] != -1)
    $this->db->limit($_POST['length'], $_POST['start']);
    $query = $this->db->get();
    return $query->result_array();
  }

  public function appoinments_count_filtered($type)
  {
    $this->_get_appoinments__datatables_query($type);
    $query = $this->db->get();
    return $query->num_rows();
  }

  public function appoinments_count_all($type)
  {
    $this->db->from($this->appoinments);
     if($type == 1){
          //Get completed appointmets
         $this->db->where('a.appointment_status',1);
         $this->db->where('a.call_status',1);


        }elseif($type == 2){
          //Upcoming appointments
         $this->db->where('a.appointment_status',0);
         $this->db->where('a.call_status',0);


        }elseif($type == 3){
          //missed apppointments
         $this->db->where('a.appointment_status',1);
         $this->db->where('a.call_status',0);

        }
    $this->db->order_by('a.from_date_time','ASC');
    return $this->db->count_all_results();
  }


  public function get_favourites($user_id)
  {
        $this->db->select('u.first_name,u.last_name,u.email,u.username,u.mobileno,u.profileimage,ud.*,c.country as countryname,s.statename,ci.city as cityname,sp.specialization as speciality,sp.specialization_img,(select COUNT(rating) from rating_reviews where doctor_id=u.id) as rating_count,(select ROUND(AVG(rating)) from rating_reviews where doctor_id=u.id) as rating_value');
        $this->db->from('favourities f');
        $this->db->join('users u','u.id = f.doctor_id','left');
        $this->db->join('users_details ud','ud.user_id = u.id','left');
        $this->db->join('country c','ud.country = c.countryid','left');
        $this->db->join('state s','ud.state = s.id','left');
        $this->db->join('city ci','ud.city = ci.id','left');
        $this->db->join('specialization sp','ud.specialization = sp.id','left');
        $this->db->where('f.patient_id',$user_id);
        return $result = $this->db->get()->result_array();
  }

  public function get_appoinment_call_details($appoinment_id)
  {
       $this->db->select('a.*, CONCAT(d.first_name," ", d.last_name) as doctor_name,d.username as doctor_username,d.profileimage as doctor_profileimage, CONCAT(p.first_name," ", p.last_name) as patient_name,p.profileimage as patient_profileimage,p.id as patient_id,d.id as doctor_id');
        $this->db->from($this->appoinments);
        $this->db->join($this->doctor, 'd.id = a.appointment_to', 'left'); 
        $this->db->join($this->doctor_details,'dd.user_id = d.id','left'); 
        $this->db->join($this->patient, 'p.id = a.appointment_from', 'left'); 
        $this->db->join($this->patient_details,'pd.user_id = p.id','left'); 
        $this->db->where('md5(a.id)',$appoinment_id);
        return $this->db->get()->row_array();
  }

  public function get_call($user_id)
  {
    $this->db->select('c.call_type,c.id,c.appointments_id,CONCAT(u.first_name," ", u.last_name) as name,u.profileimage,u.role');
    $this->db->from('call_details c');
    $this->db->join('users u','u.id = c.call_from','left');
    $this->db->where('c.call_to',$user_id);
    return $this->db->get()->row_array();

  }

  

  


}