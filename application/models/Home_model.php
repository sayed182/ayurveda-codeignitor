<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Home_model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}

  public function get_doctors()
  {
  	   $this->db->select('u.first_name,u.last_name,u.email,u.username,u.mobileno,u.profileimage,ud.*,c.country as countryname,s.statename,ci.city as cityname,sp.specialization as speciality,sp.specialization_img,(select COUNT(rating) from rating_reviews where doctor_id=u.id) as rating_count,(select ROUND(AVG(rating)) from rating_reviews where doctor_id=u.id) as rating_value');
        $this->db->from('users u');
        $this->db->join('users_details ud','ud.user_id = u.id','left');
        $this->db->join('country c','ud.country = c.countryid','left');
        $this->db->join('state s','ud.state = s.id','left');
        $this->db->join('city ci','ud.city = ci.id','left');
        $this->db->join('specialization sp','ud.specialization = sp.id','left');
        $this->db->where('u.role','1');
        $this->db->where('u.status','1');
        $this->db->where('u.is_verified','1');
        $this->db->where('u.is_updated','1');
        $this->db->order_by('u.id','DESC');
        return $result = $this->db->get()->result_array();
        
  }

  public function get_blogs()
  {
        $this->db->select('p.*,IF(p.post_by="Admin",a.profileimage, d.profileimage) as profileimage,IF(p.post_by="Admin","Admin", CONCAT(d.first_name," ", d.last_name)) as name,d.username,c.category_name,s.subcategory_name');
        $this->db->from('posts p'); 
        $this->db->join('users d','p.user_id = d.id','left'); 
        $this->db->join('categories c','p.category = c.id','left');
        $this->db->join('subcategories s','p.subcategory = s.id','left');
        $this->db->join('tags t','p.id = t.post_id','left');
        $this->db->join('administrators a','1 = a.id','left');
        $this->db->where('p.status','1'); 
        $this->db->where('p.is_verified','1');
        $this->db->where('p.is_viewed','1');
        $this->db->order_by('rand()');
        $this->db->group_by('p.id');
        $this->db->limit('4');
       return $this->db->get()->result_array();
        

  }

  public function get_answers()
  {
        $this->db->select('a.*,CONCAT(u.first_name," ", u.last_name) as name,u.username,u.profileimage,q.questions as question,q.id as q_id,q.specilization_id as specilization_id');
        $this->db->from('answers a'); 
        $this->db->join('users u','a.user_id = u.id','left');
         $this->db->join('questions q','a.question_id = q.id','left');
        $this->db->where('a.status','1'); 
        
        $this->db->order_by('a.id','DESC');
         $this->db->limit('4');
        return $this->db->get()->result_array();
        
  }

  public function get_doctor_details($username)
  {
       $this->db->select('u.id as userid,u.first_name,u.last_name,u.email,u.username,u.mobileno,u.profileimage,ud.*,c.country as countryname,s.statename,ci.city as cityname,sp.specialization as speciality,sp.specialization_img,(select COUNT(rating) from rating_reviews where doctor_id=u.id) as rating_count,(select ROUND(AVG(rating)) from rating_reviews where doctor_id=u.id) as rating_value');
        $this->db->from('users u');
        $this->db->join('users_details ud','ud.user_id = u.id','left');
        $this->db->join('country c','ud.country = c.countryid','left');
        $this->db->join('state s','ud.state = s.id','left');
        $this->db->join('city ci','ud.city = ci.id','left');
        $this->db->join('specialization sp','ud.specialization = sp.id','left');
        $this->db->where('u.role','1');
        $this->db->where("(u.status = '1' OR u.status = '2')");
        $this->db->where('u.is_verified','1');
        $this->db->where('u.is_updated','1');
        $this->db->where('u.username',$username);
        return $result = $this->db->get()->row_array();
        
  }

  public function search_doctor($page,$limit,$type)
  {
        $this->db->select('u.first_name,u.last_name,u.email,u.username,u.mobileno,u.profileimage,ud.*,c.country as countryname,s.statename,ci.city as cityname,sp.specialization as speciality,sp.specialization_img,(select COUNT(rating) from rating_reviews where doctor_id=u.id) as rating_count,(select ROUND(AVG(rating)) from rating_reviews where doctor_id=u.id) as rating_value');
        $this->db->from('users u');
        $this->db->join('users_details ud','ud.user_id = u.id','left');
        $this->db->join('country c','ud.country = c.countryid','left');
        $this->db->join('state s','ud.state = s.id','left');
        $this->db->join('city ci','ud.city = ci.id','left');
        $this->db->join('specialization sp','ud.specialization = sp.id','left');
        $this->db->where('u.role','1');
        $this->db->where('u.status','1');
        $this->db->where('u.is_verified','1');
        $this->db->where('u.is_updated','1');

        

        
        
       if(!empty($_POST['city'])){
       $this->db->where("(ud.state = '".$_POST['city']."' OR ud.city = '".$_POST['city']."')");
       }
        if(!empty($_POST['state'])){
         $this->db->where("(ud.state = '".$_POST['state']."' OR ud.city = '".$_POST['state']."')");
       }
        if(!empty($_POST['country'])){
          $this->db->where('ud.country',$_POST['country']);
        }
        if(!empty($_POST['specialization'])) {   
         $this->db->where('ud.specialization',$_POST['specialization']);
        }

         if(!empty($_POST['citys'])){
          
           //$this->db->like("(s.statename = '".$_POST['citys']."' OR ci.citys = '".$_POST['citys']."')");
            $this->db->group_start();
            $this->db->like('s.statename',$_POST['citys'],'after');
            $this->db->or_like('ci.city',$_POST['citys'],'after');
            $this->db->group_end();
          }

         if(!empty($_POST['keywords'])) {  
          $this->db->group_start();
          $this->db->like('u.first_name',$_POST['keywords'],'after');
          $this->db->or_like('u.last_name',$_POST['keywords'],'after');
          $this->db->or_like('sp.specialization',$_POST['keywords'],'after');
          $this->db->group_end();
        }

        if(!empty($_POST['gender'])) {   
          $this->db->where('ud.gender',$_POST['gender']);
        }
        if($_POST['order_by'] == 'Free'){
          $this->db->where('ud.price_type','Free');
        }
        $this->db->group_by('ud.id');
        // if(!empty($_POST['order_by'])){

         if($_POST['order_by'] == 'Rating'){
           $this->db->order_by('rating_value','DESC');
        }
        // if($_POST['order_by'] == 'Popular'){
        //   $query .=" ORDER BY rating_count DESC ";
        // }
        if($_POST['order_by'] == 'Latest'){
         $this->db->order_by('u.id','DESC');
        }

          if($type == 1){
         return $this->db->count_all_results(); 
        }else{

          $page = !empty($page)?$page:'';
          if($page>=1){
          $page = $page - 1 ;
          }
          $page =  ($page * $limit);  
          $this->db->limit($limit,$page);
          return $this->db->get()->result_array();
        }

  }

  public function search_patient($page,$limit,$type)
  {
        $this->db->select('u.first_name,u.last_name,u.email,u.username,u.mobileno,u.profileimage,ud.*,c.country as countryname,s.statename,ci.city as cityname');
        $this->db->from('users u');
        $this->db->join('users_details ud','ud.user_id = u.id','left');
        $this->db->join('country c','ud.country = c.countryid','left');
        $this->db->join('state s','ud.state = s.id','left');
        $this->db->join('city ci','ud.city = ci.id','left');
        $this->db->where('u.role','2');
        $this->db->where('u.status','1');
        $this->db->where('u.is_verified','1');
        $this->db->where('u.is_updated','1');
        
       if(!empty($_POST['city'])){
          $this->db->where('ud.city',$_POST['city']);
        }
        if(!empty($_POST['state'])){
         $this->db->where('ud.state',$_POST['state']);
        }
        if(!empty($_POST['country'])){
          $this->db->where('ud.country',$_POST['country']);
        }
        if(!empty($_POST['gender'])) {   
          $this->db->where('ud.gender',$_POST['gender']);
        }
        if(!empty($_POST['blood_group'])) {   
          $this->db->where('ud.blood_group',$_POST['blood_group']);
        }
        $this->db->group_by('ud.id');

        if($_POST['order_by'] == 'Latest'){
         $this->db->order_by('u.id','DESC');
        }

          if($type == 1){
         return $this->db->count_all_results(); 
        }else{

          $page = !empty($page)?$page:'';
          if($page>=1){
          $page = $page - 1 ;
          }
          $page =  ($page * $limit);  
          $this->db->limit($limit,$page);
          return $this->db->get()->result_array();
        }

  }

  public function get_specialization()
  {
      $this->db->where('status', 1);
      $this->db->order_by('id', 'DESC');
      $query = $this->db->get('specialization');
      return $query->result_array();
  }

  public function review_list_view($id)
{
  
    $where  = array('r.doctor_id'=>$id);
    return  $this->db
    ->select('u.profileimage,u.first_name,u.last_name,r.*')
    ->join('users u ','u.id = r.user_id')
    ->get_where('rating_reviews r',$where)
    ->result_array();
}


public function search_specialization($search_keywords)
{
  $where  = array('status' => 1 );

    return $this->db
          ->select('specialization')
          ->like('specialization',$search_keywords)
          ->limit(5)
          ->order_by('specialization','asc')
          ->get_where('specialization',$where)
          ->result_array();

}

public function search_location($search_location)
{
  
    return $this->db
          ->select('city')
          ->like('city',$search_location,'after')
          ->limit(5)
          ->order_by('city','asc')
          ->where_in('stateid',"SELECT id from state WHERE countryid = '101'",false)
          ->get('city')
          ->result_array();

}


public function search_doctors($search_keywords)
  {
        $this->db->select('u.first_name,u.last_name,u.username,u.profileimage,sp.specialization as speciality');
        $this->db->from('users u');
        $this->db->join('users_details ud','ud.user_id = u.id','left');
        $this->db->join('specialization sp','ud.specialization = sp.id','left');
        $this->db->where('u.role','1');
        $this->db->where('u.status','1');
        $this->db->where('u.is_verified','1');
        $this->db->where('u.is_updated','1');
          $this->db->group_start();
          $this->db->like('u.first_name',$search_keywords,'after');
          $this->db->or_like('u.last_name',$search_keywords,'after');
          $this->db->or_like('ud.specialization',$search_keywords,'after');
          $this->db->or_like("FIND_IN_SET('".$search_keywords."', ud.services)",'after');
          $this->db->group_end();
        $this->db->group_by('ud.id');
        $this->db->limit('5');
          return $this->db->get()->result_array();
 }

  

 
}
?>
