<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_model extends CI_Model {

  var $doctor ='users d';
  var $doctor_details ='users_details dd';
  var $specialization ='specialization s';

  var $patient ='users p';
  var $patient_details ='users_details pd';
  

  var $doctor_column_search = array('d.first_name','d.last_name','d.profileimage','d.email','d.mobileno','d.created_date','s.specialization'); 
  var $doctor_order = array('d.id' => 'ASC'); // default order 

  var $patient_column_search = array('p.first_name','p.last_name','p.profileimage','p.email','p.mobileno','pd.dob','pd.blood_group','p.created_date'); 
  var $patient_order = array('p.id' => 'ASC'); // default order

  var $table='users';

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	

  private function _get_doctor__datatables_query()
  {
  
        $this->db->select('d.*,s.specialization');
        $this->db->from($this->doctor); 
        $this->db->join($this->doctor_details,'dd.user_id = d.id','left'); 
        $this->db->join($this->specialization,'dd.specialization = s.id','left');
        $this->db->where('d.role','1'); 
        
   
    $i = 0;
  
    foreach ($this->doctor_column_search as $item) // loop column 
    {
      if($_POST['search']['value']) // if datatable send POST for search
      {
        
        if($i===0) // first loop
        {
          $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
          $this->db->like($item, $_POST['search']['value']);
        }
        else
        {
          $this->db->or_like($item, $_POST['search']['value']);
        }

         if(count($this->doctor_column_search) - 1 == $i) //last loop
          $this->db->group_end(); //close bracket
      }
      $i++;
    }
    
    if(isset($_POST['order'])) // here order processing
    {
            $this->db->order_by('id', $_POST['order']['0']['dir']);

       //$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } 
    else if(isset($this->doctor_order))
    {
      $order = $this->doctor_order;
      $this->db->order_by(key($order), $order[key($order)]);
    }
  }

  public function get_doctor_datatables()
  {
    $this->_get_doctor__datatables_query();
    if($_POST['length'] != -1)
    $this->db->limit($_POST['length'], $_POST['start']);
    $query = $this->db->get();
    return $query->result_array();
  }

  public function doctor_count_filtered()
  {
    $this->_get_doctor__datatables_query();
    $query = $this->db->get();
    return $query->num_rows();
  }

  public function doctor_count_all()
  {
    $this->db->from($this->doctor);
    $this->db->where('d.role','1');
    return $this->db->count_all_results();
  }



private function _get_patient__datatables_query()
  {
  
        $this->db->select('p.*,pd.dob,pd.blood_group');
        $this->db->from($this->patient); 
        $this->db->join($this->patient_details,'pd.user_id = p.id','left'); 
        $this->db->where('p.role','2'); 
        
   
    $i = 0;
  
    foreach ($this->patient_column_search as $item) // loop column 
    {
      if($_POST['search']['value']) // if datatable send POST for search
      {
        
        if($i===0) // first loop
        {
          $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
          $this->db->like($item, $_POST['search']['value']);
        }
        else
        {
          $this->db->or_like($item, $_POST['search']['value']);
        }

         if(count($this->patient_column_search) - 1 == $i) //last loop
          $this->db->group_end(); //close bracket
      }
      $i++;
    }
    
    if(isset($_POST['order'])) // here order processing
    {
            $this->db->order_by('id', $_POST['order']['0']['dir']);

       //$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } 
    else if(isset($this->patient_order))
    {
      $order = $this->patient_order;
      $this->db->order_by(key($order), $order[key($order)]);
    }
  }

  public function get_patient_datatables()
  {
    $this->_get_patient__datatables_query();
    if($_POST['length'] != -1)
    $this->db->limit($_POST['length'], $_POST['start']);
    $query = $this->db->get();
    return $query->result_array();
  }

  public function patient_count_filtered()
  {
    $this->_get_patient__datatables_query();
    $query = $this->db->get();
    return $query->num_rows();
  }

  public function patient_count_all()
  {
    $this->db->from($this->patient);
    $this->db->where('p.role','2');
    return $this->db->count_all_results();
  }



  public function update($where, $data)
  {
    $this->db->update($this->table, $data, $where);
    return $this->db->affected_rows();
  }

	


}